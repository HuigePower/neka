//
//  AppDelegate+JPush.h
//  Neka1.0
//
//  Created by ma c on 2017/3/1.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "AppDelegate.h"
#import "JPUSHService.h"

#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

@interface AppDelegate (JPush)<JPUSHRegisterDelegate,UIApplicationDelegate>
- (void)configurationJPushWithLaunchOptions:(NSDictionary *)launchOptions;
@end
