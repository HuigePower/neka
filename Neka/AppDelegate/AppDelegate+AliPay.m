//
//  AppDelegate+AliPay.m
//  Neka
//
//  Created by ma c on 2017/7/22.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "AppDelegate+AliPay.h"

@implementation AppDelegate (AliPay)

- (void)registerAlipay {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID,
                           @"app_type": @"1"};
    [manager POST:[NSString stringWithFormat:@"%@c=config&a=index", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            NSString *alipayPrivateKey = responseObject[@"result"][@"config"][@"pay_alipay_app_private_key"];
            NSLog(@"%@", alipayPrivateKey);
            [[NSUserDefaults standardUserDefaults] setObject:alipayPrivateKey forKey:@"alipayPrivateKey"];
            
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.description);
    }];
    
    
    
    
}


@end
