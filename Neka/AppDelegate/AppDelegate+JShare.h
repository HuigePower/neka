//
//  AppDelegate+JShare.h
//  Neka
//
//  Created by ma c on 2017/8/1.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "AppDelegate.h"
#import <JSHAREService.h>
@interface AppDelegate (JShare)

- (void)configurationJShare;

@end
