//
//  AppDelegate+Weixin.m
//  Neka1.0
//
//  Created by ma c on 2017/3/8.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "AppDelegate+Weixin.h"

@implementation AppDelegate (Weixin)

- (void)registerWeiXin {
    [WXApi registerApp:@"wx27b5587b7b499c79"];
    
}


//- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
//    NSLog(@"%s---%@", __func__, url);
//    return [WXApi handleOpenURL:url delegate:self];
//}
//
//- (BOOL)application:(UIApplication *)app openURL:(nonnull NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(nonnull id)annotation {
//    NSLog(@"%s---%@", __func__, url);
//    return [WXApi handleOpenURL:url delegate:self];
//}

#pragma mark -- WXApiDelegate
- (void)onReq:(BaseReq *)req {
    NSLog(@"%s", __func__);
}


- (void)onResp:(BaseResp *)resp {
    NSLog(@"微信返回APP");
    NSLog(@"errCode:%d--errStr:%@--type:%d", resp.errCode, resp.errStr, resp.type);
    
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        SendAuthResp *temp = (SendAuthResp *)resp;
        NSLog(@"--%@--%@--%@--%@", temp.code, temp.lang, temp.country, temp.state);
        [self wxGetAccessTokenWithCode:temp.code];
        NSLog(@"微信授权code获取失败");
    }else if ([resp isKindOfClass:[PayResp class]]) {
        PayResp *payResp = (PayResp *)resp;
        switch (payResp.errCode) {
            case WXSuccess:
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PaySuccess" object:nil userInfo:@{@"pay_type": @"weixin"}];
            }
                break;
            case WXErrCodeCommon:
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PayError" object:nil];
            }
                break;
            case WXErrCodeUserCancel:
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PayCancel" object:nil];
            }
            default:
                break;
        }
    }
}

#pragma mark -- 通过code获取access_token
- (void)wxGetAccessTokenWithCode:(NSString *)code {
    
    NSString *urlString = [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code", WX_APPID, WX_SECRET, code];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@", data);
        
        [[NSUserDefaults standardUserDefaults] setObject:data[@"access_token"] forKey:WX_S_ACCESS_TOKEN];
        [[NSUserDefaults standardUserDefaults] setObject:data[@"openid"] forKey:WX_S_OPENID];
        [[NSUserDefaults standardUserDefaults]setObject:data[@"refresh_token"] forKey:WX_S_OPENID];
        [[NSUserDefaults standardUserDefaults]setObject:data[@"unionid"] forKey:WX_S_UNIONID];
        
        
        [self wxGetUserInfoWithAccessToken:data[@"access_token"] andOpenID:data[@"openid"]];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"failure%@", error.description);
    }];

}

#pragma mark -- 获取用户个人信息（UnionID机制）
- (void)wxGetUserInfoWithAccessToken:(NSString *)accessToken andOpenID:(NSString *)openID {
 
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@", accessToken,  openID] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"个人信息:%@", data);
        [[NSUserDefaults standardUserDefaults] setObject:data[@"headimgurl"] forKey:USER_HEADER_IMG_URL];
        
        [[NSUserDefaults standardUserDefaults] setObject:data[@"nickname"] forKey:USER_NICKNAME];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:K_LOGIN object:nil];
        NSDictionary *body = @{@"Device-Id": DEVICE_ID,
                               @"client": @"1",
                               @"weixin_open_id": data[@"openid"],
                               @"weixin_union_id": data[@"unionid"],
                               @"nickname": data[@"nickname"],
                               @"avatar": data[@"headimgurl"],
                               @"sex": data[@"sex"],
                               };
        [manager POST:[NSString stringWithFormat:@"%@c=Login&a=weixin_login", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSLog(@"%@", dic);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"WeixinLoginSuccess" object:dic];
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD showError:error.description];
        }];
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"获取个人信息失败");
    }];
}

#pragma mark -- 刷新或续期access_token
- (void)wxRefreshToken {
    AFHTTPSessionManager  *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString *urlStr = [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=%@&grant_type=refresh_token&refresh_token=%@", WX_APPID, [[NSUserDefaults standardUserDefaults] objectForKey:WX_S_REFRESH_TOKEN]];
    NSLog(@"%@", urlStr);
    [manager POST:urlStr parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"%@", data);
        [[NSUserDefaults standardUserDefaults] setObject:data[@"access_token"] forKey:WX_S_ACCESS_TOKEN];
        [[NSUserDefaults standardUserDefaults] setObject:data[@"openid"] forKey:WX_S_OPENID];
        [[NSUserDefaults standardUserDefaults]setObject:data[@"refresh_token"] forKey:WX_S_OPENID];
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"刷新accessToken失败");
    }];
}






/*
{
    "access_token" = "XPd-X7lajqwS1S9f7nxiZ1Ihp5YeqjSKQgHgHHf5EEWmtSZcQXk0xolHsqL7keG01nrIL59WJ3vZhn82d3TD2K9eItwmIKOKlqLA6kb5FxI";
    "expires_in" = 7200;
    openid = odfilwlC6JOBH0AGxyB4TqELfXSY;
    "refresh_token" = "M6m2tIvbAZiRbTEUns3-svCnlxVlaTnSfyreJbMyX3NAfoV6bUut-rUPcmAnTH7W-inQ020LEpAZ3218jJAA_DaLyF_O0-bIJ6W-ci3oT-s";
    scope = "snsapi_userinfo";
    unionid = oun4Bw4Aty32oH7QgZEhLk5S2pRA;
}
2017-03-12 13:14:48.856 Neka1.0[13993:7678853] 个人信息:{
    city = Chaoyang;
    country = CN;
    headimgurl = "http://wx.qlogo.cn/mmopen/3KagZvNVXDu5IshvxlbPm4qGUmqC8bVAsVDKziaVm7dWXkRCpOlfyPjLlGWv7yykdotrEAEsgoaMdwB94L6Rpicric7yCZ4REZib/0";
    language = "zh_CN";
    nickname = "\U8f89\U54e5";
    openid = odfilwlC6JOBH0AGxyB4TqELfXSY;
    privilege =     (
    );
    province = Beijing;
    sex = 1;
    unionid = oun4Bw4Aty32oH7QgZEhLk5S2pRA;
}

*/

@end


//{
//    "access_token" = "ClIBZP5-KNoqm3IF9RFU4qyq5Ezb6OEEWExYQcUiPEZcJ2mOob65ehPl93MP_rBmt4OVaXtNmdyhqhUhf95z7ANWZRT6WvfMEnLst_HgrHo";
//    "expires_in" = 7200;
//    openid = odfilwlC6JOBH0AGxyB4TqELfXSY;
//    "refresh_token" = "Y7zAX8Auuk771k81GkuK13rw3LaAjNMfg1Ol6PmNIcSoG_5zUJjBWXbRw89qLwOScWV4T2D5_dUdlbryI6I1y5K4CvrhorZk9oxfsCX3YB4";
//    scope = "snsapi_userinfo";
//    unionid = oun4Bw4Aty32oH7QgZEhLk5S2pRA;
//}
//2017-03-12 17:45:13.337 Neka1.0[14057:7723793] 北京市北京市朝阳区小武基路
//2017-03-12 17:45:13.338 Neka1.0[14057:7723793] address==北京市
//2017-03-12 17:45:13.554 Neka1.0[14057:7723793] 个人信息:{
//    city = Chaoyang;
//    country = CN;
//    headimgurl = "http://wx.qlogo.cn/mmopen/3KagZvNVXDu5IshvxlbPm4qGUmqC8bVAsVDKziaVm7dWXkRCpOlfyPjLlGWv7yykdotrEAEsgoaMdwB94L6Rpicric7yCZ4REZib/0";
//    language = "zh_CN";
//    nickname = "\U8f89\U54e5";
//    openid = odfilwlC6JOBH0AGxyB4TqELfXSY;
//    privilege =     (
//    );
//    province = Beijing;
//    sex = 1;
//    unionid = oun4Bw4Aty32oH7QgZEhLk5S2pRA;
//}

/*
 //第一次微信登录
 1.授权获取code, 保存code
 2.通过code获取access_token,refresh_token,expires_in,openid,unionid,保存access_token,refresh_token,openid,unionid,和'时间戳'
 3.通过acess_token获取用户信息
 
 
 //再次登录
 1.判断access_token是否过期,
    1.1过期,通过refresh_token刷新access_token
    1.2未过期,执行第2步
 2.通过acess_token获取用户信息
 
 */


