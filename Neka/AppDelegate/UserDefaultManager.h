//
//  UserDefaultManager.h
//  UserDefault
//
//  Created by ma c on 2017/3/7.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>
#define K_YES @"1"
#define K_NO @"0"
#define K_IS_LOGIN @"isLogin"



@interface UserDefaultManager : NSObject

#pragma mark -- 登录相关

+ (BOOL)userIsLogin;
+ (void)userToLogin;
+ (void)userToLogout;


+ (void)saveTicket:(NSString *)ticket;
+ (BOOL)ticketIsExist;
+ (NSString *)ticket;


@end
