//
//  AppDelegate+BaiduMap.m
//  Neka1.0
//
//  Created by ma c on 16/7/28.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "AppDelegate+BaiduMap.h"
CLLocationCoordinate2D coordinate;
@implementation AppDelegate (BaiduMap)



- (void)initializeBaiduMapManager {
    self.mapManager = [[BMKMapManager alloc] init];
    BOOL ret = [self.mapManager start:BAIDU_APIKEY generalDelegate:nil];
    if (!ret) {
        NSLog(@"地图Manager开启失败");
    }else {
        NSLog(@"地图Manager开启成功");

        self.locService = [[BMKLocationService alloc] init];
        self.locService.delegate = self;
    
        [BMKLocationService setLocationDistanceFilter:10];
        self.search = [[BMKGeoCodeSearch alloc] init];
        self.search.delegate = self;
        [self.locService startUserLocationService];
    }
    
    
}

#pragma mark -- BMKLocationServiceDelegate

//- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation {
//    NSLog(@"heading is %@",userLocation.heading);
//}

- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation {
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.8f", userLocation.location.coordinate.latitude] forKey:@"latitude"];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.8f", userLocation.location.coordinate.longitude] forKey:@"longitude"];
    
    NSLog(@"lat:%@, lon:%@", LATITUDE, LONGITUDE);
    
    
    
    NSLog(@"didUpdateUserLocation lat %.8f,long %.8f",userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude);
    coordinate = CLLocationCoordinate2DMake(userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
    BMKReverseGeoCodeOption *priateCoordinate = [[BMKReverseGeoCodeOption alloc] init];
    priateCoordinate.reverseGeoPoint = (CLLocationCoordinate2D){userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude};
    [self.search reverseGeoCode:priateCoordinate];
    
    
}

- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error {
    NSLog(@"%@", [NSString stringWithFormat:@"%@%@%@%@%@", result.addressDetail.province, result.addressDetail.city, result.addressDetail.district, result.addressDetail.streetName, result.addressDetail.streetNumber]);
    NSLog(@"address==%@", result.addressDetail.province);
    NSString *city = result.addressDetail.city;
    //保存实际定位城市
    [[NSUserDefaults standardUserDefaults] setObject:city forKey:@"locatedCity"];

}



@end
