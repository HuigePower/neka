//
//  AppDelegate+JPush.m
//  Neka1.0
//
//  Created by ma c on 2017/3/1.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "AppDelegate+JPush.h"
#define JPushAPPKEY @"ca6028222e712f85c3622190"
#define Channel @"APP Store"
#define IsProduction NO

@implementation AppDelegate (JPush)

- (void)configurationJPushWithLaunchOptions:(NSDictionary *)launchOptions {
    NSLog(@"%s", __func__);
//    [self registerNotifications];
    
    [JPUSHService setBadge:0];
    [JPUSHService removeNotification:nil];
    //极光推送
    JPUSHRegisterEntity *entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert | JPAuthorizationOptionBadge | JPAuthorizationOptionSound;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        if (@available(iOS 10.0, *)) {
            NSLog(@"1122");
            UNNotificationAction *action1 = [UNNotificationAction actionWithIdentifier:@"action1" title:@"关闭" options:UNNotificationActionOptionDestructive];
            
            UNNotificationAction *action2 = [UNNotificationAction actionWithIdentifier:@"action2" title:@"打开APP" options:UNNotificationActionOptionForeground];
            
            UNNotificationCategory *category1 = [UNNotificationCategory categoryWithIdentifier:@"category1" actions:@[action1, action2] intentIdentifiers:@[@"action", @"action2"] options:UNNotificationCategoryOptionNone];
            
            [entity setCategories:[NSSet setWithObjects:category1, nil]];
            
        } else {
            NSLog(@"2233");
            // Fallback on earlier versions
            //创建2种推送策略，每种策略包含2种行为
            //通知category1: action1, action2
            UIMutableUserNotificationAction *action1 = [[UIMutableUserNotificationAction alloc] init];
            action1.identifier = @"action1";
            action1.title = @"策略1行为1";
            action1.activationMode = UIUserNotificationActivationModeBackground; //在后台
            action1.destructive = YES; //决定按钮显示颜色
            
            UIMutableUserNotificationAction *action2 = [[UIMutableUserNotificationAction alloc] init];
            action2.identifier = @"action2";
            action2.title = @"策略1行为2";
            action2.activationMode = UIUserNotificationActivationModeForeground; //在前台
            action2.destructive = NO;
            
            UIMutableUserNotificationCategory *category1 = [[UIMutableUserNotificationCategory alloc] init];
            category1.identifier = @"category1";
            [category1 setActions:@[action1, action2] forContext:UIUserNotificationActionContextDefault];
            
            
            //通知category2: action3, action4
            UIMutableUserNotificationAction *action3 = [[UIMutableUserNotificationAction alloc] init];
            action3.identifier = @"action3";
            action3.title = @"策略2行为3";
            action3.activationMode = UIUserNotificationActivationModeBackground; //在后台
            action3.destructive = YES; //决定按钮显示颜色
            
            UIMutableUserNotificationAction *action4 = [[UIMutableUserNotificationAction alloc] init];
            action4.identifier = @"action4";
            action4.title = @"策略2行为4";
            action4.activationMode = UIUserNotificationActivationModeForeground; //在前台
            action4.destructive = NO;
            
            UIMutableUserNotificationCategory *category2 = [[UIMutableUserNotificationCategory alloc] init];
            category1.identifier = @"category2";
            [category2 setActions:@[action3, action4] forContext:UIUserNotificationActionContextMinimal];
            
            entity.categories = [NSSet setWithObjects:category1, category2, nil];
        }
    }
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    
    [JPUSHService setupWithOption:launchOptions appKey:JPushAPPKEY channel:Channel apsForProduction:IsProduction advertisingIdentifier:nil];
    
}


#pragma mark -- 实现Appdelegate方法
//注册APNs成功并上报DeviceToken
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"%s, token=%@", __func__, deviceToken);
    [JPUSHService registerDeviceToken:deviceToken];
}

//实现注册APNs失败接口（可选）
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"%s", __func__);
    NSLog(@"注册远程通知失败: %@", error);
}

#pragma mark -- 收到远程通知
/*  收到远程通知时
 1.App在前台,直接调用此方法
 2.App在后台,点开通知进入App会调用此方法,通过App图标进入的不会调用此方
 */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSLog(@"%s===%@", __func__, userInfo);
    [JPUSHService handleRemoteNotification:userInfo];
    [JPUSHService setBadge:0];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    completionHandler(UIBackgroundFetchResultNewData);
}
#pragma mark -- 收到本地通知
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSLog(@"%s===%@", __func__, notification);
}


#pragma mark -- 代理:JPUSHRegisterDelegate
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
//前台时调用
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    NSLog(@"%s", __func__);
    NSDictionary *userInfo = notification.request.content.userInfo;
    NSLog(@"%s===%@", __func__, userInfo);
    if ([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
        [JPUSHService setBadge:0];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }else {
        NSLog(@"本地通知：%s \n%@", __func__, userInfo);
    }
    completionHandler(UNNotificationPresentationOptionAlert);
    
}

//后台进入时调用
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    NSLog(@"%s", __func__);
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    NSLog(@"%s===%@", __func__, userInfo);
    if ([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
        [JPUSHService setBadge:0];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }else {
        NSLog(@"本地通知：%s \n%@", __func__, userInfo);
    }
    completionHandler();
}

#endif


//#pragma mark -- 注册通知
//- (void)registerNotifications {
//
//    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
//    [defaultCenter addObserver:self
//                      selector:@selector(networkDidSetup:)
//                          name:kJPFNetworkDidSetupNotification
//                        object:nil];
//    [defaultCenter addObserver:self
//                      selector:@selector(networkDidClose:)
//                          name:kJPFNetworkDidCloseNotification
//                        object:nil];
//    [defaultCenter addObserver:self
//                      selector:@selector(networkDidRegister:)
//                          name:kJPFNetworkDidRegisterNotification
//                        object:nil];
//    [defaultCenter addObserver:self
//                      selector:@selector(networkDidLogin:)
//                          name:kJPFNetworkDidLoginNotification
//                        object:nil];
//    [defaultCenter addObserver:self
//                      selector:@selector(networkDidReceiveMessage:)
//                          name:kJPFNetworkDidReceiveMessageNotification
//                        object:nil];
//    [defaultCenter addObserver:self
//                      selector:@selector(serviceError:)
//                          name:kJPFNetworkDidSetupNotification
//                        object:nil];
//}

#pragma mark -- 移除通知
//- (void)removeNotifications {
//    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
//    [defaultCenter removeObserver:self name:kJPFNetworkDidSetupNotification object:nil];
//    [defaultCenter removeObserver:self name:kJPFNetworkDidCloseNotification object:nil];
//    [defaultCenter removeObserver:self name:kJPFNetworkDidRegisterNotification object:nil];
//    [defaultCenter removeObserver:self name:kJPFNetworkDidLoginNotification object:nil];
//    [defaultCenter removeObserver:self name:kJPFNetworkDidReceiveMessageNotification object:nil];
//    [defaultCenter removeObserver:self name:kJPFNetworkDidSetupNotification object:nil];
//}

#pragma mark -- 通知事件

- (void)networkDidSetup:(NSNotification *)noti {
    NSLog(@"%s==%@", __func__, noti);
}

- (void)networkDidClose:(NSNotification *)noti {
    NSLog(@"%s==%@", __func__, noti);
}

- (void)networkDidRegister:(NSNotification *)noti {
    NSLog(@"%s==%@", __func__, noti);
}

- (void)networkDidLogin:(NSNotification *)noti {
    NSLog(@"%s==%@", __func__, noti);
}

/*在接收自定义消息时
 1.在前台,直接调用
 2.在后台,不会调用,没有提醒, 再次进入后调用
 3.未启动,不调用,不提醒,进入后也不调用
 */
- (void)networkDidReceiveMessage:(NSNotification *)noti {
    NSLog(@"%s==%@", __func__, noti);
}


- (void)serviceError:(NSNotification *)noti {
    NSLog(@"%s==%@", __func__, noti);
}


@end
