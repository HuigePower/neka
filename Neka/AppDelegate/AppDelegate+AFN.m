//
//  AppDelegate+AFN.m
//  Neka1.0
//
//  Created by ma c on 16/9/15.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "AppDelegate+AFN.h"
#import "AFNetworking.h"
#import "UIKit+AFNetworking.h"
@implementation AppDelegate (AFN)

- (void)ConfigurationAFN {
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:4 * 1024 * 1024 diskCapacity:20 * 1024 * 1024 diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
}

@end
