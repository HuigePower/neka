//
//  UserDefaultManager.m
//  UserDefault
//
//  Created by ma c on 2017/3/7.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "UserDefaultManager.h"

@implementation UserDefaultManager

//登录相关

+ (BOOL)userIsLogin {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:K_IS_LOGIN] == NULL)  {
        NSLog(@"11");
        [[NSUserDefaults standardUserDefaults] setObject:K_NO forKey:K_IS_LOGIN];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:K_IS_LOGIN] isEqualToString:K_YES]) {
        return YES;
    }
    return NO;
}

+ (void)userToLogin {
    [[NSUserDefaults standardUserDefaults] setObject:K_YES forKey:K_IS_LOGIN];
}

+ (void)userToLogout {
    [[NSUserDefaults standardUserDefaults] setObject:K_NO forKey:K_IS_LOGIN];
}



//保存票据
+ (void)saveTicket:(NSString *)ticket {
    [[NSUserDefaults standardUserDefaults] setObject:ticket forKey:@"TICKET"];
}

+ (BOOL)ticketIsExist {
    NSString *ticket = [[NSUserDefaults standardUserDefaults] objectForKey:@"TICKET"];
    if (ticket == NULL || ticket.length < 5) {
        NSLog(@"ticket不存在");
        return NO;
    }else {
        NSLog(@"ticket存在");
        return YES;
    }
    
}

+ (NSString *)ticket {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"TICKET"];
}



@end
