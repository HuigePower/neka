//
//  AppDelegate+BaiduMap.h
//  Neka1.0
//
//  Created by ma c on 16/7/28.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (BaiduMap)<BMKLocationServiceDelegate, BMKGeoCodeSearchDelegate>


- (void)initializeBaiduMapManager;
@end
