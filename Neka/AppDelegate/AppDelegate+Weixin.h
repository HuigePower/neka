//
//  AppDelegate+;
//  Neka1.0
//
//  Created by ma c on 2017/3/8.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "AppDelegate.h"
#import "WXApi.h"
@interface AppDelegate (Weixin)<WXApiDelegate>

- (void)registerWeiXin;

- (void)wxRefreshToken;

@end
