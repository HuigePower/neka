//
//  UserDefaultMacro.h
//  Neka1.0
//
//  Created by ma c on 2017/2/8.
//  Copyright © 2017年 ma c. All rights reserved.
//

#ifndef UserDefaultMacro_h
#define UserDefaultMacro_h

#define K_YES @"1"
#define K_NO @"0"
#define K_KeyWindow [UIApplication sharedApplication].keyWindow
// 登入/登出
#define K_LOGIN @"login"
#define K_LOGOUT @"logout"

#ifdef DEBUG
#define PPLog(...) printf("[%s] %s [第%d行]: %s\n", __TIME__ ,__PRETTY_FUNCTION__ ,__LINE__, [[NSString stringWithFormat:__VA_ARGS__] UTF8String])
#else
#define PPLog(...)
#endif

//当前选择的城市
#define Location_Set_City(x) [[NSUserDefaults standardUserDefaults] setObject:x forKey:@"city"]
#define Location_Get_City [[NSUserDefaults standardUserDefaults] objectForKey:@"city"]
//当前定位的城市
#define Location_Set_CurrentCity(x) [[NSUserDefaults standardUserDefaults] setObject:x forKey:@"currentCity"]
#define Location_Get_CurrentCity [[NSUserDefaults standardUserDefaults] objectForKey:@"currentCity"]

//当前定位的经纬度
#define LATITUDE [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"]
#define LONGITUDE [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]




//搜索历史
#define Search_Set_History(x) [[NSUserDefaults standardUserDefaults] setObject:x forKey:@"searchHistory"]

#define Search_Get_History [[NSUserDefaults standardUserDefaults] objectForKey:@"searchHistory"]




//接口
#define BASE_URL @"https://www.nekahome.com/appapi.php?"

//设备ID
#define DEVICE_ID [[[UIDevice alloc] identifierForVendor] UUIDString]


#define WZAPI @"http://api.jisuapi.com/illegal/query?appkey=54f0487c1770f929"






#endif /* UserDefaultMacro_h */
