//
//  AppDelegate.m
//  Neka1.0
//
//  Created by ma c on 16/7/26.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"
#import "AppDelegate+BaiduMap.h"
#import "AppDelegate+AFN.h"
#import "AppDelegate+JPush.h"
#import "AppDelegate+Weixin.h"
#import "AppDelegate+AliPay.h"
#import "AppDelegate+JShare.h"
#import "LoginViewController.h"
#import "UserDefaultManager.h"
#import "AlipaySDK.h"
//#import <iflyMSC/iflyMSC.h>
#import <dlfcn.h>
@interface AppDelegate ()
@property (nonatomic, strong)RootViewController *rootViewController;
@property (nonatomic, strong)UINavigationController *loginNav;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [JPUSHService removeNotification:nil];
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _window.backgroundColor = [UIColor whiteColor];
    [_window makeKeyAndVisible];


    
    if ([LATITUDE length] == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@"39.879431" forKey:@"latitude"];
        [[NSUserDefaults standardUserDefaults] setObject:@"116.507961" forKey:@"longitude"];
    }
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [self registerNotifications];
    [self registerWeiXin];
    [self initializeBaiduMapManager];
    [self ConfigurationAFN];
    [self configurationJPushWithLaunchOptions:launchOptions];
    [self wxRefreshToken];
    [self configurationJShare];
    [self registerAlipay];
    self.window.rootViewController = self.rootViewController;

    [UserDefaultManager ticketIsExist]? NSLog(@"Y"): NSLog(@"N");
    NSLog(@"%@", [UserDefaultManager ticket]);

    return YES;

}


- (void)registerNotifications {
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(login) name:K_LOGIN object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout) name:K_LOGOUT object:nil];
}

- (RootViewController *)rootViewController {
    if (_rootViewController == nil) {
        _rootViewController = [[RootViewController alloc] init];
    }
    return _rootViewController;
}


- (UINavigationController *)loginNav {
    if (_loginNav == nil) {
        LoginViewController *login = [[LoginViewController alloc] init];
        _loginNav = [[UINavigationController alloc] initWithRootViewController:login];
    }
    return _loginNav;
}

//- (void)login {
//    self.window.rootViewController = self.rootViewController;
//    [UserDefaultManager userToLogin];
//    _loginNav = nil;
//    
//}
//
//- (void)logout {
//    self.window.rootViewController = self.loginNav;
//    [UserDefaultManager userToLogout];
//    _rootViewController = nil;
//}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    NSLog(@"%s--%@", __func__, url);
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"%s-1-%@", __func__, resultDic);
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                if ([[resultDic objectForKey:@"resultStatus"] isEqualToString:@"9000"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"PaySuccess" object:nil userInfo:@{@"pay_type": @"alipay", @"alipay_return": resultDic[@"result"]}];
                    
                }
            }];
            
        }];
        
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"%s-2-%@", __func__, resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            NSLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
    }else if ([url.scheme isEqualToString:@"wx27b5587b7b499c79"]) {
        if ([url.host isEqualToString:@"oauth"]) {
            NSLog(@"微信登录");
            return [WXApi handleOpenURL:url delegate:self];
        }else if ([url.host isEqualToString:@"platformId=wechat"]) {
            NSLog(@"微信分享");
            return [JSHAREService handleOpenUrl:url];
        }else if ([url.host isEqualToString:@"pay"]) {
            NSLog(@"微信支付");
            return [WXApi handleOpenURL:url delegate:self];
        }
    }else {
        NSLog(@"分享");
        return [JSHAREService handleOpenUrl:url];
    }
    return YES;
}


- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
    
    NSLog(@"%s--%@", __func__, url);
    
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"%s-1-%@", __func__, resultDic);
            if ([[resultDic objectForKey:@"resultStatus"] isEqualToString:@"9000"]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PaySuccess" object:nil userInfo:@{@"pay_type": @"alipay", @"alipay_return": resultDic[@"result"]}];
                
            }
        }];
        
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"%s-2-%@", __func__, resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            NSLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
    }else if ([url.scheme isEqualToString:@"wx27b5587b7b499c79"]) {
        if ([url.host isEqualToString:@"oauth"]) {
            NSLog(@"微信登录");
            return [WXApi handleOpenURL:url delegate:self];
        }else if ([url.host isEqualToString:@"platformId=wechat"]) {
            NSLog(@"微信分享");
            return [JSHAREService handleOpenUrl:url];
        }else if ([url.host isEqualToString:@"pay"]) {
            NSLog(@"微信支付");
            return [WXApi handleOpenURL:url delegate:self];
        }
        
    }else {
        NSLog(@"分享");
        return [JSHAREService handleOpenUrl:url];
    }
    return YES;
}



















- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
