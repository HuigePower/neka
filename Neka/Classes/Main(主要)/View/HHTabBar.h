//
//  HHTabBar.h
//  耐卡
//
//  Created by ma c on 16/10/16.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHTabBar;
@protocol HHTabBarDelegate <NSObject>

@optional
- (void)tabbar:(HHTabBar *)tabBar didClickButton:(NSInteger)index;

@end

@interface HHTabBar : UIView

@property (nonatomic, strong)NSArray *items;

@property (nonatomic, weak)id<HHTabBarDelegate> delegate;

- (void)cancelSelected;
@end
