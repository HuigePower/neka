//
//  HHTabBar.m
//  耐卡
//
//  Created by ma c on 16/10/16.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HHTabBar.h"
#import "HHTabBarButton.h"

@interface HHTabBar ()

@property (nonatomic, strong)UIButton *plusButton;

@property (nonatomic, strong)NSMutableArray *buttons;

@property (nonatomic, strong)UIButton *selectedButton;

@end

@implementation HHTabBar

- (NSMutableArray *)buttons {
    
    if (_buttons == nil) {
        _buttons = [[NSMutableArray alloc] init];
    }
    return _buttons;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0.5)];
        line.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:line];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setBadgeValue:) name:@"SetBadgeValue" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tabbarChange:) name:@"TABBAR_CHANGE" object:nil];
    }
    return self;
}

- (void)setBadgeValue:(NSNotification *)noti {
    NSLog(@"%@", noti.userInfo);
    NSInteger index = [noti.userInfo[@"index"] integerValue];
    HHTabBarButton *btn = self.buttons[index];
    NSString *value = noti.userInfo[@"value"];
    [btn setBadgeValue:value];
    
    
}

//切换tabbar
- (void)tabbarChange:(NSNotification *)noti {
    NSInteger index = [noti.userInfo[@"index"] integerValue];
    UIButton *btn = [self viewWithTag:10 + index];
    _selectedButton.selected = NO;
    btn.selected = YES;
    _selectedButton = btn;
    //通知tabBarVC切换控制器
    if ([self.delegate respondsToSelector:@selector(tabbar:didClickButton:)]) {
        [_delegate tabbar:self didClickButton:index];
    }
}


- (void)setItems:(NSArray *)items {
    
    _items = items;
    
    //遍历模型数组,创建对应的tabBarButton
    for (int i = 0; i < _items.count; i++) {
        UITabBarItem *item = (UITabBarItem *)_items[i];
        HHTabBarButton *btn = [[HHTabBarButton alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        
        //给按钮模型赋值,按钮的内容由模型对应决定
        btn.item = item;
        //btn.backgroundColor = [UIColor blueColor];
        btn.tag = 10 + i;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchDown];
        if (btn.tag == 10) {//选中第0个
            [self btnClick:btn];
        }
        [self.buttons addObject:btn];
        [self addSubview:btn];
//        if (i == 2) {
//            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 49, 49)];
//            [btn setImage:[UIImage imageNamed:@"center_btn"] forState:UIControlStateNormal];
//            btn.tag = 10 + i;
//            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//            [self.buttons addObject:btn];
//            [self addSubview:btn];
//        }else {
//
//        }
        
    }
    

}



//点击tabBarButton调用
- (void)btnClick:(UIButton *)btn {
    NSLog(@"%ld", (long)btn.tag);
    _selectedButton.selected = NO;
    btn.selected = YES;
    _selectedButton = btn;
    
    //通知tabBarVC切换控制器
    if ([self.delegate respondsToSelector:@selector(tabbar:didClickButton:)]) {
        [_delegate tabbar:self didClickButton:btn.tag - 10];
    }
    
}

- (void)cancelSelected {
    _selectedButton.selected = NO;
}

- (UIButton *)plusButton {
    if (_plusButton == nil) {
        NSLog(@"plus");
        _plusButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 49, 49)];
        [_plusButton setImage:[UIImage imageNamed:@"center_btn"] forState:UIControlStateNormal];
        _plusButton.tag = 14;
        [_plusButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        //默认按钮的尺寸跟北京图片一样大
        //sizeToFit:默认会根据按钮的背景图片或image和文字计算出按钮的最合适的尺寸
//        [_plusButton sizeToFit];
        
        [self addSubview:_plusButton];
        
    }
    return _plusButton;
}

//self.items  UItabBarItem模型,有多少个控制器就有多少个UITabBarItem模型
//调控子控件的位置
- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat w = self.bounds.size.width;
    CGFloat h = self.bounds.size.height;
    
    CGFloat btnX = 0;
    CGFloat btnY = 0;
    CGFloat btnW = w / 4;
    CGFloat btnH = h;
    
    int i = 0;
    //设置tabBarButton的frame
    for (UIView *tabBarButton in self.buttons) {
        btnX = i * btnW;
        tabBarButton.frame = CGRectMake(btnX, btnY, btnW, btnH);
        i++;
    
    }
    //设置添加按钮的位置
//    self.plusButton.center = CGPointMake(w * 0.5, h * 0.5);
//    [self addSubview:self.plusButton];

}






- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SetBadgeValue" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TABBAR_CHANGE" object:nil];

}
   
@end
