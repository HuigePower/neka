//
//  HHTabBarButton.m
//  耐卡
//
//  Created by ma c on 16/10/16.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HHTabBarButton.h"
#import "HHBadgeView.h"


#define HHImageRidio 0.7
@interface HHTabBarButton ()

@property (nonatomic, strong)HHBadgeView *badgeView;


@end

@implementation HHTabBarButton


- (HHBadgeView *)badgeView {
    if (_badgeView == nil) {
        HHBadgeView *badge = [HHBadgeView buttonWithType:UIButtonTypeCustom];
        [self addSubview:badge];
        _badgeView = badge;
        
    }
    return _badgeView;
}



- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //设置字体颜色
        [self setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        [self setTitleColor:mRGB(0, 126, 223, 1) forState:UIControlStateSelected];
        
        //图片居中
        self.imageView.contentMode = UIViewContentModeCenter;
        //文字居中
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        //设置文字字体
        self.titleLabel.font = [UIFont systemFontOfSize:12];
        
        
    }
    return self;
}



- (void)setItem:(UITabBarItem *)item {
    _item = item;
//    NSLog(@"==%@", _item.title);
    [self observeValueForKeyPath:nil ofObject:nil change:nil context:nil];
    
    //KVO:时刻监视一个对象的属性有没有改变
    [_item addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
    [_item addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
    [_item addObserver:self forKeyPath:@"selectedImage" options:NSKeyValueObservingOptionNew context:nil];
    [_item addObserver:self forKeyPath:@"badgeValue" options:NSKeyValueObservingOptionNew context:nil];
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    [self setTitle:_item.title forState:UIControlStateNormal];
    [self setImage:_item.image forState:UIControlStateNormal];
    [self setImage:_item.selectedImage forState:UIControlStateSelected];
    
    //设置badgeValue
    self.badgeView.badgeValue = _item.badgeValue;
}

- (void)setBadgeValue:(NSString *)value {
    //设置badgeValue
    self.badgeView.badgeValue = value;
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.imageView
    CGFloat imageX = 0;
    CGFloat imageY = 0;
    CGFloat imageW = self.bounds.size.width;
    CGFloat imageH = self.bounds.size.height * HHImageRidio;
    
    self.imageView.frame = CGRectMake(imageX, imageY, imageW, imageH);
//    NSLog(@"%f--%f--%f--%f",imageX, imageY, imageW, imageH);
    //2.title
    CGFloat titleX = 0;
    CGFloat titleY = imageH - 3;
    CGFloat titleW = self.bounds.size.width;
    CGFloat titleH = self.bounds.size.height - imageH;
    self.titleLabel.frame = CGRectMake(titleX, titleY, titleW, titleH);
//    NSLog(@"%f--%f--%f--%f",titleX, titleY, titleW, titleH);
    //3.badgeView
    self.badgeView.x = self.width - self.badgeView.width - 15;
    self.badgeView.y = 0;
}

- (void)dealloc {
    
    [_item removeObserver:self forKeyPath:@"title"];
    [_item removeObserver:self forKeyPath:@"image"];
    [_item removeObserver:self forKeyPath:@"selectedImage"];
    [_item removeObserver:self forKeyPath:@"badgeValue"];
    
}



@end











