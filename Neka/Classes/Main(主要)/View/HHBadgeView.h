//
//  HHBadgeView.h
//  耐卡
//
//  Created by ma c on 16/10/16.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HHBadgeView : UIButton

@property (nonatomic, copy)NSString *badgeValue;
@end
