//
//  HHBadgeView.m
//  耐卡
//
//  Created by ma c on 16/10/16.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HHBadgeView.h"
#define HHBadgeViewFont [UIFont systemFontOfSize:11]
@implementation HHBadgeView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = NO;
        self.imageView.contentMode = UIViewContentModeCenter;
        [self setBackgroundImage:[UIImage imageNamed:@"main_badge"] forState:UIControlStateNormal];
        //设置字体大小
        self.titleLabel.font = HHBadgeViewFont;
        [self sizeToFit];
    }
    return self;
}

- (void)setBadgeValue:(NSString *)badgeValue {
    _badgeValue = badgeValue;
    //判断badgeValue是否有内容
    if (badgeValue.length == 0 || [badgeValue isEqualToString:@"0"]) {
        self.hidden = YES;
    }else {
        self.hidden = NO;
    }
    
//    CGRect rect = [badgeValue boundingRectWithSize:CGSizeMake(self.frame.size.width, self.frame.size.height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:HHBadgeViewFont} context:nil];
//    NSLog(@"%f--%f", rect.size.width, rect.size.height);
//    NSLog(@"%f--%f", rect.size.width, self.width);
    if (badgeValue.length > 2) {//文字宽度大于控件宽度
        [self setImage:[UIImage imageNamed:@"new_dot"] forState:UIControlStateNormal];
        [self setTitle:@"" forState:UIControlStateNormal];
        [self setBackgroundImage:nil forState:UIControlStateNormal];
    }else {
        [self setBackgroundImage:[UIImage imageNamed:@"main_badge"] forState:UIControlStateNormal];
        [self setTitle:badgeValue forState:UIControlStateNormal];
        [self setImage:nil forState:UIControlStateNormal];
    }
    
}



@end
