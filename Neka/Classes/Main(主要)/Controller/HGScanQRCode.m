//
//  HGScanQRCode.m
//  Neka
//
//  Created by ma c on 2017/8/11.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGScanQRCode.h"
#import <AVFoundation/AVFoundation.h>
@implementation HGScanQRCode


+ (BOOL)hasQRCodeWithImage:(UIImage *)img {
    if (img) {
        CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{CIDetectorAccuracy: CIDetectorAccuracyHigh}];
        NSArray *features = [detector featuresInImage:[CIImage imageWithCGImage:img.CGImage]];
        if (features.count == 0) {
            NSLog(@"二维码不存在");
            return NO;
        }else {
            CIQRCodeFeature *feature = [features objectAtIndex:0];
            if (feature) {
                NSLog(@"二维码扫描结果:%@", feature.messageString);
                return YES;
            }
            return NO;
        }
    }else {
        NSLog(@"图片不存在");
        return NO;
    }
}

+ (NSString *)infoOfQRCodeWithImage:(UIImage *)img {
    if (img) {
        CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{CIDetectorAccuracy: CIDetectorAccuracyHigh}];
        NSArray *features = [detector featuresInImage:[CIImage imageWithCGImage:img.CGImage]];
        if (features.count == 0) {
            NSLog(@"二维码不存在");
            return nil;
        }else {
            CIQRCodeFeature *feature = [features objectAtIndex:0];
            if (feature) {
                NSLog(@"二维码扫描结果:%@", feature.messageString);
                return feature.messageString;
            }
            return nil;
        }
    }else {
        NSLog(@"图片不存在");
        return nil;
    }
}




@end
