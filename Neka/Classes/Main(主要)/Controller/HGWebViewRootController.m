//
//  HGWebViewRootController.m
//  Neka
//
//  Created by ma c on 2017/7/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGWebViewRootController.h"
#import "HGWebAgentSetting.h"
#import "WYWebProgressLayer.h"
#import <JSHAREService.h>
#import <AVFoundation/AVFoundation.h>
#import "HGScanQRCode.h"
#import "HGOrderWebViewController.h"

@interface HGWebViewRootController ()<UIGestureRecognizerDelegate>

@property (nonatomic, copy)NSString *startURL;
@property (nonatomic, strong)MBProgressHUD *progressHUD;
@property (nonatomic, strong)WYWebProgressLayer *progressLayer;
@property (nonatomic, strong)UILabel *titleLabel;
@property (nonatomic, strong)UIButton *shareBtn;
@property (nonatomic, strong)NSArray *shareTypes;


@property (nonatomic, copy)NSString *sendFriendLink;
@property (nonatomic, copy)NSString *imgUrl;
@property (nonatomic, copy)NSString *tTitle;
@property (nonatomic, copy)NSString *tContent;

@property (nonatomic, strong)UILongPressGestureRecognizer *longPressed;
@property (nonatomic, strong)UIImage *touchImg;
           
@end

@implementation HGWebViewRootController


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@", _urlString);
    // Do any additional setup after loading the view.
    
    [self.tabBarController.tabBar setHidden:YES];
    [self.navigationController.navigationBar setHidden:YES];
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 64)];
    bar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nav_bg"]];
    
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, 64, 44)];
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 7, 30, 30)];
    img.image = [UIImage imageNamed:@"fanh"];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, 44, 44)];
    label.text = @"返回";
    label.font = [UIFont systemFontOfSize:16];
    label.textColor = [UIColor whiteColor];
    [backBtn addSubview:img];
    [backBtn addSubview:label];
    
    UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(64, 20, 40, 44)];
    [closeBtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    closeBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [closeBtn setTitle:@"关闭" forState:UIControlStateNormal];
    
    [bar addSubview:backBtn];
    [bar addSubview:closeBtn];
    [self.view addSubview:bar];
    
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 2 - 80, 20, 160, 44)];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.font = [UIFont systemFontOfSize:18];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [bar addSubview:_titleLabel];
    
    
    _shareBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - 54, 20, 44, 44)];
    [_shareBtn setTitle:@"分享" forState:UIControlStateNormal];
    [_shareBtn addTarget:self action:@selector(shareAction) forControlEvents:UIControlEventTouchUpInside];
    [bar addSubview:_shareBtn];
    [_shareBtn setHidden:YES];

    _longPressed = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAction:)];
    _longPressed.delegate = self;
    
    
}

//创建webview
- (void)createWebView {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [HGWebAgentSetting updateWebAgentSetting];
    });
    
    if (_webView == nil) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
        _webView.delegate = self;
        [self.view addSubview:_webView];
        
        if (_urlString.length > 0) {
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]]];
        }
        //添加长按手势
        [_webView addGestureRecognizer:_longPressed];
    }
    

}
//重新创建webview
- (void)recreateWebView {
    
    if (self.webView == nil) {
        NSLog(@"创建新的web");
        self.automaticallyAdjustsScrollViewInsets = NO;
        self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
        self.webView.delegate = self;
        [self.view addSubview:self.webView];
        
        
        NSString *url = nil;
        if (self.currentURL.length > 0) {
            url = self.currentURL;
        }else if (self.urlString.length > 0) {
            url = self.urlString;
        }else {
            [MBProgressHUD showError:@"网页出错"];
            return;
        }
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
        //添加长按手势
        [_webView addGestureRecognizer:_longPressed];
    }
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self createWebView];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)shareAction {
    
    _shareTypes = @[@{@"title": @"微信好友",
                        @"image": @"wechat",
                        @"type": @(JSHAREPlatformWechatSession)},
                      @{@"title": @"微信朋友圈",
                        @"image": @"wechat_moment",
                        @"type": @(JSHAREPlatformWechatTimeLine)},
                      @{@"title": @"微信收藏",
                        @"image": @"wechat_fav",
                        @"type": @(JSHAREPlatformWechatFavourite)},
                      @{@"title": @"QQ好友",
                        @"image": @"qq",
                        @"type": @(JSHAREPlatformQQ)},
                      @{@"title": @"QQ空间",
                        @"image": @"qzone",
                        @"type": @(JSHAREPlatformQzone)},
                      @{@"title": @"新浪微博",
                        @"image": @"weibo",
                        @"type": @(JSHAREPlatformSinaWeibo)}];
    
    if ([JSHAREService isWeChatInstalled]) {
    }
    
    if ([JSHAREService isQQInstalled]) {
    }
    
    if ([JSHAREService isSinaWeiBoInstalled]) {
    }
    
    
    
    
    UIAlertController *alert = [[UIAlertController alloc] init];
    for (int i = 0; i < _shareTypes.count; i++) {
        NSString *title = _shareTypes[i][@"title"];
        __weak HGWebViewRootController *weakSelf = self;
        UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf shareWithIndex:i];
        }];
        [alert addAction:action];
    }
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
 
}

- (void)shareWithIndex:(NSInteger)index {
    JSHAREMessage *message = [JSHAREMessage message];
    NSString *p = _shareTypes[index][@"type"];
    JSHAREPlatform platform = [p integerValue];
    if (platform == JSHAREPlatformSinaWeibo) {
        if (![JSHAREService isSinaWeiBoInstalled]) {
            [MBProgressHUD showError:@"未安装新浪微博客户端"];
            return;
        }
    }else if (platform == JSHAREPlatformWechatSession || platform == JSHAREPlatformWechatFavourite || platform == JSHAREPlatformWechatTimeLine) {
        if (![JSHAREService isWeChatInstalled]) {
            [MBProgressHUD showError:@"未安装微信客户端"];
            return;
        }
    }else if (platform == JSHAREPlatformQQ || platform == JSHAREPlatformQzone) {
        if (![JSHAREService isQQInstalled]) {
            [MBProgressHUD showError:@"未安装QQ客户端"];
            return;
        }
    }
    message.platform = platform;
    message.mediaType = JSHARELink;
    message.url = _sendFriendLink;
    message.text = _tContent;
    message.title = _tTitle;
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:_imgUrl]];
    message.image = imageData;
    
    [JSHAREService share:message handler:^(JSHAREState state, NSError *error) {
        NSLog(@"%ld---%@", state, error.description);
        NSLog(@"分享回调");
        
    }];
}




- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
    
}



- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

}


- (void)webViewDidStartLoad:(UIWebView *)webView {
    [_shareBtn setHidden:YES];
    _progressLayer = [WYWebProgressLayer layerWithFrame:CGRectMake(0, 64, SCREEN_SIZE.width, 2)];
    [self.view.layer addSublayer:_progressLayer];
    [_progressLayer startLoad];
    _titleLabel.text = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [_progressLayer finishedLoad];
    NSString *url = webView.request.URL.absoluteString;
    if (_startURL.length == 0) {
        _startURL = url;
    }
    _currentURL = url;
    _titleLabel.text = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.title = _titleLabel.text;
    NSLog(@"FinishLoad:%@", url);
    
    _sendFriendLink = [webView stringByEvaluatingJavaScriptFromString:@"window.shareData.sendFriendLink"];
    if (_sendFriendLink.length > 0) {
        [_shareBtn setHidden:NO];
        _imgUrl = [webView stringByEvaluatingJavaScriptFromString:@"window.shareData.imgUrl"];
        _tTitle = [webView stringByEvaluatingJavaScriptFromString:@"window.shareData.tTitle"];
        _tContent = [webView stringByEvaluatingJavaScriptFromString:@"window.shareData.tContent"];
    }else {
        [_shareBtn setHidden:YES];
    }
    
    
   
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"%@", error);
    NSLog(@"host:%@", _lastURL.host);
    if ([_lastURL.host isEqualToString:@"api.map.baidu.com"]) {
        [_webView loadRequest:[NSURLRequest requestWithURL:_lastURL]];
    }else {
        [_progressLayer finishedLoad];
        [webView goBack];
    }
    
    
    
}

//后退
- (void)back {
    if ([_startURL isEqualToString:_currentURL]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    [self.webView goBack];
    
}

//关闭
- (void)close {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -- 长按保存图片
//获取图片
- (void)longPressAction:(UILongPressGestureRecognizer *)recognizer {
    NSLog(@"tap");
    if (recognizer.state != UIGestureRecognizerStateBegan) {
        return;
    }
    CGPoint touchPoint = [recognizer locationInView:self.webView];
    NSString *imgURL = [NSString stringWithFormat:@"document.elementFromPoint(%f,%f).src", touchPoint.x, touchPoint.y];
    NSLog(@"%@", imgURL);
    NSString *url = [_webView stringByEvaluatingJavaScriptFromString:imgURL];
    if (url.length == 0) {
        return;
    }else {
        _touchImg = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
        if (_touchImg) {
            if ([HGScanQRCode hasQRCodeWithImage:_touchImg]) {
                [self touchImageActionWithImage:_touchImg hasQRCode:YES];
            }else {
                [self touchImageActionWithImage:_touchImg hasQRCode:NO];
            }
        }
        
    
    }
    NSLog(@"%@", url);
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(nonnull UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


- (void)touchImageActionWithImage:(UIImage *)img hasQRCode:(BOOL)hasQRCode {
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *action1= [UIAlertAction actionWithTitle:@"保存图片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"保存图片");
        UIImageWriteToSavedPhotosAlbum(img, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void*)self);
    }];
    [alert addAction:action1];
    if (hasQRCode) {
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"识别图中二维码" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSLog(@"识别图中二维码");
            NSString *resultURL = [HGScanQRCode infoOfQRCodeWithImage:img];
            HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
            web.urlString = resultURL;
            [self.navigationController pushViewController:web animated:YES];
        }];
        [alert addAction:action2];
    }
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"识别图中二维码");
    }];
    [alert addAction:action3];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    NSLog(@"image = %@, error = %@, contextInfo = %@", image, error, contextInfo);
    if (error) {
        [MBProgressHUD showError:@"保存失败"];
    }else {
        [MBProgressHUD showSuccess:@"保存成功"];
    }
}


- (void)saveImgWithUrlString:(NSString *)urlString {
    
    
    
    
}

- (BOOL)hasSqcode {
    
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
