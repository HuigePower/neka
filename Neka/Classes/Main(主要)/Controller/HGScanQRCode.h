//
//  HGScanQRCode.h
//  Neka
//
//  Created by ma c on 2017/8/11.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGScanQRCode : NSObject
+ (BOOL)hasQRCodeWithImage:(UIImage *)img;
+ (NSString *)infoOfQRCodeWithImage:(UIImage *)img;

@end
