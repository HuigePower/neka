//
//  HGWebViewRootController.h
//  Neka
//
//  Created by ma c on 2017/7/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGWebViewRootController : UIViewController<UIWebViewDelegate>
@property (nonatomic, copy)NSString *urlString;
@property (nonatomic, copy)NSString *currentURL;
@property (nonatomic, strong)UIWebView *webView;


@property (nonatomic, strong)NSURL *lastURL;


- (void)recreateWebView;
@end
