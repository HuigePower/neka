//
//  RootViewController.m
//  耐卡
//
//  Created by ma c on 16/10/16.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "RootViewController.h"
#import "HHTabBar.h"
#import "HGMyCarViewController.h"
@interface RootViewController ()<HHTabBarDelegate>
@property (nonatomic, strong)NSMutableArray *items;
@property (nonatomic, strong)HHTabBar *myTabBar;

@end

@implementation RootViewController

- (NSMutableArray *)items {
    if (_items == nil) {
        _items = [[NSMutableArray alloc] init];
    }
    return _items;
}




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpAllChildViewControllers];

    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 添加所有的子控制器
- (void)setUpAllChildViewControllers {
    NSArray *arr = @[
                     @{@"name":@"HomeViewController", @"title":@"首页", @"image":@"footer_home", @"selectedImage":@"footer_home_active"},
                     @{@"name":@"HGMyCarViewController", @"title":@"爱车", @"image":@"footer_lovecar", @"selectedImage":@"footer_lovecar_active"},
//                     @{@"name":@"KPViewController", @"title":@"服务快派", @"image":@"footer_my", @"selectedImage":@"footer_my_active"},
                     @{@"name":@"WelfareViewController", @"title":@"福利", @"image":@"footer_welfare", @"selectedImage":@"footer_welfare_active"},
                     @{@"name":@"MineViewController", @"title":@"我的", @"image":@"footer_my", @"selectedImage":@"footer_my_active"},
                     ];
    for (NSDictionary *dic in arr) {
        Class class = NSClassFromString(dic[@"name"]);
        UIViewController *vc = [[class alloc] init];
        
        [self setUpChildViewController:vc withTitle:dic[@"title"] image:dic[@"image"] seletedImage:dic[@"selectedImage"]];
    }
    [self setUpTabBar];
}

#pragma mark - 添加一个子控制器
- (void)setUpChildViewController:(UIViewController *)viewController withTitle:(NSString *)title image:(NSString *)imageName seletedImage:(NSString *)selectedImageName {
    
    viewController.title = title;
    viewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:[UIImage imageNamed:imageName] selectedImage:[UIImage imageNamed:selectedImageName]];
    
    //保存tabBarItem模型到数组
    [self.items addObject:viewController.tabBarItem];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    nav.navigationBar.translucent = NO;
    nav.edgesForExtendedLayout = UIRectEdgeNone;
    [nav.navigationBar setTintColor:[UIColor whiteColor]];
    [nav.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bg"] forBarMetrics:UIBarMetricsDefault];
    [nav.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [self addChildViewController:nav];
    
}

#pragma mark -- 设置tabbar
- (void)setUpTabBar {
    //自定义tabbar
    _myTabBar = [[HHTabBar alloc] initWithFrame:self.tabBar.frame];
    _myTabBar.backgroundColor = [UIColor whiteColor];
    
    //设置代理
    _myTabBar.delegate = self;
    
    //给tabBar传递tabBarItem模型
    _myTabBar.items = self.items;
    
    //添加自定义tabBar
    [self.view addSubview:_myTabBar];
    
    //移除系统的tabBar
    [self.tabBarController.tabBar removeFromSuperview];
    [self.tabBar addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:nil];
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    NSLog(@"%@--%@--%@--%@", keyPath, object, change, context);
    self.myTabBar.hidden = [change[@"new"] boolValue];
    
}


#pragma mark -- 点击tabBar上的按钮调用
- (void)tabbar:(HHTabBar *)tabBar didClickButton:(NSInteger)index {
    NSLog(@"%ld", (long)index);
    self.selectedIndex = index;
    
}

- (void)carBtnClick:(UIButton *)btn {
    self.selectedIndex = 4;
    [_myTabBar cancelSelected];
}



- (void)dealloc {
    [self.tabBar removeObserver:self forKeyPath:@"hidden"];
}


@end
