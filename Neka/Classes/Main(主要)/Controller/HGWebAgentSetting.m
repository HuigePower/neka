//
//  HGWebAgentSetting.m
//  Neka
//
//  Created by ma c on 2017/7/16.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGWebAgentSetting.h"

@implementation HGWebAgentSetting

//修改webAgent里保存的信息
+ (void)updateWebAgentSetting {
    
    UIWebView *tempWebView = [[UIWebView alloc] init];
    NSString *oldAgent = [tempWebView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    NSLog(@"oldAgent:%@", oldAgent);
    NSString *myAgent = nil;
    NSString *newAgent = nil;
    if ([oldAgent rangeOfString:@"ticket"].length > 0) {
        newAgent = [oldAgent stringByReplacingOccurrencesOfString:@"ticket=(NULL)" withString:[NSString stringWithFormat:@"ticket=%@", [UserDefaultManager ticket]]];
    }else {
        myAgent = [NSString stringWithFormat:@", 10.3,pigcmso2oreallifeapp,versioncode=80,life_app,ticket=%@,device-id=%@,",  [UserDefaultManager ticket], DEVICE_ID];
        newAgent = [NSString stringWithFormat:@"%@ %@", oldAgent, myAgent];
    }
    
    NSLog(@"newAgent:%@", newAgent);
    NSDictionary *dic = @{@"UserAgent": newAgent};
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:dic];
    
}

@end
