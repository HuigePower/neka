//
//  HGWebAgentSetting.h
//  Neka
//
//  Created by ma c on 2017/7/16.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGWebAgentSetting : NSObject
//更新webview的Agent设置
+ (void)updateWebAgentSetting;
@end
