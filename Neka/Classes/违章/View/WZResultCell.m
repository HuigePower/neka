//
//  WZResultCell.m
//  Neka
//
//  Created by ma c on 2017/11/14.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "WZResultCell.h"

@implementation WZResultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    if (_carNumLabel != nil) {
        _carNumLabel.layer.borderColor = [UIColor whiteColor].CGColor;
        _carNumLabel.layer.borderWidth = 1.0;
    }
}

- (void)setPrice:(NSString *)price andScore:(NSString *)score {
    

    _priceLabel.text = [NSString stringWithFormat:@"罚款：%@", price];
    _scoreLabel.text = [NSString stringWithFormat:@"记分：%@", score];
    
    NSMutableAttributedString *att1 = [[NSMutableAttributedString alloc] initWithString:_priceLabel.text];
    [att1 addAttributes:@{NSForegroundColorAttributeName: [UIColor redColor],
                         NSFontAttributeName: [UIFont systemFontOfSize:18]}
                 range:NSMakeRange(3, _priceLabel.text.length - 3)];
    _priceLabel.attributedText = att1;
    
    NSMutableAttributedString *att2 = [[NSMutableAttributedString alloc] initWithString:_scoreLabel.text];
    [att2 addAttributes:@{NSForegroundColorAttributeName: [UIColor redColor],
                         NSFontAttributeName: [UIFont systemFontOfSize:18]}
                 range:NSMakeRange(3, _scoreLabel.text.length - 3)];
    _scoreLabel.attributedText = att2;
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
