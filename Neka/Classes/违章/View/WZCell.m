//
//  WZCell.m
//  Neka
//
//  Created by ma c on 2017/11/12.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "WZCell.h"

@implementation WZCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (instancetype)cellWithIndex:(NSInteger)index andTableView:(UITableView *)tableView {
    
    NSString *cellIdentifier = [NSString stringWithFormat:@"WZCell%ld", index];
    WZCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"WZCell" owner:self options:nil] objectAtIndex:index];
    }
    return cell;
}



- (void)selectCarNumberPrefix:(id)sender {
    NSLog(@"111");
    if (_selectPrefixBlock) {
        _selectPrefixBlock(_carNumPrefixButton.titleLabel.text);
    }
}

- (IBAction)selectCarType:(id)sender {
    if (_carTypeBlock) {
        _carTypeBlock(_carType.titleLabel.text);
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
