//
//  WZMyCarListCell.h
//  Neka
//
//  Created by ma c on 2017/11/16.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGCarInfoModel.h"
@interface WZMyCarListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *carImageView;

@property (weak, nonatomic) IBOutlet UILabel *carSeries;

@property (weak, nonatomic) IBOutlet UILabel *carNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *engineNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *frameNumberLabel;

@property (nonatomic, strong)HGCarInfoModel *carInfoModel;

@end
