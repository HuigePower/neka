//
//  WZMyCarListCell.m
//  Neka
//
//  Created by ma c on 2017/11/16.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "WZMyCarListCell.h"

@implementation WZMyCarListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCarInfoModel:(HGCarInfoModel *)carInfoModel {
    _carInfoModel = carInfoModel;
    _carSeries.text = [NSString stringWithFormat:@"车系：%@", _carInfoModel.cat_name];
    _carNumberLabel.text = [NSString stringWithFormat:@"车牌号：%@%@", _carInfoModel.cat_num_name, _carInfoModel.cat_num];
    _engineNumberLabel.text = [NSString stringWithFormat:@"发动机号：%@", _carInfoModel.cat_engine_num];
    _frameNumberLabel.text = [NSString stringWithFormat:@"车架号：%@", _carInfoModel.cat_frame_num];
    [_carImageView setImageWithURL:[NSURL URLWithString:_carInfoModel.cat_pic] placeholderImage:nil];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
