//
//  WZResultCell.h
//  Neka
//
//  Created by ma c on 2017/11/14.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WZResultCell : UITableViewCell

//时间
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
//内容
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
//地址
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
//扣除分数
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
//罚款
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
//
@property (weak, nonatomic) IBOutlet UILabel *carNumLabel;

- (void)setPrice:(NSString *)price andScore:(NSString *)score;
@end
