//
//  WZCell.h
//  Neka
//
//  Created by ma c on 2017/11/12.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^CarTypeSelected)(NSString *type);
typedef void(^CarSelectPrefixBlock)(NSString *prefixString);

@interface WZCell : UITableViewCell




@property (weak, nonatomic) IBOutlet UIButton *carNumPrefixButton;

@property (weak, nonatomic) IBOutlet UITextField *carNumTextField;

@property (weak, nonatomic) IBOutlet UILabel *city;

@property (weak, nonatomic) IBOutlet UITextField *engineNumTextField;

@property (weak, nonatomic) IBOutlet UITextField *frameNumTextField;

@property (weak, nonatomic) IBOutlet UIButton *carType;



@property (nonatomic, copy)CarTypeSelected carTypeBlock;
@property (nonatomic, copy)CarSelectPrefixBlock selectPrefixBlock;


+ (instancetype)cellWithIndex:(NSInteger)index andTableView:(UITableView *)tableView;



@end
