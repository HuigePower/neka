//
//  CarTypeViewController.m
//  Neka
//
//  Created by yu on 2017/12/4.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "CarTypeViewController.h"

@interface CarTypeViewController ()<UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, strong)NSArray *list;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (nonatomic, strong)NSDictionary *data;
@end

@implementation CarTypeViewController

//- (instancetype)init {
//    if (self = [super init]) {
//
//
//        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//        [manager POST:@"http://api.jisuapi.com/illegal/lstype" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//            NSLog(@"%@", responseObject);
//            if ([responseObject[@"status"] isEqualToString:@"0"]) {
//                _list = responseObject[@"result"];
//            }
//            [_tableView reloadData];
//        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSLog(@"%@", error);
//        }];
//    }
//    return self;
//}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _list = @[
              @{
                  @"code": @"02",
                  @"name": @"小型汽车"
                  },
              @{
                  @"code": @"01",
                  @"name": @"大型汽车"
                  },
              @{
                  @"code": @"03",
                  @"name": @"使馆汽车"
                  },
              @{
                  @"code": @"04",
                  @"name": @"领馆汽车"
                  },
              @{
                  @"code": @"05",
                  @"name": @"境外汽车"
                  },
              @{
                  @"code": @"06",
                  @"name": @"外籍汽车"
                  },
              @{
                  @"code": @"07",
                  @"name": @"两、三轮摩托车"
                  },
              @{
                  @"code": @"08",
                  @"name": @"轻便摩托车"
                  },
              @{
                  @"code": @"09",
                  @"name": @"使馆摩托车"
                  },
              @{
                  @"code": @"10",
                  @"name": @"领馆摩托车"
                  },
              @{
                  @"code": @"11",
                  @"name": @"境外摩托车"
                  },
              @{
                  @"code": @"12",
                  @"name": @"外籍摩托车"
                  },
              @{
                  @"code": @"13",
                  @"name": @"低速车"
                  },
              @{
                  @"code": @"14",
                  @"name": @"拖拉机"
                  },
              @{
                  @"code": @"15",
                  @"name": @"挂车"
                  },
              @{
                  @"code": @"16",
                  @"name": @"教练汽车"
                  },
              @{
                  @"code": @"17",
                  @"name": @"教练摩托车"
                  },
              @{
                  @"code": @"18",
                  @"name": @"试验汽车"
                  },
              @{
                  @"code": @"19",
                  @"name": @"试验摩托车"
                  },
              @{
                  @"code": @"20",
                  @"name": @"临时入境汽车"
                  },
              @{
                  @"code": @"21",
                  @"name": @"临时入境摩托车"
                  },
              @{
                  @"code": @"22",
                  @"name": @"临时行驶车"
                  },
              @{
                  @"code": @"23",
                  @"name": @"警用汽车"
                  },
              @{
                  @"code": @"24",
                  @"name": @"警用摩托"
                  },
              @{
                  @"code": @"25",
                  @"name": @"原农机"
                  },
              @{
                  @"code": @"26",
                  @"name": @"香港入出境车"
                  },
              @{
                  @"code": @"27",
                  @"name": @"澳门入出境车"
                  },
              @{
                  @"code": @"51",
                  @"name": @"大型新能源汽车"
                  },
              @{
                  @"code": @"52",
                  @"name": @"小型新能源汽车"
                  }
              ];
    _data = _list[0];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _list.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return _list[row][@"name"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _data = _list[row];
    
}


- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)confirm:(id)sender {
    if (_carTypeBlock) {
        _carTypeBlock(_data);
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}




- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
