//
//  TargetCityViewController.h
//  Neka
//
//  Created by ma c on 2017/11/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ResultBlock)(NSInteger index);
@interface TargetCityViewController : UIViewController


@property (nonatomic, copy)ResultBlock resultBlock;

@end
