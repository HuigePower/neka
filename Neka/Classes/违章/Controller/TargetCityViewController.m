//
//  TargetCityViewController.m
//  Neka
//
//  Created by ma c on 2017/11/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "TargetCityViewController.h"

@interface TargetCityViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong)NSMutableArray *cityList;
@property (nonatomic, strong)NSMutableArray *provinceList;
@end

@implementation TargetCityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"选择城市";
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    _cityList = [NSMutableArray new];
    _provinceList = [NSMutableArray new];
    [self initDataSource];
    
}

- (void)initDataSource {
    [self showLoading];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID};
    [manager POST:@"http://www.nekahome.com/appapi.php?c=Car_live&a=get_all_citys" parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        _cityList = data[@"result"][@"data"];
        NSLog(@"%@", data);
        [self hideLoading];
        [_tableView reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
    
}






- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _cityList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *list = _cityList[section][@"list"];
    return [list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dic = _cityList[indexPath.section][@"list"][indexPath.row];
    cell.textLabel.text = dic[@"city"];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 40)];
    view.backgroundColor = mRGB(240, 240, 240, 1);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 60, 40)];
    label.text = _cityList[section][@"province"];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:16];
    [view addSubview:label];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = _cityList[indexPath.section][@"list"][indexPath.row];
    if (_resultBlock) {
        _resultBlock(dic);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
