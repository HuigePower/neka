//
//  WZResultViewController.m
//  Neka
//
//  Created by ma c on 2017/11/14.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "WZResultViewController.h"
#import "WZResultCell.h"
#define WZResultCellIdentifier01 @"WZResultCellIdentifier01"
#define WZResultCellIdentifier02 @"WZResultCellIdentifier02"
@interface WZResultViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong)NSArray *wzList;
@end

@implementation WZResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"查询结果";
    _wzList = _wzInfo[@"list"];
    [_tableView reloadData];
    
    
}







- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _wzList.count + 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WZResultCell *cell;
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:WZResultCellIdentifier01];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"WZResultCell" owner:nil options:nil] firstObject];
        }
        cell.carNumLabel.text = [NSString stringWithFormat:@"%@ %@", _wzInfo[@"lsprefix"], _wzInfo[@"lsnum"]];
    }else {
        cell = [tableView dequeueReusableCellWithIdentifier:WZResultCellIdentifier02];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"WZResultCell" owner:nil options:nil] lastObject];
        }
        NSDictionary *data = _wzList[indexPath.section - 1];
        cell.timeLabel.text =  data[@"time"];
        cell.contentLabel.text = data[@"content"];
        cell.addressLabel.text = data[@"address"];
        [cell setPrice:data[@"price"] andScore:data[@"score"]];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        return 115;
//    }else {
//        return 150;
//    }
    return 150;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}



- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.backgroundColor = [UIColor whiteColor];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *)view;
    footer.backgroundColor = [UIColor whiteColor];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
