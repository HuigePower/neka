//
//  WZCheckViewController.m
//  Neka
//
//  Created by ma c on 2017/11/12.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "WZCheckViewController.h"
#import "WZCell.h"
#import "CarNumPrefixViewController.h"
#import "TargetCityViewController.h"
#import "WZResultViewController.h"
#import "CarTypeViewController.h"
#define JiSUAPI @"7241db4a958c9862"

@interface WZCheckViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong)WZCell *carTypeCell;
@property (nonatomic, strong)WZCell *carNumberCell;
@property (nonatomic, strong)WZCell *cityCell;
@property (nonatomic, strong)WZCell *engineNumberCell;
@property (nonatomic, strong)WZCell *frameNumberCell;
@property (nonatomic, strong)NSMutableArray *cellArr;
@property (nonatomic, strong)CarNumPrefixViewController *prefixViewController;
@property (nonatomic, copy)NSString *carType;

@property (nonatomic, strong)NSArray *provinceList;
@property (nonatomic, strong)NSDictionary *cityData;
@property (nonatomic, assign)NSInteger index;
@property (nonatomic, strong)CarTypeViewController *carTypeSelectVC;
@property (nonatomic, strong)NSString *carTypeCode;
@end

@implementation WZCheckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tabBarController.tabBar setHidden:YES];
    self.title = @"违章查询";
    
    __weak WZCheckViewController *weakSelf = self;
    
    _carTypeSelectVC = [[CarTypeViewController alloc] init];
    _carTypeSelectVC.definesPresentationContext = YES;
    _carTypeSelectVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    //选择车牌号前缀
    _prefixViewController = [[CarNumPrefixViewController alloc] init];
    _prefixViewController.definesPresentationContext = YES;
    _prefixViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    _prefixViewController.resultBlock = ^(NSInteger index) {
        weakSelf.index = index;
        [weakSelf updateCarNumberPrefixWithProvinceIndex:weakSelf.index];
    };
    
    
    
    _cellArr = [NSMutableArray new];
    
    _carTypeCell = [WZCell cellWithIndex:0 andTableView:_tableView];
    _carTypeCell.carTypeBlock = ^(NSString *type) {
        NSLog(@"%@", type);
        [weakSelf presentViewController:weakSelf.carTypeSelectVC animated:YES completion:nil];
    };
    _carTypeSelectVC.carTypeBlock = ^(NSDictionary *data) {
        [weakSelf.carTypeCell.carType setTitle:data[@"name"] forState:UIControlStateNormal];
        weakSelf.carTypeCode = data[@"code"];
        [weakSelf.carTypeCell.carType setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    };
    
    
    
    
    _carNumberCell = [WZCell cellWithIndex:1 andTableView:_tableView];
    _carNumberCell.selectPrefixBlock = ^(NSString *prefixString) {
        weakSelf.prefixViewController.provinceList = weakSelf.provinceList;
        weakSelf.prefixViewController.index = weakSelf.index;
        [weakSelf presentViewController:weakSelf.prefixViewController animated:YES completion:nil];
    };

    
//    _cityCell = [WZCell cellWithIndex:2 andTableView:_tableView];
    _engineNumberCell = [WZCell cellWithIndex:3 andTableView:_tableView];
    _frameNumberCell = [WZCell cellWithIndex:4 andTableView:_tableView];
    
    [_cellArr addObject:_carTypeCell];
    [_cellArr addObject:_carNumberCell];
//    [_cellArr addObject:_cityCell];
    [_cellArr addObject:_engineNumberCell];
    [_cellArr addObject:_frameNumberCell];
    
    //获取省份列表
    _index = 0;
    _provinceList = [NSArray new];
    [self initDataSource];
    
    
    _tableView.tableFooterView = [UIView new];
    [_tableView reloadData];
    
    self.automaticallyAdjustsScrollViewInsets = YES;
    
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"%ld", _cellArr.count);
    return _cellArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WZCell *cell = [_cellArr objectAtIndex:indexPath.row];
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 40)];
    bgView.backgroundColor = mRGB(240, 240, 240, 1);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 7, 200, 26)];
    label.text = @"机动车信息";
    label.textColor = mRGB(138, 138, 140, 1);
    label.font = [UIFont systemFontOfSize:15];
    [bgView addSubview:label];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(15, 13.5, 1, 13)];
    line.backgroundColor = mRGB(0, 78, 206, 1);
    [bgView addSubview:line];
    
    return bgView;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 60)];

    UIButton *check = [[UIButton alloc] initWithFrame:CGRectMake(15, 20, SCREEN_SIZE.width - 30, 40)];
    check.backgroundColor = mRGB(0, 127, 221, 1);
    check.layer.masksToBounds = YES;
    check.layer.cornerRadius = 2;
    check.titleLabel.font = [UIFont systemFontOfSize:17];
    [check setTitle:@"查  询" forState:UIControlStateNormal];
    [check setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [check addTarget:self action:@selector(requestWZData) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:check];
    
    UITextView *textview = [[UITextView alloc] initWithFrame:CGRectMake(15, 80, SCREEN_SIZE.width - 30, 300) textContainer:nil];
    textview.editable = NO;
    textview.scrollEnabled = NO;
    textview.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    textview.backgroundColor = [UIColor clearColor];
    textview.font = [UIFont systemFontOfSize:15];
    textview.textColor = mRGB(154, 154, 154, 1);
    textview.text = @"1、在输入过程中请仔细区分D和0，z和2，1、i和l等易混淆字符。\n2、如遇*、-、_等特殊字符，请在输入法中选择半角输入，并区分中划线和下划线。\n3、如遇空格请使用“+”代替。\n4、请注意区别车架号和发动机号。\n5、机动车车牌号后5位中0为阿拉伯数字零，非英文字母“O”。";
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 10;
    
    NSDictionary *attributes = @{NSParagraphStyleAttributeName: paragraphStyle,
                                 NSFontAttributeName: [UIFont systemFontOfSize:15],
                                 NSForegroundColorAttributeName: mRGB(154, 154, 154, 1)
                                 };
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:textview.text attributes:attributes];
    textview.attributedText = attStr;
    [bgView addSubview:textview];
    
    
    return bgView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 380;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *)view;
    footer.backgroundColor = [UIColor whiteColor];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row == 2) {
//        TargetCityViewController *targetCity = [[TargetCityViewController alloc] init];
//
//        __weak WZCheckViewController *weakSelf = self;
//        targetCity.resultBlock = ^(NSDictionary *cityData) {
//            NSLog(@"%@", cityData);
//            _cityData = cityData;
//            weakSelf.cityCell.city.text = _cityData[@"city"];
//            [weakSelf updateViewWithDic:_cityData];
//        };
//
//        [self.navigationController pushViewController:targetCity animated:YES];
//
//    }
    
}


- (void)updateCarNumberPrefixWithProvinceIndex:(NSInteger)index {
    _cityData = _provinceList[index];
    [_carNumberCell.carNumPrefixButton setTitle:_cityData[@"lsprefix"] forState:UIControlStateNormal];
    NSInteger engineno = [_cityData[@"engineno"] integerValue];
    NSInteger frameno = [_cityData[@"frameno"] integerValue];
    NSLog(@"%ld--%ld", engineno, frameno);
    _engineNumberCell.engineNumTextField.text = @"";
    _frameNumberCell.frameNumTextField.text = @"";
    if (engineno == 0) {
        if ([_cellArr indexOfObject:_engineNumberCell] != NSNotFound) {
            [_cellArr removeObject:_engineNumberCell];
        }
    }else {
        if ([_cellArr indexOfObject:_engineNumberCell] == NSNotFound) {
            [_cellArr insertObject:_engineNumberCell atIndex:2];
        }
        if (engineno == 100) {
            _engineNumberCell.engineNumTextField.placeholder = @"请输入发动机号";
            
        }else {
            _engineNumberCell.engineNumTextField.placeholder = [NSString stringWithFormat:@"请输入发动机号后%ld位", engineno];
        }
    }
    
    if (frameno == 0) {
        if ([_cellArr indexOfObject:_frameNumberCell] != NSNotFound) {
            [_cellArr removeObject:_frameNumberCell];
        }
    }else {
        if ([_cellArr indexOfObject:_frameNumberCell] == NSNotFound) {
            [_cellArr addObject:_frameNumberCell];
        }
        if (frameno == 100) {
            _frameNumberCell.frameNumTextField.placeholder = @"请输入发动机号";
        }else {
            _frameNumberCell.frameNumTextField.placeholder = [NSString stringWithFormat:@"请输入车架号后%ld位", frameno];
        }
    }
    
    
    [_tableView reloadData];
}


- (void)requestWZData {
    
//    WZResultViewController *wzResult = [[WZResultViewController alloc] init];
//    [self.navigationController pushViewController:wzResult animated:YES];
//    return;
    if ([_carTypeCell.carType.titleLabel.text isEqualToString:@"请选择机动车类型"] && _carTypeCode.length != 0) {
        [MBProgressHUD showError:@"请选择机动车类型"];
        return;
    }
    
    
    if (_carNumberCell.carNumTextField.text.length != 6) {
        [MBProgressHUD showError:@"确认车牌号输入正确"];
        return;
    }
    
//    if ([_cityCell.city.text isEqualToString:@"未选择城市"]) {
//        [MBProgressHUD showError:@"请选择城市"];
//        return;
//    }
    NSInteger engineno = [_cityData[@"engineno"] integerValue];
    NSInteger frameno = [_cityData[@"frameno"] integerValue];
    if (_engineNumberCell.engineNumTextField.text.length != engineno && engineno != 100) {
        [MBProgressHUD showError:@"请确认发动机号"];
        return;
    }
    
    if (_frameNumberCell.frameNumTextField.text.length != frameno && frameno != 100) {
        [MBProgressHUD showError:@"请确认车架号"];
        return;
    }
    
    
    [self showLoading];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *tmpBody = @{@"lstype":_carTypeCode,
                              @"lsprefix": _carNumberCell.carNumPrefixButton.titleLabel.text,
                              @"lsnum": _carNumberCell.carNumTextField.text,
                              @"lstype": _carType,
                              @"engineno": _engineNumberCell.engineNumTextField.text,
                              @"frameno": _frameNumberCell.frameNumTextField.text
                              };
    NSMutableDictionary *body = [[NSMutableDictionary alloc] initWithDictionary:tmpBody];
    if (_engineNumberCell.engineNumTextField.text.length == 0) {
        [body removeObjectForKey:@"engineno"];
    }
    if (_frameNumberCell.frameNumTextField.text.length == 0) {
        [body removeObjectForKey:@"frameno"];
    }
    
    
    NSLog(@"%@", body);
    
    [manager POST:WZAPI parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@", data);
        [self hideLoading];
        if ([data[@"count"] isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"未查到您的违章"];
            return;
        }
        
        WZResultViewController *wzResult = [[WZResultViewController alloc] init];
        wzResult.wzInfo = data[@"result"];
        [self.navigationController pushViewController:wzResult animated:YES];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.description);
        [self hideLoading];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
    
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}



- (void)initDataSource {
    [self showLoading];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID};
    [manager POST:@"http://www.nekahome.com/appapi.php?c=Car_live&a=get_all_citys" parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        _provinceList = data[@"result"][@"data"];
        NSLog(@"%@", data);
        [self updateCarNumberPrefixWithProvinceIndex:_index];
        
        [self hideLoading];
        [_tableView reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
