//
//  MyCarsWZViewController.m
//  Neka
//
//  Created by ma c on 2017/11/16.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "MyCarsWZViewController.h"
#import "WZMyCarListCell.h"
#import "HGCarInfoModel.h"
#import "WZResultViewController.h"
#import "HGAddCarInfoViewController.h"
#import "WZCheckViewController.h"
#import "UIView+Border.h"
#import "WZCheckViewController.h"
#define WZMyCarListCellIdentifier @"WZMyCarListCellIdentifier"
@interface MyCarsWZViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong)NSMutableArray *carList;
@property (nonatomic, strong)NSArray *provinceList;

@property (weak, nonatomic) IBOutlet UIView *placeholderView;

@end

@implementation MyCarsWZViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"选择车辆";
    _carList = [NSMutableArray new];
    [_placeholderView removeFromSuperview];
    [self initDataSource1];
    
    [self.tabBarController.tabBar setHidden:YES];
    
    _tableView.tableFooterView = [UIView new];
    [_tableView registerNib:[UINib nibWithNibName:@"WZMyCarListCell" bundle:nil] forCellReuseIdentifier:WZMyCarListCellIdentifier];
    _tableView.layer.masksToBounds = YES;
    _tableView.layer.cornerRadius = 2;

    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initDataSource2];
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return _carList.count;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _carList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WZMyCarListCell *cell = [tableView dequeueReusableCellWithIdentifier:WZMyCarListCellIdentifier forIndexPath:indexPath];
    cell.carInfoModel = _carList[indexPath.row];
    return cell;
}

- (void)initDataSource1 {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID};
    [manager POST:@"http://www.nekahome.com/appapi.php?c=Car_live&a=get_all_citys" parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        _provinceList = data[@"result"][@"data"];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
    
}


- (void)initDataSource2 {
    [self showLoading];
    [_carList removeAllObjects];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=user_cats", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        
        if ([errorCode isEqualToString:@"0"]) {
            NSArray *carList = responseObject[@"result"];
            if (carList.count == 0) {
                [_tableView addSubview: _placeholderView];
            }else {
                [_placeholderView removeFromSuperview];
            }
            for (NSDictionary *dic in carList) {
                HGCarInfoModel *model = [[HGCarInfoModel alloc] initWithDict:dic];
                [_carList addObject:model];
            }
            [_tableView reloadData];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD showError:error.description];
    }];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%@", indexPath);
    HGCarInfoModel *carInfoModel = _carList[indexPath.row];
    
    NSDictionary *provinceInfo = [NSDictionary new];
    for (NSDictionary *dic in _provinceList) {
        if ([dic[@"lsprefix"] isEqualToString:carInfoModel.cat_num_name]) {
            provinceInfo = dic;
        }
    }

    NSMutableDictionary *body = [NSMutableDictionary new];
    [body setObject:carInfoModel.cat_num_name forKey:@"lsprefix"];
    [body setObject:carInfoModel.cat_num forKey:@"lsnum"];
    [body setObject:@"02" forKey:@"lstype"];

    NSInteger engineno = [provinceInfo[@"engineno"] integerValue];
    NSInteger frameno = [provinceInfo[@"frameno"] integerValue];
    NSLog(@"%ld--%ld", engineno, frameno);

    if (engineno != 0) {
        if (carInfoModel.cat_engine_num.length == 0) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"发动机号为空，请完善信息！" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
            UIAlertAction *prefect = [UIAlertAction actionWithTitle:@"去完善" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSLog(@"去完善：%@", carInfoModel.cat_id);
                HGAddCarInfoViewController *addCar = [[HGAddCarInfoViewController alloc] init];
                addCar.title = @"修改爱车";
                addCar.isAdd = NO;
                addCar.carInfoModel = carInfoModel;
                [self.navigationController pushViewController:addCar animated:YES];
            }];
            [alert addAction:cancel];
            [alert addAction:prefect];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }else {
            NSString *engineNumber = nil;
            if (engineno == 100) {
                engineNumber = carInfoModel.cat_engine_num;
            }else {
                engineNumber = [carInfoModel.cat_engine_num substringWithRange:NSMakeRange(carInfoModel.cat_engine_num.length - engineno, engineno)];
            }
            [body setObject:engineNumber forKey:@"engineno"];
        }
        
    }


    if (frameno != 0) {
        if (carInfoModel.cat_frame_num.length == 0) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"车架号为空，请完善信息！" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
            UIAlertAction *prefect = [UIAlertAction actionWithTitle:@"去完善" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSLog(@"去完善：%@", carInfoModel.cat_id);
                HGAddCarInfoViewController *addCar = [[HGAddCarInfoViewController alloc] init];
                addCar.title = @"修改爱车";
                addCar.isAdd = NO;
                addCar.carInfoModel = carInfoModel;
                [self.navigationController pushViewController:addCar animated:YES];
            }];
            [alert addAction:cancel];
            [alert addAction:prefect];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }else {
            NSString * frameNumber = nil;
            if (frameno == 100) {
                frameNumber = carInfoModel.cat_frame_num;
            }else {
                frameNumber = [carInfoModel.cat_frame_num substringWithRange:NSMakeRange(carInfoModel.cat_frame_num.length - frameno, frameno)];
            }
            [body setObject:frameNumber forKey:@"frameno"];
        }
        
    }
    NSLog(@"%@", body);
    [self requestWzInfoWithBody:body];
}


- (void)requestWzInfoWithBody:(NSDictionary *)body {
    [self showLoading];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:WZAPI parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@", data);
        [self hideLoading];
        if ([data[@"count"] isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"未查到您的违章"];
            return;
        }
        
        WZResultViewController *wzResult = [[WZResultViewController alloc] init];
        wzResult.wzInfo = data[@"result"];
        [self.navigationController pushViewController:wzResult animated:YES];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.description);
        [self hideLoading];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 44)];
    view.backgroundColor = [UIColor whiteColor];
    [view setBorderTopRightLeftBottom:UIEdgeInsetsMake(0, 0, 0.5, 0) color:[UIColor grayColor]];
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(15, 7, SCREEN_SIZE.width - 30, 30)];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = 3;
    [btn setTitle:@"手动查询" forState:UIControlStateNormal];
    btn.backgroundColor = mRGB(9, 104, 212, 1);
    [btn addTarget:self action:@selector(manualCheck:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    
    return view;
}


- (IBAction)checkOnOtherWay:(id)sender {
    WZCheckViewController *checkVC = [[WZCheckViewController alloc] init];
    [self.navigationController pushViewController:checkVC animated:YES];
}

- (void)manualCheck:(UIButton *)btn {
    WZCheckViewController *vc = [[WZCheckViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
