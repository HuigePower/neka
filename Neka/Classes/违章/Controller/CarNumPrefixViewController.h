//
//  CarNumPrefixViewController.h
//  Neka
//
//  Created by ma c on 2017/11/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^SelectBlock)(NSInteger index);
@interface CarNumPrefixViewController : UIViewController


@property (nonatomic, copy)SelectBlock resultBlock;

@property (nonatomic, assign)NSInteger index;

@property (nonatomic, strong)NSArray *provinceList;

@end
