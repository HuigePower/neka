//
//  CarTypeViewController.h
//  Neka
//
//  Created by yu on 2017/12/4.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^CarTypeBlock) (NSDictionary *data);
@interface CarTypeViewController : UIViewController
@property (nonatomic, copy)CarTypeBlock carTypeBlock;

@end
