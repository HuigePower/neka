//
//  CarNumPrefixViewController.m
//  Neka
//
//  Created by ma c on 2017/11/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "CarNumPrefixViewController.h"

@interface CarNumPrefixViewController ()<UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@end

@implementation CarNumPrefixViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    _list = @[@"京", @"津", @"冀", @"晋", @"蒙", @"辽", @"吉", @"黑", @"沪", @"苏", @"浙", @"皖", @"闽", @"赣", @"鲁", @"豫", @"鄂", @"湘", @"粤", @"桂", @"琼", @"渝", @"川", @"贵", @"云", @"藏", @"陕", @"甘", @"青", @"宁", @"新", @"台"];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"111");
    
    [_pickerView selectRow:_index inComponent:0 animated:YES];
}

//- (void)setPrefixString:(NSString *)prefixString {
//    _prefixString = prefixString;
//    NSInteger row = [_list indexOfObject:_prefixString];
//    [_pickerView selectRow:row inComponent:0 animated:YES];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _provinceList.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSDictionary *dic = _provinceList[row];
    return dic[@"lsprefix"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _index = row;
}


- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)confirm:(id)sender {
    
    if (_resultBlock) {
        _resultBlock(_index);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self dismissViewControllerAnimated:YES completion:nil];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
