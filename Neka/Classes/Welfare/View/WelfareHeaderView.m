//
//  WelfareHeaderView.m
//  Neka
//
//  Created by yu on 2017/12/7.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "WelfareHeaderView.h"
#import "UIImageView+AFNetworking.h"
#import "HGActivityModel.h"
#import "SnatchCell.h"
#import "HGOrderWebViewController.h"
@interface WelfareHeaderView () <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UIScrollViewDelegate>

@property (nonatomic, strong)UIImageView *topView;
@property (nonatomic, strong)UIView *middleView;
@property (nonatomic, strong)UIView *bottomView;
@property (nonatomic, strong)UICollectionView *collectionView;
@property (nonatomic, strong)UIPageControl *pageControl;

@property (nonatomic, strong)NSMutableArray *acList;

@end

@implementation WelfareHeaderView

- (instancetype)initWithFrame:(CGRect)frame andNav:(UINavigationController *)nav {
    if (self = [super initWithFrame:frame]) {
        self.nav = nav;
        self.backgroundColor = [UIColor whiteColor];
        
        _acList = [NSMutableArray new];
        
        [self addSubview:self.topView];
        [self addSubview:self.middleView];
        self.frame = CGRectMake(0, 0, SCREEN_SIZE.width, 120 + 5 + KWIDTH_6S(80));
        
        
    }
    return self;
}

- (UIImageView *)topView {
    if (_topView == nil) {
        _topView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 120)];
        [_topView setImageWithURL:[NSURL URLWithString:@"http://www.nekahome.com/tpl/Wap/default/static/welfale_images/banner.jpg"] placeholderImage:[UIImage imageNamed:@"welfare"]];
        _topView.userInteractionEnabled = YES;
        _topView.contentMode = UIViewContentModeScaleToFill;
        _topView.tag = 10;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [_topView addGestureRecognizer:tap];
        
    }
    return _topView;
}

- (void)tapAction {
    HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
    web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=Systemcoupon&a=index";
    [self.nav pushViewController:web animated:YES];
}


- (UIView *)middleView {
    if (_middleView == nil) {
        _middleView = [[UIView alloc] initWithFrame:CGRectMake(0, 120 + 5, SCREEN_SIZE.width, KWIDTH_6S(80))];
        
        UIButton *btn1 = [self makeButtonWithRect:CGRectMake(0, 0, SCREEN_SIZE.width / 2 - 5, KWIDTH_6S(80)) imageName:@"lqyhq" majorTitle:@"领取优惠券" secondaryTitle:@"先领券 再购物"];
        [btn1 addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        btn1.tag = 11;
        [_middleView addSubview:btn1];
        
        UIButton *btn2 = [self makeButtonWithRect:CGRectMake(SCREEN_SIZE.width / 2 + 5, 0, SCREEN_SIZE.width / 2 - 5, KWIDTH_6S(80)) imageName:@"jfsc" majorTitle:@"积分商城" secondaryTitle:@"积分当钱花"];
        btn2.tag = 12;
        [btn2 addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_middleView addSubview:btn2];
    }
    return _middleView;
}



- (UIButton *)makeButtonWithRect:(CGRect)rect imageName:(NSString *)imageName majorTitle:(NSString *)majorTitle secondaryTitle:(NSString *)secondaryTitle {
    
    UIButton *btn = [[UIButton alloc] initWithFrame:rect];
    btn.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH_6S(10), KWIDTH_6S(20), KWIDTH_6S(40), KWIDTH_6S(40))];
    imageView.image = [UIImage imageNamed:imageName];
    [btn addSubview:imageView];
    
    UILabel *majorTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(60), KWIDTH_6S(20), rect.size.width - KWIDTH_6S(60), KWIDTH_6S(20))];
    majorTitleLabel.text = majorTitle;
    majorTitleLabel.textColor = mRGB(0, 126, 222, 1);
    majorTitleLabel.font = [UIFont systemFontOfSize:15];
    [btn addSubview:majorTitleLabel];
    
    UILabel *secondaryTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(60), KWIDTH_6S(40), rect.size.width - KWIDTH_6S(60), KWIDTH_6S(20))];
    secondaryTitleLabel.text = secondaryTitle;
    secondaryTitleLabel.textColor = [UIColor darkGrayColor];
    secondaryTitleLabel.font = [UIFont systemFontOfSize:14];
    [btn addSubview:secondaryTitleLabel];
    
    return btn;
 
}



- (void)setActivityData:(NSDictionary *)activityData {
    _activityData = activityData;
    NSString *count = [NSString stringWithFormat:@"%@", activityData[@"count"]];
    [_acList removeAllObjects];
    if (![count isEqualToString:@"0"]) {
        NSArray *sActivity = activityData[@"sActivity"];
        self.pageControl.numberOfPages = [count integerValue];
        for (NSDictionary *dic in sActivity) {
            HGActivityModel *model = [[HGActivityModel alloc] initWithDict:dic];
            [_acList addObject:model];
        }
        [self addBottom];
    }else {
        [self removeBottom];
    }
    [self.collectionView reloadData];
}


- (void)addBottom {
    [self addSubview:self.bottomView];
    self.frame = CGRectMake(0, 0, SCREEN_SIZE.width, 120 + 5 + KWIDTH_6S(80) + 5 + KWIDTH_6S(150));
}

- (void)removeBottom {
    [self.bottomView removeFromSuperview];
    self.frame = CGRectMake(0, 0, SCREEN_SIZE.width, 120 + 5 + KWIDTH_6S(80));
    
}

- (UIView *)bottomView {
    if (_bottomView == nil) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 120 + 5 + KWIDTH_6S(80) + 5, SCREEN_SIZE.width, KWIDTH_6S(150))];
        [_bottomView addSubview:self.collectionView];
        [_bottomView addSubview:self.pageControl];
    }
    return _bottomView;
}


- (UICollectionView *)collectionView {
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(135)) collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.pagingEnabled = YES;
        _collectionView.showsHorizontalScrollIndicator = NO;
        
        [_collectionView registerNib:[UINib nibWithNibName:@"SnatchCell" bundle:nil] forCellWithReuseIdentifier:@"acCell"];
    }
    return _collectionView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _acList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SnatchCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"acCell" forIndexPath:indexPath];
    HGActivityModel *model = _acList[indexPath.row];
    NSString *endTimeStr = _activityData[@"activity"][@"end_time"];
    NSTimeInterval endTimeInterval = [endTimeStr integerValue];
    [cell setModel:model andEndTime:endTimeInterval];
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(SCREEN_SIZE.width, KWIDTH_6S(135));
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HGActivityModel *model = _acList[indexPath.row];
    HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
    web.urlString = model.url;
    [self.nav pushViewController:web animated:YES];
    
}



- (UIPageControl *)pageControl {
    if (_pageControl == nil) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, KWIDTH_6S(140), SCREEN_SIZE.width, KWIDTH_6S(10))];
        _pageControl.numberOfPages = 0;
        _pageControl.currentPage = 0;
        _pageControl.currentPageIndicatorTintColor = [UIColor blueColor];
        _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    }
    return _pageControl;
}


#pragma mark -- UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSInteger index = scrollView.contentOffset.x / SCREEN_SIZE.width;
    self.pageControl.currentPage = index;
    
}


- (void)buttonClick:(UIView *)view {
    HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
    if (view.tag == 11) {
        web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=Systemcoupon&a=index";
    }else {
        web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=Gift&a=index";
    }
    [self.nav pushViewController:web animated:YES];
}


@end
