//
//  CouponSeletionScrollView.m
//  Neka1.0
//
//  Created by ma c on 2017/2/17.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "CouponSeletionScrollView.h"

@implementation CouponSeletionScrollView
{

    NSMutableArray *_btnArr;
    UILabel *_line;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        self.showsHorizontalScrollIndicator = NO;
        self.backgroundColor = [UIColor whiteColor];
        [self makeButtons];
        
        _line = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height - 1, KWIDTH(80), 1)];
        _line.backgroundColor = mRGB(255, 50, 102, 1);
        [self addSubview:_line];
    }
    return self;
}

- (void)makeButtons {
    NSArray *btnTitles = @[@"全部分类", @"团购", @"洗车", @"预约", @"快店", @"优惠买单"];
    self.contentSize = CGSizeMake(KWIDTH(80) * btnTitles.count, KWIDTH(44));
    _btnArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < btnTitles.count; i++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(80) * i, 0, KWIDTH(80), KWIDTH(44))];
        [btn setTitle:btnTitles[i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
        btn.tag = 10 + i;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        [_btnArr addObject:btn];
    }
    [self changePointWithIndex:0];
    
}

- (void)btnClick:(UIButton *)btn {
    for (UIButton *btn in _btnArr) {
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    [self changePointWithIndex:btn.tag - 10];
    
}


- (void)changePointWithIndex:(NSInteger)index {
    UIButton *btn = (UIButton *)[self viewWithTag:index + 10];
    [btn setTitleColor:mRGB(255, 50, 102, 1) forState:UIControlStateNormal];
    _line.frame = CGRectMake(KWIDTH(80) * index, self.frame.size.height - 1, KWIDTH(80), 1);
}






@end
