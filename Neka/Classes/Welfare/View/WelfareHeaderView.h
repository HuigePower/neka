//
//  WelfareHeaderView.h
//  Neka
//
//  Created by yu on 2017/12/7.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelfareHeaderView : UIView

@property (nonatomic, strong)UINavigationController *nav;
@property (nonatomic, strong)NSDictionary *activityData;

- (instancetype)initWithFrame:(CGRect)frame andNav:(UINavigationController *)nav;

- (void)addBottom;
@end
