//
//  SnatchCell.m
//  Neka
//
//  Created by yu on 2017/12/8.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "SnatchCell.h"

@implementation SnatchCell
{
    NSTimeInterval _endTime;
    NSTimer *_timer;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setModel:(HGActivityModel *)model andEndTime:(NSTimeInterval)endTime {
    _endTime = endTime;
    
    [_imageView setImageWithURL:[NSURL URLWithString:model.list_pic]];
    _nameLabel.text = model.title;
    _requestNumLabel.text = [NSString stringWithFormat:@"总共需要%@人", model.all_count];
    _currentNumLabel.attributedText = [self AttributedText:[NSString stringWithFormat:@"参与: %@ 人", model.part_count]];
    if (_timer == nil) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
    }
}

- (NSAttributedString *)AttributedText:(NSString *)text {
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:text];
    [att addAttributes:@{NSForegroundColorAttributeName: [UIColor redColor]} range:NSMakeRange(3, text.length - 4)];
    return att;
}



- (void)countdown {
    
    NSTimeInterval nowTime = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval timeInt = _endTime - nowTime;
    int day = (int)timeInt / (3600 * 24);
    int hour = ((int)timeInt - day * 3600 * 24) / 3600;
    int minute = ((int)timeInt - day * 3600 * 24 - hour * 3600) / 60;
    int second = ((int)timeInt) % 60;
    NSString *timeStr = [NSString stringWithFormat:@"%d天 %.02d:%.02d:%.02d ", day, hour, minute, second];
    self.theRestTimeLabel.text = timeStr;
    
//    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
//    attachment.image = [UIImage imageNamed:@"clock"];
//    attachment.bounds = CGRectMake(0, - KWIDTH(2.5), KWIDTH(15), KWIDTH(15));
//    NSMutableAttributedString *attchStr = (NSMutableAttributedString *)[NSAttributedString attributedStringWithAttachment:attachment];
//    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:timeStr];
//    [attchStr appendAttributedString:attStr];
//    self.theRestTimeLabel.attributedText = attchStr;
    
}


@end
