//
//  AcceptedView.m
//  lll
//
//  Created by ma c on 2017/2/19.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "AcceptedView.h"

@implementation AcceptedView
{
    CGFloat _bili;
}
- (instancetype)initWithFrame:(CGRect)frame andNumber:(CGFloat)number allNumber:(CGFloat)allNumber {
    
    if (self = [super initWithFrame:frame]) {
     
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(40), KWIDTH(15))];
        label.center = CGPointMake(KWIDTH(40), KWIDTH(50 - 7.5));
        
        label.text = @"已领";
        label.font = [UIFont systemFontOfSize:10];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        
        _numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(0), KWIDTH(0), KWIDTH(40), KWIDTH(15))];
        _numberLabel.center = CGPointMake(KWIDTH(40), KWIDTH(50 + 7.5));
        _numberLabel.text = @"10张";
        _numberLabel.font = [UIFont systemFontOfSize:10];
        _numberLabel.textColor = [UIColor whiteColor];
        _numberLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_numberLabel];
        _bili = number / allNumber;
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(90), KWIDTH(60), KWIDTH(20))];
        btn.backgroundColor = [UIColor whiteColor];
        [btn setTitle:@"立即领取" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = KWIDTH(10);
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        [self addSubview:btn];
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(ctx, mRGB(180, 55, 82, 0.5).CGColor);
    CGContextSetLineWidth(ctx, KWIDTH(8));
    CGContextAddArc(ctx, KWIDTH(40), KWIDTH(50), KWIDTH(25), 0, M_PI * 2, 0);
    CGContextDrawPath(ctx, kCGPathStroke);
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(ctx, KWIDTH(4));
    NSLog(@"%f", _bili);
    CGContextAddArc(ctx, KWIDTH(40), KWIDTH(50), KWIDTH(25), - M_PI * 0.5,  M_PI * 2 * _bili - M_PI * 0.5, 0);
    CGContextDrawPath(ctx, kCGPathStroke);
    
    
    
}

@end
