//
//  FightTheGroupView.m
//  Neka1.0
//
//  Created by ma c on 2017/2/17.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "JointGroupView.h"

@implementation JointGroupView

- (instancetype)initWithFrame:(CGRect)frame andJointGroupModel:(JointGroupModel *)model {
    if (self = [super initWithFrame:frame]) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(110))];
        _bgView.image = [UIImage imageNamed:@"pt_bg"];
        
        [self addSubview:_bgView];
        
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(10), KWIDTH_6S(10), SCREEN_SIZE.width - KWIDTH_6S(30), KWIDTH_6S(30))];
        _nameLabel.text = [NSString stringWithFormat:@"%@%@", model.s_name, model.saveMoney];
        _nameLabel.font = [UIFont systemFontOfSize:KWIDTH_6S(12)];
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:_nameLabel.text];
        [att addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:KWIDTH_6S(15)] range:NSMakeRange(0, _nameLabel.text.length)];
        _nameLabel.attributedText = att;
        [self addSubview:_nameLabel];
        
        
        _retailPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(10), KWIDTH_6S(50), KWIDTH_6S(80), KWIDTH_6S(30))];
        _retailPriceLabel.text = model.old_price;
        _retailPriceLabel.font = [UIFont systemFontOfSize:KWIDTH_6S(15)];
        [self addSubview:_retailPriceLabel];
        
        
        UILabel *retailMark = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(10), KWIDTH_6S(80), KWIDTH_6S(80), KWIDTH_6S(15))];
        retailMark.text = @"单独购买原价";
        retailMark.font = [UIFont systemFontOfSize:KWIDTH_6S(12)];
        [self addSubview:retailMark];
        
        
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(100), KWIDTH_6S(50), 0.5, KWIDTH_6S(50))];
        line.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:line];
        
        _groupPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(130), KWIDTH_6S(50), KWIDTH_6S(80), KWIDTH_6S(30))];
        _groupPriceLabel.text = model.price;
        _groupPriceLabel.textColor = [UIColor redColor];
        _groupPriceLabel.font = [UIFont systemFontOfSize:KWIDTH_6S(15)];
        [self addSubview:_groupPriceLabel];
        
        
        UILabel *groupMark = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(130), KWIDTH_6S(80), KWIDTH_6S(80), KWIDTH_6S(15))];
        groupMark.text = @"团购价";
        groupMark.textColor = [UIColor redColor];
        groupMark.font = [UIFont systemFontOfSize:KWIDTH_6S(12)];
        [self addSubview:groupMark];
        
        UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH_6S(210), KWIDTH_6S(60), KWIDTH_6S(90), KWIDTH_6S(20))];
        bg.image = [UIImage imageNamed:@"start_bg"];
        [self addSubview:bg];
        
        
        NSString *str = [NSString stringWithFormat:@"%@人起团", model.pin_num];
        _startLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, KWIDTH_6S(90), KWIDTH_6S(20))];
        
        _startLabel.text = str;
        _startLabel.font = [UIFont systemFontOfSize:KWIDTH_6S(12)];
        _startLabel.textColor = [UIColor whiteColor];
        _startLabel.textAlignment =NSTextAlignmentCenter;
        [bg addSubview:_startLabel];
        
        
        
    }
    return self;
}



@end
