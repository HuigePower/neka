//
//  CouponCell.h
//  lll
//
//  Created by ma c on 2017/2/19.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponModel.h"

typedef void(^CouponCellExpandBlock)(NSIndexPath *indexPath);
@interface CouponCell : UITableViewCell

@property (nonatomic, strong)UIButton *topView;
@property (nonatomic, strong)UIView *bottomView;

@property (nonatomic, strong)UIImageView *markImageView;
@property (nonatomic, strong)UIImageView *couponImageView;
@property (nonatomic, strong)UILabel *contentLabel;
@property (nonatomic, strong)UILabel *describeLabel;
@property (nonatomic, strong)UILabel *acceptedNumberLabel;
@property (nonatomic, strong)UIImageView *arrowView;

@property (nonatomic, strong)CouponModel *model;
@property (nonatomic, strong)NSIndexPath *indexPath;
@property (nonatomic, copy)CouponCellExpandBlock expendBlock;



- (void)makeSubviewsWithData:(CouponModel *)model andIndexPath:(NSIndexPath *)indexPath;



@end
