//
//  SnatchCell.h
//  Neka
//
//  Created by yu on 2017/12/8.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGActivityModel.h"
@interface SnatchCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *requestNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *theRestTimeLabel;





@property (nonatomic, strong)HGActivityModel *model;

- (void)setModel:(HGActivityModel *)model andEndTime:(NSTimeInterval)endTime;
@end
