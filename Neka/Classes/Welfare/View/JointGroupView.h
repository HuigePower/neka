//
//  FightTheGroupView.h
//  Neka1.0
//
//  Created by ma c on 2017/2/17.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JointGroupModel.h"
@interface JointGroupView : UIView

@property (nonatomic, strong)UIImageView *bgView;
@property (nonatomic, strong)UILabel *nameLabel;
@property (nonatomic, strong)UILabel *retailPriceLabel;
//@property (nonatomic, strong)UILabel *singleMarkLabel;
@property (nonatomic, strong)UILabel *groupPriceLabel;
//@property (nonatomic, strong)UILabel *groupMarkLabel;
@property (nonatomic, strong)UILabel *startLabel;


- (instancetype)initWithFrame:(CGRect)frame andJointGroupModel:(JointGroupModel *)model;
@end
