//
//  SnatchView.m
//  Neka1.0
//
//  Created by ma c on 16/9/6.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "SnatchView.h"


@interface SnatchView ()
@property (nonatomic, strong)UIImageView *productImage;
@property (nonatomic, strong)UILabel *timeLabel;
@property (nonatomic, strong)UILabel *productLabel;

@property (nonatomic) NSTimer *timer;
// 用于展示的秒， 分钟， 小时
@property (nonatomic, assign)int hour;
@property (nonatomic, assign)int minute;
@property (nonatomic, assign)int second;

@property (nonatomic, assign)NSTimeInterval countDownTimeInterval;
@property (nonatomic, assign)NSTimeInterval endTime;
@end

#define WIDTH self.frame.size.width
@implementation SnatchView

- (instancetype)initWithFrame:(CGRect)frame andImageName:(NSString *)imageName countdown:(NSDate *)date productName:(NSString *)productName {
    if (self = [super initWithFrame:frame]) {
        [self countdown];
        
        _productImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(190), KWIDTH(120))];
        _productImage.image = [UIImage imageNamed:@"1.jpg"];
        [self addSubview:self.productImage];
        
        
        _productLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(190) + 10, 0, KWIDTH(130) - 20, KWIDTH(30))];
        _productLabel.text = @"宝马行车记录仪";
        _productLabel.numberOfLines = 2;
        _productLabel.font = [UIFont systemFontOfSize:16];
        _productLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:self.productLabel];

        
        UILabel *limitNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(190) + 10, KWIDTH(30), KWIDTH(130) - 20, KWIDTH(30))];
        limitNumberLabel.text = @"总共需要1200人";
        limitNumberLabel.font = [UIFont systemFontOfSize:14];
        limitNumberLabel.textColor = [UIColor blackColor];
        [self addSubview:limitNumberLabel];
        
        //参与人数
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(190) + 10, KWIDTH(60), KWIDTH(130) - 20, KWIDTH(30))];
        label.text = @"参与: 0 人";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        label.textColor = [UIColor redColor];
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:label.text];
        [att addAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]} range:NSMakeRange(label.text.length - 1, 1)];
        label.attributedText = att;
        [self addSubview:label];
        
        
        //倒计时
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(190) + 10, KWIDTH(90), KWIDTH(130) - 20, KWIDTH(30))];
        _timeLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
        _timeLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_timeLabel];
        
        
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame andActivityData:(HGActivityModel *)model endTime:(NSTimeInterval)endTime {
    if (self = [super initWithFrame:frame]) {
        _endTime = endTime;
        NSLog(@"end:%f", _endTime);
        [self countdown];
        
        _productImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(190), KWIDTH(120))];
        [_productImage setImageWithURL:[NSURL URLWithString:model.list_pic]];
        [self addSubview:self.productImage];
        
        
        _productLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(190) + 10, 0, KWIDTH(130) - 20, KWIDTH(30))];
        _productLabel.text = model.title;
        _productLabel.numberOfLines = 2;
        _productLabel.font = [UIFont systemFontOfSize:16];
        _productLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:self.productLabel];
        
        
        UILabel *limitNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(190) + 10, KWIDTH(30), KWIDTH(130) - 20, KWIDTH(30))];
        limitNumberLabel.text = [NSString stringWithFormat:@"总共需要%@人", model.all_count];
        limitNumberLabel.font = [UIFont systemFontOfSize:14];
        limitNumberLabel.textColor = [UIColor blackColor];
        [self addSubview:limitNumberLabel];
        
        //参与人数
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(190) + 10, KWIDTH(60), KWIDTH(130) - 20, KWIDTH(30))];
        label.text = [NSString stringWithFormat:@"参与: %@ 人", model.part_count];
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        label.textColor = [UIColor redColor];
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:label.text];
        [att addAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]} range:NSMakeRange(label.text.length - 1, 1)];
        label.attributedText = att;
        [self addSubview:label];
        
        
        //倒计时
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(190) + 10, KWIDTH(90), KWIDTH(130) - 20, KWIDTH(30))];
        _timeLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
        _timeLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_timeLabel];
        
        
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
        
        
    }
    return self;
}

- (void)countdown {
    
    NSTimeInterval nowTime = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval timeInt = _endTime - nowTime;
    int day = (int)timeInt / (3600 * 24);
    int hour = ((int)timeInt - day * 3600 * 24) / 3600;
    int minute = ((int)timeInt - day * 3600 * 24 - hour * 3600) / 60;
    int second = ((int)timeInt) % 60;
    NSString *timeStr = [NSString stringWithFormat:@" %d天 %.02d:%.02d:%.02d ", day, hour, minute, second];
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:@"clock"];
    attachment.bounds = CGRectMake(0, - KWIDTH(2.5), KWIDTH(15), KWIDTH(15));
    NSMutableAttributedString *attchStr = (NSMutableAttributedString *)[NSAttributedString attributedStringWithAttachment:attachment];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:timeStr];
    [attchStr appendAttributedString:attStr];
    self.timeLabel.attributedText = attchStr;

}


@end
