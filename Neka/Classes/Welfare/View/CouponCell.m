//
//  CouponCell.m
//  lll
//
//  Created by ma c on 2017/2/19.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "CouponCell.h"
#import "AcceptedView.h"

@implementation CouponCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.topView];
        [self addSubview:self.bottomView];
        

    }
    return  self;
}

- (void)makeSubviewsWithData:(CouponModel *)model andIndexPath:(NSIndexPath *)indexPath {
    _model = model;
    _indexPath = indexPath;
    [self makeBottomView];
}


- (void)makeBottomView {
    
    if (_model.isShow) {
        self.bottomView.frame = CGRectMake(KWIDTH(10), KWIDTH(146), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(59));
        self.frame = CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(KWIDTH(205)));
        _arrowView.image = [UIImage imageNamed:@"coupon_arrow_up"];
    }else {
        self.bottomView.frame = CGRectMake(KWIDTH(10), KWIDTH(146), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(29));
        self.frame = CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(KWIDTH(185)));
        _arrowView.image = [UIImage imageNamed:@"coupon_arrow_down"];
    }
    
    if (_model.isCan) {
        NSLog(@"%@", _model.ID);
        _arrowView.transform = CGAffineTransformMakeRotation(0);
        if (_model.isShow) {
            _arrowView.image = [UIImage imageNamed:@"coupon_arrow_down"];
            [UIView animateWithDuration:0.5 animations:^{
                _arrowView.transform = CGAffineTransformMakeRotation(- M_PI * 0.999);
            } completion:^(BOOL finished) {
                _model.isCan = NO;
            }];
        }else {
            _arrowView.image = [UIImage imageNamed:@"coupon_arrow_up"];
            [UIView animateWithDuration:0.5 animations:^{
                _arrowView.transform = CGAffineTransformMakeRotation(M_PI);
            } completion:^(BOOL finished) {
                _model.isCan = NO;
            }];
            
        }
    }
}


- (UIView *)topView {
    if (_topView == nil) {
        _topView = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(135))];
        _topView.backgroundColor = [UIColor whiteColor];
        _topView.layer.masksToBounds = YES;
        _topView.layer.cornerRadius = 5;
        _topView.clipsToBounds = YES;
        
        _couponImageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(15), KWIDTH(60), KWIDTH(60))];
        _couponImageView.layer.masksToBounds = YES;
        _couponImageView.layer.cornerRadius = 5;
        _couponImageView.image = [UIImage imageNamed:@"3"];
        [_topView addSubview:_couponImageView];
        
        _markImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(35), KWIDTH(35))];
        _markImageView.image = [UIImage imageNamed:@"coupon_mark"];
        [_topView addSubview:_markImageView];
        
        UIImageView *imaginaryView = [[UIImageView alloc] initWithFrame:CGRectMake(0, KWIDTH(85), KWIDTH(210), 5)];
        imaginaryView.image = [UIImage imageNamed:@"imaginary_line"];
        [_topView addSubview:imaginaryView];
        
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(75), KWIDTH(15), KWIDTH(130), KWIDTH(60))];
        _contentLabel.numberOfLines = 0;
        _contentLabel.font = [UIFont systemFontOfSize:12];
        _contentLabel.textColor = mRGB(255, 50, 102, 1);
        _contentLabel.text = @"我回家维护好直辖市现实的";
        [_contentLabel sizeToFit];
        [_topView addSubview:_contentLabel];
        
        _describeLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(90), KWIDTH(190), KWIDTH(45))];
        _describeLabel.numberOfLines = 2;
        _describeLabel.text = @"2017年02月12日至2021年02月12日,适用平台:移动网页/App/微信";
        _describeLabel.font = [UIFont systemFontOfSize:12];
        [_topView addSubview:_describeLabel];
        
        
        UIImageView *lineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(210), 0, KWIDTH(10), KWIDTH(135))];
        lineImageView.image = [UIImage imageNamed:@"coupon_line.jpg"];
        [_topView addSubview:lineImageView];
        AcceptedView *view = [[AcceptedView alloc] initWithFrame:CGRectMake(KWIDTH(220), 0, KWIDTH(80), KWIDTH(135)) andNumber:10 allNumber:35];
        view.backgroundColor =  mRGB(255, 53, 98, 1);
        [_topView addSubview:view];
        
    }
    return _topView;
}


- (UIView *)bottomView {
    
    if (_bottomView == nil) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(146), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(29))];
        _bottomView.backgroundColor = [UIColor whiteColor];
        _bottomView.layer.masksToBounds = YES;
        _bottomView.layer.cornerRadius = 5;
        _bottomView.clipsToBounds = YES;
        
        _arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(55), 0, KWIDTH(30), KWIDTH(30))];
        _arrowView.contentMode = UIViewContentModeCenter;
        [_bottomView addSubview:_arrowView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 0, _bottomView.frame.size.width - KWIDTH(20), KWIDTH(30))];
        label.userInteractionEnabled = YES;
        label.text = @"使用说明";
        label.font = [UIFont systemFontOfSize:12];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [label addGestureRecognizer:tap];
        [_bottomView addSubview:label];
        
        
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(30), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(30))];
        label1.text = @"111222333444";
        [_bottomView addSubview:label1];
        
    }
    return _bottomView;
}

- (void)btnClick:(UIButton *)btn {
    NSLog(@"123");
}

- (void)tapAction {
    NSLog(@"111222");
    if (_expendBlock) {
        self.expendBlock(_indexPath);
    }
}







- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
