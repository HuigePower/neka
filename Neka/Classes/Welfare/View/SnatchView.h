//
//  SnatchView.h
//  Neka1.0
//
//  Created by ma c on 16/9/6.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGActivityModel.h"


@interface SnatchView : UIButton
- (instancetype)initWithFrame:(CGRect)frame andImageName:(NSString *)imageName countdown:(NSDate *)date productName:(NSString *)productName;

- (instancetype)initWithFrame:(CGRect)frame andActivityData:(HGActivityModel *)model endTime:(NSTimeInterval)endTime;


@end
