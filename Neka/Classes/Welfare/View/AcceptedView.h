//
//  AcceptedView.h
//  lll
//
//  Created by ma c on 2017/2/19.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AcceptedView : UIView
@property (nonatomic, strong)UILabel *numberLabel;
- (instancetype)initWithFrame:(CGRect)frame andNumber:(CGFloat)number allNumber:(CGFloat)allNumber;
@end
