//
//  CouponViewController.m
//  Neka1.0
//
//  Created by ma c on 2017/2/17.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "CouponViewController.h"
#import "CouponSeletionScrollView.h"
#import "CouponModel.h"
#import "CouponCell.h"
@interface CouponViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSMutableArray *data;
@end

@implementation CouponViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tabBarController.tabBar setHidden:YES];
    [self initDataSource];
    self.title = @"平台优惠券";
    self.view.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    self.automaticallyAdjustsScrollViewInsets = NO;
    CouponSeletionScrollView *couponSelection = [[CouponSeletionScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(44))];
    [self.view addSubview:couponSelection];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0 + KWIDTH(44), SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - KWIDTH(44)) style:UITableViewStyleGrouped];
    _tableView.delegate =  self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableView registerClass:[CouponCell class] forCellReuseIdentifier:@"cell"];
    
}




- (void)initDataSource {
    _data = [NSMutableArray array];
    for (int i = 0; i < 8; i++) {
        CouponModel *model = [CouponModel new];
        if (i % 2 == 0) {
            model.isShow = YES;
        }else {
            model.isShow = NO;
        }
        model.ID = [NSString stringWithFormat:@"%d", i];
        model.isCan = NO;
        [_data addObject:model];
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _data.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    CouponCell *cell = (CouponCell *)[tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell makeSubviewsWithData:_data[indexPath.row] andIndexPath:indexPath];
    cell.expendBlock = ^(NSIndexPath *indexPath) {
        CouponModel *model = _data[indexPath.row];
        model.isShow = !model.isShow;
        model.isCan = YES;
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    };
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CouponModel *model = _data[indexPath.row];
    
    return model.isShow? KWIDTH(205) : KWIDTH(185);
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(5);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return KWIDTH(5);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
