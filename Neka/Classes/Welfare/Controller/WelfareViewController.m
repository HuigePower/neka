//
//  WelfareViewController.m
//  Neka1.0
//
//  Created by ma c on 2017/2/16.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "WelfareViewController.h"
#import "CustomTableViewCell.h"
#import "SnatchView.h"
#import "CustomTableViewCell.h"
#import "JointGroupModel.h"
#import "JointGroupView.h"
#import "CouponViewController.h"
#import "HGActivityModel.h"
#import "HGOrderWebViewController.h"
#import "WelfareHeaderView.h"

@interface WelfareViewController ()<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong)NSMutableArray *groupArray;



@property (nonatomic, strong)UIScrollView *scrollView;


//下拉刷新
@property (nonatomic, strong)MJRefreshNormalHeader *header;
//上拉加载
@property (nonatomic, strong)MJRefreshBackNormalFooter *footer;

@property (nonatomic, strong)WelfareHeaderView *sectionHeaderView;
@end

@implementation WelfareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"福利";
    
    
    _sectionHeaderView = [[WelfareHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(80) + 125) andNav:self.navigationController];
    
    [self requestActivityList];
    [self requestGroupList];

    [_tableView registerClass:[CustomTableViewCell class] forCellReuseIdentifier:@"cell"];
    
    _header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refresh:)];
    _tableView.mj_header = _header;
    
    _footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerAction:)];
    _tableView.mj_footer = _footer;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)refresh:(MJRefreshNormalHeader *)header {
    [self requestActivityList];
}

- (void)footerAction:(MJRefreshBackNormalFooter *)footer {
    [self requestGroupList];
}


- (void)requestActivityList {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID};
    
    //一元夺宝
    [self hideLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Welfare&a=activity", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            [_sectionHeaderView setActivityData:responseObject[@"result"]];
            [_tableView reloadData];
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
        [_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [_header endRefreshing];
        [MBProgressHUD showError:error.description];
    }];

}

- (void)requestGroupList {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID};
    
    //团购
    _groupArray = [NSMutableArray new];
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Welfare&a=groupInfo", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        
        [self hideLoading];
        if ([errorCode isEqualToString:@"0"]) {
            NSArray *arr = responseObject[@"result"][@"group_products"];
          
            for (NSDictionary *dic in arr) {
                JointGroupModel *model = [[JointGroupModel alloc] initWithDict:dic];
                [_groupArray addObject:model];
            }
            [_tableView reloadData];
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
        [_footer endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [_footer endRefreshing];
        [MBProgressHUD showError:error.description];
    }];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _groupArray.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell) {
        [cell deleteSubviews];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            JointGroupModel *model = _groupArray[indexPath.section - 1];
            JointGroupView *view = [[JointGroupView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 110) andJointGroupModel:model];
            [cell addCustomSubview:view];
            
        }else {
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 0.5)];
            line.backgroundColor = [UIColor lightGrayColor];
            [cell addCustomSubview:line];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(20), KWIDTH_6S(10), SCREEN_SIZE.width - KWIDTH_6S(40), KWIDTH_6S(40))];
            label.text = @"马上抢";
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = [UIColor whiteColor];
            label.backgroundColor = NK_Custom_Blue;
            label.layer.masksToBounds = YES;
            label.layer.cornerRadius = 5;
            [cell addCustomSubview:label];
            cell.backgroundColor = mRGB(242, 240, 248, 1);
        }
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 0.1;
    }else {
        if (indexPath.row == 0) {
            return KWIDTH_6S(110);
        }else {
            return KWIDTH_6S(60);
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return _sectionHeaderView.frame.size.height;
    }
    return 5;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return _sectionHeaderView;
    }
    return nil;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
        web.urlString = @"https://www.nekahome.com/wap.php?g=Wap&c=Systemcoupon&a=index";
        [self.navigationController pushViewController:web animated:YES];
    }else {
        if (indexPath.row == 1) {
            JointGroupModel *model = _groupArray[indexPath.section - 1];
            HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
            web.urlString = model.url;
            [self.navigationController pushViewController:web animated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
