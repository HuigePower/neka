//
//  HGActivityModel.m
//  Neka
//
//  Created by ma c on 2017/7/14.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGActivityModel.h"

@implementation HGActivityModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}


@end
