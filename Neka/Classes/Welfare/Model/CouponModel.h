//
//  Model.h
//  ZZZ
//
//  Created by ma c on 2017/2/20.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CouponModel : NSObject

@property (nonatomic, copy)NSString *ID;
@property (nonatomic, assign)BOOL isShow;
@property (nonatomic, assign)BOOL isCan;

@end
