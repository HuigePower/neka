//
//  JointGroupModel.m
//  Neka1.0
//
//  Created by ma c on 2017/2/17.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "JointGroupModel.h"

@implementation JointGroupModel



- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        if (dict != nil) {
            
            _s_name = [NSString stringWithFormat:@"%@", dict[@"s_name"]];
            _old_price = [NSString stringWithFormat:@"%@", dict[@"old_price"]];
            _price = [NSString stringWithFormat:@"%@", dict[@"price"]];
            _pin_num = [NSString stringWithFormat:@"%@", dict[@"pin_num"]];
            _saveMoney = [NSString stringWithFormat:@"(省%.2f元)", [_old_price floatValue] - [_price floatValue]];
            _url = [NSString stringWithFormat:@"%@", dict[@"url"]];
        }
        
    }
    return self;
}


@end
