//
//  JointGroupModel.h
//  Neka1.0
//
//  Created by ma c on 2017/2/17.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JointGroupModel : NSObject
@property (nonatomic, copy)NSString *s_name;
@property (nonatomic, copy)NSString *old_price;
@property (nonatomic, copy)NSString *price;
@property (nonatomic, copy)NSString *pin_num;
@property (nonatomic, copy)NSString *saveMoney;
@property (nonatomic, copy)NSString *url;


- (instancetype)initWithDict:(NSDictionary *)dict;

@end
