//
//  HGActivityModel.h
//  Neka
//
//  Created by ma c on 2017/7/14.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGActivityModel : NSObject
@property (nonatomic, copy)NSString *pigcms_id;
@property (nonatomic, copy)NSString *name;
@property (nonatomic, copy)NSString *title;
@property (nonatomic, copy)NSString *pic;
@property (nonatomic, copy)NSString *all_count;
@property (nonatomic, copy)NSString *part_count;
@property (nonatomic, copy)NSString *price;
@property (nonatomic, copy)NSString *mer_score;
@property (nonatomic, copy)NSString *type;
@property (nonatomic, copy)NSString *list_pic;
@property (nonatomic, copy)NSString *url;


- (instancetype)initWithDict:(NSDictionary *)dict;
@end
