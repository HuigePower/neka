//
//  HGUserDataManager.m
//  Neka
//
//  Created by ma c on 2017/6/16.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGUserDataManager.h"

@implementation HGUserDataManager

static HGUserDataManager *_userDataManager;
+ (HGUserDataManager *)shareManager {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _userDataManager = [[HGUserDataManager alloc] init];
    });
    return _userDataManager;
}

- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)saveUserData:(NSDictionary *)userData {

    [self setAvatar:userData[@"avatar"]];
    [self setBg:userData[@"bg"]];
    [self setLevel:userData[@"level"]];
    [self setNickname:userData[@"nickname"]];
    [self setNow_money:userData[@"now_money"]];
    [self setPhone:userData[@"phone"]];
    [self setPhone_s:userData[@"phone_s"]];
    [self setScore_count:[NSString stringWithFormat:@"%@", userData[@"score_count"]]];
    
    
}

#pragma mark -- set


/**
 @param avatar 头像URL
 */
- (void)setAvatar:(NSString *)avatar {
    [[NSUserDefaults standardUserDefaults] setObject:avatar forKey:@"avatar"];
   
}

/**
 @param avatar 头像背景URL
 */
- (void)setBg:(NSString *)bg {
    [[NSUserDefaults standardUserDefaults] setObject:bg forKey:@"bg"];
    
}

/**
 @param avatar 用户等级
 */
- (void)setLevel:(NSString *)level {
    if (level == NULL) {
        level = @"0";
    }
    [[NSUserDefaults standardUserDefaults] setObject:level forKey:@"level"];
    
}

/**
 @param avatar 昵称
 */
- (void)setNickname:(NSString *)nickname {
    [[NSUserDefaults standardUserDefaults] setObject:nickname forKey:@"nickname"];
    
}

/**
 @param avatar 余额
 */
- (void)setNow_money:(NSString *)now_money {
    [[NSUserDefaults standardUserDefaults] setObject:now_money forKey:@"now_money"];
   
}

/**
 @param avatar 手机号
 */
- (void)setPhone:(NSString *)phone {
    [[NSUserDefaults standardUserDefaults] setObject:phone forKey:@"phone"];
    
}

/**
 @param avatar 手机号(隐藏中间4位数)
 */
- (void)setPhone_s:(NSString *)phone_s {
    [[NSUserDefaults standardUserDefaults] setObject:phone_s forKey:@"phone_s"];
    
}

/**
 @param avatar 积分
 */
- (void)setScore_count:(NSString *)score_count {
    [[NSUserDefaults standardUserDefaults] setObject:score_count forKey:@"score_count"];
}

#pragma mark -- get


- (NSString *)avatar {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"avatar"]?:@"";
}


- (NSString *)bg {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"bg"]?:@"";
}

- (NSString *)level {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"level"]?:@"";
}

- (NSString *)nickname {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"nickname"]?:@"";
}

- (NSString *)now_money {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"now_money"]?:@"--";
}

- (NSString *)phone {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"]?:@"";
}

- (NSString *)phone_s {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"phone_s"]?:@"";
}

- (NSString *)score_count {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"score_count"]?:@"--";
}





@end
