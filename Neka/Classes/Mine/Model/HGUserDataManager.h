//
//  HGUserDataManager.h
//  Neka
//
//  Created by ma c on 2017/6/16.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGUserDataManager : NSObject

@property (nonatomic, copy)NSString *avatar;
@property (nonatomic, copy)NSString *bg;
@property (nonatomic, copy)NSString *level;
@property (nonatomic, copy)NSString *nickname;
@property (nonatomic, copy)NSString *now_money;
@property (nonatomic, copy)NSString *phone;
@property (nonatomic, copy)NSString *phone_s;
@property (nonatomic, copy)NSString *score_count;




+ (HGUserDataManager *)shareManager;
- (void)saveUserData:(NSDictionary *)userData;


@end
