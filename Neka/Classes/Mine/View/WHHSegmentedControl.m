//
//  WHHSegControl.m
//  Neka1.0
//
//  Created by ma c on 16/8/20.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "WHHSegmentedControl.h"

@interface WHHSegmentedControl ()
@property (nonatomic, strong)NSArray *items;
@property (nonatomic, strong)NSMutableArray *btnArr;

@end
@implementation WHHSegmentedControl

- (instancetype)initWithFrame:(CGRect)frame andItems:(NSArray *)items {
    if (self = [super initWithFrame:frame]) {
        [self setItems:items];
        
        
    }
    return self;
}

- (void)setItems:(NSArray *)items {
    
    _btnArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < items.count; i++) {
        UIButton *itemBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width / items.count * i, 0, self.frame.size.width / items.count, self.frame.size.height)];
        [itemBtn setTitle:items[i] forState:UIControlStateNormal];
        itemBtn.tag = 10 + i;
        [itemBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:itemBtn];
        [_btnArr addObject:itemBtn];
        
    }
    [self setBtnSelected:10];
    for (int i = 1; i < items.count; i++) {
        UILabel *cuttingLine = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width / items.count * i - 0.5, 0, 1, self.frame.size.height)];
        cuttingLine.backgroundColor = [UIColor grayColor];
        [self addSubview:cuttingLine];
    }
    
    UILabel *bottomLine = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 1, self.frame.size.width, 1)];
    bottomLine.backgroundColor = [UIColor grayColor];
    [self addSubview:bottomLine];
    

}


- (void)btnClick:(UIButton *)btn {
    [self setBtnSelected:btn.tag];
    if (_resultBlock) {
        self.resultBlock(btn.tag - 10);
    }
}

- (void)setBtnSelected:(NSInteger)tag {
    for (UIButton *itemBtn in _btnArr) {
        if (itemBtn.tag == tag) {
            itemBtn.backgroundColor = [UIColor grayColor];
            [itemBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else {
            itemBtn.backgroundColor = [UIColor whiteColor];
            [itemBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
    }
}









@end
