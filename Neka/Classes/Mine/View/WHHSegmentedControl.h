//
//  WHHSegControl.h
//  Neka1.0
//
//  Created by ma c on 16/8/20.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^SelectedResult)(NSInteger index);
@interface WHHSegmentedControl : UIView
- (instancetype)initWithFrame:(CGRect)frame andItems:(NSArray *)items;
- (void)setBtnSelected:(NSInteger)tag;

@property (nonatomic, copy)SelectedResult resultBlock;
@end
