//
//  HGLoginView.m
//  Neka
//
//  Created by ma c on 2017/6/14.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGLoginView.h"
#import "WXApi.h"
#import "HGWebAgentSetting.h"
#import "JPUSHService.h"
@implementation HGLoginView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
        
        
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        scrollView.delegate = self;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height + 30);
        [self addSubview:scrollView];
        
        
        UIView *view = [[UIButton alloc] initWithFrame:CGRectMake(0, 40, SCREEN_SIZE.width, 88)];
        view.backgroundColor = [UIColor whiteColor];
        view.layer.borderColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0].CGColor;
        view.layer.borderWidth = 0.5f;
        [scrollView addSubview:view];
        
        _account = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(20), 0, SCREEN_SIZE.width - KWIDTH(40), 44)];
        _account.delegate = self;
        _account.placeholder = @"手机号";
        _account.keyboardType = UIKeyboardTypeNumberPad;
        [view addSubview:_account];
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), 44, SCREEN_SIZE.width - KWIDTH(20), 0.5)];
        line.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
        [view addSubview:line];
        
        _password = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(20), 44, SCREEN_SIZE.width - KWIDTH(40), 44)];
        _password.delegate = self;
        _password.placeholder = @"请输入您的密码";
        _password.secureTextEntry = YES;
        _password.keyboardType = UIKeyboardTypeDefault;
        
        [view addSubview:_password];
        
        UIButton *login = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(20), view.frame.origin.y + view.frame.size.height + 25, SCREEN_SIZE.width - KWIDTH(40), 44)];
        login.backgroundColor = mRGB(0, 126, 222, 1);
        login.layer.masksToBounds = YES;
        login.layer.cornerRadius = 5;
        [login setTitle:@"登录" forState:UIControlStateNormal];
        [login addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:login];
        
        //立即注册
        UIButton *registerBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(20), login.frame.origin.y + login.frame.size.height + 10, KWIDTH(60), KWIDTH(20))];
        registerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [registerBtn setTitle:@"立即注册" forState:UIControlStateNormal];
        registerBtn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
        [registerBtn setTitleColor:mRGB(0, 126, 222, 1) forState:UIControlStateNormal];
        [registerBtn addTarget:self action:@selector(gotoRegister) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:registerBtn];
        
        //立即注册
        UIButton *findPasswordBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(20) - KWIDTH(60), login.frame.origin.y + login.frame.size.height + 10, KWIDTH(60), KWIDTH(20))];
        findPasswordBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [findPasswordBtn setTitle:@"忘记密码" forState:UIControlStateNormal];
        findPasswordBtn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
        [findPasswordBtn setTitleColor:mRGB(0, 126, 222, 1) forState:UIControlStateNormal];
        [findPasswordBtn addTarget:self action:@selector(findPassword) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:findPasswordBtn];
        
        
        if ([WXApi isWXAppInstalled]) {
            
            UIView *leftLine = [[UIView alloc] initWithFrame:CGRectMake(KWIDTH(20), frame.size.height - 100, SCREEN_SIZE.width / 2 - KWIDTH(20) - 50, 0.5)];
            leftLine.backgroundColor = [UIColor lightGrayColor];
            [scrollView addSubview:leftLine];
            
            
            UILabel *thirdLoginLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 2 - 50, frame.size.height - 110, 100, 20)];
            thirdLoginLabel.text = @"第三方登录";
            thirdLoginLabel.font = [UIFont systemFontOfSize:14];
            thirdLoginLabel.textAlignment = NSTextAlignmentCenter;
            [scrollView addSubview:thirdLoginLabel];
            
            UIView *rightLine = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 2 + 50, frame.size.height - 100, SCREEN_SIZE.width / 2 - KWIDTH(20) - 50, 0.5)];
            rightLine.backgroundColor = [UIColor lightGrayColor];\
            [scrollView addSubview:rightLine];
            
            
            UIButton *wxLoginBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 2 - 25, frame.size.height - 80, 50, 50)];
            [wxLoginBtn setImage:[UIImage imageNamed:@"wx_logo_64"] forState:UIControlStateNormal];
            [wxLoginBtn addTarget:self action:@selector(wxLogin:) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:wxLoginBtn];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxLoginSuccess:) name:@"WeixinLoginSuccess" object:nil];
        }else {
            
            NSLog(@"未安装微信");
            
        }
        
    }
    return self;
}

- (void)wxLoginSuccess:(NSNotification *)noti {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    NSLog(@"noti:%@", noti.object);
    NSDictionary *data = noti.object;
    NSString *errorCode = [NSString stringWithFormat:@"%@", data[@"errorCode"]];
    NSLog(@"errorCode:%@", errorCode);
    if ([errorCode isEqualToString:@"0"]) {
        NSLog(@"loginSuccess:%@", data);
        [hud hideAnimated:YES];
        [MBProgressHUD showSuccess:@"登录成功" toView:self];
        
        NSDictionary *result = data[@"result"];
        if (result[@"ticket"]) {
            [UserDefaultManager saveTicket:result[@"ticket"]];
        }
        if (_loginSucceedBlock) {
            [HGWebAgentSetting updateWebAgentSetting];
            _loginSucceedBlock();
        }
        
    }else {
        [hud hideAnimated:YES];
        NSLog(@"%@", data[@"errorMsg"]);
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _account) {
        if ([_account.text stringByAppendingString:string].length > 11) {
            return NO;
        }
    }else if (textField == _password) {
        if ([_password.text stringByAppendingString:string].length > 18) {
            return NO;
        }
    }
    
    return YES;
}


- (void)login {
    [self endEditing:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    UIDevice *device = [[UIDevice alloc] init];
    NSString *identifier = [device.identifierForVendor UUIDString];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSDictionary *data = @{@"Device-Id": identifier,
                           @"phone": _account.text,
                           @"passwd": _password.text,
                           @"client": @"1"};
    
    [manager POST:[NSString stringWithFormat:@"%@c=Login&a=login", BASE_URL] parameters:data progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        NSLog(@"errorCode:%@", errorCode);
        if ([errorCode isEqualToString:@"0"]) {
            NSLog(@"登录成功:%@", responseObject);
            [hud hideAnimated:YES];
            [MBProgressHUD showSuccess:@"登录成功"];
            
            NSDictionary *result = responseObject[@"result"];
            if (result[@"ticket"]) {
                [UserDefaultManager saveTicket:result[@"ticket"]];
                //给用户设置别名
                [JPUSHService setTags:nil alias:result[@"user"][@"uid"] fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias) {
                    NSLog(@"%d--%@--%@", iResCode, iTags, iAlias);
                }];
            }
            
            if (_loginSucceedBlock) {
                [HGWebAgentSetting updateWebAgentSetting];
                _loginSucceedBlock();
            }
            
        }else {
            [hud hideAnimated:YES];
            NSLog(@"%@", responseObject[@"errorMsg"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [hud hideAnimated:YES];
        NSLog(@"%@", error);
    }];
    
    
    
    
    
    [UIView animateWithDuration:1 delay:5 options:UIViewAnimationOptionCurveLinear animations:^{
        
    } completion:^(BOOL finished) {
        
    }];
    
}

- (void)gotoRegister {
    if (_startRegisterBlock) {
        _startRegisterBlock();
    }
    
    
    
    
}


- (void)findPassword {
    
    if (_findPasswordBlock) {
        _findPasswordBlock();
    }
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [self endEditing:YES];
    
}


- (void)wxLogin:(UIButton *)btn {
    SendAuthReq *req = [[SendAuthReq alloc] init];
    req.scope = @"snsapi_userinfo";
    req.state = @"123";
    
    [WXApi sendReq:req];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"WeixinLoginSuccess" object:nil];
}

@end
