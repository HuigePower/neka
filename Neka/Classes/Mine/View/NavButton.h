//
//  NavButton.h
//  NEKA
//
//  Created by ma c on 16/4/22.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavButton : UIButton

@property (nonatomic, strong)UIView *view;
+ (id)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)color selectedColor:(UIColor *)selectedColor;

@end
