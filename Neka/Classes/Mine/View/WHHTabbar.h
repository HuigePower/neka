//
//  WHHTabbar.h
//  Neka1.0
//
//  Created by ma c on 16/8/21.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^SelectedResult)(NSInteger index);
@interface WHHTabbar : UIView

@property (nonatomic, strong)NSMutableArray *btnArr;
- (instancetype)initTopTabbarWithFrame:(CGRect)frame andTitles:(NSArray *)titles imageNames:(NSArray *)imageNames selectedImageNames:(NSArray *)selectedImageNames;
- (void)changeSelectBtnWithIndex:(NSInteger)index;

@property (nonatomic, copy)SelectedResult selectedBlock;
@end
