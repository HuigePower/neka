//
//  HGVerificationButton.h
//  Neka
//
//  Created by ma c on 2017/7/29.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGVerificationButton : UIButton

- (void)startTiming;

@end

