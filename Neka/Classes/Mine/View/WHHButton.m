//
//  WHHButton.m
//  Neka1.0
//
//  Created by ma c on 16/8/20.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "WHHButton.h"
#import "UIImageView+AFNetworking.h"
@implementation WHHButton
{
    NSString *_imageName;
    NSString *_selectedImageName;
}

- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title image:(NSString *)imageName selectedImage:(NSString *)selectedImageName {
    if (self = [super initWithFrame:frame]) {
        self.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        self.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [self setTitle:title forState:UIControlStateNormal];
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
        [self setTitleColor:[UIColor colorWithRed:0.9882 green:0.2902 blue:0.4824 alpha:1.0] forState:UIControlStateSelected];
        
        _imageName = imageName;
        _selectedImageName = selectedImageName;
        [self setImage:[UIImage imageNamed:_imageName] forState:UIControlStateNormal];
        [self setImage:[UIImage imageNamed:_selectedImageName] forState:UIControlStateHighlighted];
        [self setImage:[UIImage imageNamed:_selectedImageName] forState:UIControlStateSelected];
        self.imageEdgeInsets = UIEdgeInsetsMake(KWIDTH(5), frame.size.width / 2 - KWIDTH(15), 0, 0);
        self.titleEdgeInsets = UIEdgeInsetsMake(KWIDTH(5) + self.imageView.frame.size.height, - self.imageView.frame.size.width + KWIDTH(5), 0, 0);

    }
    return self;
}



- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title number:(NSString *)num {
    if (self = [super initWithFrame:frame]) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH(10), frame.size.width, KWIDTH(20))];
        _contentLabel.textAlignment = NSTextAlignmentCenter;
        _contentLabel.text = num;
        _contentLabel.textColor = [UIColor redColor];
        [self addSubview:_contentLabel];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH(30), frame.size.width, KWIDTH(20))];
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
        titleLabel.textColor = [UIColor blackColor];
        [self addSubview:titleLabel];
        
       
    }
    return self;
}


- (void)setContentLabelWithNumber:(NSString *)num {
    _contentLabel.text = num;
    
}



- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title image:(NSString *)imageName {
    if (self = [super initWithFrame:frame]) {
        CGFloat width = KWIDTH(25);
        CGFloat imageX = (frame.size.width - KWIDTH(25)) / 2;
        
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageX, KWIDTH(15), width, width)];
        imageView.image = [UIImage imageNamed:imageName];
        [self addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height - KWIDTH(25), frame.size.width, KWIDTH(20))];
        label.text = title;
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title imageUrl:(NSString *)imageUrl {
    if (self = [super initWithFrame:frame]) {
        float width = frame.size.height / 3;
        CGFloat padding = frame.size.height / 6;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width / 2 - width / 2, padding, width, width)];
        [imageView setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"1"]];
        [self addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, width * 1.5, frame.size.width, width)];
        label.text = title;
        if ([title isEqualToString:@"全部"]) {
            label.textColor = [UIColor colorWithRed:0.6431 green:0.7569 blue:0.2471 alpha:1.0];
        }else {
            label.textColor = [UIColor blackColor];
        }
        
        
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:KWIDTH(10)];
        [self addSubview:label];

    }
    return self;
}


- (void)addLine {
    UILabel *rightLine = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width + 0.5, 0, 0.5, self.frame.size.height)];
    rightLine.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:rightLine];
    
    UILabel *bottomLine = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 0.5, self.frame.size.width + 0.5, 0.5)];
    bottomLine.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:bottomLine];
}


- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title icon:(NSString *)iconName hadPrise:(BOOL)hadPrise {
    
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        if (hadPrise) {
            label.textColor = [UIColor orangeColor];
            iconName = @"zan2";
        }else {
            label.textColor = [UIColor blackColor];
        }
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image = [UIImage imageNamed:iconName];
        attachment.bounds = CGRectMake(- KWIDTH(5), - KWIDTH(3), KWIDTH(15), KWIDTH(15));
        
        NSMutableAttributedString *attStr = (NSMutableAttributedString *)[NSAttributedString attributedStringWithAttachment:attachment];
        
        NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:title];
        [attStr appendAttributedString:titleStr];
        
        label.attributedText = attStr;
        [self addSubview:label];
    }
    return self;
}
@end
