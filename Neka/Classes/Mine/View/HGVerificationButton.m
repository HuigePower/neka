//
//  HGVerificationButton.m
//  Neka
//
//  Created by ma c on 2017/7/29.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGVerificationButton.h"

@implementation HGVerificationButton {
    UILabel *_timeCount;
    int _seconds;
    NSTimer *_timer;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setTitle:@"获取短信验证码" forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont systemFontOfSize:12];
        [self setTitleColor:mRGB(0, 126, 222, 1) forState:UIControlStateNormal];
        self.layer.borderColor = mRGB(0, 126, 222, 1).CGColor;
        self.layer.borderWidth = 0.5f;
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = KWIDTH(3);
        
        _timeCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _timeCount.backgroundColor = [UIColor lightGrayColor];
        _timeCount.textColor = [UIColor grayColor];
        _timeCount.text = @"重新发送(60s)";
        _timeCount.textAlignment = NSTextAlignmentCenter;
        _timeCount.font = [UIFont systemFontOfSize:12];
        _timeCount.layer.masksToBounds = YES;
        _timeCount.layer.cornerRadius = KWIDTH(3);
        
        _seconds = 10;
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeGo) userInfo:nil repeats:YES];
        [_timer setFireDate:[NSDate distantFuture]];
    }
    return self;
}

//开始计时
- (void)startTiming {
    [self addSubview:_timeCount];
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.enabled = NO;
    [_timer setFireDate:[NSDate distantPast]];
}

- (void)timeGo {
    if (_seconds > 0) {
        _timeCount.text = [NSString stringWithFormat:@"重新发送(%ds)", _seconds--];
    }else {
        [_timer setFireDate:[NSDate distantFuture]];
        self.layer.borderColor = mRGB(0, 126, 222, 1).CGColor;
        [_timeCount removeFromSuperview];
        _seconds = 60;
        self.enabled = YES;
    }
}

- (void)dealloc {
    [_timer invalidate];
    _timer = nil;
}

@end
