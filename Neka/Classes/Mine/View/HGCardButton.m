//
//  HGCardButton.m
//  Neka1.0
//
//  Created by ma c on 2016/12/28.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGCardButton.h"

@implementation HGCardButton {
    UIImageView *_imageView;
    UILabel *_titleLabel;
    UILabel *_numberLabel;
}

- (instancetype)initWithFrame:(CGRect)frame withImage:(NSString *)imageName title:(NSString *)title number:(NSString *)num {
    if (self = [super initWithFrame:frame]) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(25), KWIDTH(20))];
        _imageView.image = [UIImage imageNamed:imageName];
        [self addSubview:_imageView];
        
        _imageView.center = CGPointMake(frame.size.width / 2, KWIDTH(25));
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH(45), frame.size.width, KWIDTH(20))];
        _titleLabel.text = title;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [self addSubview:_titleLabel];
        
        _numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH(65), frame.size.width, KWIDTH(20))];
        _numberLabel.textAlignment = NSTextAlignmentCenter;
        _numberLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [self addSubview:_numberLabel];
        
        [self setNumberLabelWithNumber:num];
    }
    return self;
}

- (void)setNumberLabelWithNumber:(NSString *)numberStr {
    NSString *str = [NSString stringWithFormat:@"共%@张", numberStr];
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:str];
    [att addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:KWIDTH(14)], NSForegroundColorAttributeName:[UIColor redColor]} range:NSMakeRange(1, str.length - 2)];
    _numberLabel.attributedText = att;
}




@end
