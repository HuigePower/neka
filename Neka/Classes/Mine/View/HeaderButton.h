//
//  HeaderButton.h
//  Neka1.0
//
//  Created by ma c on 16/8/21.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderButton : UIButton
- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title imageName:(NSString *)imageName haveNext:(BOOL)haveNext  bottomline:(BOOL)hasBottomLine;
@end
