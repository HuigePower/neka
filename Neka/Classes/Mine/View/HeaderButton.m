//
//  HeaderButton.m
//  Neka1.0
//
//  Created by ma c on 16/8/21.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HeaderButton.h"

@implementation HeaderButton
{
    UIImageView *_nextView;
}
- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title imageName:(NSString *)imageName haveNext:(BOOL)haveNext  bottomline:(BOOL)hasBottomLine{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(10), KWIDTH(25), KWIDTH(25))];
        imageView.image = [UIImage imageNamed:imageName];
        [self addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(45), KWIDTH(15), KWIDTH(120), KWIDTH(15))];
        label.text = title;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont boldSystemFontOfSize:KWIDTH(14)];
        [self addSubview:label];
        
        _nextView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(25), KWIDTH(15), KWIDTH(10), KWIDTH(15))];
        _nextView.image = [UIImage imageNamed:@"tubiao2_11"];
        [self addSubview:_nextView];
        [_nextView setHidden:!haveNext];
        
        if (hasBottomLine) {
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), self.frame.size.height - 1, self.frame.size.width - KWIDTH(20), 1)];
            line.backgroundColor = mRGB(234, 234, 234, 1);
            [self addSubview:line];
        }
    }
    return self;
}

//- (void)layoutSubviews {
//    [super layoutSubviews];
//    
//    self.imageView.frame = CGRectMake(10, self.frame.size.height / 6, self.frame.size.height / 6 * 4, self.frame.size.height / 6 * 4);
//    self.titleLabel.frame = CGRectMake(10 + self.frame.size.height, self.frame.size.height / 8, 100, self.frame.size.height / 4 * 3);
//}

@end
