//
//  HGCardButton.h
//  Neka1.0
//
//  Created by ma c on 2016/12/28.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGCardButton : UIButton

- (instancetype)initWithFrame:(CGRect)frame withImage:(NSString *)imageName title:(NSString *)title number:(NSString *)num;
- (void)setNumberLabelWithNumber:(NSString *)numberStr;
@end
