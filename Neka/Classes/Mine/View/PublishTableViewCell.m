//
//  PublishTableViewCell.m
//  Neka1.0
//
//  Created by ma c on 16/9/11.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "PublishTableViewCell.h"

@implementation PublishTableViewCell

- (instancetype)init {
    if (self = [super init]) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(80), KWIDTH(20))];
        _titleLabel.font = [UIFont boldSystemFontOfSize:KWIDTH(12)];

        
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(70), KWIDTH(10), KWIDTH(60), KWIDTH(20))];
        _dateLabel.font = [UIFont systemFontOfSize:KWIDTH(10)];
        _dateLabel.textAlignment = NSTextAlignmentRight;

        
        _deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(60), KWIDTH(5), KWIDTH(50), KWIDTH(30))];
        [_deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _deleteButton.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(10)];
        _deleteButton.layer.masksToBounds = YES;
        _deleteButton.layer.cornerRadius = KWIDTH(5);
        _deleteButton.layer.borderColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0].CGColor;
        _deleteButton.layer.borderWidth = 0.5f;
        [_deleteButton addTarget:self action:@selector(deleteClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return self;
}


- (void)makeContentWithData:(NSDictionary *)data {
    _titleLabel.text = data[@"name"];
    [self addSubview:_titleLabel];
    
    _dateLabel.text = data[@"date"];
    [self addSubview:_dateLabel];
}

- (void)addDeleteButtonForSubview {
    [self addSubview:_deleteButton];
    [_titleLabel removeFromSuperview];
    [_dateLabel removeFromSuperview];
}

- (void)deleteClick:(UIButton *)btn {
    if (_deleteItem) {
        self.deleteItem(self.data);
    }
}


- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
