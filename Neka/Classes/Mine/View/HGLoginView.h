//
//  HGLoginView.h
//  Neka
//
//  Created by ma c on 2017/6/14.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^HGLoginSucceed)();
typedef void(^HGStartRegister)();
typedef void(^HGFindPassword)();
@interface HGLoginView : UIView<UIScrollViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong)UITextField *account;
@property (nonatomic, strong)UITextField *password;


@property (nonatomic, copy)HGLoginSucceed loginSucceedBlock;
@property (nonatomic, copy)HGStartRegister startRegisterBlock;
@property (nonatomic, copy)HGFindPassword findPasswordBlock;
@end
