//
//  OrdelDetailCell.h
//  Neka1.0
//
//  Created by ma c on 16/8/18.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrdelDetailCell : UITableViewCell

@property (nonatomic, strong)UILabel *orderID;//订单编号
@property (nonatomic, strong)UIImageView *icon;//图片
@property (nonatomic, strong)UILabel *title;//名称
@property (nonatomic, strong)UILabel *detail;//详情
@property (nonatomic, strong)UIButton *stateLabel;

@property (nonatomic, strong)NSIndexPath *indexPath;

@property (nonatomic, strong)NSDictionary *data;
@end
