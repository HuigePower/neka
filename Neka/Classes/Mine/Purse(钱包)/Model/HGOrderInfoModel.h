//
//  HGOrderInfoModel.h
//  Neka
//
//  Created by ma c on 2017/6/30.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGOrderInfoModel : NSObject


@property (nonatomic, copy)NSString *delivery_comment;
@property (nonatomic, copy)NSString *order_id;
@property (nonatomic, copy)NSString *order_name;
@property (nonatomic, copy)NSString *order_num;
@property (nonatomic, copy)NSString *order_price;
@property (nonatomic, copy)NSString *order_total_money;
@property (nonatomic, copy)NSString *order_txt_type;
@property (nonatomic, copy)NSString *order_type;

- (instancetype)initWithDic:(NSDictionary *)dic;

@end
