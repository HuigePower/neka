//
//  HGOrderInfoModel.m
//  Neka
//
//  Created by ma c on 2017/6/30.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGOrderInfoModel.h"

@implementation HGOrderInfoModel


- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
        NSArray *arr = [dic allValues];
        for (id item in arr) {
            NSLog(@"%@", [item class]);
        }
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (NSString *)description {
    if (self.order_id) {
        NSString *str = [NSString stringWithFormat:@"%@,\n%@,\n%@,\n%@,\n%@,\n%@,\n%@,\n%@,\n", _delivery_comment, _order_id, _order_name, _order_num, _order_price, _order_total_money, _order_txt_type, _order_type];
        return str;
    }
    return @"nil";
}


@end
