//
//  PurseViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/16.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "PurseViewController.h"
#import "RechargeViewController.h"
#import "WithdrawViewController.h"
#import "HGWebViewRootController.h"
#import "HGOrderWebViewController.h"
@interface PurseViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong)UITableView *tableView;

@property (nonatomic, strong)UILabel *money;
@property (nonatomic, strong)NSDictionary *userData;
@end

@implementation PurseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的钱包";
    [self registerNotification];
    [self.tabBarController.tabBar setHidden:YES];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initDataSource];
}


- (void)initDataSource {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [self showLoading];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=my_wallet", BASE_URL] parameters:body progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        NSLog(@"errorCode:%@", errorCode);
        if ([errorCode isEqualToString:@"0"]) {
            NSLog(@"钱包数据请求成功");
            NSLog(@"%@", responseObject);
            _userData = responseObject[@"result"][@"user"];
            [_tableView reloadData];
        }else {
            NSLog(@"%@", responseObject[@"errorMsg"]);
        }
        
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@", error.description);
        [self hideLoading];
    }];
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (indexPath.section == 0) {

        _money = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(50), KWIDTH(20), SCREEN_SIZE.width - KWIDTH(100), KWIDTH(20))];
        _money.font = [UIFont boldSystemFontOfSize:KWIDTH(14)];
        _money.font = [UIFont fontWithName:@"Times New Roman" size:KWIDTH(14)];
        _money.textColor = [UIColor whiteColor];
        _money.textAlignment = NSTextAlignmentCenter;
        if (_userData[@"now_money"]) {
            _money.text = _userData[@"now_money"];
        }else {
            _money.text = @"--";
        }
//        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
//        numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
//        NSString *moneyFormatterString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:10000.50]];
//        _money.text = [moneyFormatterString stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
        
        
        [cell addSubview:_money];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(50), KWIDTH(40), SCREEN_SIZE.width - KWIDTH(100), KWIDTH(20))];
        label.text = @"我的余额";
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont boldSystemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:label];
        
        NSArray *titles = @[@"充值", @"提现"];
        NSArray *images = @[@"recharge", @"withdraw"];
        for (int i = 0; i < 2; i++) {
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 2 * i, KWIDTH(70), SCREEN_SIZE.width / 2, KWIDTH(30))];
            btn.backgroundColor = [UIColor colorWithRed:0.9583 green:0.0027 blue:0.1106 alpha:1.0];
            btn.tag = 10 + i;
            [btn addTarget:self action:@selector(rechargeOrWithDrawClick:) forControlEvents:UIControlEventTouchUpInside];
            UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 4 + KWIDTH(5), KWIDTH(5), KWIDTH(50), KWIDTH(20))];
            title.text = titles[i];
            title.textColor = [UIColor whiteColor];
            title.font = [UIFont systemFontOfSize:KWIDTH(12)];
            [btn addSubview:title];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 4 - KWIDTH(25), KWIDTH(5), KWIDTH(20),KWIDTH(20))];
            imageView.image = [UIImage imageNamed:images[i]];
            [btn addSubview:imageView];
            
            [cell addSubview:btn];
        }
        UILabel *line1 = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH(70), SCREEN_SIZE.width, 1)];
        line1.backgroundColor = [UIColor colorWithRed:0.7453 green:0.0055 blue:0.0544 alpha:1.0];
        [cell addSubview:line1];
        
        UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 2, KWIDTH(70), 1, KWIDTH(30))];
        line2.backgroundColor = [UIColor colorWithRed:0.7453 green:0.0055 blue:0.0544 alpha:1.0];
        [cell addSubview:line2];
        
        cell.backgroundColor = [UIColor colorWithRed:0.9765 green:0.2431 blue:0.2314 alpha:1.0];
        
        
    }else if (indexPath.section == 1) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(10), KWIDTH(20) * 1.2, KWIDTH(20))];
        imageView.image = [UIImage imageNamed:@"transaction"];
        [cell addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(40), KWIDTH(10), KWIDTH(100), KWIDTH(20))];
        label.text = @"余额记录";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        label.textColor = [UIColor blackColor];
        [cell addSubview:label];
        
        UIImageView *next = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(20), KWIDTH(12.5), KWIDTH(8), KWIDTH(15))];
        next.image = [UIImage imageNamed:@"tubiao2_11"];
        [cell addSubview:next];
        
    }else if (indexPath.section == 2) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(10), KWIDTH(20) * 1.2, KWIDTH(20))];
        imageView.image = [UIImage imageNamed:@"integral"];
        [cell addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(40), KWIDTH(10), KWIDTH(100), KWIDTH(20))];
        label.text = @"积分记录";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        label.textColor = [UIColor blackColor];
        [cell addSubview:label];
        
        UIImageView *next = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(20), KWIDTH(12.5), KWIDTH(8), KWIDTH(15))];
        next.image = [UIImage imageNamed:@"tubiao2_11"];
        [cell addSubview:next];

    }else if (indexPath.section == 3) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(10), KWIDTH(20) * 1.2, KWIDTH(20))];
        imageView.image = [UIImage imageNamed:@"grade"];
        [cell addSubview:imageView];

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(40), KWIDTH(10), KWIDTH(100), KWIDTH(20))];
        label.text = @"等级管理";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        label.textColor = [UIColor blackColor];
        [cell addSubview:label];

        UIImageView *next = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(20), KWIDTH(12.5), KWIDTH(8), KWIDTH(15))];
        next.image = [UIImage imageNamed:@"tubiao2_11"];
        [cell addSubview:next];
        
        UIFont *font = [UIFont systemFontOfSize:KWIDTH(14)];

        CGRect rect = [@"VIP0" boundingRectWithSize:CGSizeMake(KWIDTH(100), KWIDTH(20)) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        CGFloat width = rect.size.width + KWIDTH(10);
        UILabel *grade = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(25) - width, KWIDTH(10), width, KWIDTH(20))];
        grade.text = @"VIP0";
        grade.textColor = [UIColor whiteColor];
        grade.layer.masksToBounds = YES;
        grade.layer.cornerRadius = KWIDTH(5);
        grade.textAlignment = NSTextAlignmentCenter;
        grade.font = font;
        grade.backgroundColor = [UIColor colorWithRed:0.8549 green:0.8549 blue:0.8549 alpha:1.0];
        
        
        [cell addSubview:grade];
        
    }
    cell.imageView.frame = CGRectMake(KWIDTH(10), KWIDTH(10), KWIDTH(30), KWIDTH(30));
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(0.1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return KWIDTH(100);
    }else {
        return KWIDTH(40);
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return KWIDTH(5);
}

//充值或取现
- (void)rechargeOrWithDrawClick:(UIButton *)btn {
//    HGWebViewRootController *webview = [[HGWebViewRootController alloc] init];
    //        webview.URL = @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=recharge";
    //        webview.URL = @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=withdraw";
    if (btn.tag == 10) {

        RechargeViewController *recharge = [[RechargeViewController alloc] init];
        
        [self.navigationController pushViewController:recharge animated:YES];
    }else {
        HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
        web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=withdraw";
//        WithdrawViewController *withdraw = [[WithdrawViewController alloc] init];
        [self.navigationController pushViewController:web animated:YES];
    }
//    [self.navigationController pushViewController:webview animated:YES];
    
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        
    }else if (indexPath.section == 1) {
        
        HGWebViewRootController *webview = [[HGWebViewRootController alloc] init];
        webview.title = @"余额记录";
        webview.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=transaction";
        [self.navigationController pushViewController:webview animated:YES];
        
        
        
//        ExchangeRecordViewController *exchangeRecord = [[ExchangeRecordViewController alloc] init];
//        exchangeRecord.title = @"余额记录";
        
    }else if (indexPath.section == 2) {
        
        HGWebViewRootController *webview = [[HGWebViewRootController alloc] init];
        webview.title = @"积分记录";
        webview.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=integral";
        [self.navigationController pushViewController:webview animated:YES];

        
        
//        ExchangeRecordViewController *exchangeRecord = [[ExchangeRecordViewController alloc] init];
//        exchangeRecord.title = @"积分记录";
//        [self.navigationController pushViewController:exchangeRecord animated:YES];
    }else if (indexPath.section == 3) {
        
        HGWebViewRootController *webview = [[HGWebViewRootController alloc] init];
        webview.title = @"等级管理";
        webview.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=levelUpdate";
        [self.navigationController pushViewController:webview animated:YES];

        
//        GradeViewController *grade = [[GradeViewController alloc] init];
//        [self.navigationController pushViewController:grade animated:YES];
    }
}

- (void)registerNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkBalanceRecord) name:@"BalanceRecord" object:nil];
}


- (void)checkBalanceRecord {
    HGWebViewRootController *webview = [[HGWebViewRootController alloc] init];
    webview.title = @"余额记录";
    webview.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=transaction";
    [self.navigationController pushViewController:webview animated:YES];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BalanceRecord" object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
