//
//  WithdrawViewController.m
//  Neka1.0
//
//  Created by ma c on 2017/3/9.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "WithdrawViewController.h"
#import "CustomTableViewCell.h"
@interface WithdrawViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)UITextField *realNameTF;
@property (nonatomic, strong)UITextField *amountTF;
@end

@implementation WithdrawViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"余额提现";
    [self makeSubviews];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    [self.view addSubview:_tableView];
    [_tableView registerClass:[CustomTableViewCell class] forCellReuseIdentifier:@"cell"];
    
}

- (void)initDataSource {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=withdraw_list", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSString *errorCode = [NSString stringWithFormat:@"%@", data[@"error"]];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
        
    }];
}

- (void)makeSubviews {
    _realNameTF = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(10), 0, SCREEN_SIZE.width - KWIDTH(20), 44)];
    _realNameTF.placeholder = @"填写真实姓名,否者无法提现";
    
    _amountTF = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(10), 44, SCREEN_SIZE.width - KWIDTH(20), 44)];
    _amountTF.placeholder = @"请填写提现金额";
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }else {
        return 2;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell) {
        [cell deleteSubviews];
    }
    
    if (indexPath.section == 0) {
        
        [cell addCustomSubview:_realNameTF];
        [cell addCustomSubview:_amountTF];
        
    }else {
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(100), 44)];
        label1.text = @"100.0";
        label1.font = [UIFont systemFontOfSize:14];
        label1.textAlignment = NSTextAlignmentCenter;
        [cell addCustomSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(100), 0, KWIDTH(100), 44)];
        label2.text = @"已完成";
        label2.font = [UIFont systemFontOfSize:14];
        label2.textColor = [UIColor lightGrayColor];
        label2.textAlignment = NSTextAlignmentCenter;
        [cell addCustomSubview:label2];
        
        UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(200), 0, KWIDTH(100), 44)];
        label3.text = @"2017-02-28";
        label3.font = [UIFont systemFontOfSize:14];
        label2.textColor = [UIColor lightGrayColor];
        label3.textAlignment = NSTextAlignmentCenter;
        [cell addCustomSubview:label3];

    }
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 132)];
        
        UILabel *reminderLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 0, SCREEN_SIZE.width - KWIDTH(20), 44)];
        reminderLabel.text = @"金额最多仅支持两位小数,只支持微信提现";
        reminderLabel.font = [UIFont systemFontOfSize:14];
        [view addSubview:reminderLabel];
        
        UILabel *balanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 44, SCREEN_SIZE.width - KWIDTH(20), 44)];
        balanceLabel.text = [NSString stringWithFormat:@"余额: %d元, 可提现: %d元", 100, 100];
        balanceLabel.font = [UIFont systemFontOfSize:14];
        [view addSubview:balanceLabel];
        
        
        UIButton *withDrawBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 88, SCREEN_SIZE.width - KWIDTH(20), 44)];
        [withDrawBtn setTitle:@"提现" forState:UIControlStateNormal];
        withDrawBtn.backgroundColor = [UIColor colorWithRed:0.9882 green:0.2902 blue:0.4824 alpha:1.0];
        [view addSubview:withDrawBtn];
        
        return view;
    }
    return nil;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 44)];
        view.backgroundColor = [UIColor whiteColor];
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(100), 44)];
        label1.text = @"提现金额";
        label1.font = [UIFont systemFontOfSize:14];
        label1.textAlignment = NSTextAlignmentCenter;
        [view addSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(100), 0, KWIDTH(100), 44)];
        label2.text = @"状态";
        label2.font = [UIFont systemFontOfSize:14];
        label2.textColor = [UIColor lightGrayColor];
        label2.textAlignment = NSTextAlignmentCenter;
        [view addSubview:label2];
        
        UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(200), 0, KWIDTH(100), 44)];
        label3.text = @"日期";
        label3.font = [UIFont systemFontOfSize:14];
        label2.textColor = [UIColor lightGrayColor];
        label3.textAlignment = NSTextAlignmentCenter;
        [view addSubview:label3];
        
        return view;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 88;
    }
    return 44;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 5;
    }else {
        return 44;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        return 142;
    }else {
        return 10;
    }
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
