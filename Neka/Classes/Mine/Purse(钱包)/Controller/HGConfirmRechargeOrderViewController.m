//
//  ConfirmOrderViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/16.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGConfirmRechargeOrderViewController.h"
#import <WXApi.h>
#import "HGNoncestrTool.h"
#import "HGMD5Tool.h"
#import "HGASCIISortTool.h"
#import "GDataXMLNode.h"
#import "HGOrderInfoModel.h"
#import "HGIPAddressTool.h"
#import <AlipaySDK/AlipaySDK.h>
#import "HGOrderWebViewController.h"
#import "HGEscapeCharacter.h"
#import "DataSigner.h"
#import "Order.h"



@interface HGConfirmRechargeOrderViewController ()<UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSMutableArray *options;
@property (nonatomic, strong)HGOrderInfoModel *orderInfoModel;
@property (nonatomic, copy)NSString *longOrderID;
@property (nonatomic, strong)UILabel *needPayOnLineLabel;



@property (nonatomic, strong)UIImageView *productImageView;     //产品图片
@property (nonatomic, strong)UITextView *productNameTextView;   //产品名称
@property (nonatomic, strong)UILabel *totalPriceLabel;          //总价
@property (nonatomic, assign)BOOL useWeixinPay;     //是否使用微信支付
@property (nonatomic, assign)BOOL useAliPay;        //是否使用支付宝支付
@property (nonatomic, strong)UIImageView *weixinPayBtn;     //微信支付
@property (nonatomic, strong)UIImageView *aliPayBtn;        //支付宝支付
@property (nonatomic, assign)CGFloat needPayMoney;      //还需支付的金额
@property (nonatomic, strong)UIButton *goPayBtn;        //确认支付
@property (nonatomic, strong)UILabel *needPayLabel;     //还需支付
@end

@implementation HGConfirmRechargeOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"确认订单";
    _useWeixinPay = YES;
    _useAliPay = !_useWeixinPay;
    [self registerNotifications];
    [self createViews];
    [self getOrderInfo];
    
    _options = [[NSMutableArray alloc] init];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 * 2) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    
}

- (void)createViews {
    
    //产品图片
    _productImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(80), KWIDTH(80))];
    _productImageView.image = [UIImage imageNamed:@"loading"];
    //产品名称
    _productNameTextView = [[UITextView alloc] initWithFrame:CGRectMake(15 + KWIDTH(85), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(85) - 30, KWIDTH(80))];
    _productNameTextView.editable = NO;
    _productNameTextView.font = [UIFont systemFontOfSize:16];
    
    
    //订单总价
    _totalPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - 15 - KWIDTH(100), KWIDTH(10), KWIDTH(100), KWIDTH(20))];
    _totalPriceLabel.font = [UIFont systemFontOfSize:14];
    _totalPriceLabel.text = @"¥ --";
    _totalPriceLabel.textColor = [UIColor redColor];
    _totalPriceLabel.textAlignment = NSTextAlignmentRight;
    
    
    _weixinPayBtn = [UIImageView new];
    _aliPayBtn = [UIImageView new];
    
    NSArray *arr = @[_weixinPayBtn,
                     _aliPayBtn];
    
    for (int i = 0; i < arr.count; i++) {
        UIImageView * imageView = arr[i];
        imageView.frame = CGRectMake(SCREEN_SIZE.width - KWIDTH(20) - 15, KWIDTH(10), KWIDTH(20), KWIDTH(20));
        imageView.image = [UIImage imageNamed:@"tickGray"];
        imageView.tag = 10 + i;
    }
    
    
    //还需支付
    _needPayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height - 64 * 2, SCREEN_SIZE.width / 2, 64)];
    _needPayLabel.textAlignment = NSTextAlignmentCenter;
    _needPayLabel.font = [UIFont systemFontOfSize:14];
    _needPayLabel.text = @"还需支付：¥2000.00";
    [self.view addSubview:_needPayLabel];
    //确认支付
    _goPayBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 2, SCREEN_SIZE.height - 64 * 2, SCREEN_SIZE.width / 2, 64)];
    _goPayBtn.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nav_bg"]];
    [_goPayBtn setTitle:@"确认支付" forState:UIControlStateNormal];
    [_goPayBtn addTarget:self action:@selector(goPay) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_goPayBtn];
    
    

}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }else {
        return 2;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            _productImageView.image = [UIImage imageNamed:@"nothing"];
            [cell addSubview:_productImageView];
            
            _productNameTextView.text = _orderInfoModel.order_name;
            [cell addSubview:_productNameTextView];
        }else {
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(100), KWIDTH(20))];
            label.text = @"订单总价";
            label.font = [UIFont systemFontOfSize:14];
            [cell addSubview:label];
            
            _totalPriceLabel.text = [NSString stringWithFormat:@"¥ %@", _orderInfoModel.order_total_money];
            [cell addSubview:_totalPriceLabel];
            
        }
        
    }else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(20), KWIDTH(20))];
            imageView.image = [UIImage imageNamed:@"wxpay"];
            [cell addSubview:imageView];
        
            UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15 + KWIDTH(25), KWIDTH(10), KWIDTH(100), KWIDTH(20))];
            name.text = @"微信支付";
            name.font = [UIFont systemFontOfSize:14];
            [cell addSubview:name];
            [cell addSubview:_weixinPayBtn];
            if (_useWeixinPay) {
                //选择微信
                _weixinPayBtn.image = [UIImage imageNamed:@"tickGreen"];
            }else {
                _weixinPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
        }else {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(20), KWIDTH(20))];
            imageView.image = [UIImage imageNamed:@"alipay"];
            [cell addSubview:imageView];
            
            UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15 + KWIDTH(25), KWIDTH(10), KWIDTH(100), KWIDTH(20))];
            name.text = @"支付宝支付";
            name.font = [UIFont systemFontOfSize:14];
            [cell addSubview:name];
            [cell addSubview:_aliPayBtn];
            if (_useAliPay) {
                //选择支付宝
                _aliPayBtn.image = [UIImage imageNamed:@"tickGreen"];
            }else {
                _aliPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
        }

        
    }
 
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return KWIDTH(0.1);
    }else if (section == 1) {
        return KWIDTH(40);
    }else {
        return KWIDTH(10);
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return KWIDTH(1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return KWIDTH(100);
        }
        return KWIDTH(40);
    }else if (indexPath.section == 1) {
        return KWIDTH(40);
    }else {
        return KWIDTH(40);
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(20))];
        label.text = @"  选择支付方式";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        return label;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 1) {
        if (indexPath.row == 0) {
            
            _useWeixinPay = YES;
            if (_useWeixinPay) {
                //选择微信
                _weixinPayBtn.image = [UIImage imageNamed:@"tickGreen"];
                _useAliPay = NO;
                //取消支付宝
                _aliPayBtn.image = [UIImage imageNamed:@"tickGray"];
                
            }else {
                _weixinPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
            
            
        }else {
            
            _useAliPay = YES;
            if (_useAliPay) {
                //选择支付宝
                _aliPayBtn.image = [UIImage imageNamed:@"tickGreen"];
                //取消微信支付
                _useWeixinPay = NO;
                _weixinPayBtn.image = [UIImage imageNamed:@"tickGray"];
                
            }else {
                _aliPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
            
            
        }
    }

}

#pragma mark -- 获取订单信息
- (void)getOrderInfo {
    
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"order_id": _order_id, @"type": _type};
    [manager POST:[NSString stringWithFormat:@"%@c=Pay&a=check", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@", responseObject);
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            NSDictionary *orderInfo = responseObject[@"result"][@"order_info"];
            _orderInfoModel = [[HGOrderInfoModel alloc] initWithDic:orderInfo];
            [self refreshView];
            [_tableView reloadData];
            NSLog(@"%@", [_orderInfoModel description]);
            
        }else {
            
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD showError:error.description];
    }];
    
}

- (void)refreshView {
    _needPayMoney = [_orderInfoModel.order_price floatValue];
    _needPayLabel.text = [NSString stringWithFormat:@"还需支付 ¥ %.2f", _needPayMoney];
    _needPayLabel.textColor = [UIColor redColor];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:_needPayLabel.text];
    [attStr addAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]} range:NSMakeRange(0, 6)];
    _needPayLabel.attributedText = attStr;
    
    [_tableView reloadData];
}

#pragma mark -- 确定订单信息是否正确，并获取长订单号

- (void)goPay {
    
    if (_orderInfoModel == nil) {
        [MBProgressHUD showError:@"订单信息错误" toView:self.view];
        return;
    }
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    if (_useWeixinPay && !_useAliPay) {
        
        NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"order_id": _order_id, @"order_type": _type, @"pay_type": @"weixin"};
        [self showLoading];
        [manager POST:[NSString stringWithFormat:@"%@c=Pay&a=go_pay", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self hideLoading];
            NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
            if ([errorCode isEqualToString:@"0"]) {
                
                _longOrderID = responseObject[@"result"];
                [self weixinGoPay];
                
            }else {
                [MBProgressHUD showError:responseObject[@"errorMsg"]];
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self hideLoading];
            [MBProgressHUD showError:error.description];
        }];
        
        
        
    }else if (!_useWeixinPay && _useAliPay) {
        NSLog(@"支付宝支付");
        NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"order_id": _order_id, @"order_type": _type, @"pay_type": @"alipay"};
        [self showLoading];
        [manager POST:[NSString stringWithFormat:@"%@c=Pay&a=go_pay", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self hideLoading];
            NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
            if ([errorCode isEqualToString:@"0"]) {
                
                _longOrderID = responseObject[@"result"];

                [self alipayGoPay];
            }else {
                [MBProgressHUD showError:responseObject[@"errorMsg"]];
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self hideLoading];
            [MBProgressHUD showError:error.description];
        }];
    
    }
}

#pragma mark -- 微信支付--统一下单

- (void)weixinGoPay {
    NSLog(@"微信支付");
    NSString *url = @"https://api.mch.weixin.qq.com/pay/unifiedorder";
    NSDictionary *tempBody =
    @{@"appid": @"wx27b5587b7b499c79",                     //应用ID
      @"mch_id": @"1448060602",                            //商户号
      @"nonce_str": [HGNoncestrTool noncestrWithLength:32],//随机字符串
      @"body": [NSString stringWithFormat:@"耐卡汽车平台-%@", _orderInfoModel.order_name],                       //商品描述
      @"out_trade_no": [NSString stringWithFormat:@"%@_%@", _orderInfoModel.order_type, _longOrderID],                              //商户订单号
      @"total_fee": [NSString stringWithFormat:@"%.f", [_orderInfoModel.order_total_money floatValue] * 100],                                 //总金额
      @"spbill_create_ip": [HGIPAddressTool getIPAddress:YES],               //终端IP
      @"notify_url": @"https://www.nekahome.com/source/appapi_weixin_notice.php", //通知地址
      @"trade_type": @"APP"                                     //交易类型
      };
    
    NSArray *sortedArr = [HGASCIISortTool sortedWithDictionary:tempBody];
    
    NSMutableString *tempStr = [NSMutableString new];
    for (int i = 0; i < sortedArr.count; i++) {
        if (i == 0) {
            [tempStr appendFormat:@"%@", [NSString stringWithFormat:@"%@=%@", sortedArr[i],  tempBody[sortedArr[i]]]];
        }else {
            [tempStr appendFormat:@"&%@", [NSString stringWithFormat:@"%@=%@", sortedArr[i],  tempBody[sortedArr[i]]]];
        }
    }
    [tempStr appendFormat:@"%@", [NSString stringWithFormat:@"&key=%@", WeixinKey]];
    
    NSLog(@"temp:%@", tempStr);
    NSString *signValue = [HGMD5Tool MD5ForUpper32Bate:tempStr];
    NSLog(@"sign:%@", signValue);
    
    NSMutableDictionary *body = [NSMutableDictionary dictionaryWithDictionary:tempBody];
    [body setObject:signValue forKey:@"sign"];
    NSLog(@"===%@", body);
    GDataXMLElement *xmlEle = [GDataXMLElement elementWithName:@"xml"];
    for (int i = 0; i < sortedArr.count; i++) {
        GDataXMLElement *childXml = [GDataXMLElement elementWithName:sortedArr[i] stringValue:tempBody[sortedArr[i]]];
        [xmlEle addChild:childXml];
    }
    GDataXMLElement *childXml = [GDataXMLElement elementWithName:@"sign" stringValue:signValue];
    [xmlEle addChild:childXml];
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithRootElement:xmlEle];
    NSLog(@"doc:%@", doc);
    NSData *data = [doc XMLData];
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"str:%@", str);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:10];
    request.HTTPMethod = @"POST";
    request.HTTPBody = data;
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:data encoding:NSUTF8StringEncoding error:nil];
        NSData *data1 = [doc XMLData];
        NSString *str = [[NSString alloc] initWithData:data1 encoding:NSUTF8StringEncoding];
        NSLog(@"xml:%@", str);
        
        [self praserXMLWithXML:doc];
    }];
    
    [task resume];

}

#pragma mark -- 微信--调用支付接口

- (void)praserXMLWithXML:(GDataXMLDocument *)xmlDoc {
    GDataXMLElement *rootEle = [xmlDoc rootElement];
    NSArray *array = [rootEle children];
    NSLog(@"%@", array);
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    for (int i = 0; i < array.count; i++) {
        GDataXMLElement *ele = [array objectAtIndex:i];
        NSLog(@"%@--%@", [ele name], [ele stringValue]);
        [dic setObject:[ele stringValue] forKey:[ele name]];
    }
    NSLog(@"%@", dic);
    
    if ([dic[@"return_code"] isEqualToString:@"SUCCESS"] && [dic[@"result_code"] isEqualToString:@"SUCCESS"]) {

       
        
        PayReq *request = [[PayReq alloc] init];
        request.partnerId = dic[@"mch_id"];
        request.prepayId = dic[@"prepay_id"];
        request.package = @"Sign=WXPay";
        request.nonceStr = dic[@"nonce_str"];
        NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
        request.timeStamp = (UInt32)interval;
        
        
        NSDictionary *tempDic = @{@"appid": WX_APPID, @"partnerid": request.partnerId, @"prepayid": request.prepayId, @"package": request.package, @"noncestr": request.nonceStr, @"timestamp": [NSString stringWithFormat:@"%d", request.timeStamp]};
        NSArray *sortedArr = [HGASCIISortTool sortedWithDictionary:tempDic];
        NSMutableString *tempStr = [NSMutableString new];
        for (int i = 0; i < sortedArr.count; i++) {
            if (i == 0) {
                [tempStr appendFormat:@"%@", [NSString stringWithFormat:@"%@=%@", sortedArr[i],  tempDic[sortedArr[i]]]];
            }else {
                [tempStr appendFormat:@"&%@", [NSString stringWithFormat:@"%@=%@", sortedArr[i],  tempDic[sortedArr[i]]]];
            }
        }
        [tempStr appendFormat:@"%@", [NSString stringWithFormat:@"&key=%@", WeixinKey]];
        NSLog(@"temp:%@", tempStr);
        NSString *signValue = [HGMD5Tool MD5ForUpper32Bate:tempStr];
        NSLog(@"sign:%@", signValue);
        
        
        
        request.sign = signValue;
        
        [WXApi sendReq:request];
    }else {
        if (dic[@"err_code_des"]) {
            NSLog(@"错误原因:%@", dic[@"err_code_des"]);
        }else {
            NSLog(@"%@", dic[@"return_msg"]);
        }
        
    }
    
    
}


- (void)alipayGoPay {
    NSString *partnerID = @"2088421249688882";
    NSString *sellerID = @"453813097@qq.com";
    NSString *notify_url = @"https://www.nekahome.com/source/appapi_alipay_notice.php";
    Order *order = [[Order alloc] init];
    order.partner = partnerID;
    order.sellerID = sellerID;
    order.outTradeNO = [NSString stringWithFormat:@"%@_%@", _orderInfoModel.order_type, _longOrderID];
    order.subject = _orderInfoModel.order_name;
    order.body = @"无";
    order.totalFee = _orderInfoModel.order_total_money;
    order.notifyURL = notify_url;
    order.service = @"mobile.securitypay.pay";
    order.paymentType = @"1";
    order.inputCharset = @"utf-8";
    order.itBPay = @"30m";
    order.showURL = @"m.alipay.com";
    
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [order description];
    NSLog(@"orderSpec = %@",orderSpec);
    //签名
    id<DataSigner> signer = CreateRSADataSigner(AlipayPrivateKey);
    NSString *signedString = [signer signString:orderSpec];
    NSLog(@"signedString = %@",signedString);
    NSString *appScheme = @"nekaAlipay";
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            NSLog(@"reslut = %@",resultDic);
        }];
    }
    
}

#pragma mark -- 注册通知
- (void)registerNotifications {
    //支付成功
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySuccessAction:) name:@"PaySuccess" object:nil];
    //支付出错
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payErrorAction:) name:@"PayError" object:nil];
    //支付取消
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payCancelAction:) name:@"PayCancel" object:nil];
    
}

#pragma mark -- 支付成功事件
- (void)paySuccessAction:(NSNotification *)noti {
    
    NSString *payType = noti.userInfo[@"pay_type"];
    if ([payType isEqualToString:@"weixin"]) {//微信支付成功
    
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"order_id": _orderInfoModel.order_id, @"order_type": _orderInfoModel.order_type, @"pay_status": @"0"};
        NSLog(@"微信支付成功事件：%@", body);
        [self showLoading];
        [manager POST:[NSString stringWithFormat:@"%@c=Pay&a=app_weixin_back", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"%@", responseObject);
            [self hideLoading];
            NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
            if ([errorCode isEqualToString:@"0"]) {
                NSLog(@"%@", responseObject);
                NSString *url = responseObject[@"result"];
                HGOrderWebViewController *webView = [[HGOrderWebViewController alloc] init];
                webView.urlString = url;
                [self.navigationController pushViewController:webView animated:YES];
            }else {
                [MBProgressHUD showError:responseObject[@"errorMsg"]];
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD showError:error.description];
        }];

        
    }else {//支付宝支付成功
        NSString *alipayReturn = noti.userInfo[@"alipay_return"];
        NSLog(@"alipayReturn:%@", alipayReturn);
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSDictionary *body = @{@"Device-Id": DEVICE_ID,
                               @"ticket": [UserDefaultManager ticket],
                               @"order_id": _orderInfoModel.order_id,
                               @"order_type": _orderInfoModel.order_type,
                               @"pay_type": @"alipay",
                               @"alipay_return": alipayReturn};
        NSLog(@"支付宝支付成功事件：%@", body);
        [self showLoading];
        [manager POST:[NSString stringWithFormat:@"%@c=Pay&a=app_alipay_back", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"%@", responseObject);
            [self hideLoading];
            NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
            if ([errorCode isEqualToString:@"0"]) {
                NSString *url = responseObject[@"result"][@"url"];
                HGOrderWebViewController *webView = [[HGOrderWebViewController alloc] init];
                webView.urlString = url;
                [self.navigationController pushViewController:webView animated:YES];
            }else {
                [MBProgressHUD showError:responseObject[@"errorMsg"]];
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self hideLoading];
            [MBProgressHUD showError:error.description];
        }];

        
    }
    
}

#pragma mark -- 支付成功事件
- (void)payErrorAction:(NSNotification *)noti {
    
}

#pragma mark -- 支付成功事件
- (void)payCancelAction:(NSNotification *)noti {
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end














