//
//  RechargeViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/16.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "RechargeViewController.h"
#import "HGConfirmRechargeOrderViewController.h"
@interface RechargeViewController ()
@property (nonatomic, strong)UITextField *moneyTF;
@end

@implementation RechargeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"余额充值";
    [self.tabBarController.tabBar setHidden:YES];
    self.view.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0 + KWIDTH(10), SCREEN_SIZE.width, KWIDTH(40))];
    view.backgroundColor = [UIColor whiteColor];
    _moneyTF = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(30))];
    _moneyTF.placeholder = @"请填写充值金额";
    _moneyTF.font = [UIFont systemFontOfSize:KWIDTH(12)];
    _moneyTF.keyboardType = UIKeyboardTypeDecimalPad;
    [view addSubview:_moneyTF];
    [self.view addSubview:view];
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 0 + KWIDTH(60), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(20))];
    label.text = @"金额最多仅支持两位小数";
    label.font = [UIFont systemFontOfSize:KWIDTH(12)];
    [self.view addSubview:label];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 0 + KWIDTH(90), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(30))];
    btn.backgroundColor = mRGB(13, 103, 214, 1);
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = KWIDTH(2);
    [btn setTitle:@"充值" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    [btn addTarget:self action:@selector(rechargeClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btn];

    
}

- (void)rechargeClick:(UIButton *)btn {
    NSLog(@"充值");
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"money": _moneyTF.text, @"type": @"recharge"};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Recharge&a=index", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSLog(@"%@", responseObject);
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            
            HGConfirmRechargeOrderViewController *confirm = [[HGConfirmRechargeOrderViewController alloc] init];
            confirm.order_id = responseObject[@"result"][@"order_id"];
            confirm.type = responseObject[@"result"][@"type"];
            [self.navigationController pushViewController:confirm animated:YES];
            
            
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
    }];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end











