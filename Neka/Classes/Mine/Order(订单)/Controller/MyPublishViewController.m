//
//  MyPublishViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/21.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "MyPublishViewController.h"
#import "CustomTableViewCell.h"
#import "PublishTableViewCell.h"
#define SELECTE_COLOR [UIColor colorWithRed:0.0 green:0.7255 blue:0.6235 alpha:1.0]
@interface MyPublishViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
@property (nonatomic, strong)UIButton *hadBtn;
@property (nonatomic, strong)UIButton *willBtn;

@property (nonatomic, strong)UITableView *hadTableView;
@property (nonatomic, strong)UITableView *willTableView;

@property (nonatomic, strong)NSMutableArray *hadData;
@property (nonatomic, strong)NSMutableArray *willData;

@property (nonatomic, strong)UILabel *line;

@property (nonatomic, strong)NSDictionary *tmpData;
@end

@implementation MyPublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的发布";
    [self.tabBarController.tabBar setHidden:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.view.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
    [self initDataSource];
    
    //已发布
    _hadBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width / 2, KWIDTH(40))];
    _hadBtn.backgroundColor = [UIColor whiteColor];
    [_hadBtn setTitle:@"已发布(0)" forState:UIControlStateNormal];
    [_hadBtn setTitleColor:SELECTE_COLOR forState:UIControlStateNormal];
    _hadBtn.titleLabel.font = [UIFont boldSystemFontOfSize:KWIDTH(14)];
    [self.view addSubview:_hadBtn];
    [_hadBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _hadTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0 + KWIDTH(42), SCREEN_SIZE.width, SCREEN_SIZE.height - KWIDTH(42) - 64) style:UITableViewStyleGrouped];
    _hadTableView.delegate = self;
    _hadTableView.dataSource = self;
    [self.view addSubview:_hadTableView];
//    [_hadTableView registerClass:[PublishTableViewCell class] forCellReuseIdentifier:@"myCell"];
    
    
    //审核中
    _willBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 2, 0, SCREEN_SIZE.width / 2, KWIDTH(40))];
    _willBtn.backgroundColor = [UIColor whiteColor];
    [_willBtn setTitle:@"审核中(0)" forState:UIControlStateNormal];
    [_willBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _willBtn.titleLabel.font = [UIFont boldSystemFontOfSize:KWIDTH(14)];
    [self.view addSubview:_willBtn];
    [_willBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _willTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0 + KWIDTH(42), SCREEN_SIZE.width, SCREEN_SIZE.height - KWIDTH(42) - 64)  style:UITableViewStyleGrouped];
    _willTableView.delegate = self;
    _willTableView.dataSource = self;
    [self.view addSubview:_willTableView];
//    [_willTableView registerClass:[PublishTableViewCell class] forCellReuseIdentifier:@"myCell"];
    [_willTableView setHidden:YES];
    
    _line = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH(39), SCREEN_SIZE.width / 2, KWIDTH(1))];
    _line.backgroundColor = SELECTE_COLOR;
    [_hadBtn addSubview:_line];
    
}

- (void)initDataSource {
    _hadData = [[NSMutableArray alloc] init];
    _hadData.array = @[@{@"name":@"招聘1", @"date":@"今天15.30"}, @{@"name":@"招聘2", @"date":@"今天15.30"}, @{@"name":@"招聘3", @"date":@"今天15.30"}];
    _willData = [[NSMutableArray alloc] init];
    _willData.array = @[@{@"name":@"招聘4", @"date":@"今天15.30"}, @{@"name":@"招聘5", @"date":@"今天15.30"}, @{@"name":@"招聘6", @"date":@"今天15.30"}];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == _hadTableView) {
        return _hadData.count;
    }else if (tableView == _willTableView) {
        return _willData.count;
    }
    return 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    PublishTableViewCell *cell = (PublishTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[PublishTableViewCell alloc] init];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dic = [[NSDictionary alloc] init];
    NSInteger type = 0;
    if (tableView == _hadTableView) {
        dic = _hadData[indexPath.section];
        type = 0;
    }else if (tableView == _willTableView) {
        dic =  _willData[indexPath.section];
        type = 1;
    }
    
    if (indexPath.row == 0) {
        [cell makeContentWithData:dic];
    }else {
        [cell addDeleteButtonForSubview];
        cell.data = @{@"type":[NSNumber numberWithInteger:type], @"indexPath":indexPath};
        cell.deleteItem = ^(NSDictionary *data) {
            _tmpData = data;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您确认要删除此信息吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
            
        };
        
    }

    return cell;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSIndexPath *indexPath = _tmpData[@"indexPath"];
        if ([_tmpData[@"type"] isEqual:[NSNumber numberWithInteger:0]]) {
            
            [_hadData removeObjectAtIndex:indexPath.section];
            [_hadTableView reloadData];
        }else {
            
            [_willData removeObjectAtIndex:indexPath.section];
            [_willTableView reloadData];
        }
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == _hadTableView) {
        if (indexPath.row == 0) {
            NSLog(@"%@", _hadData[indexPath.section]);
        }
    }else {
        if (indexPath.row == 0) {
            NSLog(@"%@", _willData[indexPath.section]);
        }
    }
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return KWIDTH(40);
    }else {
        return KWIDTH(40);
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(2);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return KWIDTH(3);
}




- (void)btnClick:(UIButton *)btn {
    if (btn == _hadBtn) {
        [_hadBtn addSubview:_line];
        [_hadBtn setTitleColor:SELECTE_COLOR forState:UIControlStateNormal];
        
        [_willBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_hadTableView setHidden:NO];
        [_willTableView setHidden:YES];
    }else if (btn == _willBtn) {
        [_willBtn addSubview:_line];
        [_willBtn setTitleColor:SELECTE_COLOR forState:UIControlStateNormal];
        
        [_hadBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_hadTableView setHidden:YES];
        [_willTableView setHidden:NO];
    }
}

#pragma mark -- cell的分割线宽度等于屏幕宽度
-(void)viewDidLayoutSubviews {
    
    if ([_hadTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_hadTableView setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([_hadTableView respondsToSelector:@selector(setLayoutMargins:)])  {
        [_hadTableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([_willTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_willTableView setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([_willTableView respondsToSelector:@selector(setLayoutMargins:)])  {
        [_willTableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
