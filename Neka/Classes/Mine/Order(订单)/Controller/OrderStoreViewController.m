//
//  FastOrderViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/18.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "OrderStoreViewController.h"

#import "NavButton.h"
#import "MJRefresh.h"
#import "OrdelDetailCell.h"
#import "NextButton.h"
@interface OrderStoreViewController ()<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong)NSMutableArray *navBtnArr;
@property (nonatomic, strong)UIScrollView *scrollView;
@property (nonatomic, strong)NSMutableArray *allData;
@property (nonatomic, strong)NSMutableArray *willPayData;
@property (nonatomic, strong)NSMutableArray *noPayData;
@property (nonatomic, strong)NSMutableArray *willCommentData;

@end

@implementation OrderStoreViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"到店付订单列表";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.tabBarController.tabBar setHidden:YES];
    [self initDataSource];
    [self makeView];
    
}

- (void)makeView {
    NSArray *navTitleArr = @[@"全部", @"代付款", @"未消费", @"待评价"];
    _navBtnArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < navTitleArr.count; i++) {
        NavButton *btn = [NavButton  initWithFrame:CGRectMake(SCREEN_SIZE.width / navTitleArr.count * i, 64, SCREEN_SIZE.width / navTitleArr.count, 30) title:navTitleArr[i] titleColor:[UIColor blackColor] selectedColor:[UIColor colorWithRed:0.098 green:0.7137 blue:0.6784 alpha:1.0]];
        btn.tag = 10 + i;
        [btn addTarget:self action:@selector(selectNavBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_navBtnArr addObject:btn];
        [self.view addSubview:btn];
    }
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 93.5, SCREEN_SIZE.width, 0.5)];
    line.backgroundColor = [UIColor grayColor];
    [self.view addSubview:line];
    NavButton *btn = _navBtnArr[0];
    [btn setSelected:YES];
    btn.view.backgroundColor = [UIColor colorWithRed:0.098 green:0.7137 blue:0.6784 alpha:1.0];
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 94, SCREEN_SIZE.width, SCREEN_SIZE.height - 94)];
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(SCREEN_SIZE.width * navTitleArr.count, SCREEN_SIZE.height - 94);
    _scrollView.pagingEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = NO;
    
    _scrollView.bounces = NO;
    [self.view addSubview:_scrollView];
    
    
    for (int i = 0; i < navTitleArr.count; i++) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width * i, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 94) style:UITableViewStyleGrouped];
        tableView.delegate = self;
        tableView.dataSource =self;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshDataWithHeader:)];
        
        
        
        tableView.mj_footer.tag = 200 + i;
        tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadNewDataWithFooter:)];
        tableView.tag = 20 + i;
        tableView.tableFooterView = [[UIView alloc] init];
        [_scrollView addSubview:tableView];
        
    }
    
}


- (void)initDataSource {
    _allData = [[NSMutableArray alloc] init];
    _willPayData = [[NSMutableArray alloc] init];
    _noPayData = [[NSMutableArray alloc] init];
    _willCommentData = [[NSMutableArray alloc] init];
    NSDictionary *dic = @{@"全部":@[@{@"order_id":@"16081104344014091002",
                                    @"image_url":@"http://image3.suning.cn/uimg/b2c/newcatentries/0070079775-000000000141288559_1_200x200.jpg",
                                    @"title":@"一分钱试吃",
                                    @"count":@"1",
                                    @"total_price":@"178",
                                    @"state":@"1"},
                                  @{@"order_id":@"16081104344014091002",
                                    @"image_url":@"http://image3.suning.cn/uimg/b2c/newcatentries/0070079775-000000000141288559_1_200x200.jpg",
                                    @"title":@"一分钱试吃",
                                    @"count":@"1",
                                    @"total_price":@"178",
                                    @"state":@"2"},
                                  @{@"order_id":@"16081104344014091002",
                                    @"image_url":@"http://image3.suning.cn/uimg/b2c/newcatentries/0070079775-000000000141288559_1_200x200.jpg",
                                    @"title":@"一分钱试吃",
                                    @"count":@"1",
                                    @"total_price":@"178",
                                    @"state":@"3"}],
                          @"待付款":@[@{@"order_id":@"16081104344014091002",
                                     @"image_url":@"http://image3.suning.cn/uimg/b2c/newcatentries/0070079775-000000000141288559_1_200x200.jpg",
                                     @"title":@"一分钱试吃",
                                     @"count":@"1",
                                     @"total_price":@"178",
                                     @"state":@"1"},
                                   @{@"order_id":@"16081104344014091002",
                                     @"image_url":@"http://image3.suning.cn/uimg/b2c/newcatentries/0070079775-000000000141288559_1_200x200.jpg",
                                     @"title":@"一分钱试吃",
                                     @"count":@"1",
                                     @"total_price":@"178",
                                     @"state":@"1"}],
                          @"未消费":@[@{@"order_id":@"16081104344014091002",
                                     @"image_url":@"http://image3.suning.cn/uimg/b2c/newcatentries/0070079775-000000000141288559_1_200x200.jpg",
                                     @"title":@"一分钱试吃",
                                     @"count":@"1",
                                     @"total_price":@"178",
                                     @"state":@"2"},
                                   @{@"order_id":@"16081104344014091002",
                                     @"image_url":@"http://image3.suning.cn/uimg/b2c/newcatentries/0070079775-000000000141288559_1_200x200.jpg",
                                     @"title":@"一分钱试吃",
                                     @"count":@"1",
                                     @"total_price":@"178",
                                     @"state":@"2"}],
                          @"待评价":@[@{@"order_id":@"16081104344014091002",
                                     @"image_url":@"http://image3.suning.cn/uimg/b2c/newcatentries/0070079775-000000000141288559_1_200x200.jpg",
                                     @"title":@"一分钱试吃",
                                     @"count":@"1",
                                     @"total_price":@"178",
                                     @"state":@"3"},
                                   @{@"order_id":@"16081104344014091002",
                                     @"image_url":@"http://image3.suning.cn/uimg/b2c/newcatentries/0070079775-000000000141288559_1_200x200.jpg",
                                     @"title":@"一分钱试吃",
                                     @"count":@"1",
                                     @"total_price":@"178",
                                     @"state":@"3"}]};
    _allData.array = dic[@"全部"];
    _willPayData.array = dic[@"待付款"];
    _noPayData.array = dic[@"未消费"];
    _willCommentData.array = dic[@"待评价"];
    
}


#pragma mark -- tableView的代理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger tag = tableView.tag;
    if (tag == 20) {
        return _allData.count;
    }else if(tag == 21) {
        return _willPayData.count;
    }else if (tag == 22) {
        return _noPayData.count;
    }else {
        return _willCommentData.count;
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrdelDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell) {
        cell = [[OrdelDetailCell alloc] init];
    }
    cell.backgroundColor = [UIColor colorWithRed:0.9412 green:0.9412 blue:0.9412 alpha:1.0];
    NSInteger tag = tableView.tag;
    [cell setIndexPath:indexPath];
    if (tag == 20) {
        [cell setData:_allData[indexPath.section]];
    }else if(tag == 21) {
        [cell setData:_willPayData[indexPath.section]];
    }else if (tag == 22) {
        [cell setData:_noPayData[indexPath.section]];
    }else {
        [cell setData:_willCommentData[indexPath.section]];
    }
    return cell;
    
}



/**
 *  切换导航的button
 *
 *  @param btn navBtn
 */
- (void)selectNavBtn:(UIButton *)btn {
    NSInteger index = btn.tag - 10;
    for (int i = 0; i < _navBtnArr.count; i++) {
        NavButton *navBtn = _navBtnArr[i];
        if (navBtn.tag == btn.tag) {
            [navBtn setSelected:YES];
            navBtn.view.backgroundColor = [UIColor colorWithRed:0.098 green:0.7137 blue:0.6784 alpha:1.0];
            _scrollView.contentOffset = CGPointMake(SCREEN_SIZE.width * index, 0);
        }else {
            [navBtn setSelected:NO];
            navBtn.view.backgroundColor = [UIColor whiteColor];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return KWIDTH(40);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return KWIDTH(80);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(50);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(50))];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH(10), SCREEN_SIZE.width, KWIDTH(40))];
    label.text = @"  订单编号：16081104344014091002";
    label.font = [UIFont systemFontOfSize:KWIDTH(12)];
    label.textColor = [UIColor lightGrayColor];
    label.backgroundColor = [UIColor whiteColor];
    [view addSubview:label];
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(40))];
    view.backgroundColor = [UIColor whiteColor];
    NSInteger tag = tableView.tag;
    
    NextButton *nextBtn = [[NextButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(50), KWIDTH(5), KWIDTH(40), KWIDTH(30))];
    
    nextBtn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    [nextBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    nextBtn.layer.masksToBounds = YES;
    nextBtn.layer.cornerRadius = KWIDTH(2);
    nextBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    nextBtn.layer.borderWidth = 0.5f;
    
    
    if (tag == 20) {
        [nextBtn setData:_allData[section]];
    }else if (tag == 21) {
        [nextBtn setData:_willPayData[section]];
    }else if (tag == 22) {
        [nextBtn setData:_noPayData[section]];
    }else {
        [nextBtn setData:_willCommentData[section]];;
    }
    
    
    [nextBtn addTarget:self action:@selector(nextBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:nextBtn];
    return view;
}

- (void)nextBtnClick:(NextButton *)btn {
    NSLog(@"%@", btn.data);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -- scrollView的代理
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int index = _scrollView.contentOffset.x / SCREEN_SIZE.width;
    for (int i = 0; i < _navBtnArr.count; i++) {
        NavButton *navBtn = _navBtnArr[i];
        if (navBtn.tag == index + 10) {
            [navBtn setSelected:YES];
            navBtn.view.backgroundColor = [UIColor blueColor];
            
        }else {
            [navBtn setSelected:NO];
            navBtn.view.backgroundColor = [UIColor whiteColor];
        }
    }
}

- (void)refreshDataWithHeader:(MJRefreshNormalHeader *)header {
    NSLog(@"%ld", header.superview.tag);
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0   * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSLog(@"123");
        [header endRefreshing];
    });
    
}

- (void)loadNewDataWithFooter:(MJRefreshBackNormalFooter *)footer {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0   * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSLog(@"123");
        [footer endRefreshing];
    });
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

