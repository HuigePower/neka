//
//  ExtendListViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/21.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "ExtendListViewController.h"
#import "WHHTabbar.h"
@interface ExtendListViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong)UITableView *tableView;
@end

@implementation ExtendListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"推广列表";
    self.automaticallyAdjustsScrollViewInsets = NO;

    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(30))];
    label.text = @"  您只要将您挑选的商品分享给朋友，购买后您即可获得丰厚的佣金。  ";
    label.adjustsFontSizeToFitWidth = YES;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:0.7961 green:0.4627 blue:0.0275 alpha:1.0];
    label.backgroundColor = [UIColor colorWithRed:0.9922 green:0.9569 blue:0.8431 alpha:1.0];
    [self.view addSubview:label];
    
    
    WHHTabbar *tabbar = [[WHHTabbar alloc] initTopTabbarWithFrame:CGRectMake(0, 0 + KWIDTH(30), SCREEN_SIZE.width, KWIDTH(60)) andTitles:@[@"未结算", @"全部", @"已结算", @"已退款"] imageNames:@[@"money", @"order", @"money", @"order"] selectedImageNames:@[@"meal", @"pay", @"meal", @"pay"]];
    [self.view addSubview:tabbar];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0 + KWIDTH(90), SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - KWIDTH(30)) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"myCell"];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(0.1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return KWIDTH(5);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
