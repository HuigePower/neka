//
//  MyCouponViewController.m
//  Neka1.0
//
//  Created by ma c on 2017/3/10.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "MyCouponViewController.h"

@interface MyCouponViewController ()

@property (nonatomic, strong)UIButton *unuseBtn;
@property (nonatomic, strong)UIButton *expiredBtn;
@property (nonatomic, strong)UIButton *usedBtn;
@property (nonatomic, strong)NSArray *btnArr;
@end

@implementation MyCouponViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self makeSubviews];
    
}
//72366 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11 + 12  13920781079

- (void)makeSubviews {
    _unuseBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 64, SCREEN_SIZE.width / 3, 44)];
    [_unuseBtn setTitle:[NSString stringWithFormat:@"未使用(%d)", 1] forState:UIControlStateNormal];
    [self.view addSubview:_unuseBtn];
    
    _expiredBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 3, 64, SCREEN_SIZE.width / 3, 44)];
    [_expiredBtn setTitle:[NSString stringWithFormat:@"已过期(%d)", 1] forState:UIControlStateNormal];
    [self.view addSubview:_expiredBtn];
    
    _usedBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 3 * 2, 64, SCREEN_SIZE.width / 3, 44)];
    [_usedBtn setTitle:[NSString stringWithFormat:@"已使用(%d)", 1] forState:UIControlStateNormal];
    [self.view addSubview:_usedBtn];
    
    _btnArr = @[_unuseBtn, _expiredBtn, _usedBtn];
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
