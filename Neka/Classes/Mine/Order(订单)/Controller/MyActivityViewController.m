//
//  MyActivityViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/20.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "MyActivityViewController.h"
#import "WHHSegmentedControl.h"
#import "UITableView+EmptyData.h"
@interface MyActivityViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property (nonatomic, strong)WHHSegmentedControl *seg;
@property (nonatomic, strong)UIScrollView *sc;
@property (nonatomic, strong)UITableView *merchant;
@property (nonatomic, strong)UITableView *platform;
@property (nonatomic, strong)NSMutableArray *merchantActivities;
@property (nonatomic, strong)NSMutableArray *platformActivities;

@end

@implementation MyActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我参与过的活动";
    [self.tabBarController.tabBar setHidden:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self initDataSource];
    
    _sc = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0 + KWIDTH(40), SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - KWIDTH(40))];
    _sc.contentSize = CGSizeMake(SCREEN_SIZE.width * 2, SCREEN_SIZE.height - 64 - KWIDTH(40));
    _sc.delegate = self;
    _sc.pagingEnabled = YES;
    _sc.delegate = self;
    _sc.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_sc];
    
    _merchant = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - KWIDTH(40)) style:UITableViewStylePlain];
    _merchant.delegate = self;
    _merchant.dataSource = self;
    _merchant.tableFooterView = [[UIView alloc] init];
    [_merchant registerClass:[UITableViewCell class] forCellReuseIdentifier:@"myCell"];
    
    _platform = [[UITableView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - KWIDTH(40)) style:UITableViewStylePlain];
    _platform.delegate = self;
    _platform.dataSource = self;
    _platform.tableFooterView = [[UIView alloc] init];
    [_platform registerClass:[UITableViewCell class] forCellReuseIdentifier:@"myCell"];
    [_sc addSubview:_merchant];
    [_sc addSubview:_platform];
    
    NSArray *arr = @[@"平台活动", @"商家活动"];
    _seg = [[WHHSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(40)) andItems:arr];
    __block MyActivityViewController *weakSelf = self;
    _seg.resultBlock = ^(NSInteger index) {
        NSLog(@"%ld", (long)index);
        [UIView animateWithDuration:0.5 animations:^{
            weakSelf.sc.contentOffset = CGPointMake(SCREEN_SIZE.width * index, 0);
        }];
        
    };
    [self.view addSubview:_seg];
}

- (void)initDataSource {
    _merchantActivities = [[NSMutableArray alloc] init];
    _merchantActivities.array = @[@"1", @"2"];
    
    _platformActivities = [[NSMutableArray alloc] init];
    _platformActivities.array = @[@"2", @"3"];
    

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _merchant) {
        [tableView tableViewDisplayWithMsg:@"您还没有参加过活动呢" ifNecessaryForRowCount:_merchantActivities.count];
        return _merchantActivities.count;
    }else {
        [tableView tableViewDisplayWithMsg:@"您还没有参加过活动呢" ifNecessaryForRowCount:_platformActivities.count];
        return _platformActivities.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (tableView == _merchant) {
        cell.textLabel.text = _merchantActivities[indexPath.row];
    }else {
        cell.textLabel.text = _platformActivities[indexPath.row];
    }

    return cell;
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSInteger index = scrollView.contentOffset.x / SCREEN_SIZE.width;
    [_seg setBtnSelected:index + 10];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
