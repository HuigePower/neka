//
//  OrderDetailViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/19.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "OrderDetailViewController.h"

@interface OrderDetailViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong)UITableView *tableView;

@end

@implementation OrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"订单详情";
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - KWIDTH(40)) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];
    
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(80), SCREEN_SIZE.height - KWIDTH(40) + KWIDTH(5), KWIDTH(70), KWIDTH(30))];
    [btn setTitle:@"取消订单" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = KWIDTH(2);
    btn.layer.borderColor = [UIColor blackColor].CGColor;
    btn.layer.borderWidth = 0.5f;
    
    [self.view addSubview:btn];
    
};



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }else if (section == 1) {
        return 1;
    }else if (section == 2) {
        return 3;
    }else {
        return 2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(5), SCREEN_SIZE.width / 2, KWIDTH(30))];
            label.text = @"耐卡改装贴膜连锁";
            [cell addSubview:label];
            
            UIImageView *next = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(15), KWIDTH(7.5 + 5), KWIDTH(7.5), KWIDTH(15))];
            next.image = [UIImage imageNamed:@"tubiao2_11"];
            [cell addSubview:next];
        }else if (indexPath.row == 1) {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(60))];
            view.backgroundColor = [UIColor colorWithRed:0.949 green:0.949 blue:0.949 alpha:1.0];
            [cell addSubview:view];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(5), KWIDTH(5), KWIDTH(50), KWIDTH(50))];
            imageView.backgroundColor = [UIColor redColor];
            [view addSubview:imageView];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(70), KWIDTH(5), view.frame.size.width - KWIDTH(70), KWIDTH(20))];
            label.text = @"地图升级";
            label.font = [UIFont systemFontOfSize:KWIDTH(12)];
            [view addSubview:label];
            
            UILabel *count = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(70), KWIDTH(30), view.frame.size.width - KWIDTH(70), KWIDTH(20))];
            count.text = @"¥18 x 1";
            count.font = [UIFont systemFontOfSize:KWIDTH(12)];
            [view addSubview:count];
            
            UILabel *total = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(70), KWIDTH(200), KWIDTH(30))];
            total.text = @"实付款: ¥18";
            total.font = [UIFont boldSystemFontOfSize:KWIDTH(12)];
            [cell addSubview:total];
            
        }
        
    }else if (indexPath.section == 1) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(5), KWIDTH(100), KWIDTH(30))];
        label.text = @"商家信息";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [cell addSubview:label];
        
        UILabel *look = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(50), KWIDTH(5), KWIDTH(30), KWIDTH(30))];
        look.text = @"查看";
        look.textColor = [UIColor grayColor];
        look.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [cell addSubview:look];
        
        UIImageView *next = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(15), KWIDTH(7.5 + 5), KWIDTH(7.5), KWIDTH(15))];
        next.image = [UIImage imageNamed:@"tubiao2_11"];
        [cell addSubview:next];

    }else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(5), KWIDTH(100), KWIDTH(30))];
            label.text = @"团购券";
            label.font = [UIFont systemFontOfSize:KWIDTH(14)];
            [cell addSubview:label];
        }else if (indexPath.row == 1) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(80), KWIDTH(30))];
            label.text = @"消费密码: 1838 0925 1608 19";
            label.textColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1.0];
            label.font = [UIFont systemFontOfSize:KWIDTH(12)];
            [cell addSubview:label];
            
            UILabel *staus = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(80), KWIDTH(5), KWIDTH(70), KWIDTH(30))];
            staus.text = @"未消费";
            staus.textAlignment = NSTextAlignmentRight;
            staus.font = [UIFont systemFontOfSize:KWIDTH(12)];
            [cell addSubview:staus];
            
        }else if (indexPath.row == 2) {
            CGRect rect = [@"消费二维码:" boundingRectWithSize:CGSizeMake(KWIDTH(200), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:KWIDTH(12)]} context:nil];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), KWIDTH(5), rect.size.width, KWIDTH(30))];
            label.text = @"消费二维码:";
            label.textColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1.0];
            label.font = [UIFont systemFontOfSize:KWIDTH(12)];
            [cell addSubview:label];
        
            UIButton *lookBtn = [[UIButton alloc]initWithFrame:CGRectMake(KWIDTH(20) + rect.size.width, KWIDTH(5), rect.size.width, KWIDTH(30))];
            [lookBtn setTitle:@"查看二维码" forState:UIControlStateNormal];
            [lookBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [lookBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
            lookBtn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
            [lookBtn addTarget:self action:@selector(lookQRCode) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:lookBtn];
            
        }
    }else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(5), KWIDTH(100), KWIDTH(30))];
            label.text = @"订单详情";
            label.font = [UIFont systemFontOfSize:KWIDTH(14)];
            [cell addSubview:label];

        }else {
            NSArray *arr = @[@"订单编号: 16081909183322919402", @"下单时间: 2016-08-19 09:18", @"手机号: 15701690165", @"平台余额抵扣: 18.00元", @"付款方式: 余额支付", @"付款时间: 2016-08-19 09:18"];
            for (int i = 0; i < 6; i++) {
                
                UIView *view = [[UIView alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(13) + KWIDTH(20) * i, KWIDTH(4), KWIDTH(4))];
                view.backgroundColor = [UIColor blackColor];
                view.layer.masksToBounds = YES;
                view.layer.cornerRadius = KWIDTH(2);
                [cell addSubview:view];
                
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), KWIDTH(5) + KWIDTH(20) * i, SCREEN_SIZE.width - KWIDTH(30), KWIDTH(20))];
                label.text = arr[i];
                label.font = [UIFont systemFontOfSize:KWIDTH(12)];
                [cell addSubview:label];
            }
        }
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return KWIDTH(40);
        }else if (indexPath.row == 1) {
            return KWIDTH(100);
        }
    }else if (indexPath.section == 1) {
        return KWIDTH(40);
    }else if (indexPath.section == 2) {
        return KWIDTH(40);
    }else {
        if (indexPath.row == 0) {
            return KWIDTH(40);
        }else if (indexPath.row == 1) {
            return KWIDTH(130);
        }
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(0.1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return KWIDTH(10);
}


- (void)lookQRCode {
    NSLog(@"查看二维码");
}


#pragma mark -- cell的分割线宽度等于屏幕宽度
-(void)viewDidLayoutSubviews {
    
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)])  {
        [_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
