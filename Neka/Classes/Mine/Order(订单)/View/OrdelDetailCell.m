//
//  OrdelDetailCell.m
//  Neka1.0
//
//  Created by ma c on 16/8/18.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "OrdelDetailCell.h"
#import "UIImageView+AFNetworking.h"
@implementation OrdelDetailCell

- (instancetype)init {
    if (self = [super init]) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(5), KWIDTH(10), KWIDTH(60), KWIDTH(60))];
        [self addSubview:_icon];
        
        _title = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(70), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(80), KWIDTH(20))];
        _title.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [self addSubview:_title];
        
        _detail = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(70), KWIDTH(30), SCREEN_SIZE.width - KWIDTH(80), KWIDTH(20))];
        _detail.font = [UIFont systemFontOfSize:KWIDTH(12)];
        _detail.textColor = [UIColor colorWithRed:0.5612 green:0.5612 blue:0.5612 alpha:1.0];
        [self addSubview:_detail];

    }
    return self;
}


- (void)setData:(NSDictionary *)data {
    [_icon setImageWithURL:[NSURL URLWithString:data[@"image_url"]] placeholderImage:[UIImage imageNamed:@"cart"]];
    _title.text = @"一分钱试吃";
    _detail.text = [NSString stringWithFormat:@"数量: %@ 总价: %@", data[@"count"], data[@"total_price"]];
}


- (void)setIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row % 2 == 0) {
        self.backgroundColor = [UIColor colorWithRed:0.9412 green:0.9412 blue:0.9412 alpha:1.0];
    }else {
        self.backgroundColor = [UIColor colorWithRed:0.7452 green:0.7416 blue:0.7488 alpha:1.0];
    }
    
    
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
