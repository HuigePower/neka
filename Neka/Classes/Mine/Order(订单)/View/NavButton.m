//
//  NavButton.m
//  NEKA
//
//  Created by ma c on 16/4/22.
//  Copyright © 2016年 ma c. All rights reserved.
/// 自定义导航Button

#import "NavButton.h"

@implementation NavButton


+ (id)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)color selectedColor:(UIColor *)selectedColor {
    
    NavButton *btn = [[NavButton alloc] initWithFrame:frame title:title titleColor:color selectedColor:selectedColor];
    
    return btn;
}

- (id)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)color selectedColor:(UIColor *)selectedColor {
    
    if (self = [super init]) {
        self.frame = frame;
        [self setTitle:title forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont systemFontOfSize:12];
        [self setTitleColor:color forState:UIControlStateNormal];
        [self setTitleColor:selectedColor forState:UIControlStateSelected];
        _view = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 2, self.frame.size.width, 2)];
        [self addSubview:_view];
    }
    return self;
}





@end
