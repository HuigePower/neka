//
//  WHHButton.h
//  Neka1.0
//
//  Created by ma c on 16/8/20.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WHHButton : UIButton
@property (nonatomic, strong)NSString *markStr;
@property (nonatomic, strong)UILabel *contentLabel;

- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title image:(NSString *)imageName selectedImage:(NSString *)selectedImageName;

- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title number:(NSString *)num;

- (void)setContentLabelWithNumber:(NSString *)num;

- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title image:(NSString *)imageName;
- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title imageUrl:(NSString *)imageUrl;


- (void)addLine;


- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title icon:(NSString *)iconName hadPrise:(BOOL)hadPrise;


@end
