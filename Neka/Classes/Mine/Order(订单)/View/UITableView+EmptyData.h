//
//  UITableView+EmptyData.h
//  Neka1.0
//
//  Created by ma c on 16/8/21.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (EmptyData)
- (void)tableViewDisplayWithMsg:(NSString *)message ifNecessaryForRowCount:(NSInteger)rowCount;
@end
