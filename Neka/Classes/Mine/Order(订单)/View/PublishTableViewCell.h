//
//  PublishTableViewCell.h
//  Neka1.0
//
//  Created by ma c on 16/9/11.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^DeleteItem)(NSDictionary *data);
@interface PublishTableViewCell : UITableViewCell
@property (nonatomic, strong)UILabel *titleLabel;
@property (nonatomic, strong)UILabel *dateLabel;
@property (nonatomic, strong)UIButton *deleteButton;

@property (nonatomic, copy)DeleteItem deleteItem;
@property (nonatomic, strong)NSDictionary *data;

- (void)makeContentWithData:(NSDictionary *)data;
- (void)addDeleteButtonForSubview;


@end
