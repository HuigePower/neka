//
//  NextButton.m
//  Neka1.0
//
//  Created by ma c on 16/8/19.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "NextButton.h"

@implementation NextButton

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = KWIDTH(5);
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.layer.borderWidth = 0.5f;
    }
    return self;
}


- (void)setData:(NSDictionary *)data {
    _data = data;
    if ([_data[@"state"] isEqualToString:@"1"]) {
        [self setTitle:@"付款" forState:UIControlStateNormal];
        self.frame = CGRectMake(SCREEN_SIZE.width - KWIDTH(50), KWIDTH(5), KWIDTH(40), KWIDTH(30));
    }else if ([_data[@"state"] isEqualToString:@"2"]) {
        [self setTitle:@"取消订单" forState:UIControlStateNormal];
        self.frame = CGRectMake(SCREEN_SIZE.width - KWIDTH(90), KWIDTH(5), KWIDTH(80), KWIDTH(30));
    }else if ([_data[@"state"] isEqualToString:@"3"]) {
        [self setTitle:@"评价" forState:UIControlStateNormal];
        self.frame = CGRectMake(SCREEN_SIZE.width - KWIDTH(50), KWIDTH(5), KWIDTH(40), KWIDTH(30));
    }
    
}


@end
