//
//  WHHTabbar.m
//  Neka1.0
//
//  Created by ma c on 16/8/21.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "WHHTabbar.h"
#import "WHHButton.h"
@implementation WHHTabbar

- (instancetype)initTopTabbarWithFrame:(CGRect)frame andTitles:(NSArray *)titles imageNames:(NSArray *)imageNames selectedImageNames:(NSArray *)selectedImageNames {
    if (self = [super initWithFrame:frame]) {
        _btnArr = [[NSMutableArray alloc] init];
        for (int i = 0; i < titles.count; i++) {
            WHHButton *btn = [[WHHButton alloc] initWithFrame:CGRectMake(frame.size.width / titles.count * i, 0, frame.size.width / titles.count, frame.size.height) andTitle:titles[i] image:imageNames[i] selectedImage:selectedImageNames[i]];
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
            btn.tag = 10 + i;
            [_btnArr addObject:btn];
            btn.tag = 10 + i;
            if (i == 0) {
                [btn setSelected:YES];
            }
        }
    }
    return self;
}

- (void)setSelectedColor:(UIColor *)color {
    for (UIButton *btn in _btnArr) {
        [btn setTitleColor:color forState:UIControlStateSelected];
    }
}


- (void)btnClick:(WHHButton *)btn {
    if (_selectedBlock) {
        self.selectedBlock(btn.tag - 10);
    }
    for (WHHButton *itemBtn in _btnArr) {
        if (itemBtn == btn) {
            [itemBtn setSelected:YES];
        }else {
            [itemBtn setSelected:NO];
        }
    }
}

- (void)changeSelectBtnWithIndex:(NSInteger)index {
    
    for (WHHButton *itemBtn in _btnArr) {
        if (itemBtn.tag == index + 10) {
            [itemBtn setSelected:YES];
        }else {
            [itemBtn setSelected:NO];
        }
    }
}


@end
