//
//  OwnerCertificationViewController.m
//  Neka1.0
//
//  Created by ma c on 2017/1/5.
//  Copyright © 2017年 ma c. All rights reserved.
//  车主认证

#import "OwnerCertificationViewController.h"
#import "CustomTableViewCell.h"
#import "UITextField+BottomLine.h"
#import "UILabel+AlignmentBothSides.h"
#import "HGPushPicker.h"
#import "HGUnfoldButton.h"
@interface OwnerCertificationViewController ()<UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)UITextField *nameTF;
@property (nonatomic, strong)UITextField *idNumberTF;
@property (nonatomic, strong)UITextField *carNumberTF;
@property (nonatomic, strong)UITextField *carTypeTF;
@property (nonatomic, strong)UITextField *drivingTF;
@property (nonatomic, strong)UITextField *carAgeTF;
@property (nonatomic, strong)UITextField *carPriceTF;

//车牌号省份
//@property (nonatomic, strong)UIButton *pickerBtn;
@property (nonatomic, strong)HGPushPicker *picker;
@property (nonatomic, strong)HGUnfoldButton *pickerBtn;

@property (nonatomic, strong)UIButton *idCardZM;
@property (nonatomic, strong)UIButton *idCardBM;
@property (nonatomic, strong)UIButton *driveCardZM;
@property (nonatomic, strong)UIButton *driveCardBM;
@property (nonatomic, assign)NSInteger index;

@property (nonatomic, strong)UIImage *idCardZMImage;
@property (nonatomic, strong)UIImage *idCardBMImage;
@property (nonatomic, strong)UIImage *driveCardZMImage;
@property (nonatomic, strong)UIImage *driveCardBMImage;

@property (nonatomic, strong)NSArray *tfArray;
@end

@implementation OwnerCertificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"车主认证";
    
    [self makeSubviews];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    [_tableView registerClass:[CustomTableViewCell class] forCellReuseIdentifier:@"myCell"];
    
    
}

- (void)makeSubviews {
    _nameTF = [UITextField new];
    _idNumberTF = [UITextField new];
    _carNumberTF = [UITextField new];
    _carTypeTF = [UITextField new];
    _drivingTF = [UITextField new];
    _carAgeTF = [UITextField new];
    _carPriceTF = [UITextField new];
    
    _tfArray = @[_nameTF, _idNumberTF, _carNumberTF, _carTypeTF, _drivingTF, _carAgeTF, _carPriceTF];
    NSArray *placeholderArray = @[@"请输入姓名", @"请输入身份证号", @"请输入车牌号", @"请输入车辆车型", @"请输入驾龄", @"请输入车龄", @"请输入车龄裸车价"];
    for (int i = 0; i < _tfArray.count; i++) {
        UITextField *tf = _tfArray[i];
        tf.frame = CGRectMake(KWIDTH(105), 0, KWIDTH(205), 50);
        tf.font = [UIFont systemFontOfSize:KWIDTH(14)];
        tf.placeholder = placeholderArray[i];
        [tf addBottomLine];
    }
    
    _carNumberTF.frame = CGRectMake(KWIDTH(200), 0, KWIDTH(110), 50);
    
    //省份选择
    
    _pickerBtn = [[HGUnfoldButton alloc] initWithFrame:CGRectMake(KWIDTH(105), 10, KWIDTH(90), 30)];
    [_pickerBtn setTitle:@"京(北京)" forState:UIControlStateNormal];
    [_pickerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_pickerBtn addTarget:self action:@selector(showPicker) forControlEvents:UIControlEventTouchUpInside];

    
    _picker = [[HGPushPicker alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height - 220, SCREEN_SIZE.width, 220)];
    _picker.makeDataSource = ^(NSString *str) {
        return str;
    };
    
    __weak UIButton *weakPickerBtn = _pickerBtn;
    __weak HGPushPicker *weakPicker = _picker;
    _picker.resultBlock = ^(NSString *str) {
        [weakPickerBtn setTitle:str forState:UIControlStateNormal];
        [weakPicker removeFromSuperview];
    };
    [_picker setDataSource:@[@"京",@"津",@"沪",@"渝",@"蒙",@"新",@"藏",@"宁",@"桂",@"港",@"澳",@"黑",@"吉",@"辽",@"晋",@"冀",@"青",@"鲁",@"豫",@"苏",@"皖",@"浙",@"闽",@"赣",@"湘",@"鄂",@"粤",@"琼",@"甘",@"陕",@"黔",@"滇",@"川"]];
    
//     @[@"京(北京)",@"津(天津)",@"沪(上海)",@"渝(重庆)",@"蒙(内蒙古自治区)",@"新(维吾尔自治区)",@"藏(西藏自治区)",@"宁(宁夏回族自治区)",@"桂(广西壮族自治区)",@"港(香港特别行政区)",@"澳(澳门特别行政区)",@"黑(黑龙江省)",@"吉(吉林省)",@"辽(辽宁省)", @"晋(山西省)",@"冀(河北省)",@"青(青海省)",@"鲁(山东省)",@"豫(河南省)",@"苏(江苏省)",@"皖(安徽省)",@"浙(浙江省)",@"闽(福建省)", @"赣(江西省)", @"湘(湖南省)", @"鄂(湖北省)", @"粤(广东省)", @"琼(海南省)", @"甘(甘肃省)", @"陕(陕西省)", @"黔(贵州省)", @"滇(云南省)", @"川(四川省)"]
  
    
    
    
    //身份证
    //正面
    _idCardZM = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 10, KWIDTH(145), KWIDTH(85))];
    [_idCardZM setImage:[UIImage imageNamed:@"yz_zm"] forState:UIControlStateNormal];
    [_idCardZM addTarget:self action:@selector(selectPhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    //反面
    _idCardBM = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(165), 10, KWIDTH(145), KWIDTH(85))];
    [_idCardBM setImage:[UIImage imageNamed:@"yz_bm"] forState:UIControlStateNormal];
    [_idCardBM addTarget:self action:@selector(selectPhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    //驾驶证
    //正面
    _driveCardZM = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(115), 15, KWIDTH(195), KWIDTH(125))];
    [_driveCardZM setImage:[UIImage imageNamed:@"yz_sc"] forState:UIControlStateNormal];
    [_driveCardZM addTarget:self action:@selector(selectPhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    //反面
    _driveCardBM = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(115), 15, KWIDTH(195), KWIDTH(125))];
    [_driveCardBM setImage:[UIImage imageNamed:@"yz_sc"] forState:UIControlStateNormal];
    [_driveCardBM addTarget:self action:@selector(selectPhoto:) forControlEvents:UIControlEventTouchUpInside];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 7;
    }else {
        return 5;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell != nil) {
        [cell deleteSubviews];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0) {
        
        UIImageView *xing = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(10), (50 - KWIDTH(10)) / 2, KWIDTH(10), KWIDTH(10))];
        xing.image = [UIImage imageNamed:@"xing"];
        [cell addCustomSubview:xing];
        
        NSArray *tits = @[@"姓名", @"身份证号", @"车牌号", @"车辆车型", @"驾龄", @"车龄", @"车辆裸车价"];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), 0, KWIDTH(80), 50)];
        label.font = [UIFont systemFontOfSize:KWIDTH(14)];
        label.text = tits[indexPath.row];
        [label bothSidesOfAlignment];
        
        [cell addCustomSubview:label];
        [cell addCustomSubview:_tfArray[indexPath.row]];
        
        if (indexPath.row == 2) {
            [cell addCustomSubview:_pickerBtn];
        }
        
    }else {
        if (indexPath.row == 0) {
            UIImageView *xing = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(10), (50 - KWIDTH(10)) / 2, KWIDTH(10), KWIDTH(10))];
            xing.image = [UIImage imageNamed:@"xing"];
            [cell addCustomSubview:xing];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), 0, KWIDTH(80), 50)];
            label.font = [UIFont systemFontOfSize:KWIDTH(14)];
            label.text = @"上传文件";
            [label bothSidesOfAlignment];
            
            [cell addCustomSubview:label];
        }else if (indexPath.row == 1) {
           
            [cell addCustomSubview:_idCardZM];
            
            UILabel *labelZM = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 10 + KWIDTH(85), KWIDTH(145), 40)];
            labelZM.text = @"身份证正面";
            labelZM.font = [UIFont systemFontOfSize:KWIDTH(12)];
            labelZM.textAlignment = NSTextAlignmentCenter;
            [cell addCustomSubview:labelZM];
            
            
            [cell addCustomSubview:_idCardBM];
            
            UILabel *labelBM = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(165), 15 + KWIDTH(85), KWIDTH(145), 40)];
            labelBM.text = @"身份证背面";
            labelBM.font = [UIFont systemFontOfSize:KWIDTH(12)];
            labelBM.textAlignment = NSTextAlignmentCenter;
            [cell addCustomSubview:labelBM];
            
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 55 + KWIDTH(85) - 0.5, SCREEN_SIZE.width - KWIDTH(20), 0.5)];
            line.backgroundColor = [UIColor lightGrayColor];
            [cell addCustomSubview:line];
            
        }else if (indexPath.row == 2) {
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 15, KWIDTH(95), KWIDTH(125))];
            label.text = @"驾驶证正面上传";
            label.numberOfLines = 2;
            label.font = [UIFont systemFontOfSize:KWIDTH(12)];
            label.textAlignment = NSTextAlignmentCenter;
            [cell addCustomSubview:label];
            
            [cell addCustomSubview:_driveCardZM];

            UIImageView *unfold = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(105), 15, KWIDTH(90), 20)];
            unfold.image = [UIImage imageNamed:@"unfold_down"];
            [cell addCustomSubview:unfold];
            
            [cell addCustomSubview:_picker];
            
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 30 + KWIDTH(125) - 0.5, SCREEN_SIZE.width - KWIDTH(20), 0.5)];
            line.backgroundColor = [UIColor lightGrayColor];
            [cell addCustomSubview:line];
            
        }else  if (indexPath.row == 3) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 15, KWIDTH(95), KWIDTH(125))];
            label.text = @"驾驶证背面上传";
            label.numberOfLines = 2;
            label.font = [UIFont systemFontOfSize:KWIDTH(12)];
            label.textAlignment = NSTextAlignmentCenter;
            [cell addCustomSubview:label];
            
            [cell addCustomSubview:_driveCardBM];
            
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 30 + KWIDTH(125) - 0.5, SCREEN_SIZE.width - KWIDTH(20), 0.5)];
            line.backgroundColor = [UIColor lightGrayColor];
            [cell addCustomSubview:line];
            
        }else {
            
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(15), 10, SCREEN_SIZE.width - KWIDTH(30), KWIDTH(40))];
            btn.backgroundColor = mRGB(0, 185, 159, 1);
            [btn setTitle:@"提交" forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
            btn.layer.masksToBounds = YES;
            btn.layer.cornerRadius = KWIDTH(20);
            [btn addTarget:self action:@selector(commit) forControlEvents:UIControlEventTouchUpInside];
            [cell addCustomSubview:btn];
            
        }
        
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 50;
    }else {
        if (indexPath.row == 0) {
            return 50;
        }else if (indexPath.row == 1) {
            return 55 + KWIDTH(85);
        }else if (indexPath.row == 2 || indexPath.row == 3) {
            return 30 + KWIDTH(125);
        }else {
            return 20 + KWIDTH(40);
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return 0.1;
    }else {
        return 5;
    }
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
    [_picker removeFromSuperview];
}

- (void)selectPhoto :(UIButton *)btn {
    if (btn == _idCardZM) {
        _index = 0;
        NSLog(@"身份证正面");
    }else if (btn == _idCardBM) {
        _index = 1;
        NSLog(@"身份证反面");
    }else if (btn == _driveCardZM) {
        _index = 2;
        NSLog(@"驾驶证正面");
    }else {
        _index = 3;
        NSLog(@"驾驶证反面");
    }
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相机", @"相册", nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%ld", (long)buttonIndex);
    
    if (buttonIndex == 0) {
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
            
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                imagePicker.allowsEditing = YES;
                
                [self presentViewController:imagePicker animated:YES completion:nil];
            }
        }else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"相机不可用" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
            [alert show];
        }
    }else {
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] || [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            imagePicker.allowsEditing = YES;
            
            [self presentViewController:imagePicker animated:YES completion:nil];
            
            
        }else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"访问相册受限" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    NSLog(@"%@", info);
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSLog(@"found an image");
        NSData *data = UIImageJPEGRepresentation(image, 1.0f);
        if (_index == 0) {
            [_idCardZM setImage:image forState:UIControlStateNormal];
//            _zmImage = image;
        }else if (_index == 1) {
            [_idCardBM setImage:image forState:UIControlStateNormal];
//            _bmImage = image;
        }else if (_index == 2) {
            [_driveCardZM setImage:image forState:UIControlStateNormal];
//            _scImage = image;
        }else {
            [_driveCardBM setImage:image forState:UIControlStateNormal];
        }
        
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)showPicker {
    [self.view addSubview:_picker];
}


- (void)commit {
    NSLog(@"提交");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
