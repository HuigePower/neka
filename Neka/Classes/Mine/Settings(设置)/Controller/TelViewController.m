//
//  TelViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/11.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "TelViewController.h"
#import "HGVerificationButton.h"
@interface TelViewController ()
@property (nonatomic, strong)UITextField *telTF;
@property (nonatomic, strong)UITextField *testNumber;
@property (nonatomic, strong)HGVerificationButton *verficationBtn;


@end

@implementation TelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"绑定手机号码";
    
    self.view.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0 + KWIDTH(10), SCREEN_SIZE.width, 88)];
    view.backgroundColor = [UIColor whiteColor];
    _telTF = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(10), 0, SCREEN_SIZE.width - KWIDTH(20), 44)];
    _telTF.placeholder = @"手机号";
    _telTF.font = [UIFont systemFontOfSize:KWIDTH(14)];
    [view addSubview:_telTF];
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 43.75, SCREEN_SIZE.width - KWIDTH(20), 0.5)];
    line.backgroundColor = [UIColor lightGrayColor];
    [view addSubview:line];
    
    _testNumber = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(10), 44, SCREEN_SIZE.width / 2, 44)];
    _testNumber.placeholder = @"请填写短信验证码";
    _testNumber.font = [UIFont systemFontOfSize:KWIDTH(14)];
    [view addSubview:_testNumber];
    
    _verficationBtn = [[HGVerificationButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(110), 51, KWIDTH(100), 30)];
    [_verficationBtn addTarget:self action:@selector(getTestNumber:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_verficationBtn];
    
    [self.view addSubview:view];
    
    UIButton *next = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 0 + KWIDTH(10) + 100, SCREEN_SIZE.width - KWIDTH(20), KWIDTH(40))];
    next.backgroundColor = mRGB(13, 103, 214, 1);
    next.layer.masksToBounds = YES;
    next.layer.cornerRadius = KWIDTH(2);
    [next setTitle:@"下一步" forState:UIControlStateNormal];
    next.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    [next addTarget:self action:@selector(next:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:next];
    
}

- (void)getTestNumber:(id)sender {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"phone": _telTF.text, @"type": @"3"};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Login&a=sendCode", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"已发送"];
        }else {
            [MBProgressHUD showSuccess:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
    }];
    
    
}


- (void)next:(id)sender {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"phone": _telTF.text, @"code": _testNumber.text, @"type": @"2", @"client": @"1"};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Login&a=modify_phone", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"修改成功"];
            [[HGUserDataManager shareManager] setPhone:_telTF.text];
            NSString *phone_s = [_telTF.text stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
            NSLog(@"%@", phone_s);
            [[HGUserDataManager shareManager] setPhone_s:phone_s];
            sleep(1);
            if (_backViewController != nil) {
                [self.navigationController popToViewController:_backViewController animated:YES];
            }
        }else {
            
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
    }];
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
