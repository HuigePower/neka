//
//  HGAddressListViewController.h
//  Neka
//
//  Created by ma c on 2017/6/20.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGAddressModel.h"

typedef void(^AddressSelectResult)(HGAddressModel *model);
@interface HGAddressSelectViewController : UIViewController

@property (nonatomic, copy)AddressSelectResult resultBlock;

@end
