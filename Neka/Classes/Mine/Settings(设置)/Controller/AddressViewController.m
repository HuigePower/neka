//
//  AddressViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/12.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "AddressViewController.h"
#import "AddressCell.h"
#import "AddAddressViewController.h"
#import "AddressInfoModel.h"

@interface AddressViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSMutableArray *dataSource;
@property (nonatomic, strong)NSIndexPath *tmpIndexPath;
@end

@implementation AddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"收货地址管理";
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _dataSource = [[NSMutableArray alloc] init];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - 44) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.bounces = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tableView];

    
    UIButton *addBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height - 44 - 64, SCREEN_SIZE.width, 44)];
    [addBtn setTitle:@"新建收货地址" forState:UIControlStateNormal];
    [addBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    addBtn.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nav_bg"]];
    [addBtn addTarget:self action:@selector(addNewAddress) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addBtn];

    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initDataSource];
}



- (void)initDataSource {
    [_dataSource removeAllObjects];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=adress", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSLog(@"%@", responseObject);
        NSArray *array = responseObject[@"result"];
        for (int i = 0; i < array.count; i++) {
            AddressInfoModel *model = [[AddressInfoModel alloc] initWithDic:array[i]];
            if ([model.defaults isEqualToString:@"1"]) {
                [_dataSource insertObject:model atIndex:0];
            }else {
                [_dataSource addObject:model];
            }
            
            [_tableView reloadData];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@", error.description);
        [self hideLoading];
    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataSource.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[AddressCell alloc] init];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.indexPath = indexPath;
    [cell setModel:_dataSource[indexPath.section]];
//    cell.editBlock = ^(NSIndexPath *indexPath) {
//        NSLog(@"编辑");
//        AddAddressViewController *edit = [[AddAddressViewController alloc] init];
//        edit.btnTitle = @"保存";
//        [edit setAddressInfo:_dataSource[indexPath.section]];
//        [self.navigationController pushViewController:edit animated:YES];
//    };
//    
//    cell.deleteBlock = ^(NSIndexPath *indexPath) {
//        _tmpIndexPath = indexPath;
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"你确定要删除此地址吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//        [alert show];
////        [_dataSource removeObjectAtIndex:indexPath.section];
////        [_tableView reloadData];
//    };
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressInfoModel *model = _dataSource[indexPath.section];
    NSLog(@"model.adress_id:%@", model.adress_id);
    AddAddressViewController *addAddress = [[AddAddressViewController alloc] init];
    addAddress.addressId = [NSString stringWithFormat:@"%@", model.adress_id];
    
    addAddress.isAddNewAddress = NO;
    [self.navigationController pushViewController:addAddress animated:YES];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 5;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return KWIDTH(5);
}



- (void)addNewAddress {
    
    NSLog(@"添加新地址");
    AddAddressViewController *addAddress = [[AddAddressViewController alloc] init];
    addAddress.isAddNewAddress = YES;
    [self.navigationController pushViewController:addAddress animated:YES];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
