//
//  ProvinceViewController.h
//  Neka1.0
//
//  Created by ma c on 16/8/11.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^SelectedBlock)(NSString *result);
@interface ProvinceViewController : UIViewController
@property (nonatomic, strong)NSString *selected;

@property (nonatomic, copy)SelectedBlock result;

@end
