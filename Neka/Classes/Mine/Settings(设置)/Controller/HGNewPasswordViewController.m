//
//  HGNewPasswordViewController.m
//  Neka1.0
//
//  Created by ma c on 2016/12/30.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGNewPasswordViewController.h"

@interface HGNewPasswordViewController ()
@property (nonatomic, strong)UITextField *oldpasswordTF;
@property (nonatomic, strong)UITextField *newpasswordTF1;
@property (nonatomic, strong)UITextField *newpasswordTF2;

@end

@implementation HGNewPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _oldpasswordTF = [UITextField new];
    _oldpasswordTF.placeholder = @"请输入当前密码";
    [self.view addSubview:_oldpasswordTF];
    
    _newpasswordTF1 = [UITextField new];
    _newpasswordTF1.placeholder = @"请输入新密码(密码长度在6-32个字符之间)";
    [self.view addSubview:_newpasswordTF1];
    
    _newpasswordTF2 = [UITextField new];
    _newpasswordTF2.placeholder = @"请再次输入一次新密码";
    [self.view addSubview:_newpasswordTF2];
    
    NSArray *arr = @[_oldpasswordTF, _newpasswordTF1, _newpasswordTF2];
    for (int i = 0; i < 3; i++) {
        UITextField *tf = arr[i];
        tf.frame = CGRectMake(KWIDTH(10), 0 + 44 * i, SCREEN_SIZE.width - KWIDTH(20), 44);
        tf.font = [UIFont systemFontOfSize:KWIDTH(14)];
    }
    
    
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 0 + 44 * 3 + KWIDTH(10), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(50))];
    [btn setTitle:@"确认提交" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:KWIDTH(14)];
    btn.backgroundColor = mRGB(13, 103, 214, 1);
//    [btn addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = KWIDTH(5);
    [self.view addSubview:btn];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
