//
//  SettingViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/10.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "SettingViewController.h"
#import "NicknameViewController.h"
#import "AddAddressViewController.h"
#import "AddressViewController.h"
#import "CertificationViewController.h"
#import "EntityViewController.h"
#import "AboutUsViewController.h"
#import "VerificationTelViewController.h"
#import "OwnerCertificationViewController.h"
#import "HGWebAgentSetting.h"
@interface SettingViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSArray *titles;
@property (nonatomic, assign)BOOL noAddress;
@property (nonatomic, strong)UILabel *nicknameLabel;
@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"设置";
    [self.tabBarController.tabBar setHidden:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self initDataSource];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStyleGrouped];
//    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];  
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.nicknameLabel.text = [[HGUserDataManager shareManager] nickname];
}

- (void)initDataSource {
    _titles = @[@[@"昵称", @"修改密码", @"修改手机号", @"收货地址", @"我的实体卡"],
                @[@"关于我们"]];
}

- (UILabel *)nicknameLabel {
    if (_nicknameLabel == nil) {
        _nicknameLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(120), 0, KWIDTH(90), 44)];
        _nicknameLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
        _nicknameLabel.textAlignment = NSTextAlignmentRight;
    }
    return _nicknameLabel;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _titles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_titles[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = _titles[indexPath.section][indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        [cell addSubview:self.nicknameLabel];
    }
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return KWIDTH(5);
    }else {
        return KWIDTH(0.1);
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return KWIDTH(70);
    }
    
    return KWIDTH(5);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 60)];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 20, SCREEN_SIZE.width - KWIDTH(20), 40)];
        [btn setTitle:@"退出登录" forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
        btn.backgroundColor = mRGB(0, 126, 222, 1);
        [btn addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = KWIDTH(5);
        [view addSubview:btn];
        return view;
    }
    return nil;
}

- (void)logout:(id)sender {
    [UserDefaultManager saveTicket:@"NULL"];
    [HGWebAgentSetting updateWebAgentSetting];
    [self.navigationController popViewControllerAnimated:YES];
//    [[NSNotificationCenter defaultCenter] postNotificationName:K_LOGOUT object:nil];
    NSLog(@"退出登录");
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            NicknameViewController *nicknameVC = [[NicknameViewController alloc] init];
            [self.navigationController pushViewController:nicknameVC animated:YES];
        }else if (indexPath.row == 1) {
            
            VerificationTelViewController *ver = [[VerificationTelViewController alloc] initWithVerificationTelAim:VerificationTelAimForChangePassword andBackViewcontroller:self];
            [self.navigationController pushViewController:ver animated:YES];
            
            
        }else if (indexPath.row == 2) {
            VerificationTelViewController *ver = [[VerificationTelViewController alloc] initWithVerificationTelAim:VerificationTelAimForChangeTelNumber andBackViewcontroller:self];
            [self.navigationController pushViewController:ver animated:YES];
            
        }else if (indexPath.row == 3) {
            AddressViewController *address = [[AddressViewController alloc] init];
            [self.navigationController pushViewController:address animated:YES];
            
        }else if (indexPath.row == 4) {
            
            //实体卡
            EntityViewController *entity = [[EntityViewController alloc] init];
            [self.navigationController pushViewController:entity animated:YES];
    
        }
        
    }else if (indexPath.section == 1) {
        AboutUsViewController *aboutUs = [[AboutUsViewController alloc] init];
        [self.navigationController pushViewController:aboutUs animated:YES];

    }
    
    //身份验证
//    CertificationViewController *certification = [[CertificationViewController alloc] init];
//    [self.navigationController pushViewController:certification animated:YES];
    
    //车主认证
//    OwnerCertificationViewController *ownerCer = [[OwnerCertificationViewController alloc] init];
//    [self.navigationController pushViewController:ownerCer animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
