//
//  HGAddressListViewController.m
//  Neka
//
//  Created by ma c on 2017/6/20.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGAddressSelectViewController.h"
#import "CustomTableViewCell.h"
@interface HGAddressSelectViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property (nonatomic, strong)NSMutableArray *dataSource;
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)UITextField *searchTF;
@end

@implementation HGAddressSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"查看地址";
    [self initDataSource];

    _searchTF = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, SCREEN_SIZE.width - 20, 44)];
    _searchTF.placeholder = @"请输入小区、大厦或学校";
//    _searchTF.returnKeyType = UIReturnKeySearch;
    _searchTF.backgroundColor = [UIColor whiteColor];
    _searchTF.layer.masksToBounds = YES;
    _searchTF.layer.cornerRadius = 5;
    _searchTF.leftViewMode = UITextFieldViewModeAlways;
    _searchTF.delegate = self;
    [_searchTF addTarget:self action:@selector(searchTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, 20, 20)];
    imageView.image = [UIImage imageNamed:@"location_black"];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [view addSubview:imageView];
    _searchTF.leftView = view;
    
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    [_tableView registerClass:[CustomTableViewCell class] forCellReuseIdentifier:@"cell"];
    
    
    
}

- (void)initDataSource {
    _dataSource = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"location": [NSString stringWithFormat:@"%@,%@", LATITUDE, LONGITUDE]};
    [self showLoading];
    [manager POST:@"https://api.map.baidu.com/geocoder/v2/?ak=4c1bb2055e24296bbaef36574877b4e2&output=json&pois=1" parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];

        NSDictionary *result = responseObject[@"result"];
        NSArray *pois = result[@"pois"];
        for (NSDictionary *data in pois) {
            HGAddressModel *model = [HGAddressModel new];
            model.name = data[@"name"];
            model.address = data[@"addr"];
            model.latitude = [NSString stringWithFormat:@"%@", data[@"point"][@"y"]];
            model.longitude = [NSString stringWithFormat:@"%@", data[@"point"][@"x"]];
            model.isRecommend = NO;
            [_dataSource addObject:model];
        }
        NSString *name = [NSString stringWithFormat:@"%@", result[@"sematic_description"]];
        if (name.length > 0) {
            HGAddressModel *model = [HGAddressModel new];
            model.name = result[@"sematic_description"];
            model.address = result[@"formatted_address"];
            model.latitude = [NSString stringWithFormat:@"%@", result[@"location"][@"lat"]];
            model.longitude = [NSString stringWithFormat:@"%@", result[@"location"][@"lng"]];
            model.isRecommend = YES;
            [_dataSource insertObject:model atIndex:0];
        }
        
        [_tableView reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@", error);
        [self hideLoading];
    }];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell) {
        [cell deleteSubviews];
    }
    
    HGAddressModel *model = _dataSource[indexPath.row];
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(10,10 , 20, 20)];
    icon.image = [UIImage imageNamed:@"location_black"];
    [cell addCustomSubview:icon];
    
    //位置名称
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, SCREEN_SIZE.width - 50, 20)];
    nameLabel.font = [UIFont systemFontOfSize:14];
    if (model.isRecommend) {
        nameLabel.text = [NSString stringWithFormat:@"[推荐位置]%@", model.name];
        nameLabel.textColor = [UIColor redColor];
        
    }else {
        nameLabel.text = model.name;
        nameLabel.textColor = [UIColor blackColor];
    }
    [cell addCustomSubview:nameLabel];
    
    //地址
    UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 30, SCREEN_SIZE.width - 50, 30)];
    addressLabel.font = [UIFont systemFontOfSize:12];
    addressLabel.text = model.address;
    addressLabel.textColor = [UIColor blackColor];
    [cell addCustomSubview:addressLabel];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 64;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 64)];
    [view addSubview:_searchTF];
    
    return view;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HGAddressModel *model = _dataSource[indexPath.row];
    
    if (_resultBlock) {
        _resultBlock(model);
    }
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}




- (void)searchTextChanged:(UITextField *)tf {
    
    if (tf.text.length == 0) {
        NSLog(@"search为空");
        [self initDataSource];
    }else {
        [self goSearch];
    }
}


- (void)goSearch {
    if (_searchTF.text.length == 0) {
        [MBProgressHUD showError:@"请输入关键字"];
        return;
    }
    [_dataSource removeAllObjects];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"query": _searchTF.text};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Map&a=suggestion", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSLog(@"%@", responseObject);
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            NSArray *list = responseObject[@"result"];
            for (NSDictionary *data in list) {
                HGAddressModel *model = [HGAddressModel new];
                model.name = data[@"name"];
                model.address = data[@"address"];
                model.latitude = [NSString stringWithFormat:@"%@", data[@"lat"]];
                model.longitude = [NSString stringWithFormat:@"%@", data[@"lat"]];
                model.isRecommend = NO;
                [_dataSource addObject:model];
                
            }
            
            [_tableView reloadData];
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
    }];
}













- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
