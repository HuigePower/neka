//
//  CertificationViewController.m
//  Neka1.0
//
//  Created by ma c on 2017/1/4.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "CertificationViewController.h"
#import "UITextField+BottomLine.h"
@interface CertificationViewController ()<UIScrollViewDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic, strong)UITextField *nameTF;
@property (nonatomic, strong)UITextField *numberTF;

@property (nonatomic, strong)UIButton *pic1;
@property (nonatomic, strong)UIButton *pic2;
@property (nonatomic, strong)UIButton *pic3;

@property (nonatomic, assign)NSInteger index;

@property (nonatomic, strong)UIImage *zmImage;
@property (nonatomic, strong)UIImage *bmImage;
@property (nonatomic, strong)UIImage *scImage;

@end

@implementation CertificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"身份验证";
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
    scrollView.contentSize = CGSizeMake(SCREEN_SIZE.width, SCREEN_SIZE.height);
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    
    NSArray *arr1 = @[@"真实身份", @"身份证号", @"上传文件"];
    for (int i= 0; i < 3; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 50 * i, KWIDTH(60), 50)];
        label.text = arr1[i];
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [scrollView addSubview:label];
    }
    
    //姓名
    _nameTF = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(80), 0, KWIDTH(230), 50)];
    _nameTF.placeholder = @"请输入姓名";
    _nameTF.font = [UIFont systemFontOfSize:KWIDTH(12)];
    [_nameTF addBottomLine];
    [scrollView addSubview:_nameTF];
    
//    UILabel *line1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 49.5, KWIDTH(230), 0.5)];
//    line1.backgroundColor = [UIColor lightGrayColor];
//    [_nameTF addSubview:line1];
    
    //身份证
    _numberTF = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(80), 50, KWIDTH(230), 50)];
    _numberTF.placeholder = @"请输入姓名";
    _numberTF.font = [UIFont systemFontOfSize:KWIDTH(12)];
    [_numberTF addBottomLine];
    [scrollView addSubview:_numberTF];
//    UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 49.5, KWIDTH(230), 0.5)];
//    line2.backgroundColor = [UIColor lightGrayColor];
//    [_numberTF addSubview:line2];
    
    //正面
    _pic1 = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 150, KWIDTH(145), KWIDTH(85))];
    [_pic1 setImage:[UIImage imageNamed:@"yz_zm"] forState:UIControlStateNormal];
    [_pic1 addTarget:self action:@selector(selectPhoto:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:_pic1];
    
    //反面
    _pic2 = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(165), 150, KWIDTH(145), KWIDTH(85))];
    [_pic2 setImage:[UIImage imageNamed:@"yz_bm"] forState:UIControlStateNormal];
    [_pic2 addTarget:self action:@selector(selectPhoto:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:_pic2];
    
    UILabel *zmLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 150 + KWIDTH(85), KWIDTH(145), KWIDTH(40))];
    zmLabel.text = @"身份证正面";
    zmLabel.textAlignment = NSTextAlignmentCenter;
    zmLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    [scrollView addSubview:zmLabel];
    
    UILabel *bmLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(165), 150 + KWIDTH(85), KWIDTH(145), KWIDTH(40))];
    bmLabel.text = @"身份证背面";
    bmLabel.textAlignment = NSTextAlignmentCenter;
    bmLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    [scrollView addSubview:bmLabel];

    //手持
    _pic3 = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 150 + KWIDTH(85) + KWIDTH(40), KWIDTH(300), KWIDTH(185))];
    [_pic3 setImage:[UIImage imageNamed:@"yz_sc"] forState:UIControlStateNormal];
    [_pic3 addTarget:self action:@selector(selectPhoto:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:_pic3];
    
    UILabel *scLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 150 + KWIDTH(85) + KWIDTH(40) + KWIDTH(185) + KWIDTH(10),     KWIDTH(300), KWIDTH(20))];
    scLabel1.text = @"本人手持身份证照片";
    scLabel1.textAlignment = NSTextAlignmentCenter;
    scLabel1.font = [UIFont systemFontOfSize:KWIDTH(12)];
    [scrollView addSubview:scLabel1];
    
    UILabel *scLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 150 + KWIDTH(85) + KWIDTH(40) + KWIDTH(185) + KWIDTH(10) + KWIDTH(20), KWIDTH(300), KWIDTH(20))];
    scLabel2.text = @"(请确保姓名、身份证号码清晰)";
    scLabel2.textColor = [UIColor redColor];
    scLabel2.textAlignment = NSTextAlignmentCenter;
    scLabel2.font = [UIFont systemFontOfSize:KWIDTH(12)];
    [scrollView addSubview:scLabel2];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, scLabel2.frame.origin.y + KWIDTH(40), SCREEN_SIZE.width, KWIDTH(60))];
    view.backgroundColor = mRGB(240, 240, 240, 1);
    
    [scrollView addSubview:view];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(10), KWIDTH(300), KWIDTH(40))];
    [button setTitle:@"提交" forState:UIControlStateNormal];
    button.backgroundColor = mRGB(25, 183, 158, 1);
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = KWIDTH(20);
    [button addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];

    scrollView.contentSize = CGSizeMake(SCREEN_SIZE.width, view.frame.origin.y + KWIDTH(60));
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}


- (void)comment:(id)sender {
    NSLog(@"提交");
    if (_zmImage == nil || _bmImage == nil || _scImage == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请确认文件是否齐全!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
    }
    
    
    
    
}

- (void)selectPhoto :(UIButton *)btn {
    if (btn == _pic1) {
        _index = 0;
        NSLog(@"正面");
    }else if (btn == _pic2) {
        _index = 1;
        NSLog(@"反面");
    }else {
        _index = 2;
        NSLog(@"手持");
    }
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相机", @"相册", nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%ld", (long)buttonIndex);
    
    if (buttonIndex == 0) {
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
            
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                imagePicker.allowsEditing = YES;
                
                [self presentViewController:imagePicker animated:YES completion:nil];
            }
        }else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"相机不可用" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
            [alert show];
        }
    }else {
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] || [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            imagePicker.allowsEditing = YES;
            
            [self presentViewController:imagePicker animated:YES completion:nil];
            
            
        }else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"访问相册受限" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    NSLog(@"%@", info);
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSLog(@"found an image");
        NSData *data = UIImageJPEGRepresentation(image, 1.0f);
        if (_index == 0) {
            [_pic1 setImage:image forState:UIControlStateNormal];
            _zmImage = image;
        }else if (_index == 1) {
            [_pic2 setImage:image forState:UIControlStateNormal];
            _bmImage = image;
        }else {
            [_pic3 setImage:image forState:UIControlStateNormal];
            _scImage = image;
        }
        
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
