//
//  NicknameViewController.h
//  Neka1.0
//
//  Created by ma c on 16/8/10.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^NicknameChanged)(NSString *nickname);
@interface NicknameViewController : UIViewController

@property (nonatomic, copy)NicknameChanged nicknameChanged;
@property (nonatomic, copy)NSString *nickname;
@end
