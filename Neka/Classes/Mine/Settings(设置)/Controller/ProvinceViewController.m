//
//  ProvinceViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/11.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "ProvinceViewController.h"
#import "HGAreaManager.h"
#import "HGAreaModel.h"
@interface ProvinceViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSArray *provinces;

@end

@implementation ProvinceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"省份";
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _provinces = [HGAreaManager provinceList];
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];
    
    NSInteger index = [_provinces indexOfObject:_selected];
    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"%lu", (unsigned long)_provinces.count);
    
    return _provinces.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    HGAreaModel *model = _provinces[indexPath.row];
    cell.textLabel.text = model.area_name;
    cell.textLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    if ([_selected isEqualToString:_provinces[indexPath.row]]) {
        cell.backgroundColor = [UIColor lightGrayColor];
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return KWIDTH(30);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _selected = _provinces[indexPath.row];
    
    if (_result) {
        _result(_provinces[indexPath.row]);
    }
    [self.navigationController popViewControllerAnimated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}



@end
