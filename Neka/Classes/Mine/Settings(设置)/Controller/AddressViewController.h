//
//  AddressViewController.h
//  Neka1.0
//
//  Created by ma c on 16/8/12.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressViewController : UIViewController

@property (nonatomic, strong)NSString *name;
@property (nonatomic, strong)NSString *tel;
@property (nonatomic, strong)NSString *province;
@property (nonatomic, strong)NSString *city;
@property (nonatomic, strong)NSString *area;
@property (nonatomic, strong)NSString *location;
@property (nonatomic, strong)NSString *address;
@property (nonatomic, strong)NSString *postcard;
@property (nonatomic, assign)BOOL isDeafult;



@end
