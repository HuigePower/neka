//
//  PasswordViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/10.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "PasswordViewController.h"
#import "HGNewPasswordViewController.h"
@interface PasswordViewController ()
//@property (nonatomic, strong)UILabel *tel;
//@property (nonatomic, strong)UITextField *testNumber;
//@property (nonatomic, strong)NSTimer *timer;
//@property (nonatomic, strong)UIButton *getTestNumber;
//@property (nonatomic, strong)UILabel *timeCount;
//@property (nonatomic, assign)int time;

@property (nonatomic, strong)UITextField *newpasswordTF1;
@property (nonatomic, strong)UITextField *newpasswordTF2;
@end

@implementation PasswordViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    
    _newpasswordTF1 = [UITextField new];
    _newpasswordTF1.placeholder = @"请输入新密码(密码长度在6-32个字符之间)";
    [self.view addSubview:_newpasswordTF1];
    
    _newpasswordTF2 = [UITextField new];
    _newpasswordTF2.placeholder = @"请再次输入一次新密码";
    [self.view addSubview:_newpasswordTF2];
    
    NSArray *arr = @[_newpasswordTF1, _newpasswordTF2];
    for (int i = 0; i < 2; i++) {
        UITextField *tf = arr[i];
        tf.frame = CGRectMake(KWIDTH(10), 0 + 44 * i, SCREEN_SIZE.width - KWIDTH(20), 44);
        tf.font = [UIFont systemFontOfSize:KWIDTH(14)];
    }
    
    
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 0 + 44 * 2 + KWIDTH(10), SCREEN_SIZE.width - KWIDTH(20), 44)];
    [btn setTitle:@"确认提交" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:KWIDTH(14)];
    btn.backgroundColor = mRGB(13, 103, 214, 1);
    [btn addTarget:self action:@selector(confirmBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = 5;
    [self.view addSubview:btn];
    
}


- (void)confirmBtnClick:(UIButton *)btn {
    
    if (![_newpasswordTF1.text isEqualToString:_newpasswordTF2.text]) {
        [MBProgressHUD showError:@"两次密码不一致"];
        return;
    }
    
    if (_newpasswordTF1.text.length < 6 || _newpasswordTF2.text.length < 6) {
        [MBProgressHUD showError:@"密码不能少于6位"];
        return;
    }
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"phone": self.telNumber, @"passwd": _newpasswordTF2.text, @"code": self.code};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Login&a=forget", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"修改成功"];
            if (_backViewController != nil) {
                [self.navigationController popToViewController:_backViewController animated:YES];
            }
        }else {
            [MBProgressHUD showSuccess:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showSuccess:error.description];
    }];
    
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)viewDidLoad {
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//    self.title = @"验证原手机";
//    
//    self.view.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
//    
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0 + KWIDTH(10), SCREEN_SIZE.width, 88)];
//    view.backgroundColor = [UIColor whiteColor];
//    _tel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 0, SCREEN_SIZE.width - KWIDTH(20), 44)];
//    _tel.text = @"15701690165";
//    _tel.font = [UIFont systemFontOfSize:KWIDTH(14)];
//    [view addSubview:_tel];
//    
//    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 43.75, SCREEN_SIZE.width - KWIDTH(20), 0.5)];
//    line.backgroundColor = [UIColor lightGrayColor];
//    [view addSubview:line];
//    
//    _testNumber = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(10), 44, SCREEN_SIZE.width / 2, 44)];
//    _testNumber.placeholder = @"请填写短信验证码";
//    _testNumber.font = [UIFont systemFontOfSize:KWIDTH(14)];
//    [view addSubview:_testNumber];
//    
//    _getTestNumber = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(110), 51, KWIDTH(100), 30)];
//    [_getTestNumber setTitle:@"获取短信验证码" forState:UIControlStateNormal];
//    [_getTestNumber setTitleColor:mRGB(13, 103, 214, 1) forState:UIControlStateNormal];
//    _getTestNumber.layer.borderWidth = 0.5f;
//    _getTestNumber.layer.borderColor = mRGB(13, 103, 214, 1).CGColor;
//    _getTestNumber.layer.masksToBounds = YES;
//    _getTestNumber.layer.cornerRadius = KWIDTH(2);
//    _getTestNumber.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
//    [_getTestNumber addTarget:self action:@selector(getTestNumber:) forControlEvents:UIControlEventTouchUpInside];
//    [view addSubview:_getTestNumber];
//    
//    [self.view addSubview:view];
//    
//    
//    UIButton *next = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 0 + KWIDTH(10) + 100, SCREEN_SIZE.width - KWIDTH(20), KWIDTH(50))];
//    next.backgroundColor = mRGB(13, 103, 214, 1);
//    next.layer.masksToBounds = YES;
//    next.layer.cornerRadius = KWIDTH(2);
//    [next setTitle:@"下一步" forState:UIControlStateNormal];
//    next.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
//    [next addTarget:self action:@selector(next:) forControlEvents:UIControlEventTouchUpInside];
//    
//    [self.view addSubview:next];
//
//    _timeCount = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(110), 51, KWIDTH(100), 30)];
//    _timeCount.backgroundColor = [UIColor lightGrayColor];
//    _timeCount.textColor = [UIColor grayColor];
//    _timeCount.text = @"重新发送(60)";
//    _timeCount.textAlignment = NSTextAlignmentCenter;
//    _timeCount.font = [UIFont systemFontOfSize:KWIDTH(10)];
//    _timeCount.layer.masksToBounds = YES;
//    _timeCount.layer.cornerRadius = KWIDTH(2);
//    [view addSubview:_timeCount];
//    [_timeCount setHidden:YES];
//    
//    _time = 60;
//    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeGo) userInfo:nil repeats:YES];
//    [_timer setFireDate:[NSDate distantFuture]];
//    
//    
//    
//    
//}
//
//- (void)getTestNumber:(id)sender {
//    [_timeCount setHidden:NO];
//    [_getTestNumber setHidden:YES];
//    [_timer setFireDate:[NSDate distantPast]];
//}
//
//
//- (void)next:(id)sender {
//    
//    HGNewPasswordViewController *newPaasordVC = [[HGNewPasswordViewController alloc] init];
//    [self.navigationController pushViewController:newPaasordVC animated:YES];
//    
//}
//
//- (void)timeGo {
//    if (_time > 0) {
//        _timeCount.text = [NSString stringWithFormat:@"重新发送(%d)", _time--];
//    }else {
//        [_timer setFireDate:[NSDate distantFuture]];
//        [_getTestNumber setHidden:NO];
//        [_timeCount setHidden:YES];
//        
//    }
//}
//
//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    [self.view endEditing:YES];
//}
//
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}

@end
