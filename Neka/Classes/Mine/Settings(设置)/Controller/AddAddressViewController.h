//
//  AdressViewController.h
//  Neka1.0
//
//  Created by ma c on 16/8/11.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressInfoModel.h"
@interface AddAddressViewController : UIViewController

@property (nonatomic, assign)BOOL isAddNewAddress;
@property (nonatomic, copy)NSString *addressId;
@property (nonatomic, strong)NSString *btnTitle;
@property (nonatomic, strong)AddressInfoModel *addressInfo;
@end
