//
//  NicknameViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/10.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "NicknameViewController.h"
#import "RegEx.h"
@interface NicknameViewController ()
@property (nonatomic, strong)UITextField *tf;
@end

@implementation NicknameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"修改昵称";
    self.view.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0 + KWIDTH(10), SCREEN_SIZE.width, KWIDTH(40))];
    view.backgroundColor = [UIColor whiteColor];
    _tf = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(30))];
    _tf.placeholder = @"请填写用户名";
    _tf.font = [UIFont systemFontOfSize:KWIDTH(12)];
    _tf.text = [[HGUserDataManager shareManager] nickname];
    [view addSubview:_tf];
    [self.view addSubview:view];
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 0 + KWIDTH(60), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(20))];
    label.text = @"以英文字母或汉字开头,限2-16个字符";
    label.font = [UIFont systemFontOfSize:KWIDTH(12)];
    [self.view addSubview:label];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 0 + KWIDTH(90), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(30))];
    btn.backgroundColor = mRGB(13, 103, 214, 1);
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = KWIDTH(2);
    [btn setTitle:@"修改" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    [btn addTarget:self action:@selector(changeNickname) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btn];
}

- (void)changeNickname {
    if (_tf.text.length < 2 || _tf.text.length > 16) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"限2-16个字符" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if ([RegEx isCorrectNickname:_tf.text]) {
        NSLog(@"通过正则");
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"nickname": _tf.text};
        [self showLoading];
        [manager POST:[NSString stringWithFormat:@"%@c=my&a=username", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self hideLoading];
            NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
            if ([errorCode isEqualToString:@"0"]) {
                [MBProgressHUD showSuccess:@"修改成功"];
                [[HGUserDataManager shareManager] setNickname:_tf.text];
                [self.navigationController popViewControllerAnimated:YES];
            }else if ([errorCode isEqualToString:@"20090098"]) {
                [MBProgressHUD showSuccess:@"请输入新用户名"];
            }else if ([errorCode isEqualToString:@"20090099"]) {
                [MBProgressHUD showSuccess:@"您还没有修改用户名"];
            }else if ([errorCode isEqualToString:@"20090100"]) {
                [MBProgressHUD showSuccess:@"修改失败，请重试"];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self hideLoading];
        }];

    }else {
        NSLog(@"错误");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"用户名只能以英文字母或汉字开头!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}




- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
