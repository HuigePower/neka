//
//  VerificationTelViewController.m
//  Neka1.0
//
//  Created by ma c on 2016/12/31.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "VerificationTelViewController.h"
#import "PasswordViewController.h"
#import "TelViewController.h"
#import "HGVerificationButton.h"
@interface VerificationTelViewController ()
@property (nonatomic, copy)NSString *type; //1:注册账号,2:忘记密码,3:修改手机
@property (nonatomic, strong)UILabel *tel;
@property (nonatomic, strong)UITextField *testNumber;
@property (nonatomic, strong)NSTimer *timer;
@property (nonatomic, strong)HGVerificationButton *verificationBtn;

@end

@implementation VerificationTelViewController


- (instancetype)initWithVerificationTelAim:(VerificationTelAim)aim andBackViewcontroller:(UIViewController *)backViewController {
    if (self = [super init]) {
        _aim = aim;
        _backViewController = backViewController;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"验证原手机";
    
    self.view.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0 + KWIDTH(10), SCREEN_SIZE.width, 88)];
    view.backgroundColor = [UIColor whiteColor];
    _tel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 0, SCREEN_SIZE.width - KWIDTH(20), 44)];
    _tel.text = [[HGUserDataManager shareManager] phone];
    _tel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    [view addSubview:_tel];
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 43.75, SCREEN_SIZE.width - KWIDTH(20), 0.5)];
    line.backgroundColor = [UIColor lightGrayColor];
    [view addSubview:line];
    
    _testNumber = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(10), 44, SCREEN_SIZE.width / 2, 44)];
    _testNumber.placeholder = @"请填写短信验证码";
    _testNumber.font = [UIFont systemFontOfSize:KWIDTH(14)];
    [view addSubview:_testNumber];
    
    _verificationBtn = [[HGVerificationButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(110), 51, KWIDTH(100), 30)];
    [_verificationBtn addTarget:self action:@selector(getTestNumber:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_verificationBtn];
    
    [self.view addSubview:view];
    
    
    UIButton *next = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 0 + KWIDTH(10) + 100, SCREEN_SIZE.width - KWIDTH(20), 44)];
    next.backgroundColor = mRGB(13, 103, 214, 1);
    next.layer.masksToBounds = YES;
    next.layer.cornerRadius = 5;
    [next setTitle:@"下一步" forState:UIControlStateNormal];
    next.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    [next addTarget:self action:@selector(next:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:next];
    
    if (_aim == VerificationTelAimForChangePassword) {
        _type = @"2";
    }else if (_aim == VerificationTelAimForChangeTelNumber) {
        _type = @"2";
    }
    
    
    
}

- (void)getTestNumber:(id)sender {
    
   AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"phone": _tel.text, @"type": _type};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Login&a=sendCode", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"已发送"];
        }else {
            [MBProgressHUD showSuccess:responseObject[@"errorMsg"]];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
    
}


- (void)next:(id)sender {
    if (_testNumber.text.length < 4) {
        [MBProgressHUD showError:@"验证码错误"];
        return;
    }
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"phone": _tel.text, @"code": _testNumber.text};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Login&a=verifyCode", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"验证成功"];
            if (_aim == VerificationTelAimForChangePassword) {
                PasswordViewController *password = [[PasswordViewController alloc] init];
                password.backViewController = _backViewController;
                password.telNumber = _tel.text;
                password.code = _testNumber.text;
                [self.navigationController pushViewController:password animated:YES];
            }else if (_aim == VerificationTelAimForChangeTelNumber) {
                TelViewController *tel = [[TelViewController alloc] init];
                tel.backViewController = _backViewController;
                tel.code = _testNumber.text;
                [self.navigationController pushViewController:tel animated:YES];
            }
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
    }];

}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
