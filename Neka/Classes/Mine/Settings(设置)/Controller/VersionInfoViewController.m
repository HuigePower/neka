//
//  VersionInfoViewController.m
//  Neka
//
//  Created by ma c on 2017/6/2.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "VersionInfoViewController.h"

@interface VersionInfoViewController ()
@property (nonatomic, strong)UITextView *textView;
@end

@implementation VersionInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"版本信息";
    self.automaticallyAdjustsScrollViewInsets = NO;
    _textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
    _textView.scrollEnabled = NO;
    _textView.textAlignment = NSTextAlignmentCenter;
    _textView.text = @"当前版本\n\nNEKA平台1.0版";
    [self.view addSubview:_textView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
