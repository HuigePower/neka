//
//  AdressViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/11.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "AddAddressViewController.h"
#import "ProvinceViewController.h"
#import "HGAreaManager.h"
#import "HGAreaModel.h"
#import "HGPushPicker.h"
#import "MBProgressHUD.h"
#import "HGAddressSelectViewController.h"
@interface AddAddressViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSArray *titles;

@property (nonatomic, strong)UITextField *nameTF;//名字
@property (nonatomic, strong)UITextField *telTF;//电话
@property (nonatomic, strong)UILabel *provinceLabel;//省份
@property (nonatomic, strong)UILabel *cityLabel;//城市
@property (nonatomic, strong)UILabel *areaLabel;//区县
@property (nonatomic, strong)UILabel *locationLabel;//位置

@property (nonatomic, copy)NSString *longitude; //经度
@property (nonatomic, copy)NSString *latitude;  //纬度

@property (nonatomic, strong)UITextField *addressTF;//地址
@property (nonatomic, strong)UITextField *postcodeTF;//邮编
@property (nonatomic, strong)UIImageView *defaultAddress;//默认地址
@property (nonatomic, assign)BOOL isDefault;

@property (nonatomic, strong)HGAreaModel *provinceModel;
@property (nonatomic, strong)HGAreaModel *cityModel;
@property (nonatomic, strong)HGAreaModel *areaModel;

@property (nonatomic, strong)HGPushPicker *provincePicker;
@property (nonatomic, strong)HGPushPicker *cityPicker;
@property (nonatomic, strong)HGPushPicker *areaPicker;

@property (nonatomic, strong)NSArray *provinceList;
@property (nonatomic, strong)NSArray *cityList;
@property (nonatomic, strong)NSArray *areaList;

@property (nonatomic, strong)NSDictionary *provinceData;
@property (nonatomic, strong)NSDictionary *cityData;
@property (nonatomic, strong)NSDictionary *areaData;

@property (nonatomic, strong)NSDictionary *addressData;


@end

@implementation AddAddressViewController





- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (_isAddNewAddress) {
        self.title = @"新建收货地址";
    }else {
        self.title = @"修改收货地址";
    }
    
    [self getAddressInfo];
    
    
    _titles = @[@"姓名:", @"电话:", @"省份:", @"城市:", @"县区:", @"位置:", @"地址:", @"邮编:"];
    _isDefault = NO;
    
    //姓名
    _nameTF = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(50), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(60), KWIDTH(20))];
    _nameTF.delegate = self;
    _nameTF.placeholder = @"最少2个字";
    _nameTF.font = [UIFont systemFontOfSize:KWIDTH(12)];
    
    //电话
    _telTF = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(50), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(60), KWIDTH(20))];
    _telTF.delegate = self;
    _telTF.placeholder = @"不少于7位";
    _telTF.font = [UIFont systemFontOfSize:KWIDTH(12)];
    
    //省份
    _provinceLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(50), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(60), KWIDTH(20))];
    _provinceLabel.text = @"北京";
    _provinceLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    
    //城市
    _cityLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(50), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(60), KWIDTH(20))];
    _cityLabel.text = @"";
    _cityLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    
    
    //县区
    _areaLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(50), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(60), KWIDTH(20))];
    _areaLabel.text = @"";
    _areaLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    
    //位置
    _locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(50), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(60), KWIDTH(20))];
    _locationLabel.text = @"点击选择位置";
    _locationLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    
    //地址
    _addressTF = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(50), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(60), KWIDTH(20))];
    _addressTF.delegate = self;
    _addressTF.placeholder = @"请填写详细的地址和门牌号";
    _addressTF.font = [UIFont systemFontOfSize:KWIDTH(12)];
    
    //邮编
    _postcodeTF = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(50), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(60), KWIDTH(20))];
    _postcodeTF.delegate = self;
    _postcodeTF.placeholder = @"6位邮政编码,可不填写";
    _postcodeTF.font = [UIFont systemFontOfSize:KWIDTH(12)];
    
    //默认
    _defaultAddress = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(15), KWIDTH(5), KWIDTH(20), KWIDTH(20))];
    [self setIsDefault:_isDefault];
    
    
    
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];
    
}


- (void)setAddressInfo:(AddressInfoModel *)addressInfo {

    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row != 8) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(15), KWIDTH(5), KWIDTH(30), KWIDTH(20))];
        label.text = _titles[indexPath.row];
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [cell addSubview:label];
    }
    
    if (indexPath.row == 0) {
        
        [cell addSubview:_nameTF];
    }else if (indexPath.row ==1) {
        
        [cell addSubview:_telTF];
    }else if (indexPath.row ==2) {
        
        [cell addSubview:_provinceLabel];
        [cell addSubview:[self optionIcon]];
    }else if (indexPath.row ==3) {
        
        [cell addSubview:_cityLabel];
        [cell addSubview:[self optionIcon]];
    }else if (indexPath.row ==4) {
        
        [cell addSubview:_areaLabel];
        [cell addSubview:[self optionIcon]];
    }else if (indexPath.row ==5) {
        
        [cell addSubview:_locationLabel];
    }else if (indexPath.row ==6) {
        
        [cell addSubview:_addressTF];
    }else if (indexPath.row ==7) {
        
        [cell addSubview:_postcodeTF];
    }else if (indexPath.row ==8) {
        
        [cell addSubview:_defaultAddress];
        
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(40), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(60), KWIDTH(20))];
        label.text = @"设为默认地址";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [cell addSubview:label];
    }
    
    return cell;
    
}

- (UIImageView *)optionIcon {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(20), KWIDTH(12.5), KWIDTH(10), KWIDTH(5))];
    imageView.image = [UIImage imageNamed:@"tubiao_down"];
    return imageView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(5);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return KWIDTH(30);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 100;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 100)];
    view.userInteractionEnabled = YES;
    
    if (_isAddNewAddress) {
        UIButton *addBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(20), 44)];
        [addBtn setTitle:@"添加" forState:UIControlStateNormal];
        addBtn.backgroundColor = mRGB(25, 183, 173, 1);
        addBtn.layer.masksToBounds = YES;
        addBtn.layer.cornerRadius = 5;
        [addBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:addBtn];
        
    
    }else {
        
        UIButton *saveBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 5, SCREEN_SIZE.width - KWIDTH(20), 44)];
        [saveBtn setTitle:@"保存" forState:UIControlStateNormal];
        saveBtn.backgroundColor = mRGB(25, 183, 173, 1);
        saveBtn.layer.masksToBounds = YES;
        saveBtn.layer.cornerRadius = KWIDTH(2);
        [saveBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:saveBtn];
        
        UIButton *deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), 55, SCREEN_SIZE.width - KWIDTH(20), 44)];
        [deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        [deleteBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        deleteBtn.backgroundColor = [UIColor whiteColor];
        deleteBtn.layer.masksToBounds = YES;
        deleteBtn.layer.cornerRadius = KWIDTH(2);
        [deleteBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:deleteBtn];
 
    }
    
    
    
    
    return view;
}

- (void)btnClick:(UIButton *)btn {
    
    if (_nameTF.text.length < 2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"姓名最少2个字 " delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if (_telTF.text.length < 7) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"电话不少于7位 " delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if (_latitude.length == 0 || _longitude.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请选择位置" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if (_addressTF.text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"地址不可为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    
//    NSLog(@"%@--%@--%@--%@--%@--%@--%@--%@--%d--%@--%@", _nameTF.text, _telTF.text, _provinceLabel.text, _cityLabel.text, _areaLabel.text, _locationLabel.text, _addressTF.text, _postcodeTF.text, _isDefault, _latitude, _longitude);
    
    if ([btn.titleLabel.text isEqualToString:@"添加"]) {
        NSLog(@"添加");
        
        [self addNewAddress];
        
    }else if ([btn.titleLabel.text isEqualToString:@"保存"]) {
        NSLog(@"保存");
        [self editAddress];
        
    }else if ([btn.titleLabel.text isEqualToString:@"删除"]) {
        NSLog(@"删除");
        [self deleteAddress];
        
    }
}

#pragma mark -- 添加新地址
- (void)addNewAddress {
    
    
    
    NSDictionary *body = nil;
    if ([_areaData[@"area_id"] isEqualToString:@" "]) {
        body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"name": _nameTF.text, @"phone": _telTF.text, @"province": _provinceData[@"area_id"], @"city": _cityData[@"area_id"], @"adress": _locationLabel.text, @"detail": _addressTF.text, @"longitude": _longitude, @"latitude": _latitude, @"default": [NSString stringWithFormat:@"%d", _isDefault]};
    }else {
        body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"name": _nameTF.text, @"phone": _telTF.text, @"province": _provinceData[@"area_id"], @"city": _cityData[@"area_id"], @"adress": _locationLabel.text, @"detail": _addressTF.text, @"longitude": _longitude, @"latitude": _latitude, @"latitude": _latitude, @"default": [NSString stringWithFormat:@"%d", _isDefault], @"area": _areaData[@"area_id"]};
    }

    NSLog(@"%@", body);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=edit_adress", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"添加成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
    }];
    
}

#pragma mark -- 保存修改
- (void)editAddress {
    NSMutableDictionary *body = [NSMutableDictionary new];
    if (_addressId.length == 0) {
        [MBProgressHUD showError:@"未获取到地址ID"];
        return;
    }
    
    [body setObject:DEVICE_ID forKey:@"Device-Id"];
    [body setObject:[UserDefaultManager ticket] forKey:@"ticket"];
    [body setObject:_addressId forKey:@"adress_id"];
    [body setObject:_nameTF.text forKey:@"name"];
    [body setObject:_telTF.text forKey:@"phone"];
    [body setObject:_provinceData[@"area_id"] forKey:@"province"];
    [body setObject:_cityData[@"area_id"] forKey:@"city"];
    [body setObject:_locationLabel.text forKey:@"adress"];
    [body setObject:_addressTF.text forKey:@"detail"];
    [body setObject:_longitude forKey:@"longitude"];
    [body setObject:_latitude forKey:@"latitude"];
    [body setObject:[NSString stringWithFormat:@"%d", _isDefault] forKey:@"default"];
    
    if (![_areaData[@"area_id"] isEqualToString:@" "]) {
        [body setObject:_areaData[@"area_id"] forKey:@"area"];
    }
    
    if (_postcodeTF.text.length != 0) {
        [body setObject:_postcodeTF.text forKey:@"zipcode"];
    }
    
    NSLog(@"%@", body);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=edit_adress", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"修改成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [MBProgressHUD showSuccess:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
    }];
    
}

#pragma mark -- 删除地址
- (void)deleteAddress {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = nil;
    NSLog(@"_addressId:%@", _addressId);
    if (_addressId.length > 0 && _isAddNewAddress == NO) {
        body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"adress_id": _addressId};
    }else {
        body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    }
    NSLog(@"body:%@", body);
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=del_adress", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"删除成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [MBProgressHUD showSuccess:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@", error.description);
        [self hideLoading];
    }];
    
}







- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.5 animations:^{
        _tableView.frame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64);
    }];
    _provincePicker.hidden = YES;
    _cityPicker.hidden = YES;
    _areaPicker.hidden = YES;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    __weak AddAddressViewController *weakSelf = self;
    if (indexPath.row == 0) {
        
    }else if (indexPath.row ==1) {
        
    }else if (indexPath.row ==2) {

        if (_provincePicker == nil) {
            _provincePicker = [[HGPushPicker alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height - 220, SCREEN_SIZE.width, 220)];
            _provincePicker.hidden = YES;
            _provincePicker.makeDataSource = ^(NSDictionary *dic) {
                return dic[@"area_name"];
            };
            
            __weak HGPushPicker *weakPicker = _provincePicker;
            _provincePicker.resultBlock = ^(NSDictionary *provinceData) {
                if (provinceData != nil) {
                    [weakSelf changeProvinceWithData:provinceData];
                    weakPicker.hidden = YES;
                }
            };
            
            [self.view addSubview:_provincePicker];
        }
        
        [_provincePicker setDataSource:_provinceList];
        _cityPicker.hidden = YES;
        _areaPicker.hidden = YES;
        _provincePicker.hidden = NO;
        
        
        
    }else if (indexPath.row ==3) {
        
        if (_cityPicker == nil) {
            _cityPicker = [[HGPushPicker alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height - 220, SCREEN_SIZE.width, 220)];
            _cityPicker.hidden = YES;
            __weak HGPushPicker *weakPicker = _cityPicker;
            _cityPicker.resultBlock = ^(NSDictionary *cityData) {
                if (cityData != nil) {
                    [weakSelf changeCityWithData:cityData];
                    weakPicker.hidden = YES;
                }
                
            };
            
            [self.view addSubview:_cityPicker];
        }
        
        [_cityPicker setDataSource:_cityList];
        
        _areaPicker.hidden = YES;
        _provincePicker.hidden = YES;
        _cityPicker.hidden = NO;
        
    }else if (indexPath.row ==4) {
        if (_areaPicker == nil) {
            _areaPicker = [[HGPushPicker alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height - 220, SCREEN_SIZE.width, 220)];
            _areaPicker.hidden = YES;

            __weak HGPushPicker *weakPicker = _areaPicker;
            _areaPicker.resultBlock = ^(NSDictionary *areaData) {
                if (areaData != nil) {
                    [weakSelf changeAreaWithData:areaData];
                    weakPicker.hidden = YES;
                }
            };
            [self.view addSubview:_areaPicker];
        }
        if (_areaList.count == 0) {
            [_areaPicker setDataSource:@[@{@"area_name": @"请手动填写区域", @"area_id": @" ", @"area_pid": @" "}]];
        }else {
            [_areaPicker setDataSource:_areaList];
        }
        
        
        _cityPicker.hidden = YES;
        _provincePicker.hidden = YES;
        _areaPicker.hidden = NO;
        
    }else if (indexPath.row ==5) {
        HGAddressSelectViewController *addressSelect = [[HGAddressSelectViewController alloc] init];
        addressSelect.resultBlock = ^(HGAddressModel *model) {
            NSLog(@"%@--%@--%@", model.longitude, model.latitude, model.name);
            _longitude = model.longitude;
            _latitude = model.latitude;
            _locationLabel.text = model.name;
        };
        [self.navigationController pushViewController:addressSelect animated:YES];
    }else if (indexPath.row ==6) {
        
    }else if (indexPath.row ==7) {
        
    }else if (indexPath.row ==8) {
        [self setIsDefault:!_isDefault];
    }
    
}



- (void)setIsDefault:(BOOL)isDefault {
    _isDefault = isDefault;
    if (isDefault) {
        _defaultAddress.image = [UIImage imageNamed:@"support"];
        NSLog(@"默认");
    }else {
        _defaultAddress.image = [UIImage imageNamed:@"support_layer"];
        NSLog(@"非默认");
    }
}




- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSLog(@"111");
    
    if (textField == _addressTF) {
        [UIView animateWithDuration:0.5 animations:^{
            _tableView.frame = CGRectMake(0, 0 - KWIDTH(5) - KWIDTH(30) * 6, SCREEN_SIZE.width, SCREEN_SIZE.height - 64);
        }];
    }else if (textField == _postcodeTF) {
        [UIView animateWithDuration:0.5 animations:^{
            _tableView.frame = CGRectMake(0, 0 - KWIDTH(5) - KWIDTH(30) * 7, SCREEN_SIZE.width, SCREEN_SIZE.height - 64);
        }];
    }
    
    return YES;
    
}





- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    return YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark -- 请求数据
- (void)getAddressInfo {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = nil;
    NSLog(@"_addressId:%@", _addressId);
    if (_addressId.length > 0 && _isAddNewAddress == NO) {
        body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"adress_id": _addressId};
    }else {
        body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    }
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=adress_show", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        
        if ([errorCode isEqualToString:@"0"]) {
            
            _provinceList = responseObject[@"result"][@"province_list"];
            _cityList = responseObject[@"result"][@"city_list"];
            _areaList = responseObject[@"result"][@"area_list"];
            
            if (_isAddNewAddress) {
            
                
                _provinceData = _provinceList[0];
                _cityData = _cityList[0];
                _areaData = _areaList[0];
                NSLog(@"省市区=%@--%@--%@", _provinceData, _cityData, _areaData);
                _provinceLabel.text = _provinceData[@"area_name"];
                _cityLabel.text = _cityData[@"area_name"];
                _areaLabel.text = _areaData[@"area_name"];
                
            }else {
                
                _addressData = responseObject[@"result"][@"now_adress"][0];
                
                _nameTF.text = _addressData[@"name"];
                _telTF.text = _addressData[@"phone"];
                _latitude = _addressData[@"latitude"];
                _longitude = _addressData[@"longitude"];
                
                
                for (NSDictionary *item in _provinceList) {
                    if ([item[@"area_id"] isEqualToString:_addressData[@"province"]]) {
                        _provinceLabel.text = item[@"area_name"];
                        _provinceData = item;
                    }
                }
                
                for (NSDictionary *item in _cityList) {
                    if ([item[@"area_id"] isEqualToString:_addressData[@"city"]]) {
                        _cityLabel.text = item[@"area_name"];
                        _cityData = item;
                    }
                }
                
                for (NSDictionary *item in _areaList) {
                    if ([item[@"area_id"] isEqualToString:_addressData[@"area"]]) {
                        _areaLabel.text = item[@"area_name"];
                        _areaData = item;
                    }
                }
                
                _locationLabel.text = _addressData[@"adress"];
                _addressTF.text = _addressData[@"detail"];
                _postcodeTF.text = _addressData[@"zipcode"];
                
                if ([_addressData[@"default"] isEqualToString:@"1"]) {
                    _isDefault = YES;
                    
                }else {
                    _isDefault = NO;
                }
                [self setIsDefault:_isDefault];
                
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@", error.description);
        [self hideLoading];
    }];
    
    
    
}


#pragma mark -- 省份更换后,调整市县

- (void)changeProvinceWithData:(NSDictionary *)provinceData {
    
    if (![provinceData[@"area_name"] isEqualToString:_provinceData[@"area_name"]]) {
        
        _provinceData = provinceData;
        _provinceLabel.text = _provinceData[@"area_name"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"pid": _provinceData[@"area_id"], @"area_type": @"1"};
        [self showLoading];
        [manager POST:[NSString stringWithFormat:@"%@c=mY&a=select_area", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            [self hideLoading];
            NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
            
            if ([errorCode isEqualToString:@"0"]) {
                NSLog(@"%@", responseObject);
                
                _cityList = responseObject[@"result"][@"city_list"];
                _cityData = _cityList[0];
                _cityLabel.text = _cityData[@"area_name"];
                
                _areaList = responseObject[@"result"][@"area_list"];
                if (_areaList.count != 0) {
                    _areaData = _areaList[0];
                    _areaLabel.text = _areaData[@"area_name"];
                }else {
                    _areaData = @{@"area_name": @" ", @"area_id": @" ", @"area_pid": @" "};
                    _areaLabel.text = @"请手动填写区域";
                }
                
                
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self hideLoading];
        }];
        
    }
}


- (void)changeCityWithData:(NSDictionary *)cityData {
    if (![cityData[@"area_name"] isEqualToString:_cityData[@"area_name"]]) {
        _cityData = cityData;
        _cityLabel.text = _cityData[@"area_name"];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"pid": _cityData[@"area_id"], @"area_type": @"2"};
        [self showLoading];
        [manager POST:[NSString stringWithFormat:@"%@c=mY&a=select_area", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            [self hideLoading];
            NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
            
            if ([errorCode isEqualToString:@"0"]) {
                NSLog(@"%@", responseObject);
                
                _areaList = responseObject[@"result"][@"area_list"];
                if (_areaList.count != 0) {
                    _areaData = _areaList[0];
                    _areaLabel.text = _areaData[@"area_name"];
                }else {
                    _areaData = @{@"area_name": @" ", @"area_id": @" ", @"area_pid": @" "};
                    _areaLabel.text = @"请手动填写区域";
                }
                
                
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self hideLoading];
        }];
    }
}

- (void)changeAreaWithData:(NSDictionary *)areaData {
    _areaData = areaData;
    _areaLabel.text = _areaData[@"area_name"];
}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
