//
//  AboutUsViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/16.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "AboutUsViewController.h"
#import "VersionInfoViewController.h"
@interface AboutUsViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong)UITableView *tableView;
@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"关于我们";
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    [self.view addSubview:_tableView];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    
    
//    self.view.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(128), KWIDTH(60))];
//    imageView.image = [UIImage imageNamed:@"neka_icon"];
//    imageView.center = CGPointMake(self.view.center.x, KWIDTH(30) + 84);
//    [self.view addSubview:imageView];
//    
//    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(KWIDTH(50), imageView.frame.origin.y + imageView.frame.size.height + KWIDTH(30), SCREEN_SIZE.width - KWIDTH(100), KWIDTH(120))];
//    textView.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
//    textView.scrollEnabled = NO;
//    textView.layer.masksToBounds = YES;
//    textView.layer.cornerRadius = 5;
//    textView.layer.borderColor = [UIColor grayColor].CGColor;
//    textView.layer.borderWidth = 0.5;
//    textView.text = @"耐卡（北京）汽车用品有限公司隶属于2005年成立的全国唯一专业汽车轻改装品牌耐卡连锁。北京地区最专业的汽车贴膜改装机构，贴膜产品均来自全球最大窗膜集团伊士曼（龙膜、威固、琥珀母公司），并为最新产品琥珀光学顶级型号的国内独家签约经销商。严格质量体系、负责的服务态度吸引着车主不远千里到店服务，并获得车友一致好评。";
//    
//    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:textView.text];
//    [attString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, textView.text.length)];
//    
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    paragraphStyle.lineSpacing = 5;
//    paragraphStyle.firstLineHeadIndent = 30;
//    
//    [attString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, textView.text.length)];
//    
//    textView.attributedText = attString;
//    
//    [textView sizeToFit];
//    [self.view addSubview:textView];
//    
//    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height - KWIDTH(100), SCREEN_SIZE.width, KWIDTH(20))];
//    dateLabel.text = @"All Rights Reserved Copyright(1995-2016)";
//    dateLabel.font = [UIFont systemFontOfSize:14];
//    dateLabel.textAlignment = NSTextAlignmentCenter;
//    [self.view addSubview:dateLabel];
//    
//    UILabel *companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height - KWIDTH(70), SCREEN_SIZE.width, KWIDTH(20))];
//    companyLabel.text = @"耐卡（北京）汽车用品有限公司";
//    companyLabel.font = [UIFont systemFontOfSize:14];
//    companyLabel.textAlignment = NSTextAlignmentCenter;
//    [self.view addSubview:companyLabel];
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = @"NEKA平台1.0版";
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    VersionInfoViewController *versionInfo = [[VersionInfoViewController alloc] init];
    [self.navigationController pushViewController:versionInfo animated:YES];
}
















- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
