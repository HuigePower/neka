//
//  VerificationTelViewController.h
//  Neka1.0
//
//  Created by ma c on 2016/12/31.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, VerificationTelAim) {
    VerificationTelAimForChangePassword,
    VerificationTelAimForChangeTelNumber
};
@interface VerificationTelViewController : UIViewController

@property (nonatomic, assign)VerificationTelAim aim;
@property (nonatomic, strong)UIViewController *backViewController;

- (instancetype)initWithVerificationTelAim:(VerificationTelAim)aim andBackViewcontroller:(UIViewController *)backViewController;

@end
