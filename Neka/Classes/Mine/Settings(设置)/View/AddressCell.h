//
//  AddressCell.h
//  Neka1.0
//
//  Created by ma c on 16/8/12.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressInfoModel.h"

typedef void(^EditAddressBlock)(NSIndexPath *indexPath);
typedef void(^DeleteAddressBlock)(NSIndexPath *indexPath);
@interface AddressCell : UITableViewCell

@property (nonatomic, strong)AddressInfoModel *model;

@property (nonatomic, strong)NSDictionary *data;
@property (nonatomic, strong)UILabel *nameAndPhoneLabel;
@property (nonatomic, strong)UILabel *addressLabel;
@property (nonatomic, strong)UILabel *isDefaultMark;

@property (nonatomic, strong)NSIndexPath *indexPath;

@property (nonatomic, copy)EditAddressBlock editBlock;
@property (nonatomic, copy)DeleteAddressBlock deleteBlock;

@end
