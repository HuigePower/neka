//
//  HGUnfoldButton.m
//  Neka1.0
//
//  Created by ma c on 2017/1/9.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGUnfoldButton.h"

@implementation HGUnfoldButton
{
    UILabel *_label;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        UIImageView *im = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        im.image = [UIImage imageNamed:@"unfold_down"];
        [self addSubview:im];
        
        _label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, frame.size.width - 10, frame.size.height)];
        _label.font = [UIFont systemFontOfSize:14];
        _label.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_label];
        
    }
    return self;
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state {
    _label.text = title;
}

- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state {
    _label.textColor = color;
}


@end
