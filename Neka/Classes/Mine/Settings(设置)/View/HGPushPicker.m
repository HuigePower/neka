//
//  HGPushPicker.m
//  Neka1.0
//
//  Created by ma c on 2017/1/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGPushPicker.h"

@implementation HGPushPicker {
    NSInteger _row;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor lightGrayColor];
        
        _dataSource = [[NSArray alloc] init];
        _row = 0;
        
        
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
        view.backgroundColor = mRGB(240, 240, 240, 1);
        [self addSubview:view];
        
        UILabel *line1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0.5)];
        line1.backgroundColor = [UIColor blackColor];
        [view addSubview:line1];
        
        UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 39.5, self.frame.size.width, 0.5)];
        line2.backgroundColor = [UIColor blackColor];
        [view addSubview:line2];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 50, 5, 40, 30)];
        [btn setTitle:@"完成" forState:UIControlStateNormal];
        [btn setTitleColor:mRGB(13, 103, 214, 1) forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(finished:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        [self addSubview:self.picker];
        
    }
    return self;
}

- (void)finished:(UIButton *)btn {
    NSLog(@"完成");
    if (_resultBlock != nil) {
        
        _resultBlock(_dataSource[_row]);
    }
}

- (UIPickerView *)picker {
    if (_picker == nil) {
        _picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, self.frame.size.width, self.frame.size.height - 40)];
        _picker.delegate = self;
        _picker.dataSource = self;
    }
    return _picker;
}

- (void)setDataSource:(NSArray *)dataSource {
    _dataSource = dataSource;
    for (NSDictionary *dic in _dataSource) {
        NSLog(@"%@", dic[@"area_name"]);
    }
    
    [_picker reloadComponent:0];
    [_picker selectRow:0 inComponent:0 animated:NO];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _dataSource.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *obj = _dataSource[row][@"area_name"];
//    if (_makeDataSource != nil) {
//        return _makeDataSource(obj);
//    }
    return obj;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _row = row;
}



@end
