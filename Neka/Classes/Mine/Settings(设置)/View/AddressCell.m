//
//  AddressCell.m
//  Neka1.0
//
//  Created by ma c on 16/8/12.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "AddressCell.h"
#import "AddressInfoModel.h"
@implementation AddressCell

- (instancetype)init {
    if (self = [super init]) {
        
        self.backgroundColor = [UIColor whiteColor];
        _nameAndPhoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 10, KWIDTH(250), 20)];
        _nameAndPhoneLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_nameAndPhoneLabel];
        _isDefaultMark = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(100), 10, KWIDTH(80), 20)];
        
        _isDefaultMark.text = @"【默认】";
        _isDefaultMark.textColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nav_bg"]];
        
        
        _addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 30, SCREEN_SIZE.width - KWIDTH(40), 40)];
        _addressLabel.font = [UIFont systemFontOfSize:14];
        _addressLabel.numberOfLines = 2;
        [self addSubview:_addressLabel];
        
        
        UIImageView *editView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - 30, 40, 20, 20)];
        editView.image = [UIImage imageNamed:@"address_edit"];
        [self addSubview:editView];
        
        
    }
    return self;
}

- (void)setModel:(AddressInfoModel *)model {
    
    _nameAndPhoneLabel.text = [NSString stringWithFormat:@"%@    %@", model.name, model.phone];
    if ([model.defaults isEqualToString:@"1"]) {
        [self addSubview:_isDefaultMark];
    }else {
        [_isDefaultMark removeFromSuperview];
    }
    
    NSString *address = [NSString stringWithFormat:@"%@  %@  %@ %@ %@", model.province_txt, model.city_txt, model.area_txt, model.adress, model.detail];
    
    _addressLabel.text = address;
    
}


- (void)btnClick:(UIButton *)btn {
    if (btn.tag == 10) {
        if (_editBlock) {
            self.editBlock(_indexPath);
        }
    }else {
        if (_deleteBlock) {
            self.deleteBlock(_indexPath);
        }
    }
}


- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
