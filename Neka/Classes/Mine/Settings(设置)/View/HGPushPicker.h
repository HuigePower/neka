//
//  HGPushPicker.h
//  Neka1.0
//
//  Created by ma c on 2017/1/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NSString*(^HGPushPickerDataSource)(id obj);
typedef void (^HGPushPickerfinished)(id obj);
@interface HGPushPicker : UIView <UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, strong)UIPickerView *picker;
@property (nonatomic, strong)NSArray *dataSource;

@property (nonatomic, copy)HGPushPickerDataSource makeDataSource;
@property (nonatomic, copy)HGPushPickerfinished resultBlock;

@end
