//
//  UILabel+AlignmentBothSides.h
//  Neka1.0
//
//  Created by ma c on 2017/1/6.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (AlignmentBothSides)
- (void)bothSidesOfAlignment;
@end
