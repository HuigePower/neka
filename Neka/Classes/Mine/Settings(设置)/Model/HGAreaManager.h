//
//  HGAreaManager.h
//  Neka1.0
//
//  Created by ma c on 2017/1/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HGAreaModel.h"
@interface HGAreaManager : NSObject

@property (nonatomic, strong)NSArray *areaList;

- (void)initDataSource;



+ (NSArray *)provinceList;
+ (NSArray *)cityListWithProvinceCode:(NSInteger)code;
+ (NSArray *)areaListWithCityCode:(NSInteger)code;
+ (HGAreaModel *)provinceModelWithAreaName:(NSString *)areaName;
+ (HGAreaModel *)cityModelOrAreaModelWithAreaName:(NSString *)areaName;

@end
