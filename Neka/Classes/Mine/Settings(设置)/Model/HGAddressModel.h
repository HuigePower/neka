//
//  HGAddressModel.h
//  Neka
//
//  Created by ma c on 2017/6/20.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGAddressModel : NSObject
@property (nonatomic, copy)NSString *name;
@property (nonatomic, copy)NSString *address;
@property (nonatomic, copy)NSString *latitude; //纬度
@property (nonatomic, copy)NSString *longitude; //经度
@property (nonatomic, assign)BOOL isRecommend; //是否推荐

@end
