//
//  HGAreaManager.m
//  Neka1.0
//
//  Created by ma c on 2017/1/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGAreaManager.h"

@implementation HGAreaManager 

- (instancetype)init {
    if (self = [super init]) {
        [self initDataSource];
    }
    return self;
}

- (void)initDataSource {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"pigcms_area.json" ofType:nil];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSArray *arr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSMutableArray *tmpArr = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in arr) {
        HGAreaModel *model = [[HGAreaModel alloc] initWithDict:dic];
        [tmpArr addObject:model];
    }
    _areaList = tmpArr;
    
}

//省
+ (NSArray *)provinceList {
    HGAreaManager *manager = [[HGAreaManager alloc] init];
    NSMutableArray *provinceList = [[NSMutableArray alloc] init];
    for (HGAreaModel *model in manager.areaList) {
        if (model.area_pid == 0) {
            [provinceList addObject:model];
        }
    }
    return provinceList;
}

//市
+ (NSArray *)cityListWithProvinceCode:(NSInteger)code {
    HGAreaManager *manager = [[HGAreaManager alloc] init];
    NSMutableArray *cityList = [[NSMutableArray alloc] init];
    for (HGAreaModel *model in manager.areaList) {
        if (model.area_pid == code) {
            [cityList addObject:model];
        }
    }
    return cityList;
}

//区
+ (NSArray *)areaListWithCityCode:(NSInteger)code {
    HGAreaManager *manager = [[HGAreaManager alloc] init];
    NSMutableArray *areaList = [[NSMutableArray alloc] init];
    for (HGAreaModel *model in manager.areaList) {
        if (model.area_pid == code) {
            [areaList addObject:model];
        }
    }
    return areaList;
}


+ (HGAreaModel *)provinceModelWithAreaName:(NSString *)areaName {
    HGAreaManager *manager = [[HGAreaManager alloc] init];
    for (HGAreaModel *model in manager.areaList) {
        if (model.area_pid == 0 && [model.area_name isEqualToString:areaName]) {
            return model;
        }
    }
    return nil;
}

+ (HGAreaModel *)cityModelOrAreaModelWithAreaName:(NSString *)areaName {
    HGAreaManager *manager = [[HGAreaManager alloc] init];
    for (HGAreaModel *model in manager.areaList) {
        if (model.area_pid != 0 && [model.area_name isEqualToString:areaName]) {
            return model;
        }
    }
    return nil;
}




@end
