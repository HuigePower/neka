//
//  AddressInfo.m
//  Neka1.0
//
//  Created by ma c on 16/8/12.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "AddressInfoModel.h"

@implementation AddressInfoModel

- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
//        NSLog(@"地址%@", dic);
        [self setValuesForKeysWithDictionary:dic];
    
    }
    return self;
}

@end
