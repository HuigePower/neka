//
//  HGAreaModel.m
//  Neka1.0
//
//  Created by ma c on 2017/1/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGAreaModel.h"

@implementation HGAreaModel



- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
//        NSLog(@"%@", [self description]);
    }
    return self;
}

//- (NSString *)description {
//    
//    NSString *str = [NSString stringWithFormat:@"1.id:%ld\n 2.pid:%ld\n 3.name:%@\n 4.sort:%ld\n 5.pinyin:%@\n 6.open:%ld\n 7.url:%@\n ip:%@\n 8.type:%ld\n 9.hot:%ld", (long)self.area_id, (long)self.area_pid, self.area_name, (long)self.area_sort, self.first_pinyin, (long)self.is_open, self.area_url, self.area_ip_desc, (long)self.area_type, (long)self.is_hot];
//    
//    return str;
//}

@end
