//
//  UITextField+BottomLine.m
//  Neka1.0
//
//  Created by ma c on 2017/1/5.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "UITextField+BottomLine.h"

@implementation UITextField (BottomLine)

- (void)addBottomLine {
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 0.5, self.frame.size.width, 0.5)];
    line.backgroundColor = [UIColor lightGrayColor];
    
    [self addSubview:line];
}

@end
