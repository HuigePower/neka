//
//  UILabel+AlignmentBothSides.m
//  Neka1.0
//
//  Created by ma c on 2017/1/6.
//  Copyright © 2017年 ma c. All rights reserved.
//  lable+两边对齐

#import "UILabel+AlignmentBothSides.h"

@implementation UILabel (AlignmentBothSides)

- (void)bothSidesOfAlignment {
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:self.text];
    
    CGRect rect = [self.text boundingRectWithSize:CGSizeMake(200, 50) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:KWIDTH(16)]} context:nil];
    NSLog(@"%f", rect.size.width);
    CGFloat spacing = (self.frame.size.width - rect.size.width) / (self.text.length - 1);
    NSLog(@"%@", @(spacing));
    NSLog(@"%f", rect.size.width + spacing * (self.text.length - 1));
    spacing = ceil(spacing);
    
    [att addAttribute:NSKernAttributeName value:@(spacing == 0?1.8:spacing) range:NSMakeRange(0, self.text.length - 1)];
    
    self.attributedText = att;
}

@end
