//
//  HGAreaModel.h
//  Neka1.0
//
//  Created by ma c on 2017/1/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGAreaModel : NSObject

@property (nonatomic, assign)NSInteger area_id;
@property (nonatomic, assign)NSInteger area_pid;
@property (nonatomic, copy)NSString *area_name;
@property (nonatomic, assign)NSInteger area_sort;
@property (nonatomic, copy)NSString *first_pinyin;
@property (nonatomic, assign)NSInteger is_open;
@property (nonatomic, copy)NSString *area_url;
@property (nonatomic, copy)NSString *area_ip_desc;
@property (nonatomic, assign)NSInteger area_type;
@property (nonatomic, assign)NSInteger is_hot;

- (instancetype)initWithDict:(NSDictionary *)dict;
@end
