//
//  AddressInfo.h
//  Neka1.0
//
//  Created by ma c on 16/8/12.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressInfoModel : NSObject


@property (nonatomic, strong)NSString *adress;
@property (nonatomic, strong)NSString *adress_id;  //传值
@property (nonatomic, strong)NSString *province_txt; //省
@property (nonatomic, strong)NSString *city_txt;   //市
@property (nonatomic, strong)NSString *area_txt;   //区
@property (nonatomic, strong)NSString *defaults;   //是否默认
@property (nonatomic, strong)NSString *detail;     //地址组合
@property (nonatomic, strong)NSString *distance;   //距离
@property (nonatomic, strong)NSString *is_deliver; //
@property (nonatomic, strong)NSString *lat;        //纬度
@property (nonatomic, strong)NSString *lng;        //经度
@property (nonatomic, strong)NSString *name;       //姓名
@property (nonatomic, strong)NSString *phone;       //手机号
@property (nonatomic, strong)NSString *zipcode;     //邮编













- (instancetype)initWithDic:(NSDictionary *)dic;
@end
