//
//  LoginViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/8.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "FindPasswordViewController.h"
#import "WXApi.h"
#import "HGLoginView.h"

@interface LoginViewController ()
@property (nonatomic, strong)HGLoginView *loginView;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self.tabBarController.tabBar setHidden:NO];
    self.title = @"会员登录";
    self.automaticallyAdjustsScrollViewInsets = NO;
    _loginView = [[HGLoginView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];

    __weak LoginViewController *weakSelf = self;
    _loginView.loginSucceedBlock = ^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
        
    };
    
    _loginView.startRegisterBlock = ^{
        RegisterViewController *registerVC = [[RegisterViewController alloc] init];
        [weakSelf.navigationController pushViewController:registerVC animated:YES];
    };
    
    _loginView.findPasswordBlock = ^{
        FindPasswordViewController *findPW = [[FindPasswordViewController alloc] init];
        [weakSelf.navigationController pushViewController:findPW animated:YES];
    };

    
    [self.view addSubview:_loginView];
    
}




- (void)wxLogin:(UIButton *)btn {
    SendAuthReq *req = [[SendAuthReq alloc] init];
    req.scope = @"snsapi_userinfo";
    req.state = @"123";
    
    [WXApi sendReq:req];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
