//
//  HGWebRechargeViewController.h
//  Neka
//
//  Created by ma c on 2017/7/11.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGWebViewRootController.h"

@interface HGWebRechargeViewController : HGWebViewRootController
@property (nonatomic, copy)NSString *URL;
@end
