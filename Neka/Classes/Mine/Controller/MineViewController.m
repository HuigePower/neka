//
//  MineViewController.m
//  Neka1.0
//
//  Created by ma c on 16/7/26.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "MineViewController.h"
#import "LoginViewController.h"
#import "SettingViewController.h"
#import "PurseViewController.h"

#import "WHHButton.h"
#import "HeaderButton.h"
#import "HGCardButton.h"
#import "HGLoginView.h"
#import "RegisterViewController.h"
#import "FindPasswordViewController.h"
#import "HGOrderWebViewController.h"

@interface MineViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, assign)BOOL isLogin;
@property (nonatomic, strong)HGLoginView *loginView;
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)UILabel *balanceLabel;//余额
@property (nonatomic, strong)UILabel *scoresLabel;//积分
@property (nonatomic, strong)UILabel *levelLabel;//等级
@property (nonatomic, strong)UILabel *coupon;//商家优惠券
@property (nonatomic, strong)UILabel *myActivities;//我参加的活动
@property (nonatomic, strong)UILabel *memberCard;//我的会员卡

@property (nonatomic, strong)NSArray *orderTitles;
@property (nonatomic, strong)NSArray *orderIcons;
@property (nonatomic, strong)NSArray *orderURLs;

@property (nonatomic, strong)NSArray *collectTitles;
@property (nonatomic, strong)NSArray *couponTitles;
@property (nonatomic, strong)NSArray *couponNums;
@property (nonatomic, strong)NSArray *couponURLs;

@property (nonatomic, strong)NSArray *collectionURLs;

@property (nonatomic, strong)NSDictionary *userData;
@end

@implementation MineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    if ([UserDefaultManager ticketIsExist]) {
        
    }else {
        [self showLoginView];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
    if ([UserDefaultManager ticketIsExist]) {
        [_loginView removeFromSuperview];
        [self showMyInfo];
        self.title = @"我的";
    }else {
        [_tableView removeFromSuperview];
        [self showLoginView];
        self.title = @"登录";
    }
    
}

- (void)showLoginView {
    
    if (_loginView == nil) {
        _loginView = [[HGLoginView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - 49)];
        __weak HGLoginView *weakLoginView = _loginView;
        __weak MineViewController *weakSelf = self;
        _loginView.loginSucceedBlock = ^{
            [weakLoginView removeFromSuperview];
            [weakSelf showMyInfo];
        };
        
        _loginView.startRegisterBlock = ^{
            RegisterViewController *registerVC = [[RegisterViewController alloc] init];
            [weakSelf.navigationController pushViewController:registerVC animated:YES];
        };
        
        _loginView.findPasswordBlock = ^{
            FindPasswordViewController *findPW = [[FindPasswordViewController alloc] init];
            [weakSelf.navigationController pushViewController:findPW animated:YES];
        };
        
        
    }
    [self.view addSubview:_loginView];
 
}

- (void)showMyInfo {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - 49) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    [self initDataSource];
    [self.view addSubview:_tableView];
}




- (void)initDataSource {
    _orderTitles = @[@"预约订单", @"团购订单", @"到店付订单", @"礼品订单"];
    _orderIcons = @[@"appoint", @"group", @"store", @"gift"];
    _orderURLs = @[@"http://www.nekahome.com/wap.php?g=Wap&c=My&a=appoint_order_list",
                   @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=group_order_list",
                   @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=store_order_list",
                   @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=gift_order_list"];
    
    _collectTitles = @[@"预约收藏", @"团购收藏", @"关注商家"];
    _couponTitles = @[@"平台优惠券", @"商家优惠券", @"会员卡", @"参与活动"];
    _couponNums = @[@"-", @"-", @"-", @"-"];
    _couponURLs = @[@"http://www.nekahome.com/wap.php?g=Wap&c=My&a=card_list&coupon_type=system",
                    @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=card_list&coupon_type=mer",
                    @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=cards",
                    @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=join_activity"];
    
    _collectionURLs = @[@"http://www.nekahome.com/wap.php?g=Wap&c=My&a=appoint_collect",
                       @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=group_collect",
                       @"http://www.nekahome.com/wap.php?g=Wap&c=My&a=follow_merchant"];
    
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=my_new", BASE_URL] parameters:body progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject:%@", responseObject);
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        [self hideLoading];
        if ([errorCode isEqualToString:@"0"]) {
            [[HGUserDataManager shareManager] saveUserData:responseObject[@"result"][@"user"]];
            NSMutableArray *couponNums = [NSMutableArray new];
            NSArray *couponData = responseObject[@"result"][@"discount"];
            [couponNums addObject:couponData[1][@"number"]];
            [couponNums addObject:couponData[0][@"number"]];
            [couponNums addObject:couponData[3][@"number"]];
            [couponNums addObject:couponData[2][@"number"]];
            _couponNums = couponNums;
            [_tableView reloadData];
        }else {
            
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        NSLog(@"error:%@", error.description);
    }];
    
    
    
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }else if (section == 1) {
        return 1;
    }else if (section == 2) {
        return 1;
    }else if (section == 3) {
        return 1;
    }else if (section == 4) {
        return 1;
    }else {
        return 0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }else {
        NSArray *subviews = cell.subviews;
        for (UIView *view in subviews) {
            [view removeFromSuperview];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    HGUserDataManager *userData = [HGUserDataManager shareManager];
    if (indexPath.section == 0) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(20), KWIDTH(20), KWIDTH(75), KWIDTH(75))];
        imageView.layer.masksToBounds = YES;
        imageView.layer.cornerRadius = KWIDTH(37.5);
        imageView.layer.borderWidth = 1;
        imageView.layer.borderColor = [UIColor whiteColor].CGColor;
        imageView.backgroundColor = [UIColor whiteColor];

        if ([userData.avatar length] > 0) {
            [imageView setImageWithURL:[NSURL URLWithString:[userData avatar]]];
        }else {
            imageView.image = [UIImage imageNamed:@"my_photo"];
        }
        
        [cell addSubview:imageView];
        
        
        UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(110), KWIDTH(35), KWIDTH(160), KWIDTH(20))];
        if ([userData.nickname length] > 0) {
            name.text = [userData nickname];
        }else {
            name.text = @"未设置";
        }
        name.textColor = [UIColor whiteColor];
        name.font = [UIFont systemFontOfSize:KWIDTH(14)];
        [cell addSubview:name];
        
        UILabel *tel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(110), KWIDTH(55), KWIDTH(160), KWIDTH(20))];
        tel.text = userData.phone;
        tel.textColor = [UIColor whiteColor];
        tel.font = [UIFont boldSystemFontOfSize:KWIDTH(12)];
        [cell addSubview:tel];
        
        
        
        UIImageView *setting = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(40), KWIDTH(45), KWIDTH(30), KWIDTH(30))];
        setting.image = [UIImage imageNamed:@"set_up"];

        [cell addSubview:setting];
        
        cell.backgroundColor = mRGB(11, 104, 212, 1);
    }else if (indexPath.section == 1) {
        _balanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH(10), SCREEN_SIZE.width / 3, KWIDTH(20))];
        _balanceLabel.text = userData.now_money;
        _balanceLabel.font = [UIFont systemFontOfSize:KWIDTH(18)];
        _balanceLabel.textColor = [UIColor lightGrayColor];
        _balanceLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:_balanceLabel];
        
        _scoresLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 3, KWIDTH(10), SCREEN_SIZE.width / 3, KWIDTH(20))];
        _scoresLabel.text = userData.score_count;
        _scoresLabel.font = [UIFont systemFontOfSize:KWIDTH(18)];
        _scoresLabel.textColor = [UIColor lightGrayColor];
        _scoresLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:_scoresLabel];

        _levelLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 3 * 2, KWIDTH(10), SCREEN_SIZE.width / 3, KWIDTH(20))];
        _levelLabel.text = [NSString stringWithFormat:@"VIP%@", userData.level];
        _levelLabel.font = [UIFont fontWithName:@"Marion" size:KWIDTH(18)];
        _levelLabel.textColor = [UIColor lightGrayColor];
        _levelLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:_levelLabel];
        
        NSArray *arr = @[@"余额", @"积分", @"等级"];
        for (int i = 0; i < 3; i++) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 3 * i, KWIDTH(30), SCREEN_SIZE.width / 3, KWIDTH(30))];
            label.text = arr[i];
            label.font = [UIFont systemFontOfSize:KWIDTH(14)];
            label.textColor = [UIColor blackColor];
            label.textAlignment = NSTextAlignmentCenter;
            [cell addSubview:label];
        }
        
    }else if (indexPath.section == 2) {
        
        for (int i = 0; i < _orderTitles.count; i++) {
            NSLog(@"%d", i/4);
            WHHButton *btn = [[WHHButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 4 * (i % 4) ,KWIDTH(70) * (i / 4), SCREEN_SIZE.width / 4, KWIDTH(70)) andTitle:_orderTitles[i] image:_orderIcons[i]];
            btn.tag = 100 + i;
            [btn addTarget:self action:@selector(orderList:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:btn];
        }
    }else if (indexPath.section == 3) {
        NSArray *titles = @[@"平台优惠券", @"商家优惠券", @"会员卡", @"参与活动"];
        NSArray *images = @[@"gezxtp_09", @"gezxtp_14", @"gezxtp_06", @"gezxtp_03"];
        
        for (int i = 0; i < 4; i++) {
            HGCardButton *btn = [[HGCardButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 4 * i, 0, SCREEN_SIZE.width / 4, KWIDTH(90)) withImage:images[i] title:titles[i] number:_couponNums[i]];
            btn.tag = 1000 + i;
            [btn addTarget:self action:@selector(privilegeAndActivity:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:btn];
        }
    }else if (indexPath.section == 4) {

        for (int i = 0; i < _collectTitles.count; i++) {
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 3 * i, 0, SCREEN_SIZE.width / 3, KWIDTH(60))];
            [btn setTitle:_collectTitles[i] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
            btn.tag = 10 + i;
            [btn addTarget:self action:@selector(collectionAndAttention:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:btn];
            
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / _collectTitles.count * (i + 1), KWIDTH(15), 1, KWIDTH(30))];
            line.backgroundColor = [UIColor lightGrayColor];
            [cell addSubview:line];
        }
    }
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 1) {
        UIView *view = [self viewWithTitle:@"我的钱包" section:0 next:YES bottomLine:YES];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(myPurseTap:)];
        [view addGestureRecognizer:tap];
        return view;
    }else if (section == 2) {
        return [self viewWithTitle:@"我的订单" section:1 next:NO bottomLine:YES];
    }else if (section == 3) {
        return [self viewWithTitle:@"我的卡券" section:2 next:NO bottomLine:YES];
    }else if (section == 4) {
        return [self viewWithTitle:@"收藏关注" section:3 next:NO bottomLine:YES];
    }else if (section == 5) {
        return [self viewWithTitle:@"我的车型" section:5 next:YES bottomLine:NO];
    }
    return nil;
}


- (UIView *)viewWithTitle:(NSString *)title section:(NSInteger)section next:(BOOL)next bottomLine:(BOOL)hasBottomLine {
    NSArray *imageArr = @[@"money", @"order", @"action", @"follow", @"extension", @"cx"];
    HeaderButton *btn = [[HeaderButton alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(45)) andTitle:title imageName:imageArr[section] haveNext:next bottomline:hasBottomLine];
    [btn addTarget:self action:@selector(gotoDetail:) forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.1;
    }else {
        return KWIDTH(45);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return KWIDTH(120);
    }else if (indexPath.section == 1) {
        return KWIDTH(60);
    }else if (indexPath.section == 2) {
        return KWIDTH(70);
    }else if (indexPath.section == 3) {
        return KWIDTH(90);
    }else if (indexPath.section == 4) {
        return KWIDTH(60);
    }else {
        return KWIDTH(50);
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return KWIDTH(0.1);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(0.1))];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        SettingViewController *setting = [[SettingViewController alloc] init];
        [self.navigationController pushViewController:setting animated:YES];
    }else if (indexPath.section == 1) {
        PurseViewController *purse = [[PurseViewController alloc] init];
        [self.navigationController pushViewController:purse animated:YES];
    }
}

- (void)myPurseTap:(UITapGestureRecognizer *)tap {
    PurseViewController *purse = [[PurseViewController alloc] init];
    [self.navigationController pushViewController:purse animated:YES];
}



- (void)gotoDetail:(UIButton *)btn {
    
    if ([btn.titleLabel.text isEqualToString:@"我的钱包"]) {
        PurseViewController *purse = [[PurseViewController alloc] init];
        [self.navigationController pushViewController:purse animated:YES];
    }else if ([btn.titleLabel.text isEqualToString:@"我的订单"]) {
        
    }else if ([btn.titleLabel.text isEqualToString:@"我的卡券"]) {
        
    }else if ([btn.titleLabel.text isEqualToString:@"优惠/活动"]) {
        
    }else if ([btn.titleLabel.text isEqualToString:@"我的车型"]) {
        
    }
}

#pragma mark -- 订单列表

- (void)orderList:(UIButton *)btn {
    HGOrderWebViewController *webview = [[HGOrderWebViewController alloc] init];
    NSInteger index = btn.tag - 100;
    webview.urlString
    = _orderURLs[index];
    [self.navigationController pushViewController:webview animated:YES];
//    switch (btn.tag - 100) {
//        case 0:
//        {
//            
//            OrderAppointmentViewController *appointmentOrder = [[OrderAppointmentViewController alloc] init];
//            [self.navigationController pushViewController:appointmentOrder animated:YES];
//            
//            
//        }
//            break;
//        case 1:
//        {
//            OrderGroupViewController *groupOrder = [[OrderGroupViewController alloc] init];
//            [self.navigationController pushViewController:groupOrder animated:YES];
//            
//        }
//            break;
//        case 2:
//        {
//            OrderStoreViewController *storeOrder = [[OrderStoreViewController alloc] init];
//            [self.navigationController pushViewController:storeOrder animated:YES];
//
//        }
//            break;
//        case 3:
//        {
//            OrderGiftViewController *carOrder = [[OrderGiftViewController alloc] init];
//            [self.navigationController pushViewController:carOrder animated:YES];
//        }
//            break;
//        default:
//            break;
//    }
}

#pragma mark -- 登录

- (void)gotoLogin {
    LoginViewController *login = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:login animated:NO];
}

#pragma mark -- 优惠/活动
- (void)privilegeAndActivity:(WHHButton *)btn {
    HGOrderWebViewController *webview = [[HGOrderWebViewController alloc] init];
    NSInteger index = btn.tag - 1000;
    webview.urlString = _couponURLs[index];
    [self.navigationController pushViewController:webview animated:YES];
//    switch (btn.tag - 1000) {
//        case 0:
//        {
//            
//            MyCouponViewController *myCoupon = [[MyCouponViewController alloc] init];
//            [self.navigationController pushViewController:myCoupon animated:YES];
//        }
//            break;
//        case 1:
//        {
//            MyCouponViewController *myCoupon = [[MyCouponViewController alloc] init];
//            [self.navigationController pushViewController:myCoupon animated:YES];
//        }
//            break;
//        case 2:
//        {
//            MyMembershipCardViewController *myMembership = [[MyMembershipCardViewController alloc] init];
//            [self.navigationController pushViewController:myMembership animated:YES];
//        }
//            break;
//        case 3:
//        {
//            MyActivityViewController *myActivity = [[MyActivityViewController alloc] init];
//            [self.navigationController pushViewController:myActivity animated:YES];
//        }
//            break;
//        default:
//            break;
//    }
    
}


- (void)collectionAndAttention:(UIButton *)btn {
    
    HGOrderWebViewController *webview = [[HGOrderWebViewController alloc] init];
    NSInteger index = btn.tag - 10;
    webview.urlString = _collectionURLs[index];
    [self.navigationController pushViewController:webview animated:YES];

//    if ([btn.titleLabel.text isEqualToString:@"关注商家"]) {
//        
//        AttentionMerchantViewController *attention = [[AttentionMerchantViewController alloc] init];
//        [self.navigationController pushViewController:attention animated:YES];
//    }else if ([btn.titleLabel.text isEqualToString:@"团购收藏"]) {
//        CollectGroupViewController *collectGroup = [[CollectGroupViewController alloc] init];
//        [self.navigationController pushViewController:collectGroup animated:YES];
//    }else if ([btn.titleLabel.text isEqualToString:@"预约收藏"]) {
//        CollectAppointmentViewController *collectAppointment = [[CollectAppointmentViewController alloc] init];
//        [self.navigationController pushViewController:collectAppointment animated:YES];
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
