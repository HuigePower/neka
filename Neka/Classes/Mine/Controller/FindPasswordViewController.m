//
//  FindPasswordViewController.m
//  Neka1.0
//
//  Created by ma c on 2017/3/7.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "FindPasswordViewController.h"
#import "HGVerificationButton.h"
@interface FindPasswordViewController ()<UIScrollViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong)UITextField *account;
@property (nonatomic, strong)UITextField *testing;
@property (nonatomic, strong)UITextField *password;

@property (nonatomic, strong)HGVerificationButton *verificationBtn;
@end

@implementation FindPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"找回密码";
    [self.tabBarController.tabBar setHidden:YES];
    self.view.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
    scrollView.delegate = self;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.contentSize = CGSizeMake(SCREEN_SIZE.width, scrollView.bounds.size.height + 30);
    [self.view addSubview:scrollView];
    
    
    
    UIView *view = [[UIButton alloc] initWithFrame:CGRectMake(0, KWIDTH(30), SCREEN_SIZE.width, 132)];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.borderColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0].CGColor;
    view.layer.borderWidth = 0.5f;
    [scrollView addSubview:view];
    
    //手机号
    _account = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(20), 0, SCREEN_SIZE.width - KWIDTH(20) - KWIDTH(100), 44)];
    _account.placeholder = @"手机号";
    _account.delegate = self;
    _account.keyboardType = UIKeyboardTypeNumberPad;
    [view addSubview:_account];
    
    for (int i = 1; i < 3; i++) {
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), 44 * i, SCREEN_SIZE.width - KWIDTH(20), 0.5)];
        line.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
        [view addSubview:line];
    }

    //验证码
    _password = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(20), 44, SCREEN_SIZE.width - KWIDTH(140), 44)];
    _password.placeholder = @"新密码";
    _password.delegate = self;
    _password.keyboardType = UIKeyboardTypeNumberPad;
    [view addSubview:_password];
    
    
    //验证码
    _testing = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(20), 88, SCREEN_SIZE.width - KWIDTH(140), 44)];
    _testing.placeholder = @"填写短信验证码";
    _testing.delegate = self;
    _testing.keyboardType = UIKeyboardTypeNumberPad;
    [view addSubview:_testing];
    
    //获取验证码
    _verificationBtn = [[HGVerificationButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(100), KWIDTH(12), KWIDTH(80), 44 - 24)];
    [_verificationBtn addTarget:self action:@selector(getTestingNumber:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_verificationBtn];
    
    
    //注册
    UIButton *commitBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(20), view.frame.origin.y + view.frame.size.height + KWIDTH(20), SCREEN_SIZE.width - KWIDTH(40), KWIDTH(40))];
    commitBtn.backgroundColor = mRGB(0, 126, 222, 1);
    commitBtn.layer.masksToBounds = YES;
    commitBtn.layer.cornerRadius = 5;
    [commitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [commitBtn addTarget:self action:@selector(commit:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:commitBtn];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _account) {
        if ([_account.text stringByAppendingString:string].length > 11) {
            return NO;
        }
    }else if (textField == _testing) {
        if ([_testing.text stringByAppendingString:string].length > 4) {
            return NO;
        }
    }else if (textField == _password) {
        if ([_password.text stringByAppendingString:string].length > 18) {
            return NO;
        }
    }
    
    return YES;
}

- (void)getTestingNumber:(HGVerificationButton *)btn {
    NSLog(@"获取验证码");
    
    if (_account.text.length == 0) {
        [MBProgressHUD showSuccess:@"手机号不能为空！"];
        return;
    }else if (_account.text.length < 11) {
        [MBProgressHUD showSuccess:@"手机号错误！"];
        return;
    }
    [btn startTiming];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [self showLoading];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"phone": _account.text, @"type": @"2"};
    [manager POST:[NSString stringWithFormat:@"%@c=Login&a=sendCode", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"已发送"];
        }else {
            [MBProgressHUD showSuccess:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showSuccess:error.description];
    }];

}

- (void)commit:(UIButton *)btn {
    
    if (_password.text.length < 6) {
        [MBProgressHUD showError:@"密码不能少于6位"];
        return;
    }
    
    if (_testing.text.length != 4) {
        [MBProgressHUD showError:@"验证码格式错误"];
        return;
    }
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"phone": _account.text, @"passwd": _password.text, @"code": _testing.text};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Login&a=forget", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"修改成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [MBProgressHUD showSuccess:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showSuccess:error.description];
    }];
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
