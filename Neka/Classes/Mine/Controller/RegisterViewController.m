//
//  RegisterViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/8.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "RegisterViewController.h"
#import "HGVerificationButton.h"
@interface RegisterViewController ()<UIScrollViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong)UITextField *account;
@property (nonatomic, strong)UITextField *password;
@property (nonatomic, strong)UITextField *testing;
@property (nonatomic, strong)HGVerificationButton *verficationBtn;
@property (nonatomic, strong)UIButton *showPassword;


@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"注册";
    [self.tabBarController.tabBar setHidden:YES];
    self.view.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
    scrollView.delegate = self;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.contentSize = CGSizeMake(SCREEN_SIZE.width, scrollView.bounds.size.height + 30);
    [self.view addSubview:scrollView];
    
    
    
    UIView *view = [[UIButton alloc] initWithFrame:CGRectMake(0, KWIDTH(30), SCREEN_SIZE.width, 132)];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.borderColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0].CGColor;
    view.layer.borderWidth = 0.5f;
    [scrollView addSubview:view];
    
    //手机号
    _account = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(20), 0, SCREEN_SIZE.width - KWIDTH(40), 44)];
    _account.placeholder = @"手机号";
    _account.delegate = self;
    _account.keyboardType = UIKeyboardTypeNumberPad;
    [view addSubview:_account];
    
    for (int i = 1; i < 3                                                                                                  ; i++) {
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), 44 * i, SCREEN_SIZE.width - KWIDTH(20), 0.5)];
        line.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
        [view addSubview:line];
    }
    //密码
    _password = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(20), 44, SCREEN_SIZE.width - KWIDTH(40), 44)];
    _password.placeholder = @"请输入您的密码";
    _password.delegate = self;
    _password.secureTextEntry = YES;
    _password.keyboardType = UIKeyboardTypeTwitter;
    [view addSubview:_password];
    
    //验证码
    _testing = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(20), 88, SCREEN_SIZE.width - KWIDTH(140), 44)];
    _testing.placeholder = @"填写短信验证码";
    _testing.delegate = self;
    _testing.keyboardType = UIKeyboardTypeNumberPad;
    [view addSubview:_testing];
    
    
    //获取验证码
    _verficationBtn = [[HGVerificationButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(100), 88 + 12, KWIDTH(80), 44 - 24)];
    [_verficationBtn addTarget:self action:@selector(getTestingNum) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_verficationBtn];
    
    
    //注册
    UIButton *registerBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(20), view.frame.origin.y + view.frame.size.height + KWIDTH(20), SCREEN_SIZE.width - KWIDTH(40), KWIDTH(40))];
    registerBtn.backgroundColor = mRGB(0, 126, 222, 1);
    registerBtn.layer.masksToBounds = YES;
    registerBtn.layer.cornerRadius = 5;
    [registerBtn addTarget:self action:@selector(registerClick) forControlEvents:UIControlEventTouchUpInside];
    [registerBtn setTitle:@"注册" forState:UIControlStateNormal];
    [scrollView addSubview:registerBtn];
    
    
    //立即登录
    UIButton *forgetPassword = [[UIButton alloc] initWithFrame:CGRectMake(registerBtn.frame.origin.x, registerBtn.frame.origin.y + registerBtn.frame.size.height + KWIDTH(10), KWIDTH(60), KWIDTH(20))];
    forgetPassword.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [forgetPassword setTitle:@"立即登录" forState:UIControlStateNormal];
        forgetPassword.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    [forgetPassword setTitleColor:mRGB(0, 126, 222, 1) forState:UIControlStateNormal];
    [forgetPassword addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:forgetPassword];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _account) {
        if ([_account.text stringByAppendingString:string].length > 11) {
            return NO;
        }
    }else if (textField == _testing) {
        if ([_testing.text stringByAppendingString:string].length > 4) {
            return NO;
        }
    }else if (textField == _password) {
        if ([_password.text stringByAppendingString:string].length > 18) {
            return NO;
        }
    }
    
    return YES;
}

- (void)login {
    NSLog(@"立即登录");
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)registerClick {
    NSLog(@"注册");
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [self showLoading];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"phone": _account.text, @"passwd": _password.text, @"code": _testing.text, @"client": @"1"};
    [manager POST:[NSString stringWithFormat:@"%@c=Login&a=register", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"注册成功！"];
            NSDictionary *result = responseObject[@"result"];
            if (result[@"ticket"]) {
                [UserDefaultManager saveTicket:result[@"ticket"]];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        }else {
            [MBProgressHUD showSuccess:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    
}

- (void)getTestingNum {
    NSLog(@"获取验证码");
    if (_account.text.length == 0) {
        [MBProgressHUD showSuccess:@"手机号不能为空！"];
        return;
    }else if (_account.text.length < 11) {
        [MBProgressHUD showSuccess:@"手机号错误！"];
        return;
    }
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [self showLoading];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"phone": _account.text, @"type": @"1"};
    [manager POST:[NSString stringWithFormat:@"%@c=Login&a=sendCode", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"已发送"];
        }else {
            [MBProgressHUD showSuccess:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
    }];

    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
