//
//  HGWebRechargeViewController.m
//  Neka
//
//  Created by ma c on 2017/7/11.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGWebRechargeViewController.h"
#import "HGConfirmRechargeOrderViewController.h"
@interface HGWebRechargeViewController ()

@end

@implementation HGWebRechargeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = request.URL;
    NSLog(@"%@", url);
    NSLog(@"%s", __func__);
    
    NSString *scheme = url.scheme;
    
    
    if ([scheme isEqualToString:@"pigcmso2o"]) {
        NSString *host = url.host;
        NSLog(@"%@", url.host);
        NSArray *pathItems = [url.path componentsSeparatedByString:@"/"];
        NSLog(@"paths:%@", pathItems);
        NSString *orderType = pathItems[1];
        NSString *orderId = pathItems[2];
        
        if ([host isEqualToString:@"gopay"]) {
            [self gopayWithType:orderType andOrderId:orderId];
        }else {
            NSLog(@"---------------\n\n%@\n\n---------------------", url.host);
        }
        
    }
    
    return YES;
}


- (void)gopayWithType:(NSString *)type andOrderId:(NSString *)orderId {
    NSLog(@"支付用途:%@, 订单id:%@", type, orderId);
    HGConfirmRechargeOrderViewController *confirmOrder = [[HGConfirmRechargeOrderViewController alloc] init];
    confirmOrder.type = type;
    confirmOrder.order_id = orderId;
    [self.navigationController pushViewController:confirmOrder animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
