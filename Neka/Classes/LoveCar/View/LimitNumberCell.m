//
//  LimitNumberCell.m
//  Neka
//
//  Created by ma c on 2017/11/20.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "LimitNumberCell.h"
#import "LimitNumberManager.h"
@implementation LimitNumberCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)switchValueChanged:(UISwitch *)sender {
    NSLog(@"%d", sender.isOn);
    if (_cityChangedBlock) {
        _cityChangedBlock(_city, sender.isOn);
    }
}

- (void)setCity:(NSString *)city {
    _city = city;
    self.textLabel.text = _city;
    if ([LimitNumberManager limitNumberCityhasCity:_city]) {
        [_switchView setOn:YES];
    }else {
        [_switchView setOn:NO];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
