//
//  HGDatePickerView.m
//  HGDatePicker
//
//  Created by ma c on 2016/12/24.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGDatePickerView.h"

@implementation HGDatePickerView {
    NSInteger _yearIndex;
    NSInteger _monthIndex;
    NSInteger _currentYear;
    NSInteger _currentMonth;
    NSString *_resultDate;
}


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor grayColor];
        [self initDataSource];
        [self makeSubviews];
    }
    return self;
}

- (void)initDataSource {
    _yearIndex = 0;
    _monthIndex = 0;
    _yearsList = [NSMutableArray new];
    _monthsList = [NSMutableArray new];
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM"];
    NSString *date = [formatter stringFromDate:currentDate];
    _currentYear = [[date substringWithRange:NSMakeRange(0, 4)] integerValue];
    _currentMonth = [[date substringWithRange:NSMakeRange(5, 2)] integerValue];
    for (NSInteger i = _currentYear; i >= 1990; i--) {
        NSString *yearStr = [NSString stringWithFormat:@"%ld年", i];
        [_yearsList addObject:yearStr];
    }
    
    for (int j = 1; j <= 12; j++) {
        NSString *monthStr = [NSString stringWithFormat:@"%d月", j];
        [_monthsList addObject:monthStr];
    }

}

- (void)makeSubviews {
    //选择时间
    _datePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, self.frame.size.width, 180)];
    _datePicker.backgroundColor = [UIColor colorWithRed:230 / 255.0 green:230 / 255.0 blue:230 / 255.0 alpha:1];
    _datePicker.delegate = self;
    _datePicker.showsSelectionIndicator = YES;
    [self addSubview:_datePicker];
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
    topView.backgroundColor = [UIColor colorWithRed:240 / 255.0 green:240 / 255.0 blue:240 / 255.0 alpha:1];
    UIButton *finishBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 50, 10, 40, 20)];
    [finishBtn setTitle:@"完成" forState:UIControlStateNormal];
    [finishBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    finishBtn.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [finishBtn addTarget:self action:@selector(finishedSelectDate:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:finishBtn];
    [self addSubview:topView];
}

- (void)finishedSelectDate:(UIButton *)btn {
    if (_hideView != nil) {
        _hideView();
    }
}

#pragma mark -- UIPickerDeledate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return _yearsList.count;
    }else {
        return 12;
    }
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        return _yearsList[row];
    }else {
        return _monthsList[_yearIndex][row];
    }
}

- (nullable NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (component == 0) {
        NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:_yearsList[row] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}];
        return attStr;
    }else {
        
        if (_yearIndex == 0) {
            if (row < _currentMonth) {
                NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:_monthsList[row] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}];
                NSLog(@"<");
                return attStr;
            }else {
                NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:_monthsList[row] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12], NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
                NSLog(@">");
                return attStr;
            }
            
        }
        NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:_monthsList[row] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}];
        return attStr;
    }
    
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (component == 0) {
        _yearIndex = row;
        [_datePicker reloadComponent:1];
    }else {
        _monthIndex = row;
    }
    
    NSString *dateStr = [NSString stringWithFormat:@"%@%@", _yearsList[_yearIndex], _monthsList[_monthIndex]];
    NSMutableString *yearTmp = [[NSMutableString alloc] initWithString:_yearsList[_yearIndex]];
    NSMutableString *monthTmp = [[NSMutableString alloc] initWithString:_monthsList[_monthIndex]];
    NSString *year = [yearTmp componentsSeparatedByString:@"年"][0];
    NSString *month = [monthTmp componentsSeparatedByString:@"月"][0];
    
    if (_yearIndex == 0 && _monthIndex >= _currentMonth) {
        [_datePicker selectRow:_currentMonth - 1 inComponent:1 animated:YES];
        dateStr = [NSString stringWithFormat:@"%ld年%ld月", (long)_currentYear, (long)_currentMonth];
        _resultDate = dateStr;
        year = [NSString stringWithFormat:@"%ld", (long)_currentYear];
        month = [NSString stringWithFormat:@"%ld", (long)_currentMonth];
    }
    NSDictionary *dateDic = @{@"year": year, @"month": month};
    if (_selectResult != nil) {
        _selectResult(dateDic);
    }
}



@end
