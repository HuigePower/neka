//
//  HGSelectCarSeriesView.m
//  Neka1.0
//
//  Created by ma c on 2017/4/12.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGSelectCarSeriesView.h"
#define TopHeight KWIDTH(60) //按钮的高度
#define LineHeight 2         //底部标示线高度
#define ButtonWidth SCREEN_SIZE.width / 4
@implementation HGSelectCarSeriesView 
{
    UIView *_topView;
    UITableView *_tableView;
    UIView  *_lineView;
    NSArray *_list;
    NSInteger _index;
}
- (instancetype)initWithFrame:(CGRect)frame andSeriesList:(NSArray *)seriesList {
    if (self = [super initWithFrame:frame]) {
        _index = 0;
        _list = seriesList;
        
        self.backgroundColor = [UIColor lightGrayColor];
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, TopHeight)];
        _topView.backgroundColor = [UIColor whiteColor];
        for (int i = 0; i < _list.count; i++) {
            NSDictionary *seriesData = _list[i];
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / _list.count * i, 0, ButtonWidth, TopHeight)];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(32), KWIDTH(32))];
            [imageView setImageWithURL:[NSURL URLWithString:seriesData[@"cat_pic"]]];
            imageView.center = CGPointMake(btn.frame.size.width / 2, KWIDTH(20));
            [btn addSubview:imageView];
            
            UILabel *mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH(40), ButtonWidth, KWIDTH(15))];
            mainLabel.text = seriesData[@"cat_name"];
            mainLabel.textColor = mRGB(0, 126, 222, 1);
            mainLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
            mainLabel.textAlignment = NSTextAlignmentCenter;
            [btn addSubview:mainLabel];
    
            
            [btn addTarget:self action:@selector(selectedCarBrand:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = 10 + i;
            [_topView addSubview:btn];
        }

        [self addSubview:_topView];
        
        
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, TopHeight - 0.5, frame.size.width, 0.5)];
        line.backgroundColor = [UIColor lightGrayColor];
        [_topView addSubview:line];
        
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, TopHeight - LineHeight, ButtonWidth, LineHeight)];
        _lineView.backgroundColor = mRGB(1, 125, 221, 1);
        [self addSubview:_lineView];
        

        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, TopHeight, frame.size.width, frame.size.height - TopHeight)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        [self addSubview:_tableView];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    }
    return self;
}

- (void)selectedCarBrand:(UIButton *)btn {
    _index = btn.tag - 10;
    
    [UIView animateWithDuration:0.5 animations:^{
        _lineView.frame = CGRectMake(ButtonWidth * _index, TopHeight - LineHeight, ButtonWidth, LineHeight);
    }];
    [_tableView reloadData];
    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *list = _list[_index][@"category_list"];
    return list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    NSArray *list = _list[_index][@"category_list"];
    
    cell.textLabel.text = list[indexPath.row][@"cat_name"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *info = _list[_index];
    NSDictionary *data = info[@"category_list"][indexPath.row];
    if (_result) {
        _result(data);
    }
}



#pragma mark -- cell的分割线宽度等于屏幕宽度
-(void)viewDidLayoutSubviews {
    
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)])  {
        [_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}


@end
