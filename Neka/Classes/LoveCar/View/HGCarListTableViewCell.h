//
//  HGCarListTableViewCell.h
//  Neka1.0
//
//  Created by ma c on 2016/12/24.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGCarInfoModel.h"

typedef void(^HGCarListEdit)(HGCarInfoModel *model);
typedef void(^HGCarListDelete)(HGCarInfoModel *model);

@interface HGCarListTableViewCell : UITableViewCell
@property (nonatomic, strong)HGCarInfoModel *carInfoModel;

@property (nonatomic, copy)HGCarListEdit editBlock;
@property (nonatomic, copy)HGCarListDelete deleteBlock;

@end
