//
//  HGDatePickerView.h
//  HGDatePicker
//
//  Created by ma c on 2016/12/24.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^HGDatePickerFinished)(NSDictionary *dateDic);
typedef void(^HGDatePickerHideView)();
@interface HGDatePickerView : UIView <UIPickerViewDelegate, UIPickerViewDataSource> 
//时间选择器

@property (nonatomic, strong)UIPickerView *datePicker;
@property (nonatomic, strong)NSMutableArray *yearsList;
@property (nonatomic, strong)NSMutableArray *monthsList;

@property (nonatomic, copy)HGDatePickerFinished selectResult;
@property (nonatomic, copy)HGDatePickerHideView hideView;
@end
