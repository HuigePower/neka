//
//  HGProvinceCodeView.m
//  Neka1.0
//
//  Created by ma c on 2016/12/24.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGProvinceCodeView.h"

@implementation HGProvinceCodeView
{
    NSArray *_provinceCodeList;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor grayColor];
        _provinceCodeList = @[
                              @{
                                  @"cat_city_id": @"1",
                                  @"cat_num_name": @"京"
                                  },
                              @{
                                  @"cat_city_id": @"2",
                                  @"cat_num_name": @"津"
                                  },
                              @{
                                  @"cat_city_id": @"3",
                                  @"cat_num_name": @"冀"
                                  },
                              @{
                                  @"cat_city_id": @"4",
                                  @"cat_num_name": @"沪"
                                  },
                              @{
                                  @"cat_city_id": @"5",
                                  @"cat_num_name": @"渝"
                                  },
                              @{
                                  @"cat_city_id": @"6",
                                  @"cat_num_name": @"豫"
                                  },
                              @{
                                  @"cat_city_id": @"7",
                                  @"cat_num_name": @"云"
                                  },
                              @{
                                  @"cat_city_id": @"8",
                                  @"cat_num_name": @"辽"
                                  },
                              @{
                                  @"cat_city_id": @"9",
                                  @"cat_num_name": @"黑"
                                  },
                              @{
                                  @"cat_city_id": @"10",
                                  @"cat_num_name": @"湘"
                                  },
                              @{
                                  @"cat_city_id": @"11",
                                  @"cat_num_name": @"皖"
                                  },
                              @{
                                  @"cat_city_id": @"12",
                                  @"cat_num_name": @"鲁"
                                  },
                              @{
                                  @"cat_city_id": @"13",
                                  @"cat_num_name": @"新"
                                  },
                              @{
                                  @"cat_city_id": @"14",
                                  @"cat_num_name": @"苏"
                                  },
                              @{
                                  @"cat_city_id": @"15",
                                  @"cat_num_name": @"浙"
                                  },
                              @{
                                  @"cat_city_id": @"16",
                                  @"cat_num_name": @"赣"
                                  },
                              @{
                                  @"cat_city_id": @"17",
                                  @"cat_num_name": @"鄂"
                                  },
                              @{
                                  @"cat_city_id": @"18",
                                  @"cat_num_name": @"桂"
                                  },
                              @{
                                  @"cat_city_id": @"19",
                                  @"cat_num_name": @"甘"
                                  },
                              @{
                                  @"cat_city_id": @"20",
                                  @"cat_num_name": @"晋"
                                  },
                              @{
                                  @"cat_city_id": @"21",
                                  @"cat_num_name": @"蒙"
                                  },
                              @{
                                  @"cat_city_id": @"22",
                                  @"cat_num_name": @"陕"
                                  },
                              @{
                                  @"cat_city_id": @"23",
                                  @"cat_num_name": @"吉"
                                  },
                              @{
                                  @"cat_city_id": @"24",
                                  @"cat_num_name": @"闽"
                                  },
                              @{
                                  @"cat_city_id": @"25",
                                  @"cat_num_name": @"贵"
                                  },
                              @{
                                  @"cat_city_id": @"26",
                                  @"cat_num_name": @"粤"
                                  },
                              @{
                                  @"cat_city_id": @"27",
                                  @"cat_num_name": @"青"
                                  },
                              @{
                                  @"cat_city_id": @"28",
                                  @"cat_num_name": @"藏"
                                  },
                              @{
                                  @"cat_city_id": @"29",
                                  @"cat_num_name": @"川"
                                  },
                              @{
                                  @"cat_city_id": @"30",
                                  @"cat_num_name": @"宁"
                                  },
                              @{
                                  @"cat_city_id": @"31",
                                  @"cat_num_name": @"琼"
                                  }
                              ];
        [self makeSubviews];
    }
    return self;
}

- (void)makeSubviews {
    
    
    _picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, SCREEN_SIZE.width, 180)];
    _picker.backgroundColor = mRGB(230, 230, 230, 1);
    _picker.delegate = self;
    [self addSubview:_picker];
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 40)];
    topView.backgroundColor = mRGB(240, 240, 240, 1);
    UIButton *finishBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - 50, 10, 40, 20)];
    [finishBtn setTitle:@"完成" forState:UIControlStateNormal];
    [finishBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    finishBtn.titleLabel.font = [UIFont boldSystemFontOfSize:KWIDTH(12)];
    [finishBtn addTarget:self action:@selector(finishedSelectProvinceCode:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:finishBtn];
    [self addSubview:topView];
}

- (void)finishedSelectProvinceCode:(UIButton *)btn {
    if (_hideView != nil) {
        _hideView();
    }
}


#pragma mark -- UIPickerDeledate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _provinceCodeList.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return _provinceCodeList[row][@"cat_num_name"];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (_selectResult != nil) {
        _selectResult(_provinceCodeList[row]);
    }
}




@end
