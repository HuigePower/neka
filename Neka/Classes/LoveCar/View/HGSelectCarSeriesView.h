//
//  HGSelectCarSeriesView.h
//  Neka1.0
//
//  Created by ma c on 2017/4/12.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^SelectResultBlock)(NSDictionary *data);
@interface HGSelectCarSeriesView : UIView<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy)SelectResultBlock result;

- (instancetype)initWithFrame:(CGRect)frame andSeriesList:(NSArray *)seriesList;

@end
