//
//  HGTextField.m
//  Neka1.0
//
//  Created by ma c on 2016/12/21.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGTextField.h"

@implementation HGTextField

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 40)];
        topView.backgroundColor = mRGB(240, 240, 240, 1);
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - 50, 10, 40, 20)];
        [button setTitle:@"完成" forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(finish:) forControlEvents:UIControlEventTouchUpInside];
        [topView addSubview:button];
        [self setInputAccessoryView:topView];
    }
    return self;
}


- (void)finish:(UIButton *)btn {
    [self resignFirstResponder];
}

@end
