//
//  HGCarServiceCell.m
//  Neka1.0
//
//  Created by ma c on 2016/12/25.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGCarServiceCell.h"

@implementation HGCarServiceCell {
    UIImageView *_topLine;
    UIImageView *_middleBottomLineView;
    UIImageView *_iconView;
    UILabel *_nameLabel;
    UILabel *_merchantNameLabel;
    UILabel *_oldPriceLabel;
    UILabel *_priceLabel;
    UILabel *_numberLabel;
    
    UIButton *_btn1;
    UIButton *_btn2;
    UIButton *_btn3;
    
    UIView *_topView;
    UIView *_middleAndBottomView;
    
}



- (void)makeSubviews {
    
    //1.顶部
    _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(40))];
    //顶部cell标线
    _topLine = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(12), KWIDTH(20), SCREEN_SIZE.width - KWIDTH(25), KWIDTH(20))];
    _topLine.image = [UIImage imageNamed:@"top_line"];
    [_topView addSubview:_topLine];
    
//    _btn1 = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(30), KWIDTH(5), KWIDTH(90), KWIDTH(30))];
//    _btn1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btnSelectedBack"]];
//    [_btn1 setTitle:@"升级项目" forState:UIControlStateNormal];
//    _btn1.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
//    [_btn1 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//    [_btn1 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [_topView addSubview:_btn1];
//    
//    _btn2 = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(125), KWIDTH(5), KWIDTH(90), KWIDTH(30))];
//    _btn2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btnBack"]];
//    [_btn2 setTitle:@"贴膜项目" forState:UIControlStateNormal];
//    _btn2.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
//    [_btn2 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//    [_btn2 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [_topView addSubview:_btn2];
//    
//    _btn3 = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(220), KWIDTH(5), KWIDTH(90), KWIDTH(30))];
//    _btn3.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btnBack"]];
//    [_btn3 setTitle:@"常用配件" forState:UIControlStateNormal];
//    _btn3.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
//    [_btn3 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//    [_btn3 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [_topView addSubview:_btn3];
    
    
    //2.中间
    _middleAndBottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(120))];
    //中部cell标线
    _middleBottomLineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(40), KWIDTH(120))];
    [_middleAndBottomView addSubview:_middleBottomLineView];
    
    
    _iconView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(35), KWIDTH(20), KWIDTH(120), KWIDTH(80))];
    _iconView.layer.borderColor = mRGB(109, 179, 233, 1).CGColor;
    _iconView.layer.borderWidth = 1;
    [_middleAndBottomView addSubview:_iconView];
    
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(160), KWIDTH(20), KWIDTH(140), KWIDTH(20))];
    _nameLabel.font = [UIFont systemFontOfSize:14];
    [_middleAndBottomView addSubview:_nameLabel];
    
    _merchantNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(160), KWIDTH(40), KWIDTH(140), KWIDTH(20))];
    _merchantNameLabel.font = [UIFont systemFontOfSize:12];
    [_middleAndBottomView addSubview:_merchantNameLabel];
    
    
    _oldPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(160), KWIDTH(60), KWIDTH(140), KWIDTH(20))];
    _oldPriceLabel.font = [UIFont systemFontOfSize:12];
    _oldPriceLabel.textColor = [UIColor grayColor];
    [_middleAndBottomView addSubview:_oldPriceLabel];
    
    
    
    _priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(160), KWIDTH(80), KWIDTH(100), KWIDTH(20))];
    _priceLabel.font = [UIFont systemFontOfSize:12];
    [_middleAndBottomView addSubview:_priceLabel];
    
    _numberLabel  = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(260), KWIDTH(80), KWIDTH(40), KWIDTH(20))];
    _numberLabel.font = [UIFont systemFontOfSize:12];
    _numberLabel.textAlignment = NSTextAlignmentRight;
    [_middleAndBottomView addSubview:_numberLabel];
}


//- (void)makeTopView {
//    [self addSubview:_topView];
//    [_middleAndBottomView removeFromSuperview];
//}

- (void)makeMiddleViewWithData:(NSDictionary *)data {
    [self addSubview:_middleAndBottomView];
    _middleBottomLineView.image = [UIImage imageNamed:@"middle_line"];
    [_topView removeFromSuperview];
    
    
    [_iconView setImageWithURL:[NSURL URLWithString:data[@"pic"]]];
    _nameLabel.text = data[@"appoint_content"];
    _merchantNameLabel.text = data[@"merchant_name"];
    _oldPriceLabel.text = [NSString stringWithFormat:@"原价:¥%@元", data[@"custom_original_price"]];
    _priceLabel.text = [NSString stringWithFormat:@"平台价:¥%@元", data[@"appoint_price"]];
    _numberLabel.text = [NSString stringWithFormat:@"已售%@", data[@"appoint_sum"]];
    [self refreshLabel];
    
}

- (void)makeBottomViewWithData:(NSDictionary *)data {
    [self addSubview:_middleAndBottomView];
    _middleBottomLineView.image = [UIImage imageNamed:@"bottom_line"];
    [_topView removeFromSuperview];
    
    [_iconView setImageWithURL:[NSURL URLWithString:data[@"pic"]]];
    _nameLabel.text = data[@"appoint_content"];
    _merchantNameLabel.text = data[@"merchant_name"];
    _oldPriceLabel.text = [NSString stringWithFormat:@"原价:¥%@元", data[@"custom_original_price"]];
    _priceLabel.text = [NSString stringWithFormat:@"平台价:¥%@元", data[@"appoint_price"]];
    _numberLabel.text = [NSString stringWithFormat:@"已售%@", data[@"appoint_sum"]];
    [self refreshLabel];
}

- (void)refreshLabel {
    NSMutableAttributedString *att1 = [[NSMutableAttributedString alloc] initWithString:_oldPriceLabel.text];
    [att1 addAttributes:@{NSBaselineOffsetAttributeName: @(NSUnderlineStyleSingle), NSStrikethroughStyleAttributeName: @(NSUnderlineStyleSingle)} range:NSMakeRange(3, _oldPriceLabel.text.length - 3)];
    _oldPriceLabel.attributedText = att1;
    
    NSMutableAttributedString *att2 = [[NSMutableAttributedString alloc] initWithString:_priceLabel.text];
    [att2 addAttributes:@{NSForegroundColorAttributeName: [UIColor redColor]} range:NSMakeRange(4, _priceLabel.text.length - 4)];
    _priceLabel.attributedText = att2;
    
}


//- (void)btnClick:(UIButton *)btn {
//    _btn1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btnBack"]];
//    _btn2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btnBack"]];
//    _btn3.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btnBack"]];
//    btn.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btnSelectedBack"]];
//    NSInteger typeCode = 0;
//    if (btn == _btn1) {
//        typeCode = 0;
//    }else if (btn == _btn2) {
//        typeCode = 1;
//    }else {
//        typeCode = 2;
//    }
//    if (_changeServiceType != nil) {
//        _changeServiceType(typeCode);
//    }
//}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self makeSubviews];
    }
    return self;
}







- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
