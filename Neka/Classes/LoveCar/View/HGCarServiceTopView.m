
//
//  HGCarServiceTopView.m
//  Neka1.0
//
//  Created by ma c on 2016/12/26.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGCarServiceTopView.h"

@implementation HGCarServiceTopView {
    UIImageView *_topLine;
    UIButton *_btn1;
    UIButton *_btn2;
    UIButton *_btn3;
    
}


- (instancetype)initWithFrame:(CGRect)frame andCarInfoList:(NSArray *)carInfoList {
    if (self = [super initWithFrame:frame]) {
        //1.顶部
        
        //顶部cell标线
        _topLine = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(12), KWIDTH(20), SCREEN_SIZE.width - KWIDTH(25), KWIDTH(20))];
        _topLine.image = [UIImage imageNamed:@"top_line"];
        [self addSubview:_topLine];
        
        //
        _btn1 = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(30), KWIDTH(5), KWIDTH(90), KWIDTH(30))];
        _btn1.layer.contents = (id)[UIImage imageNamed:@"btnSelectedBack"].CGImage;
        [_btn1 setTitle:carInfoList[0][@"small_cat_name"] forState:UIControlStateNormal];
        _btn1.titleLabel.font = [UIFont systemFontOfSize:14];
        [_btn1 setTitleColor:mRGB(71, 125, 218, 1) forState:UIControlStateNormal];
        [_btn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [_btn1 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btn1];
        _btn1.selected = YES;
        //
        _btn2 = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(125), KWIDTH(5), KWIDTH(90), KWIDTH(30))];
        _btn2.layer.contents = (id)[UIImage imageNamed:@"btnBack"].CGImage;
        [_btn2 setTitle:carInfoList[1][@"small_cat_name"] forState:UIControlStateNormal];
        _btn2.titleLabel.font = [UIFont systemFontOfSize:14];
        [_btn2 setTitleColor:mRGB(71, 125, 218, 1) forState:UIControlStateNormal];
        [_btn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [_btn2 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btn2];
        
        
        //
        _btn3 = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(220), KWIDTH(5), KWIDTH(90), KWIDTH(30))];
        _btn3.layer.contents = (id)[UIImage imageNamed:@"btnBack"].CGImage;
        [_btn3 setTitle:carInfoList[2][@"small_cat_name"] forState:UIControlStateNormal];
        _btn3.titleLabel.font = [UIFont systemFontOfSize:14];
        [_btn3 setTitleColor:mRGB(71, 125, 218, 1) forState:UIControlStateNormal];
        [_btn3 setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [_btn3 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btn3];
    }
    return self;
}

- (void)btnClick:(UIButton *)btn {
    _btn1.layer.contents = (id)[UIImage imageNamed:@"btnBack"].CGImage;
    _btn2.layer.contents = (id)[UIImage imageNamed:@"btnBack"].CGImage;
    _btn3.layer.contents = (id)[UIImage imageNamed:@"btnBack"].CGImage;
    _btn1.selected = NO;
    _btn2.selected = NO;
    _btn3.selected = NO;
    
    
    btn.layer.contents = (id)[UIImage imageNamed:@"btnSelectedBack"].CGImage;
    btn.selected = YES;
    
    NSInteger typeCode = 0;
    if (btn == _btn1) {
        typeCode = 0;
    }else if (btn == _btn2) {
        typeCode = 1;
    }else {
        typeCode = 2;
    }
    if (_changeServiceType != nil) {
        _changeServiceType(typeCode);
    }
}

- (UILabel *)labelWithString:(NSString *)str {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH(5), KWIDTH(90), KWIDTH(20))];
    label.text = str;
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor whiteColor];
    return label;
}

@end


