//
//  HGCarListTableViewCell.m
//  Neka1.0
//
//  Created by ma c on 2016/12/24.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGCarListTableViewCell.h"

@implementation HGCarListTableViewCell {
    UIImageView *_imageView;
    UILabel *_seriesLabel;
    UILabel *_carNumberLabel;
    UIButton *_editBtn;
    UIButton *_deleteBtn;
    HGCarInfoModel *_carInfoModel;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self makeSubviews];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}


- (void)makeSubviews {
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, KWIDTH(5), SCREEN_SIZE.width, KWIDTH(85))];
    backView.backgroundColor = mRGB(240, 240, 240, 1);
    [self addSubview:backView];
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(5), 0, KWIDTH(145), KWIDTH(85))];
    [backView addSubview:_imageView];
    
    _seriesLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(160), KWIDTH(5), KWIDTH(100), KWIDTH(20))];
    _seriesLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    [backView addSubview:_seriesLabel];
    
    _carNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(160), KWIDTH(25), KWIDTH(120), KWIDTH(20))];
    _carNumberLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    [backView addSubview:_carNumberLabel];
    
    _editBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(160), KWIDTH(60), KWIDTH(40), KWIDTH(20))];
    _editBtn.backgroundColor = mRGB(13, 103, 212, 1);
    [_editBtn setTitle:@"编辑" forState:UIControlStateNormal];
    _editBtn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    _editBtn.layer.masksToBounds = YES;
    _editBtn.layer.cornerRadius = 2;
    [_editBtn addTarget:self action:@selector(editBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:_editBtn];
    
    _deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(205), KWIDTH(60), KWIDTH(40), KWIDTH(20))];
    _deleteBtn.backgroundColor = mRGB(13, 103, 212, 1);
    [_deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    _deleteBtn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    _deleteBtn.layer.masksToBounds = YES;
    _deleteBtn.layer.cornerRadius = 2;
    [_deleteBtn addTarget:self action:@selector(deleteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:_deleteBtn];
    
}

- (void)setCarInfoModel:(HGCarInfoModel *)carInfoModel {
    _carInfoModel = carInfoModel;
    [_imageView setImageWithURL:[NSURL URLWithString:_carInfoModel.cat_pic]];
    _seriesLabel.text = _carInfoModel.cat_name;
    _carNumberLabel.text = [NSString stringWithFormat:@"%@%@", _carInfoModel.cat_num_name, _carInfoModel.cat_num];
}


- (void)editBtnClick:(UIButton *)btn {
    if (_editBlock != nil) {
        _editBlock(_carInfoModel);
    }
    NSLog(@"编辑");
}

- (void)deleteBtnClick:(UIButton *)btn {
    if (_deleteBlock != nil) {
        _deleteBlock(_carInfoModel);
    }
    NSLog(@"删除");
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
