//
//  HGCarServiceTopView.h
//  Neka1.0
//
//  Created by ma c on 2016/12/26.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ChangeServiceType) (NSInteger typeCode);

@interface HGCarServiceTopView : UIView
@property (nonatomic, copy)ChangeServiceType changeServiceType;

- (instancetype)initWithFrame:(CGRect)frame andCarInfoList:(NSArray *)carInfoList;

@end
