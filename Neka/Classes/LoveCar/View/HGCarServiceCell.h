//
//  HGCarServiceCell.h
//  Neka1.0
//
//  Created by ma c on 2016/12/25.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ChangeServiceType) (NSInteger typeCode);

@interface HGCarServiceCell : UITableViewCell

@property (nonatomic, copy)ChangeServiceType changeServiceType;


//- (void)makeTopView;
- (void)makeMiddleViewWithData:(NSDictionary *)data;
- (void)makeBottomViewWithData:(NSDictionary *)data;



@end
