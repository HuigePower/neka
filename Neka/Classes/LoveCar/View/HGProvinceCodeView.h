//
//  HGProvinceCodeView.h
//  Neka1.0
//
//  Created by ma c on 2016/12/24.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^HGProvinceCodePickerFinished)(NSDictionary *provinceData);
typedef void(^HGProvinceCodePickerHideView)();
@interface HGProvinceCodeView : UIView <UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, strong)UIPickerView *picker;
@property (nonatomic, copy)HGProvinceCodePickerFinished selectResult;
@property (nonatomic, copy)HGProvinceCodePickerHideView hideView;

@end
