//
//  LimitNumberCell.h
//  Neka
//
//  Created by ma c on 2017/11/20.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^LimitNumberCityChanged) (NSString *city, BOOL isOpen);
@interface LimitNumberCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *switchView;

@property (nonatomic, copy)LimitNumberCityChanged cityChangedBlock;
@property (nonatomic, copy)NSString *city;
@end
