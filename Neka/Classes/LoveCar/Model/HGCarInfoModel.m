//
//  HGCarInfoModel.m
//  Neka
//
//  Created by ma c on 2017/7/24.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGCarInfoModel.h"

@implementation HGCarInfoModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        NSArray *keys = [dict allKeys];
        for (NSString *key in keys) {
            if ([key isEqualToString:@"id"]) {
                [self setValue:dict[key] forKey:@"car_id"];
            }else {
                [self setValue:dict[key] forKey:key];
            }
        }
        
    }
    return self;
}

@end
