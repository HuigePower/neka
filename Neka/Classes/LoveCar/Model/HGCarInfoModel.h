//
//  HGCarInfoModel.h
//  Neka
//
//  Created by ma c on 2017/7/24.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGCarInfoModel : NSObject

@property (nonatomic, strong)NSString *car_id;
@property (nonatomic, strong)NSString *uid;
@property (nonatomic, strong)NSString *cat_fid;
@property (nonatomic, strong)NSString *cat_id;
@property (nonatomic, strong)NSString *cat_num_name_id;
@property (nonatomic, strong)NSString *cat_num;
@property (nonatomic, strong)NSString *cat_years;
@property (nonatomic, strong)NSString *cat_months;
@property (nonatomic, strong)NSString *cat_mileages;
@property (nonatomic, strong)NSString *cat_frame_num;
@property (nonatomic, strong)NSString *cat_engine_num;
@property (nonatomic, strong)NSString *cat_add_time;
@property (nonatomic, strong)NSString *cat_update_time;
@property (nonatomic, strong)NSString *cat_status;
@property (nonatomic, strong)NSString *cat_name;
@property (nonatomic, strong)NSString *cat_pic;
@property (nonatomic, strong)NSString *url;
@property (nonatomic, strong)NSString *cat_num_name;

- (instancetype)initWithDict:(NSDictionary *)dict;
@end
