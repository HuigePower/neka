//
//  LimitNumberManager.m
//  Neka
//
//  Created by ma c on 2017/11/20.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "LimitNumberManager.h"
#define LimitNumberCity @"LimitNumberCity"
static NSInteger  seq = 0;
#import "JPUSHService.h"
@implementation LimitNumberManager


static LimitNumberManager *_manager;
+ (LimitNumberManager *)limitNumberManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (_manager == nil) {
            _manager = [[LimitNumberManager alloc] init];
        }
    });
    return _manager;
}

+ (NSArray *)checkLimitNumberCity {

    NSArray *list = [[NSUserDefaults standardUserDefaults] arrayForKey:LimitNumberCity];
    if (list == nil) {
        list = [NSArray new];
        [[NSUserDefaults standardUserDefaults] setObject:list forKey:LimitNumberCity];
    }
    NSLog(@"限号城市：%@", list);
    return list;
}

+ (void)updateLimitNumberCityWithCity:(NSString *)city isOpen:(BOOL)isOpen {
    NSMutableArray *list = [[NSUserDefaults standardUserDefaults] mutableArrayValueForKey:LimitNumberCity];
    NSLog(@"限号-修改前：%@", list);
    if (isOpen) {
        if ([list indexOfObject:city] == NSNotFound) {
            [list addObject:city];
//            [JPUSHService addTags:[NSSet setWithObjects:city, nil] completion:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
//                NSLog(@"%ld,%@,%ld", (long)iResCode, iTags, seq);
//            } seq:seq++];
        }
    }else {
        if ([list indexOfObject:city] != NSNotFound) {
            [list removeObject:city];
//            [JPUSHService deleteTags:[NSSet setWithObjects:city, nil] completion:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
//                NSLog(@"%ld,%@,%ld", (long)iResCode, iTags, seq);
//            } seq:seq++];
        }
    }
//    [[NSUserDefaults standardUserDefaults] setObject:list forKey:LimitNumberCity];
    NSArray *list2 = [[NSUserDefaults standardUserDefaults] arrayForKey:LimitNumberCity];
    NSLog(@"限号-修改后：%@", list2);
}

//判断城市是否存在
+ (BOOL)limitNumberCityhasCity:(NSString *)city {
    NSArray *list = [LimitNumberManager checkLimitNumberCity];
    if ([list indexOfObject:city] == NSNotFound) {
        return NO;
    }else {
        return YES;
    }
    
}

+ (void)clearNumberCity {
    
    NSMutableArray *list =  [[NSUserDefaults standardUserDefaults] mutableArrayValueForKey:LimitNumberCity];
    [list removeAllObjects];
    
}


@end
