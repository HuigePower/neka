//
//  LimitNumberManager.h
//  Neka
//
//  Created by ma c on 2017/11/20.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LimitNumberManager : NSObject
@property (atomic, strong)NSUserDefaults *userDefaults;





+ (LimitNumberManager *)limitNumberManager ;
+ (NSArray *)checkLimitNumberCity;
+ (void)updateLimitNumberCityWithCity:(NSString *)city isOpen:(BOOL)isOpen;
+ (BOOL)limitNumberCityhasCity:(NSString *)city;
//清空
+ (void)clearNumberCity;
@end
