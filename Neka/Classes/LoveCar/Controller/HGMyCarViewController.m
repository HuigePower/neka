//
//  HGMyCarViewController.m
//  Neka1.0
//
//  Created by ma c on 2016/12/19.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGMyCarViewController.h"
#import "HGAddCarViewController.h"
#import "HGCarServiceCell.h"
#import "HGCarServiceTopView.h"
#import "HGCarListViewController.h"
#import "LoginViewController.h"
#import "HGOrderWebViewController.h"
#import "HGCarSeriesViewController.h"
#import "MyCarsWZViewController.h"
#import "AddReminderViewController.h"
@interface HGMyCarViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong)UIView *addMyCar;
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, assign)NSInteger serviceType;
@property (nonatomic, strong)NSArray *arr;
@property (nonatomic, strong)HGCarServiceTopView *topView;
@property (nonatomic, strong)UILabel *carNumber;
@property (nonatomic, strong)UILabel *carName;

@property (nonatomic, strong)NSDictionary *carInfo;


@property (nonatomic, strong)NSArray *carDetailInfo;

@property (nonatomic, strong)UIButton *changeCarBtn;
@property (nonatomic, strong)UIImageView *carBigPic;

@property (nonatomic, copy)NSString *carId;
@end

@implementation HGMyCarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _serviceType = 0;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
    if ([UserDefaultManager ticketIsExist]) {//已登录
        [self initDataSource];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"添加提醒" style:UIBarButtonItemStylePlain target:self action:@selector(addReminder)];
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"违章查询" style:UIBarButtonItemStylePlain target:self action:@selector(wzCheck)];
    }else {//未登录
        LoginViewController *login = [[LoginViewController alloc] init];
        [self.navigationController pushViewController:login animated:YES];
    }
    
}

- (void)addReminder {
    AddReminderViewController *addReminder = [[AddReminderViewController alloc] init];
    [self.navigationController pushViewController:addReminder animated:YES];
}

- (void)wzCheck {
    MyCarsWZViewController *carWz = [[MyCarsWZViewController alloc] init];
    [self.navigationController pushViewController:carWz animated:YES];
}

- (void)initDataSource {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *city = [[NSUserDefaults standardUserDefaults] objectForKey:@"nowCity"];
    
    [[NSUserDefaults standardUserDefaults] setObject:city forKey:@"nowCity"];
    NSMutableDictionary *body = [NSMutableDictionary dictionaryWithDictionary:@{@"Device-Id": DEVICE_ID,
                                   @"ticket": [UserDefaultManager ticket],
                                   @"lat": LATITUDE,
                                   @"long": LONGITUDE,
                                   @"longlat": @"baidu",
                                  @"now_city": city[@"area_id"]}];
    if (_carId.length > 0) {
        [body setObject:_carId forKey:@"cat_id"];
    }
    
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Car_live&a=index", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            NSString *carNum = responseObject[@"result"][@"all_cat_num"];
            if ([carNum intValue] > 0) {
                _carInfo = responseObject[@"result"][@"cat_info"];
                _carDetailInfo = responseObject[@"result"][@"detailed_cat_info"];
                [self makeSubviews];
            }else {
                [self.view addSubview:self.addMyCar];
            }
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
        
    }];
    
    
    
    
}


- (UIView *)addMyCar {
    if (_addMyCar == nil) {
        _addMyCar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height)];
        NSArray *arr = @[@"车库暂空", @"添加爱车获取专属服务"];
        for (int i = 0; i < 2; i++) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), KWIDTH(100) + KWIDTH(30) * i, KWIDTH(280), KWIDTH(30))];
            label.textAlignment = NSTextAlignmentCenter;
            label.font = [UIFont systemFontOfSize:KWIDTH(12)];
            label.text = arr[i];
            [_addMyCar addSubview:label];
        }
        
        UIButton *addBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(20), KWIDTH(170), KWIDTH(280), KWIDTH(40))];
        addBtn.backgroundColor = mRGB(13, 103, 212, 1);
        [addBtn setTitle:@"+ 添加我的爱车" forState:UIControlStateNormal];
        [addBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        addBtn.titleLabel.font = [UIFont boldSystemFontOfSize:KWIDTH(14)];
        addBtn.layer.masksToBounds = YES;
        addBtn.layer.cornerRadius = KWIDTH(2);
        [addBtn addTarget:self action:@selector(addCarBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_addMyCar addSubview:addBtn];

    }
    return _addMyCar;
}

- (void)makeSubviews {
    self.automaticallyAdjustsScrollViewInsets = NO;
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - 49) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    [self.view addSubview:_tableView];
    [_tableView registerClass:[HGCarServiceCell class] forCellReuseIdentifier:@"myCell"];
    [_tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *arr = _carDetailInfo[_serviceType][@"list"];
    return arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    HGCarServiceCell *cell = (HGCarServiceCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *arr = _carDetailInfo[_serviceType][@"list"];
    NSDictionary *data = arr[indexPath.row];
    if (indexPath.row == [arr count] - 1) {
        [cell makeBottomViewWithData:data];
    }else {
        [cell makeMiddleViewWithData:data];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return KWIDTH(120);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(200);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(160))];
    view.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
    
    if (_carBigPic == nil) {
        _carBigPic = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(160))];
        _carBigPic.contentMode = UIViewContentModeScaleToFill;
    }
    [_carBigPic setImageWithURL:[NSURL URLWithString:_carInfo[@"cat_big_pic"]] placeholderImage:nil];
    [view addSubview:_carBigPic];
    
    if (_carNumber == nil) {
        _carNumber = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(20), KWIDTH(100), KWIDTH(30))];
        _carNumber.font = [UIFont systemFontOfSize:18];
    }
    _carNumber.text = _carInfo[@"cat_num"];
    [view addSubview:_carNumber];
    
    if (_carName == nil) {
        _carName = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(50), KWIDTH(100), KWIDTH(20))];
        _carName.textColor = [UIColor grayColor];
    }
    _carName.text = _carInfo[@"cat_name"];
    [view addSubview:_carName];
    
    if (_changeCarBtn == nil) {
        _changeCarBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(85), KWIDTH(40), KWIDTH(60))];
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(40), KWIDTH(40))];
        imgView.image = [UIImage imageNamed:@"ghc"];
        [_changeCarBtn addSubview:imgView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH(40), KWIDTH(40), KWIDTH(15))];
        label.text = @"更换车";
        label.font = [UIFont systemFontOfSize:15];
        label.textColor = [UIColor grayColor];
        label.textAlignment = NSTextAlignmentCenter;
        [_changeCarBtn addSubview:label];
        [_changeCarBtn addTarget:self action:@selector(changeMyCar:) forControlEvents:UIControlEventTouchUpInside];
    }
    [view addSubview:_changeCarBtn];
    [view addSubview:self.topView];
    
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = _carDetailInfo[_serviceType];
    NSString *url = dic[@"list"][indexPath.row][@"url"];
    HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
    web.urlString = url;
    [self.navigationController pushViewController:web animated:YES];
}

- (HGCarServiceTopView *)topView {
    
    if (_topView == nil) {
        _topView = [[HGCarServiceTopView alloc] initWithFrame:CGRectMake(0, KWIDTH(160), SCREEN_SIZE.width, KWIDTH(40)) andCarInfoList:_carDetailInfo];
        _topView.backgroundColor = [UIColor whiteColor];
        __weak HGMyCarViewController *weakSelf = self;
        _topView.changeServiceType = ^(NSInteger typeCode) {
            _serviceType = typeCode;
            [weakSelf ReloadDataForTableview];
        };
    }
    return _topView;
    
}




- (void)ReloadDataForTableview {
    [_tableView reloadData];
}


- (void)addCarBtnClick:(UIButton *)btn {
    
    
    HGAddCarViewController *addCar = [[HGAddCarViewController alloc] init];
    [self.navigationController pushViewController:addCar animated:YES];
}

- (void)serverBtnClick:(UIButton *)btn {
    NSLog(@"%ld", (long)btn.tag);
    if (btn.tag == 10) {
        
    }else if (btn.tag == 11) {
        
    }else if (btn.tag == 12) {
        
    }else if (btn.tag == 13) {
        HGAddCarViewController *addCar = [[HGAddCarViewController alloc] init];
        [self.navigationController pushViewController:addCar animated:YES];
    }else if (btn.tag == 14) {
        
    }

}

- (void)changeMyCar:(UIButton *)btn {
    HGCarListViewController *carList = [[HGCarListViewController alloc] init];
    carList.changeCarBlock = ^(NSString *carID) {
        _carId = carID;
    };
    [self.navigationController pushViewController:carList animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
