//
//  HGAddCarViewController.m
//  Neka1.0
//
//  Created by ma c on 2016/12/19.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGAddCarViewController.h"
#import "HGButton.h"
#import "HGAddCarInfoViewController.h"
@interface HGAddCarViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong)NSMutableArray *btnArr;
@property (nonatomic, strong)UIView *topView;
@property (nonatomic, strong)UIView *blueMark;
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSArray *carList;
@property (nonatomic, assign)NSInteger carIndex;
@end

@implementation HGAddCarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"添加爱车";
    [self.tabBarController.tabBar setHidden:YES];
    
    _carIndex = 0;
    
    self.view.backgroundColor = [UIColor whiteColor];
    _blueMark = [UIView new];
    _blueMark.backgroundColor = mRGB(12, 98, 203, 1);
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"myCell"];
    [self.view addSubview:_tableView];
    
    
}

- (NSArray *)carList {
    if (_carList == nil) {
        _carList = @[
                     @[@"1系", @"2系", @"3系", @"4系", @"5系", @"6系", @"7系"],
                     @[@"A", @"B", @"C", @"CLA", @"CLS", @"E",@"s", @"GLA"],
                     @[@"A1", @"A3", @"A4L", @"A4", @"A5", @"A6L", @"A7L"],
                     @[@"Macan", @"Cayenne", @"Panamera", @"Cayman", @"Boxster", @"911"]
                     ];
    }
    return _carList;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self changeMarkPositionWithIndex:0];

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.carList[_carIndex] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    cell.textLabel.text = self.carList[_carIndex][indexPath.row];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.topView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HGAddCarInfoViewController *addInfo = [[HGAddCarInfoViewController alloc] init];
    addInfo.isAdd = YES;
    [self.navigationController pushViewController:addInfo animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(70);
}




//顶部选择视图
- (UIView *)topView {
    if (_topView == nil) {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(70))];
        _topView.backgroundColor = [UIColor whiteColor];
        NSArray *titles = @[@"宝马升级",@"奔驰升级",@"奥迪升级",@"保时捷升级"];
        NSArray *images = @[@"baomashengji", @"benchishengji", @"aodishengji", @"baoshijieshengji"];
        _btnArr = [[NSMutableArray alloc] init];
        
        for (int i = 0 ; i < 4; i++) {
            HGButton *button = [[HGButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 4 * i, 0, SCREEN_SIZE.width / 4, KWIDTH(70)) andHorizontalSpace:KWIDTH(20) verticalSpace:KWIDTH(5)];
            [button setTitle:titles[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
            [button setImage:[UIImage imageNamed:images[i]] forState:UIControlStateNormal];
            button.tag = 10 + i;
            [button addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [_topView addSubview:button];
            
            [_btnArr addObject:button];
            
        }
    }
    return _topView;
}

- (void)btnClick:(UIButton *)btn {
    
    _carIndex = btn.tag - 10;
    [self changeMarkPositionWithIndex:_carIndex];
    
}


- (void)changeMarkPositionWithIndex:(NSInteger)index {
    UIButton *btn = _btnArr[index];
    CGRect rect = btn.titleLabel.frame;
    NSLog(@"%f--%f", btn.titleLabel.frame.origin.x, btn.titleLabel.frame.size.width);
    _blueMark.frame = CGRectMake(rect.origin.x, KWIDTH(70) - KWIDTH(2), rect.size.width, KWIDTH(2));
    [btn addSubview:_blueMark];
    [_tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
