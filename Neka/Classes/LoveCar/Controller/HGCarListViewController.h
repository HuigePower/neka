//
//  HGCarListViewController.h
//  Neka1.0
//
//  Created by ma c on 2016/12/24.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^CarListChangeCar)(NSString *carID);
@interface HGCarListViewController : UIViewController
@property (nonatomic, copy)CarListChangeCar changeCarBlock;
@end
