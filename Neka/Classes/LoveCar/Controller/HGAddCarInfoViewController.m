//
//  HGAddCarInfoViewController.m
//  Neka1.0
//
//  Created by ma c on 2016/12/20.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGAddCarInfoViewController.h"
#import "CustomTableViewCell.h"
#import "HGTextField.h"
#import "HGDatePickerView.h"
#import "HGProvinceCodeView.h"
#import "HGCarListViewController.h"
@interface HGAddCarInfoViewController ()<UITableViewDelegate, UITableViewDataSource> {
    NSInteger _yearIndex;
    NSInteger _monthIndex;
}
@property (nonatomic, strong)UITableView *tableView;
//车牌号地区
@property (nonatomic, strong)UIButton *provinceBtn;
//车牌号
@property (nonatomic, strong)HGTextField *carNumberTF;
//上牌时间
@property (nonatomic, strong)UIButton *dateBtn;
//行驶里程
@property (nonatomic, strong)HGTextField *mileageTF;
//车架号
@property (nonatomic, strong)HGTextField *frameNumberTF;
//发动机号
@property (nonatomic, strong)HGTextField *engineNunberTF;
//城市选择器
@property (nonatomic, strong)HGProvinceCodeView *provincePickerView;

//时间选择器
@property (nonatomic, strong)HGDatePickerView *datePickerView;

@property (nonatomic, strong)NSArray *provinceMarkList;

@property (nonatomic, strong)NSDictionary *provinceData;
@property (nonatomic, strong)NSDictionary *dateData;
@end

@implementation HGAddCarInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tabBarController.tabBar.hidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self makeDataSource];
    [self makeSubviews];
    [self registerKeyboardObserver];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableView registerClass:[CustomTableViewCell class] forCellReuseIdentifier:@"myCell"];
    [self.view addSubview:_tableView];
    

}




- (void)registerKeyboardObserver {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell != nil) {
        [cell deleteSubviews];
        cell.backgroundColor = [UIColor whiteColor];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(5), KWIDTH(150), KWIDTH(80))];
        [cell addCustomSubview:imageView];
    
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(170), KWIDTH(35), KWIDTH(140), KWIDTH(20))];
        [cell addCustomSubview:label];
        
        if (_isAdd) {
            [imageView setImageWithURL:[NSURL URLWithString:_carSeriesInfo[@"cat_big_pic"]]];
            label.text = _carSeriesInfo[@"cat_name"];
        }else {
            [imageView setImageWithURL:[NSURL URLWithString:_carInfoModel.cat_pic]];
            label.text = _carInfoModel.cat_name;
        }
        
    }else if (indexPath.row == 1) {
        
        cell.backgroundColor = mRGB(240, 240, 248, 1);
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(40), KWIDTH(20))];
        label.text = @"完善爱车信息，获取更多服务";
        label.font = [UIFont systemFontOfSize:KWIDTH(13)];
        label.textColor = mRGB(0, 126, 221, 1);
        [cell addCustomSubview:label];
        
    }else if (indexPath.row == 2) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), 12, KWIDTH(70), 20)];
        label.text = @"车牌号";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [cell addCustomSubview:label];
        [cell addCustomSeparatorWithHeight:43.5 leftSpace:KWIDTH(20) rightSpace:KWIDTH(20)];
        
        [cell addSubview:_provinceBtn];
        [cell addSubview:_carNumberTF];
        
    }else if (indexPath.row == 3) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), 12, KWIDTH(70), 20)];
        label.text = @"上牌年份";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [cell addCustomSubview:label];
        [cell addCustomSeparatorWithHeight:43.5 leftSpace:KWIDTH(20) rightSpace:KWIDTH(20)];
        
        [cell addSubview:_dateBtn];
        
    }else if (indexPath.row == 4) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), 12, KWIDTH(70), 20)];
        label.text = @"行驶里程";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [cell addCustomSubview:label];
        [cell addCustomSeparatorWithHeight:43.5 leftSpace:KWIDTH(20) rightSpace:KWIDTH(20)];
        
        [cell addSubview:_mileageTF];
        
    }else if (indexPath.row == 5) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), 12, KWIDTH(70), 20)];
        label.text = @"车架号";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [cell addCustomSubview:label];
        [cell addCustomSeparatorWithHeight:43.5 leftSpace:KWIDTH(20) rightSpace:KWIDTH(20)];
        
        [cell addSubview:_frameNumberTF];
        
    }else if (indexPath.row == 6) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(20), 12, KWIDTH(70), 20)];
        label.text = @"发动机号";
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [cell addCustomSubview:label];
        [cell addCustomSeparatorWithHeight:43.5 leftSpace:KWIDTH(20) rightSpace:KWIDTH(20)];
        
        [cell addSubview:_engineNunberTF];
        
    }else if (indexPath.row == 7) {
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(30), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(60), KWIDTH(40))];
        button.backgroundColor = mRGB(0, 126, 221, 1);
        button.layer.masksToBounds = YES;
        button.layer.cornerRadius = 2;
        [button addTarget:self action:@selector(addInfomationOfCar:) forControlEvents:UIControlEventTouchUpInside];
        if (_isAdd) {
            [button setTitle:@"+ 确认添加爱车信息" forState:UIControlStateNormal];
        }else {
            [button setTitle:@"+ 确认修改爱车信息" forState:UIControlStateNormal];
        }
        
        [cell addCustomSubview:button];
        
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return KWIDTH(90);
        
    }else if (indexPath.row == 1) {
        
        return KWIDTH(30);
        
    }else if (indexPath.row == 7) {
        
        return KWIDTH(60);
    }else {
        return 44;
    }

}

//添加汽车
- (void)addInfomationOfCar:(UIButton *)btn {
    
    if (_carNumberTF.text.length != 6) {
        [MBProgressHUD showError:@"车牌号错误"];
        return;
    }
    
    if (_mileageTF.text.length == 0) {
        [MBProgressHUD showError:@"请填写已行驶里程"];
        return;
    }
    
    NSDictionary *body = nil;
    NSString *url = nil;
    if (_isAdd) {
        url = [NSString stringWithFormat:@"%@c=My&a=user_cat_do_add", BASE_URL];
        body = @{@"Device-Id": DEVICE_ID,
                 @"ticket": [UserDefaultManager ticket],
                 @"cat_fid": _carSeriesInfo[@"cat_fid"],
                 @"cat_id": _carSeriesInfo[@"cat_id"],
                 @"cat_num_names": _provinceData[@"cat_city_id"],
                 @"cat_num": _carNumberTF.text,
                 @"cat_years": _dateData[@"year"],
                 @"cat_months": _dateData[@"month"],
                 @"cat_mileages": _mileageTF.text,
                 @"cat_frame_num": _frameNumberTF.text,
                 @"cat_engine_num": _engineNunberTF.text
                 };
    }else {
        url = [NSString stringWithFormat:@"%@c=My&a=user_cat_do_edit", BASE_URL];
        body = @{@"Device-Id": DEVICE_ID,
                 @"ticket": [UserDefaultManager ticket],
                 @"cat_fid": _carInfoModel.cat_fid,
                 @"cat_id": _carInfoModel.cat_id,
                 @"cat_num_names": _provinceData[@"cat_city_id"],
                 @"cat_num": _carNumberTF.text,
                 @"cat_years": _dateData[@"year"],
                 @"cat_months": _dateData[@"month"],
                 @"cat_mileages": _mileageTF.text,
                 @"cat_frame_num": _frameNumberTF.text,
                 @"cat_engine_num": _engineNunberTF.text,
                 @"id": _carInfoModel.car_id};
        
    }
    NSLog(@"%@", body);

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [self showLoading];
    [manager POST:url parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            if (_isAdd) {
                [MBProgressHUD showSuccess:@"添加成功"];
                NSArray *childView = self.navigationController.childViewControllers;
                [self.navigationController popToViewController:childView[childView.count - 3] animated:YES];
            }else {
                [MBProgressHUD showSuccess:@"修改成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
      
    }];
    

    
}


#pragma mark -- 主要子视图
- (void)makeSubviews {
    //车牌地区
    _provinceBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(100), 3, KWIDTH(30), 40)];
    [_provinceBtn setTitle:_provinceData[@"cat_num_name"] forState:UIControlStateNormal];
    [_provinceBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _provinceBtn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    _provinceBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _provinceBtn.titleEdgeInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    [_provinceBtn addTarget:self action:@selector(selectProvinceMark:) forControlEvents:UIControlEventTouchUpInside];
    
    //车牌号
    _carNumberTF = [[HGTextField alloc] initWithFrame:CGRectMake(KWIDTH(130), 5, KWIDTH(150), 34)];
    _carNumberTF.placeholder = @"请填写车牌号";
    _carNumberTF.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    _carNumberTF.font = [UIFont systemFontOfSize:KWIDTH(12)];
    if (!_isAdd) {
        _carNumberTF.text = _carInfoModel.cat_num;
    }

    
    //上牌年份
    _dateBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(100), 3, KWIDTH(150), 40)];
    
    [_dateBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _dateBtn.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(12)];
    _dateBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_dateBtn addTarget:self action:@selector(selectDate:) forControlEvents:UIControlEventTouchUpInside];
    [_dateBtn setTitle:[NSString stringWithFormat:@"%@年%@月", _dateData[@"year"], _dateData[@"month"]] forState:UIControlStateNormal];
    
    //行驶里程
    _mileageTF = [[HGTextField alloc] initWithFrame:CGRectMake(KWIDTH(100), 2, KWIDTH(100), 40)];
    _mileageTF.placeholder = @"请填写里程";
    _mileageTF.font = [UIFont systemFontOfSize:KWIDTH(12)];
    if (!_isAdd) {
        _mileageTF.text = _carInfoModel.cat_mileages;
    }
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(100), 0, KWIDTH(20), 40)];
    label.text = @"km";
    label.font = [UIFont systemFontOfSize:KWIDTH(12)];
    [_mileageTF addSubview:label];

    //车架号
    _frameNumberTF = [[HGTextField alloc] initWithFrame:CGRectMake(KWIDTH(100), 2, KWIDTH(150), 40)];
    _frameNumberTF.placeholder = @"请填写车架号";
    _frameNumberTF.font = [UIFont systemFontOfSize:KWIDTH(12)];
    if (!_isAdd) {
        _frameNumberTF.text = _carInfoModel.cat_frame_num;
    }
    
    
    //发动机号
    _engineNunberTF = [[HGTextField alloc] initWithFrame:CGRectMake(KWIDTH(100), 2, KWIDTH(150), 40)];
    _engineNunberTF.placeholder = @"请填写发动机号";
    _engineNunberTF.font = [UIFont systemFontOfSize:KWIDTH(12)];
    if (!_isAdd) {
        _engineNunberTF.text = _carInfoModel.cat_engine_num;
    }
    
    
    //城市选择器
    _provincePickerView = [[HGProvinceCodeView alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, 220)];
    __weak UIButton *weakProvinceBtn = _provinceBtn;
    __weak HGProvinceCodeView *weakProvinceCodeView = _provincePickerView;
    _provincePickerView.selectResult = ^(NSDictionary *provinceData) {
        _provinceData = provinceData;
        [weakProvinceBtn setTitle:provinceData[@"cat_num_name"] forState:UIControlStateNormal];
    };
    
    _provincePickerView.hideView = ^() {
        [UIView animateWithDuration:0.25 animations:^{
            weakProvinceCodeView.frame = CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, 200);
        } completion:^(BOOL finished) {
            [weakProvinceCodeView removeFromSuperview];
        }];
    };
    
    

    
    //选择时间
    _datePickerView = [[HGDatePickerView alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, 220)];
    __weak UIButton *weakDateBtn = _dateBtn;
    __weak HGDatePickerView *weakDatePickerView = _datePickerView;
    _datePickerView.selectResult = ^(NSDictionary *dateDic) {
        NSLog(@"111");
        _dateData = dateDic;
        [weakDateBtn setTitle:[NSString stringWithFormat:@"%@年%@月", dateDic[@"year"], dateDic[@"month"]] forState:UIControlStateNormal];
    };
    _datePickerView.hideView = ^() {
        [UIView animateWithDuration:0.25 animations:^{
            weakDatePickerView.frame = CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, 220);
        } completion:^(BOOL finished) {
            [weakDatePickerView removeFromSuperview];
        }];
    };
    
}

//选择省份别称
- (void)selectProvinceMark:(UIButton *)btn {
    NSLog(@"aaa");
    //关闭其他输入
    [self.view endEditing:YES];
    [_datePickerView removeFromSuperview];
    
    [self.view addSubview:_provincePickerView];
    [UIView animateWithDuration:0.25 animations:^{
        _provincePickerView.frame = CGRectMake(0, SCREEN_SIZE.height - 220 - 64, SCREEN_SIZE.width, 220);
    }];
}



//日期
- (void)selectDate:(UIButton *)btn {
    NSLog(@"11");
    //关闭其他输入
    [self.view endEditing:YES];
    [_provincePickerView removeFromSuperview];
    
    [self.view addSubview:_datePickerView];
    [UIView animateWithDuration:0.25 animations:^{
        _datePickerView.frame = CGRectMake(0, SCREEN_SIZE.height - 220 - 64, SCREEN_SIZE.width, 220);
        
    }];
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    //获取键盘高度
    NSValue *keyboardObject = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSLog(@"%@", keyboardObject);
    CGRect keyboardRect;
    [keyboardObject getValue:&keyboardRect];
    
    //取得键盘的动画时间,这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSLog(@"%f", duration);
    //关闭其他输入
    [_datePickerView removeFromSuperview];
    [_provincePickerView removeFromSuperview];
    
    [UIView animateWithDuration:duration animations:^{
        _tableView.frame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - keyboardRect.size.height);
    }];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    _tableView.frame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64);
    [_tableView reloadData];
}



- (void)makeDataSource {
    
    if (_isAdd) {
        _provinceData = @{
                          @"cat_city_id": @"1",
                          @"cat_num_name": @"京"
                          };
        _dateData = @{@"year": @"2017",
                      @"month": @"7"};
    }else {
        _provinceData = @{
                          @"cat_city_id": _carInfoModel.cat_num_name_id,
                          @"cat_num_name": _carInfoModel.cat_num_name
                          };
        _dateData = @{@"year": _carInfoModel.cat_years,
                      @"month": _carInfoModel.cat_months};
    }
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
