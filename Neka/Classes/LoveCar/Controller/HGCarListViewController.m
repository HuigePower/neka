//
//  HGCarListViewController.m
//  Neka1.0
//
//  Created by ma c on 2016/12/24.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGCarListViewController.h"
#import "HGCarListTableViewCell.h"
#import "HGAddCarInfoViewController.h"
#import "HGCarSeriesViewController.h"
#import "HGCarInfoModel.h"
@interface HGCarListViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSArray *carList;
@end

@implementation HGCarListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"我的爱车";
    [self.tabBarController.tabBar setHidden:YES];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initDataSource];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    [_tableView registerClass:[HGCarListTableViewCell class] forCellReuseIdentifier:@"myCell"];
}

- (void)initDataSource {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=user_cats", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            _carList = responseObject[@"result"];
            [_tableView reloadData];
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _carList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    HGCarListTableViewCell *cell = (HGCarListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSDictionary *carInfo = _carList[indexPath.row];
    HGCarInfoModel *model = [[HGCarInfoModel alloc] initWithDict:carInfo];
    [cell setCarInfoModel:model];
    cell.editBlock = ^(HGCarInfoModel *model) {
        NSLog(@"编辑");
        HGAddCarInfoViewController *addCar = [[HGAddCarInfoViewController alloc] init];
        addCar.title = @"修改爱车";
        addCar.isAdd = NO;
        addCar.carInfoModel = model;
        [self.navigationController pushViewController:addCar animated:YES];
        
    };
    
    cell.deleteBlock = ^(HGCarInfoModel *model) {
        NSLog(@"删除");
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSDictionary *body = @{@"Device-Id": DEVICE_ID,
                               @"ticket": [UserDefaultManager ticket],
                               @"cat_id": model.cat_id,
                               @"id": model.car_id};
        [self showLoading];
        [manager POST:[NSString stringWithFormat:@"%@c=My&a=user_cat_del", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            [self hideLoading];
            NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
            if ([errorCode isEqualToString:@"0"]) {
                [MBProgressHUD showSuccess:@"删除成功"];
                [self initDataSource];
            }else {
                [MBProgressHUD showError:responseObject[@"errorMsg"]];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self hideLoading];
            [MBProgressHUD showError:error.description];
        }];

    };
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return KWIDTH(95);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(0.1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return KWIDTH(50);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(40))];
    view.backgroundColor = [UIColor whiteColor];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(20), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(40), KWIDTH(30))];
    button.backgroundColor = mRGB(13, 103, 212, 1);
    [button setTitle:@"+添加我的爱车" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 2;
    [button addTarget:self action:@selector(addMyCar) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"第%ld辆车", indexPath.row);
    NSDictionary *carData = _carList[indexPath.row];
    if (_changeCarBlock) {
        _changeCarBlock(carData[@"cat_id"]);
    }
    [self.navigationController popViewControllerAnimated:YES];
    
    
}






- (void)addMyCar {
    NSLog(@"添加我的爱车");
//    HGAddCarInfoViewController *addCar = [[HGAddCarInfoViewController alloc] init];
//    addCar.title = @"添加爱车";
//    [self.navigationController pushViewController:addCar animated:YES];
    
    HGCarSeriesViewController *carSeries = [[HGCarSeriesViewController alloc] init];
    [self.navigationController pushViewController:carSeries animated:YES];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
