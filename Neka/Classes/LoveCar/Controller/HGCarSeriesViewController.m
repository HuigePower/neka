//
//  HGCarSeriesViewController.m
//  Neka
//
//  Created by ma c on 2017/7/23.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGCarSeriesViewController.h"
#import "HGSelectCarSeriesView.h"
#import "HGAddCarInfoViewController.h"
@interface HGCarSeriesViewController ()
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSArray *dataSource;
@property (nonatomic, strong)HGSelectCarSeriesView *selectCarSeriesView;
@end

@implementation HGCarSeriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"添加爱车";
    [self initSource];
}

- (void)initSource {
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=user_cat", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            self.automaticallyAdjustsScrollViewInsets = NO;
            _selectCarSeriesView = [[HGSelectCarSeriesView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) andSeriesList:responseObject[@"result"]];
            __weak HGCarSeriesViewController *weakSelf = self;
            _selectCarSeriesView.result = ^(NSDictionary *data) {
                NSLog(@"车系：%@", data);
                [weakSelf addAction:data];
                
            };
            [self.view addSubview:_selectCarSeriesView];
            
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
}


- (void)addAction:(NSDictionary *)data {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"cat_id": data[@"cat_id"], @"cat_fid": data[@"cat_fid"]};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=user_cat_add", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            NSDictionary *dic = responseObject[@"result"];
            HGAddCarInfoViewController *addCar = [[HGAddCarInfoViewController alloc] init];
            addCar.title = @"添加爱车";
            addCar.isAdd = YES;
            addCar.carSeriesInfo = dic;
            [self.navigationController pushViewController:addCar animated:YES];
        }else {
            [MBProgressHUD showError:@"该车系已添加" toView:self.view];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
    }];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
