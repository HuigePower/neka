//
//  AddReminderViewController.m
//  Neka
//
//  Created by ma c on 2017/11/18.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "AddReminderViewController.h"
#import "LimitNumberViewController.h"
#define AddReminderCellIdentifier @"AddReminderCellIdentifier"
@interface AddReminderViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation AddReminderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"添加提醒";
    [self.tabBarController.tabBar setHidden:YES];
    _tableView.tableFooterView = [UIView new];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:AddReminderCellIdentifier];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AddReminderCellIdentifier];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (indexPath.row == 0) {
        cell.textLabel.text = @"添加限行提醒";
    }else if (indexPath.row == 1) {
        cell.textLabel.text = @"添加年检提醒";
    }else if (indexPath.row == 2) {
        cell.textLabel.text = @"添加保险到期提醒";
    }
    cell.textLabel.textColor = mRGB(37, 143, 255, 1);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        LimitNumberViewController *limitNumber = [[LimitNumberViewController alloc] init];
        [self.navigationController pushViewController:limitNumber animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
