//
//  HGAddCarInfoViewController.h
//  Neka1.0
//
//  Created by ma c on 2016/12/20.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGCarInfoModel.h"
@interface HGAddCarInfoViewController : UIViewController
@property (nonatomic, strong)NSString *carSeries;
@property (nonatomic, assign)BOOL isAdd;
@property (nonatomic, strong)HGCarInfoModel *carInfoModel;
@property (nonatomic, strong)NSDictionary *carSeriesInfo;
@end
