//
//  LimitNumberViewController.m
//  Neka
//
//  Created by ma c on 2017/11/20.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "LimitNumberViewController.h"
#import "LimitNumberCell.h"
#import "LimitNumberManager.h"
#import "HGCarInfoModel.h"
#import "HGCarSeriesViewController.h"
#import "RegEx.h"
#define LimitNumberCellIdentifier @"LimitNumberCellIdentifier"
@interface LimitNumberViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong)NSArray *cityList;
@property (nonatomic, strong)UISwitch *switchView;
@property (nonatomic, strong)NSMutableArray *carList;
@property (nonatomic, strong)NSDictionary *summary;

@property (weak, nonatomic) IBOutlet UIView *ruleBackView;
@property (weak, nonatomic) IBOutlet UITextView *ruleContentView;


@end

@implementation LimitNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"添加限号提醒";
    [self.ruleBackView setHidden:YES];
    _carList = [NSMutableArray new];
    [_tableView registerNib:[UINib nibWithNibName:@"LimitNumberCell" bundle:nil] forCellReuseIdentifier:LimitNumberCellIdentifier];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initDataSource];
    
    _switchView = [[UISwitch alloc] init];
    [_switchView setOn:NO];
    [_switchView addTarget:self action:@selector(openLimitReminder:) forControlEvents:UIControlEventValueChanged];
    [self judgeIsOpen];
    
    
}

- (void)initDataSource {
    [self showLoading];
    if (_carList) {
        [_carList removeAllObjects];
    }
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    [manager POST:[NSString stringWithFormat:@"%@c=My&a=user_cats", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        
        if ([errorCode isEqualToString:@"0"]) {
            NSArray *carList = responseObject[@"result"];
            if (carList.count == 0) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"添加汽车后才能开启！" message:@"" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"返回" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                UIAlertAction *prefect = [UIAlertAction actionWithTitle:@"去添加" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    HGCarSeriesViewController *addCar = [[HGCarSeriesViewController alloc] init];
                    [self.navigationController pushViewController:addCar animated:YES];
                }];
                [alert addAction:cancel];
                [alert addAction:prefect];
                [self presentViewController:alert animated:YES completion:nil];
            }else {
                [self canUseLimit];
                [_tableView reloadData];
                NSInteger seq = 100;
                NSMutableArray *tags = [NSMutableArray new];
                for (NSDictionary *dic in carList) {
                    HGCarInfoModel *model = [[HGCarInfoModel alloc] initWithDict:dic];
                    [_carList addObject:model];
                    for (HGCarInfoModel *model in _carList) {
                        [tags addObject:model.cat_num_name];
                        NSString *lastCharcter = [model.cat_num substringWithRange:NSMakeRange(model.cat_num.length - 1, 1)];
                        if ([RegEx isCharacter:lastCharcter]) {
                            lastCharcter = @"0";
                        }
                        [tags addObject:lastCharcter];
                    }
                    
                }
                [JPUSHService addTags:[NSSet setWithArray:tags] completion:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
                    NSLog(@"成功：%ld,添加车牌号省份和车牌尾号：%@, seq: %ld", iResCode, iTags, seq);
                } seq:seq++];
            }
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD showError:error.description];
    }];
}

- (void)canUseLimit {
    _cityList = @[@"北京", @"天津", @"杭州", @"成都", @"兰州", @"南昌", @"长春", @"哈尔滨"];// @"武汉"
    _summary = @{@"北京": @"北京限号规定：本市号牌尾号限行，外地号牌工作日07:00-09:00、17:00-20:00全部限行，其他限行时间内尾号限行。法定上班的周六周日不限行。",
                 @"天津": @"天津限号规定：本市号牌尾号限行，外地号牌工作日07:00-09:00、17:00-19:00全部限行，其他限行时间内尾号限行。",
                 @"杭州": @"杭州限号规定：工作日07:00-09:00、16:30-18:30本市号牌尾号限行，外地号牌全部限行。法定上班的周六周日不限行。",
                 @"成都": @"成都限号规定：所有号牌尾号限行。法定上班的周六周日不限行。",
                 @"兰州": @"兰州限号规定：所有号牌尾号限行。外地号牌在中心城区内，高峰期(07:30-09:00、11:00-12:30、17:00-19:30)限行。",
                 @"南昌": @"南昌限号规定：所有号牌尾号限行。外地号牌：\n（1）南昌大桥、八一大桥（不含南面匝道）每日7:00-9:30，16:30-20:30禁止通行。\n（2）阳明路的象山路口至二经路口段每日7:00-20:30禁止通行。\n（3）八一大道福州路口至中山路口段每日7:00-20:30禁止通行。",
                 @"长春": @"长春限号规定：所有号牌尾号限行。外地号牌部分区域、时段限行。",
                 @"哈尔滨": @"哈尔滨限号规定：所有号牌尾号限行。外地号牌在06:30-09:00、16:00-18:30，由友谊路、大新街、南直路、公滨路、三大动力路、和兴路、康安路、前进路、河鼓街合围的区域内道路(含上述路段)限行。"
                 };
    //@"武汉": @"武汉限号规定：所有号牌尾号限行。"
    [_tableView reloadData];
}


- (void)openLimitReminder:(UISwitch *)switchView {
    NSLog(@"开关：%d", switchView.isOn);
    if (switchView.isOn) {
        
    }else {
        NSArray *list = [LimitNumberManager checkLimitNumberCity];
        NSInteger seq = 0;
        //删除城市标签
        for (NSString *city in list) {
            [JPUSHService deleteTags:[NSSet setWithObjects:city, nil] completion:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
                NSLog(@"成功：%ld, 清除限号城市标签：%@, seq: %ld", iResCode, iTags, seq);
            } seq:seq++];
        }
        [LimitNumberManager clearNumberCity];
        [_tableView reloadData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _cityList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LimitNumberCell *cell = [tableView dequeueReusableCellWithIdentifier:LimitNumberCellIdentifier forIndexPath:indexPath];
    cell.city = _cityList[indexPath.row];
    __weak LimitNumberViewController *weakSelf = self;
    cell.cityChangedBlock = ^(NSString *city, BOOL isOpen) {
        [weakSelf showLoading];
        if (isOpen) {
            [JPUSHService addTags:[NSSet setWithObjects:city, nil] completion:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
                NSLog(@"%ld,%@,%ld", (long)iResCode, iTags, seq);
                [weakSelf hideLoading];
                if (iResCode == 0) {
                    weakSelf.ruleContentView.text = weakSelf.summary[city];
                    [weakSelf.ruleBackView setHidden:!isOpen];
                    [LimitNumberManager updateLimitNumberCityWithCity:city isOpen:isOpen];
                    [weakSelf judgeIsOpen];
                }else {
                    [MBProgressHUD showError:@"选择失败，请重试！" toView:self.view];
                }
                
            } seq:10];
        }else {
            [JPUSHService deleteTags:[NSSet setWithObjects:city, nil] completion:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
                NSLog(@"%ld,%@,%ld", (long)iResCode, iTags, seq);
                [weakSelf hideLoading];
                if (iResCode == 0) {
                    weakSelf.ruleContentView.text = weakSelf.summary[city];
                    [weakSelf.ruleBackView setHidden:!isOpen];
                    [LimitNumberManager updateLimitNumberCityWithCity:city isOpen:isOpen];
                    [weakSelf judgeIsOpen];
                }else {
                    [MBProgressHUD showError:@"选择失败，请重试！" toView:self.view];
                }
                
            } seq:10];
        }
    };
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 44)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, SCREEN_SIZE.width / 2, 44)];
    label.text = @"开启限号提醒";
    [view addSubview:label];
    
    [view addSubview:_switchView];
    [_switchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view.mas_right).with.offset(-15);
        make.centerY.equalTo(view.mas_centerY);
    }];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}



//判断是否开启了限号提醒
- (void)judgeIsOpen {
    if ([LimitNumberManager checkLimitNumberCity].count != 0) {
        [_switchView setOn:YES];
    }else {
        [_switchView setOn:NO];
    }
}


- (IBAction)closeRuleView:(id)sender {
    [self.ruleBackView setHidden:YES];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
