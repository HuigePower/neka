//
//  HGAdverModel.h
//  Neka
//
//  Created by ma c on 2017/7/2.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGAdverModel : NSObject


@property (nonatomic, copy)NSString *city_id;
@property (nonatomic, copy)NSString *complete;
@property (nonatomic, copy)NSString *img_count;
@property (nonatomic, copy)NSString *name;
@property (nonatomic, copy)NSString *pic;
@property (nonatomic, copy)NSString *province_id;
@property (nonatomic, copy)NSString *sort;
@property (nonatomic, copy)NSString *url;

- (instancetype)initWithDict:(NSDictionary *)dict;
@end


