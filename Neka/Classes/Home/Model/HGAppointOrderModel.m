//
//  HGAppointOrderModel.m
//  Neka
//
//  Created by ma c on 2017/7/8.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGAppointOrderModel.h"

@implementation HGAppointOrderModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
        NSArray *arr = [dic allValues];
        for (id item in arr) {
            NSLog(@"%@", [item class]);
        }
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (NSString *)description {
    NSString *desc = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@\n%@\n%@", self.order_info, self.cheap_info, self.now_user, self.mer_coupon, self.pay_config, self.system_coupon, self.pay_method];
    return desc;
};


@end
