//
//  HGExpressenModel.m
//  Neka
//
//  Created by ma c on 2017/8/9.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGExpressenModel.h"

@implementation HGExpressenModel
- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}
@end
