//
//  HGAdverModel.m
//  Neka
//
//  Created by ma c on 2017/7/2.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGAdverModel.h"

@implementation HGAdverModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}


@end
