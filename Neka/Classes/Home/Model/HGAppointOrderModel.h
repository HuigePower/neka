//
//  HGAppointOrderModel.h
//  Neka
//
//  Created by ma c on 2017/7/8.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGAppointOrderModel : NSObject

@property (nonatomic, strong)NSDictionary *order_info;
@property (nonatomic, strong)NSDictionary *cheap_info;
@property (nonatomic, strong)NSDictionary *now_user;
@property (nonatomic, strong)NSDictionary *mer_coupon;
@property (nonatomic, strong)NSDictionary *pay_config;
@property (nonatomic, strong)NSDictionary *system_coupon;
@property (nonatomic, strong)NSArray      *pay_method;


- (instancetype)initWithDic:(NSDictionary *)dic;



@end
