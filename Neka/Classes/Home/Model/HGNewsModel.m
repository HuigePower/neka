//
//  HGNewsModel.m
//  Neka
//
//  Created by ma c on 2017/7/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGNewsModel.h"

@implementation HGNewsModel
- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}
@end
