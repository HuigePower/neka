//
//  HGCouponModel.h
//  Neka
//
//  Created by ma c on 2017/7/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGCouponModel : NSObject

@property (nonatomic, copy)NSString *name;
@property (nonatomic, copy)NSString *discount;
@property (nonatomic, copy)NSString *order_money;
@property (nonatomic, copy)NSString *had_id;
@property (nonatomic, copy)NSString *platform;
@property (nonatomic, copy)NSString *des;
@property (nonatomic, copy)NSString *cate_name;
@property (nonatomic, copy)NSString *time;

- (instancetype)initWithDict:(NSDictionary *)dict;

//"name": "预约专用优惠券",
//"discount": "100",
//"order_money": "1800.00",
//"had_id": "70",
//"platform": "移动网页,App,微信",
//"des": "满1800减100",
//"cate_name": "预约",
//"time": "有效期至17.08.21"


@end
