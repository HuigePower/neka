//
//  HGNewsModel.h
//  Neka
//
//  Created by ma c on 2017/7/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGNewsModel : NSObject
@property (nonatomic, copy)NSString *cat_name;
@property (nonatomic, copy)NSString *title;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end
