//
//  HGExpressenModel.h
//  Neka
//
//  Created by ma c on 2017/8/9.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGExpressenModel : NSObject

@property (nonatomic, copy)NSString *news_id;
@property (nonatomic, copy)NSString *category_id;
@property (nonatomic, copy)NSString *title;
@property (nonatomic, copy)NSString *custom_visits;
@property (nonatomic, copy)NSString *add_time;
@property (nonatomic, copy)NSString *url;
@property (nonatomic, strong)NSArray *content_img;

- (instancetype)initWithDict:(NSDictionary *)dict;
@end
