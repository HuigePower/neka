//
//  WHHTabbarView.m
//  轮播图
//
//  Created by ma c on 16/8/27.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "WHHTabbarView.h"
@interface WHHTabbarView ()

@property (nonatomic, strong)UILabel *line;
@property (nonatomic, strong)UIButton *selectedBtn;
@end
@implementation WHHTabbarView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
    }
    return self;
}

- (void)setCatList:(NSArray *)catList {
    _catList = catList;
    [self createSubview];
}

- (void)createSubview {
    float btnWidth = self.frame.size.width / self.catList.count;
    float btnHeight = self.frame.size.height;
    for (int i = 0; i < self.catList.count; i++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(btnWidth * i, 0, btnWidth, btnHeight)];
        [btn setTitle:self.catList[i][@"cat_name"] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        btn.tag = 10 + i;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
    }
    UIButton *btn = (UIButton *)[self viewWithTag:10];
    [btn setSelected:YES];
    _line = [[UILabel alloc] initWithFrame:CGRectMake(5, btnHeight - 1, btnWidth - 10, 1)];
    _line.backgroundColor = [UIColor blueColor];
    [self addSubview:_line];
}

- (void)btnClick:(UIButton *)btn {
    for (int i = 0; i < self.catList.count; i++) {
        UIButton *btn = (UIButton *)[self viewWithTag:10 + i];
        [btn setSelected:NO];
    }
    [btn setSelected:YES];
    NSInteger index = btn.tag - 10;
    NSLog(@"%ld", index);
    _line.frame = CGRectMake(btn.frame.origin.x + 5, btn.frame.size.height - 1, btn.frame.size.width - 10, 1);
    
    if (_result) {
        self.result(_catList[index][@"cat_id"]);
    }

}

- (void)setSelectedIndex:(NSInteger)index {
    UIButton *btn = (UIButton *)[self viewWithTag:10 + index];
    [self btnClick:btn];
}



@end
