//
//  CustomTableViewCell.h
//  Neka1.0
//
//  Created by ma c on 16/9/3.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
@property (nonatomic, strong)NSMutableArray *subviewArray;
- (void)addCustomSubview:(UIView *)view;
- (void)deleteSubviews;
- (void)addCustomSeparatorWithHeight:(float)height;
- (void)addCustomSeparatorWithHeight:(float)height leftSpace:(CGFloat)leftSapce rightSpace:(CGFloat)rightSpace;
@end
