//
//  HGServiceScrollView.h
//  Neka
//
//  Created by ma c on 2017/8/8.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGServiceScrollView : UIView<UIScrollViewDelegate>

@property (nonatomic, strong)UIScrollView *scrollView;
@property (nonatomic, strong)UIPageControl *pageControl;

@property (nonatomic, strong)UINavigationController *nav;

@end
