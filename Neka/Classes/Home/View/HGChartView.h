//
//  HGChartView.h
//  表格
//
//  Created by ma c on 2017/2/14.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGChartView : UIView<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong)UICollectionView *collectionView;
@property (nonatomic, strong)NSArray *dataSource;

//左右部分的宽度
@property (nonatomic, assign)CGFloat leftWidth;
@property (nonatomic, assign)CGFloat rightWidth;



- (instancetype)initWithFrame:(CGRect)frame andDataSource:(NSDictionary *)warrantyData;


@end
