//
//  UILabel+Alignment.h
//  Neka
//
//  Created by ma c on 2017/8/9.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Alignment)
- (void)topAlignment;
- (void)bottomAlignment;
@end
