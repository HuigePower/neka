//
//  AppointWashTableViewCell.m
//  Neka1.0
//
//  Created by ma c on 2017/3/24.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "AppointWashTableViewCell.h"

@implementation AppointWashTableViewCell
{
    UIImageView *_imageView;
    UILabel *_mainTitleLabel;
    UILabel *_descriptionLabel;
    UILabel *_oldPriceLabel;
    UILabel *_newPriceLabel;
    UILabel *_distanceLabel;
    UILabel *_soldOutNumberLabel;
    UILabel *_markLabel;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
    }
    return self;
}

- (void)setData:(NSDictionary *)data {
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, KWIDTH(5), SCREEN_SIZE.width, KWIDTH(85))];
    view.backgroundColor = [UIColor colorWithHexString:@"#F4F3F9"];
    [self addSubview:view];
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(5), KWIDTH(100), KWIDTH(85))];
    [_imageView setImageWithURL:[NSURL URLWithString:data[@"pic"]] placeholderImage:nil];
    [self addSubview:_imageView];
    
    //标题
    _mainTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(120), KWIDTH(5), KWIDTH(160), KWIDTH(20))];
    _mainTitleLabel.font = [UIFont systemFontOfSize:15];
    _mainTitleLabel.text = data[@"merchant_name"];
    [self addSubview:_mainTitleLabel];
    
    //描述
    _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(120), KWIDTH(25), SCREEN_SIZE.width - KWIDTH(150), KWIDTH(20))];
    _descriptionLabel.font = [UIFont systemFontOfSize:14];
    _descriptionLabel.text = data[@"appoint_name"];
    [self addSubview:_descriptionLabel];
    
    //原价
    _oldPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(120), KWIDTH(45), KWIDTH(100), KWIDTH(20))];
    _oldPriceLabel.font = [UIFont systemFontOfSize:12];
    _oldPriceLabel.text = [NSString stringWithFormat:@"原价: %@元", data[@"custom_original_price"]];
    _oldPriceLabel.textColor = [UIColor lightGrayColor];
    NSMutableAttributedString *attStrigOldPrice = [[NSMutableAttributedString alloc] initWithString:_oldPriceLabel.text];
    [attStrigOldPrice addAttributes:@{NSStrikethroughStyleAttributeName: @(NSUnderlineStyleSingle)} range:NSMakeRange(4, _oldPriceLabel.text.length - 5)];
    _oldPriceLabel.attributedText = attStrigOldPrice;
    [self addSubview:_oldPriceLabel];

    //平台价
    _newPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(120), KWIDTH(65), KWIDTH(120), KWIDTH(20))];
    _newPriceLabel.font = [UIFont systemFontOfSize:12];
    _newPriceLabel.text = [NSString stringWithFormat:@"平台价: %@元", data[@"appoint_price"]];
    NSMutableAttributedString *attStrigNewPrice = [[NSMutableAttributedString alloc] initWithString:_newPriceLabel.text];
    [attStrigNewPrice addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor redColor]} range:NSMakeRange(5, _newPriceLabel.text.length - 6)];
    _newPriceLabel.attributedText = attStrigNewPrice;
    [self addSubview:_newPriceLabel];

    //距离
    _distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(60), KWIDTH(45), KWIDTH(50), KWIDTH(20))];
    _distanceLabel.font = [UIFont systemFontOfSize:14];
    _distanceLabel.text = data[@"juli"];
    _distanceLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:_distanceLabel];
    
    //销量
    _soldOutNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(70), KWIDTH(65), KWIDTH(60), KWIDTH(20))];
    _soldOutNumberLabel.font = [UIFont systemFontOfSize:12];
    _soldOutNumberLabel.text = [NSString stringWithFormat:@"已售%@人", data[@"appoint_sum"]];
    _soldOutNumberLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:_soldOutNumberLabel];
    
    //到店/上门
    _markLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    _markLabel.layer.borderColor = mRGB(71, 125, 218, 1).CGColor;
    _markLabel.layer.borderWidth = 1.5;
    _markLabel.layer.masksToBounds = YES;
    _markLabel.layer.cornerRadius = KWIDTH(2);
    
    _markLabel.text = @"到店";
    _markLabel.font = [UIFont systemFontOfSize:14];
    _markLabel.textColor = mRGB(71, 125, 218, 1);
    _markLabel.textAlignment = NSTextAlignmentCenter;
    //获取14号字体时
    [_markLabel sizeToFit];
    CGSize size = _markLabel.bounds.size;
    _markLabel.font = [UIFont systemFontOfSize:12];
    
    
    _markLabel.frame = CGRectMake(SCREEN_SIZE.width - size.width - KWIDTH(10), KWIDTH(25) + (KWIDTH(20) - size.height) * 0.5, size.width, size.height);
    [self addSubview:_markLabel];
    
}


//412.17


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
