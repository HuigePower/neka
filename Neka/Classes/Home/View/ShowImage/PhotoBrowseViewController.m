//
//  PhotoBrowseViewController.m
//  ShowImages
//
//  Created by ma c on 16/9/1.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "PhotoBrowseViewController.h"
#import "ImageScroll.h"
#define SCREEN_SIZE [[UIScreen mainScreen] bounds].size
@interface PhotoBrowseViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong)UIScrollView *scrollView;
@property (nonatomic, strong)UIPageControl *control;
@property (nonatomic, strong)NSArray *imageArray;

@end

@implementation PhotoBrowseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
    _imageArray = @[@"http://www.nekahome.com/upload/appoint/26/000/000/001/m_57c7c4c87c982.jpg", @"http://www.nekahome.com/upload/appoint/83/000/000/001/m_57c7c4db1efd8.jpg", @"http://www.nekahome.com/upload/appoint/65/000/000/001/m_57c7c6ca9b549.jpg", @"http://www.nekahome.com/upload/appoint/61/000/000/001/m_57c7ca2e82c99.jpg"];
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.control];
    

    
}

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width , SCREEN_SIZE.height)];
        _scrollView.contentSize = CGSizeMake(SCREEN_SIZE.width * _imageArray.count, SCREEN_SIZE.height);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        for (int i = 0; i < _imageArray.count; i++) {
            ImageScroll *image = [[ImageScroll alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width * i, 0, SCREEN_SIZE.width, SCREEN_SIZE.height) imageUrl:_imageArray[i]];
            image.tag = 10 + i;
            image.dismissBlock = ^() {
                [self dismissView];
            };
            [_scrollView addSubview:image];
        }
    }
    return _scrollView;
}

- (UIPageControl *)control {
    if (_control == nil) {
        _control = [[UIPageControl alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height - 20, SCREEN_SIZE.width, 10)];
        _control.numberOfPages = _imageArray.count;
        _control.currentPage = 0;
        [_control addTarget:self action:@selector(pageChange:) forControlEvents:UIControlEventValueChanged];
    }
    return _control;
}


- (void)pageChange:(UIPageControl *)control {
    self.scrollView.contentOffset = CGPointMake(SCREEN_SIZE.width * control.currentPage, 0);
}


//划过一页之后,让图片会到初始的尺寸
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    for (int i = 0; i < _imageArray.count; i++) {
        ImageScroll *scroll = (ImageScroll *)[self.view viewWithTag:10 + i];
        [scroll goBackToInitial];
    }
    NSInteger offset =scrollView.contentOffset.x / SCREEN_SIZE.width;
    self.control.currentPage = offset;
    
}


//退出
- (void)dismissView {
    [self dismissViewControllerAnimated:YES completion:nil];
}








- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
