//
//  HHCityButton.h
//  Neka1.0
//
//  Created by ma c on 16/10/17.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HHCityButton : UIButton

- (void)setTitle:(NSString *)title;
@end
