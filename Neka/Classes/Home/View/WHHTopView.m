//
//  WHHTopView.m
//  Neka1.0
//
//  Created by ma c on 16/8/29.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "WHHTopView.h"
#import "CityPickerViewController.h"
#import "MapViewController.h"
#import "ScanViewController.h"
#import "HHCityButton.h"
#import "HGOrderWebViewController.h"
@interface WHHTopView()
@property (nonatomic, strong)HHCityButton *selectCity;
@property (nonatomic, strong)UIViewController *vc;
@property (nonatomic, strong)UIButton *locationBtn;
@property (nonatomic, strong)NSDictionary *nowCity;
@end

@implementation WHHTopView

- (instancetype)initWithFrame:(CGRect)frame withViewController:(UIViewController *)vc  {
    if (self = [super initWithFrame:frame]) {
        
//        self.backgroundColor = mRGB(109, 191, 173, 1);
        
        self.vc = vc;
//        UIImageView *selectCityBg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 70, 30)];
//        selectCityBg.image = [UIImage imageNamed:@"location_bg"];
//        [self addSubview:selectCityBg];
        
        _selectCity = [[HHCityButton alloc] initWithFrame:CGRectMake(5, 10, 70, 30)];
        [_selectCity setTitle:self.nowCity[@"area_name"]];
        [_selectCity addTarget:self action:@selector(citySelect:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_selectCity];
        
        //搜索
//        UIImageView *searchBg = [[UIImageView alloc] initWithFrame:CGRectMake(100, 10, SCREEN_SIZE.width - 120 - 50, 30)];
//        searchBg.layer.cornerRadius = 5;
//        searchBg.layer.masksToBounds = YES;
//        searchBg.layer.borderColor = [UIColor whiteColor].CGColor;
//        searchBg.layer.borderWidth = 1.0f;
//        searchBg.image = [UIImage imageNamed:@"search_bg"];
//        [self addSubview:searchBg];
        
        UIButton *searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(100, 10, SCREEN_SIZE.width - 120 - 50, 30)];
        searchBtn.layer.cornerRadius = 5;
        searchBtn.layer.masksToBounds = YES;
        searchBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        searchBtn.layer.borderWidth = 1.0f;
        [searchBtn addTarget:self action:@selector(searchBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:searchBtn];
        
        UIImageView *magnifier = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(10), 5, 20, 20)];
        magnifier.image = [UIImage imageNamed:@"magnifier"];
        [searchBtn addSubview:magnifier];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10) + 20 + 5, 5, KWIDTH(80), 20)];
        label.text = @"输入查找内容";
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont systemFontOfSize:KWIDTH(12)];
        [searchBtn addSubview:label];
        
        //定位
//        UIImageView *locationBg = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - 40, 10, 30, 30)];
//        locationBg.image = [UIImage imageNamed:@"location_bg"];
//        [self addSubview:locationBg];
        
        _locationBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - 40, 10, 30, 30)];
        _locationBtn.imageEdgeInsets = UIEdgeInsetsMake(KWIDTH(2.5), KWIDTH(2.5), KWIDTH(2.5), KWIDTH(2.5));
        [_locationBtn setImage:[UIImage imageNamed:@"location_white"] forState:UIControlStateNormal];
        [_locationBtn addTarget:self action:@selector(locationBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_locationBtn];
//        _locationBtn.hidden = YES;
        
        //扫描
//        UIButton *scan = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - 40, 10, 30, 30)];
//        [scan setImage:[UIImage imageNamed:@"qrcode_black"] forState:UIControlStateNormal];
//        scan.imageEdgeInsets = UIEdgeInsetsMake(KWIDTH(2.5), KWIDTH(2.5), KWIDTH(2.5), KWIDTH(2.5));
//        [scan addTarget:self action:@selector(magnifierBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:scan];
        
        
        
        
    }
    return self;
}

- (NSDictionary *)nowCity {
    NSDictionary *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"nowCity"];
    NSLog(@"city:%@", data);
    if (data) {
        
        _nowCity = data;
    }else {
        _nowCity = @{@"area_id": @"2", @"area_name": @"北京", @"area_url": @"bj", @"first_pinyin": @"b", @"is_hot": @"1"};
        [[NSUserDefaults standardUserDefaults] setObject:_nowCity forKey:@"nowCity"];
    }
    NSLog(@"city:%@", _nowCity);
    return _nowCity;
}


- (void)showLocationBtn {
    _locationBtn.hidden = NO;
}



- (void)citySelect:(UIButton *)btn {
    CityPickerViewController *picker = [[CityPickerViewController alloc] init];
    picker.result = ^(NSDictionary *cityData) {
        NSLog(@"%@", cityData);
        [self.selectCity setTitle:cityData[@"area_name"]];
    };
    [self.vc.navigationController pushViewController:picker animated:YES];
}


- (void)searchBtnClick:(UIButton *)btn {
//    HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
//    web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=Search&a=index";
    [MBProgressHUD showError:@"搜索功能正在完善中！" toView:self.vc.view];
    
}
  

- (void)locationBtnClick:(UIButton *)btn {
    
    MapViewController *mapVC = [[MapViewController alloc] init];
    [self.vc.navigationController pushViewController:mapVC animated:YES];
    
}

- (void)magnifierBtnClick:(UIButton *)btn {
    
    ScanViewController *scan = [[ScanViewController alloc] init];
    [self.vc.navigationController pushViewController:scan animated:YES];
}


@end
