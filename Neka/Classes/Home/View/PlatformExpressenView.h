//
//  PlatformExpressenView.h
//  轮播图
//
//  Created by ma c on 16/8/27.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^TapBlock)();
@interface PlatformExpressenView : UIButton

@property (nonatomic, copy)TapBlock tap;
@property (nonatomic, strong)NSArray *newsArray;
@property (nonatomic, strong)UINavigationController *nav;
@end
