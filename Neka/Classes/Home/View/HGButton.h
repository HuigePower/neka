//
//  HGButton.h
//  Neka1.0
//
//  Created by ma c on 2016/12/8.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGButton : UIButton
- (instancetype)initWithFrame:(CGRect)frame andHorizontalSpace:(CGFloat)horizontalSpace verticalSpace:(CGFloat)verticalSpace;
@end
