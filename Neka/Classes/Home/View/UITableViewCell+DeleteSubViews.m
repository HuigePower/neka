//
//  UITableViewCell+DeleteSubViews.m
//  Neka1.0
//
//  Created by ma c on 16/8/27.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "UITableViewCell+DeleteSubViews.h"

@implementation UITableViewCell (DeleteSubViews)

- (void)deleteSubviews {
    NSArray *subviews = self.subviews;
    for (UIView *subview in subviews) {
        [subview removeFromSuperview];
    }
}

@end
