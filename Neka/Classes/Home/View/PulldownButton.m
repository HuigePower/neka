//
//  PulldownButton.m
//  下拉选择控件
//
//  Created by ma c on 2017/2/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "PulldownButton.h"

@implementation PulldownButton
//{
//    UILabel *_label;
//    UIImageView *_imageView;
//    
//}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = frame.size.height * 0.2;
        self.layer.borderWidth = 1;
        self.layer.borderColor = [UIColor darkGrayColor].CGColor;
        [self setImage:[UIImage imageNamed:@"pd_back"] forState:UIControlStateNormal];
        
        self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
        
        _label = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.height * 0.2, 0, frame.size.width - frame.size.height * (1 + 0.4), frame.size.height)];
        
        _label.font = [UIFont systemFontOfSize:frame.size.height / 2];
        _label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_label];
        
        _image = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width - frame.size.height, 0, frame.size.height, frame.size.height)];
        _image.image = [UIImage imageNamed:@"pulldown"];
        _image.backgroundColor = [UIColor grayColor];
        _image.layer.borderWidth = 1;
        _image.layer.borderColor = [UIColor darkGrayColor].CGColor;
        [self addSubview:_image];
        [self setText:@"请选择"];
        
        
        
    }
    return self;
}

- (void)setText:(NSString *)text {
    _text = text;
    _label.text = text;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
