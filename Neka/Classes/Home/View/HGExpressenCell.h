//
//  HGExpressenCell.h
//  Neka
//
//  Created by ma c on 2017/8/9.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGExpressenModel.h"
#import "UILabel+Alignment.h"
@interface HGExpressenCell : UITableViewCell
- (void)makeSubviewsWithModel:(HGExpressenModel *)model;
@end
