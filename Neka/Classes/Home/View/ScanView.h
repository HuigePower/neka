//
//  ScanView.h
//  Neka1.0
//  二维码扫描
//  Created by ma c on 16/7/30.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanView : UIView
- (void)stopRunning;
@end
