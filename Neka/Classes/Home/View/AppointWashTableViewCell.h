//
//  AppointWashTableViewCell.h
//  Neka1.0
//
//  Created by ma c on 2017/3/24.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppointWashTableViewCell : UITableViewCell
- (void)setData:(NSDictionary *)data;
@end
