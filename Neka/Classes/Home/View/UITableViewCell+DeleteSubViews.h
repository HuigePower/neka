//
//  UITableViewCell+DeleteSubViews.h
//  Neka1.0
//
//  Created by ma c on 16/8/27.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (DeleteSubViews)
- (void)deleteSubviews;
@end
