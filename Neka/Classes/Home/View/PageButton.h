//
//  PageButton.h
//  Neka1.0
//
//  Created by ma c on 2017/2/15.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageButton : UIButton
@property (nonatomic, assign)BOOL availabel;
- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title;
@end
