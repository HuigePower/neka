//
//  UIButton+Fit.m
//  Neka1.0
//
//  Created by ma c on 16/8/31.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "UIButton+Fit.h"

@implementation UIButton (Fit)
- (void)sizeFit {
    NSString *str = self.titleLabel.text;
    UIFont *font = self.titleLabel.font;
    CGRect frame = self.frame;
    CGRect rect = [str boundingRectWithSize:CGSizeMake(SCREEN_SIZE.width, MAXFLOAT) options:0 attributes:@{NSFontAttributeName:font} context:nil];
    self.frame = CGRectMake(frame.origin.x, frame.origin.y, rect.size.width + 5, frame.size.height);
}
@end
