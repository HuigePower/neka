//
//  HGCarouselView.m
//  Carousel
//
//  Created by ma c on 2016/12/14.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGCarouselView.h"
#import "HGAdverModel.h"
#define WIDTH self.frame.size.width
#define HEIGHT self.frame.size.height


@implementation HGCarouselView {
    UIScrollView *_scrollView;
    UIImageView *_leftView;
    UIImageView *_middleView;
    UIImageView *_rightView;
    
    NSTimer *_timer;
    UIImageView *_placeholderView;
    BOOL _imgIsOnline;
}


- (instancetype)initWithFrame:(CGRect)frame andPlaceholderImage:(UIImage *)placeholderImage {
    if (self = [super initWithFrame:frame]) {
        if (placeholderImage != nil) {
            _placeholderImage = placeholderImage;
        }else {
            _placeholderImage = [UIImage imageNamed:@"ad_001"];
        }
        
        [self addObserver:self forKeyPath:@"currentPage" options:NSKeyValueObservingOptionNew context:nil];
        
        _placeholderView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        _placeholderView.image = self.placeholderImage;
        [self addSubview:_placeholderView];
        
        
    }
    return self;
}
 

- (void)setImagesWithURLs:(NSArray *)images showBottom:(BOOL)isShow {
    _images = images;
    _imgIsOnline = YES;
    NSLog(@"轮播图:%@", _images);
    if (_images.count == 0) return;
    [_placeholderView removeFromSuperview];
    [self createScrollView];
    if (isShow) {
        UIImageView *bottom = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 10, self.frame.size.width, 10)];
        bottom.image = [UIImage imageNamed:@"banner_bottom"];
        [self addSubview:bottom];
    }
    [self setCurrentPage:0];
    if (_images.count > 1) {
        [self createTimer];
    }
}


- (void)setImagesWithNames:(NSArray *)images showBottom:(BOOL)isShow {
    _images = images;
    _imgIsOnline = NO;
    NSLog(@"轮播图:%@", _images);
    if (_images.count == 0) return;
    [_placeholderView removeFromSuperview];
    [self createScrollView];
    if (isShow) {
        UIImageView *bottom = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 10, self.frame.size.width, 10)];
        bottom.image = [UIImage imageNamed:@"banner_bottom"];
        [self addSubview:bottom];
    }
    [self setCurrentPage:0];
    if (_images.count > 1) {
        [self createTimer];
    }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    //NSLog(@"%@--%d", keyPath, [change[@"new"] integerValue]);
//    self.pageControl.currentPage = [change[@"new"] integerValue];
    _pageLabel.text = [NSString stringWithFormat:@"%ld/%ld", [change[@"new"] integerValue] + 1, self.images.count];
}

- (void)createScrollView {
    if (_scrollView != nil) {
        NSArray *views = _scrollView.subviews;
        for (UIView *view in views) {
            [view removeFromSuperview];
        }
    }else {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    }
    
    if (_images.count == 1) {
        NSLog(@"123456");
        _scrollView.contentSize = CGSizeMake(WIDTH * 1, HEIGHT);
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        imageView.userInteractionEnabled = YES;
        [self loadImage:[_images firstObject] forImageView:imageView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImageView:)];
        [imageView addGestureRecognizer:tap];
        [_scrollView addSubview:imageView];
        
        _scrollView.contentOffset = CGPointMake(0, 0);
    }else if (_images.count > 1) {
        _scrollView.contentSize = CGSizeMake(WIDTH * 3, HEIGHT);
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        [self addSubview:_scrollView];
        
        _leftView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        
        _middleView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH, 0, WIDTH, HEIGHT)];
        
        _rightView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH * 2, 0, WIDTH, HEIGHT)];
        
        NSArray *arr = @[_leftView, _middleView, _rightView];
        for (UIImageView *imageView in arr) {
            [_scrollView addSubview:imageView];
            imageView.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImageView:)];
            [imageView addGestureRecognizer:tap];
            
        }
        [self loadImage:[_images lastObject] forImageView:_leftView];
        [self loadImage:[_images firstObject] forImageView:_middleView];
        [self loadImage:_images[1] forImageView:_rightView];

        _scrollView.contentOffset = CGPointMake(WIDTH, 0);
    }
    
    
    [self addSubview:_scrollView];
    _pageLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width - 60, self.frame.size.height - 30, 40, 20)];
    _pageLabel.layer.masksToBounds = YES;
    _pageLabel.layer.cornerRadius = 7.5;
    _pageLabel.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
    
    _pageLabel.textAlignment = NSTextAlignmentCenter;
    _pageLabel.font = [UIFont systemFontOfSize:14];
    _pageLabel.textColor = [UIColor whiteColor];
    _pageLabel.text = [NSString stringWithFormat:@"1/%ld", self.images.count];
    [self addSubview:_pageLabel];

}

- (void)loadImage:(NSString *)imgNameOrURL forImageView:(UIImageView *)imageView {
    if (_imgIsOnline) {
        [imageView setImageWithURL:[NSURL URLWithString:imgNameOrURL] placeholderImage:self.placeholderImage];
    }else {
        
        imageView.image = [UIImage imageNamed:imgNameOrURL];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    //NSLog(@"%f", scrollView.contentOffset.x);

    if (scrollView.contentOffset.x / WIDTH == 0) {
        [self scrollToFront];
    }else if (scrollView.contentOffset.x / WIDTH == 2) {
        [self scrollToback];
    }
    
    
}



- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    //NSLog(@"%s", __func__);
    if (scrollView.contentOffset.x / WIDTH == 0) {
        [self scrollToFront];
    }else if (scrollView.contentOffset.x / WIDTH == 2) {
        [self scrollToback];
    }
    [self scrollToMiddle];
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    //NSLog(@"%s", __func__);
    //NSLog(@"suspend");
    [self suspend];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    //NSLog(@"%s", __func__);
    //NSLog(@"restart");
    [self restart];
}

//新开始
- (void)restart {
    //先取消之前的线程
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(restartTimer) object:nil];
    //开始新的线程
    [self performSelector:@selector(restartTimer) withObject:nil afterDelay:2.0];
}

- (void)restartTimer {
    [_timer setFireDate:[NSDate distantPast]];
}

//暂停
- (void)suspend {
    [_timer setFireDate:[NSDate distantFuture]];
}

//翻页操作

//1.向前
- (void)scrollToFront {
    /*  向前:
        1. currentPage=0时, currentPage转到最后一张图片
        2. currentPage>0时, currentPage-1
     */
    //NSLog(@"向前");
    if (_currentPage == 0) {
        [self setCurrentPage:_images.count - 1];
    }else if (_currentPage > 0) {
        [self setCurrentPage:_currentPage - 1];
    }
//    [_middleView setImageWithURL:[NSURL URLWithString:_images[_currentPage]] placeholderImage:self.placeholderImage];
    [self loadImage:_images[_currentPage] forImageView:_middleView];
    [_scrollView setContentOffset:CGPointMake(WIDTH, 0) animated:NO];
    [self scrollToMiddle];
}

//2.向后
- (void)scrollToback {
    /*  向后:
        1. currentPage=最后一张时, currentPage转到第一张图片
        2. currentPage<图片总数时, currentPage+1
     */
    //NSLog(@"向后");
    if (_currentPage == _images.count - 1) {
        [self setCurrentPage:0];
    }else if (_currentPage < _images.count - 1) {
        [self setCurrentPage:_currentPage + 1];
    }
//    [_middleView setImageWithURL:[NSURL URLWithString:_images[_currentPage]] placeholderImage:self.placeholderImage];
    [self loadImage:_images[_currentPage] forImageView:_middleView];
    [_scrollView setContentOffset:CGPointMake(WIDTH, 0) animated:NO];
    [self scrollToMiddle];
}

//3.返回中间
- (void)scrollToMiddle {
    /*  返回中间:
        1.currentPage=0, leftView的图片为最后一张,rightView的图片为currentPage后一张
        2.0<currentPage<图片总数-1, leftView,rightView对应的图片为currentPage前一张,currentPage后一张
        3.currentPage==图片总数-1, leftView的图片为currentPage前一张,rightView的图片为第一张
     */
    
    //NSLog(@"返回中间");
    if (_currentPage == 0) {
        [self loadImage:_images[_images.count-1] forImageView:_leftView];
        [self loadImage:_images[_currentPage + 1] forImageView:_rightView];
//        [_leftView setImageWithURL:[NSURL URLWithString:_images[_images.count-1]] placeholderImage:self.placeholderImage];
//        [_rightView setImageWithURL:[NSURL URLWithString:_images[_currentPage + 1]] placeholderImage:self.placeholderImage];
    }else if (_currentPage > 0 && _currentPage < _images.count - 1) {
//        [_leftView setImageWithURL:[NSURL URLWithString:_images[_currentPage - 1]] placeholderImage:self.placeholderImage];
//        [_rightView setImageWithURL:[NSURL URLWithString:_images[_currentPage + 1]] placeholderImage:self.placeholderImage];
        [self loadImage:_images[_currentPage - 1] forImageView:_leftView];
        [self loadImage:_images[_currentPage + 1] forImageView:_rightView];
        
    }else {
//        [_leftView setImageWithURL:[NSURL URLWithString:_images[_currentPage - 1]] placeholderImage:self.placeholderImage];
//        [_rightView setImageWithURL:[NSURL URLWithString:_images[0]] placeholderImage:self.placeholderImage];
        [self loadImage:_images[_currentPage - 1] forImageView:_leftView];
        [self loadImage:_images[0] forImageView:_rightView];

    }
    
}


#pragma mark -- pageControl
//- (UIPageControl *)pageControl {
//    if (_pageControl == nil) {
//        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, HEIGHT - 20, WIDTH, 10)];
//        _pageControl.numberOfPages = _images.count;
//        _pageControl.currentPage = 0;
//        [_pageControl setCurrentPageIndicatorTintColor:[UIColor blueColor]];
//        _pageControl.userInteractionEnabled = YES;
//        [_pageControl addTarget:self action:@selector(changePageValue:) forControlEvents:UIControlEventValueChanged];
//    }
//    return _pageControl;
//}

//- (void)changePageValue:(UIPageControl *)pageControl {
//    //NSLog(@"%ld", pageControl.currentPage);
//    [self suspend];//暂停
//    _currentPage = pageControl.currentPage;
//    _middleView.image = [UIImage imageNamed:_images[_currentPage]];
//    [_scrollView setContentOffset:CGPointMake(WIDTH, 0) animated:YES];
//    [self scrollToMiddle];
//    [self restart];//出现开始
//}




#pragma mark -- 定时器
- (void)createTimer {
    if (_timer == nil) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(timeGo) userInfo:nil repeats:YES];
    }
}

- (void)timeGo {
    //NSLog(@"go");
    //NSLog(@"向后");
    [_scrollView setContentOffset:CGPointMake(WIDTH * 2, 0) animated:YES];
}


#pragma mark -- 图片点击事件
- (void)clickImageView:(UIGestureRecognizer *)ges {
    if (self.result != nil) {
//        NSLog(@"%ld", _currentPage);
        self.result(_currentPage);
    }
}


//- (UIImage *)placholderImage {
//    if (_placeholderImage == nil) {
//        _placeholderImage = [UIImage imageNamed:@"ad_001"];
//    }
//    return _placeholderImage;
//}


@end
