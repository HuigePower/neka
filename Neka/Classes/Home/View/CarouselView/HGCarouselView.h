//
//  HGCarouselView.h
//  Carousel
//
//  Created by ma c on 2016/12/14.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ResultBlock)(NSInteger index);

@interface HGCarouselView : UIView<UIScrollViewDelegate>

@property (nonatomic, strong)NSArray *images;
@property (nonatomic, copy)ResultBlock result;
@property (nonatomic, assign)NSInteger currentPage;
@property (nonatomic, strong)UILabel *pageLabel;
@property (nonatomic, strong)UIImage *placeholderImage;


//placeholderImage==nil，则使用默认图片
- (instancetype)initWithFrame:(CGRect)frame andPlaceholderImage:(UIImage *)placeholderImage;
- (void)restart;
- (void)suspend;

//通过图片的url加载网络图片
- (void)setImagesWithURLs:(NSArray *)images showBottom:(BOOL)isShow;
//通过图片的名字加载本地
- (void)setImagesWithNames:(NSArray *)images showBottom:(BOOL)isShow;

@end
