//
//  ScanView.m
//  Neka1.0
//
//  Created by ma c on 16/7/30.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "ScanView.h"

@implementation ScanView
{
    UIImageView *_line;
    CGFloat _lineY;
    NSTimer *_timer;
}


- (void)initQRLine {
    
    

    _line  = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH(50), KWIDTH(100), KWIDTH(220), 2)];
    _line.image = [UIImage imageNamed:@"qr_scan_line"];
    _line.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:_line];
    _lineY = _line.frame.origin.y;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    if (!_line) {
        
        [self initQRLine];
        
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(show) userInfo:nil repeats:YES];
        [_timer fire];
    }
    
    
}


- (void)drawRect:(CGRect)rect {
    CGSize screenSize = CGSizeMake(SCREEN_SIZE.width, SCREEN_SIZE.height);
    CGRect screenDrawRect = CGRectMake(0, 0, screenSize.width, screenSize.height);
    
    CGRect clearDrawRect = CGRectMake(KWIDTH(50), KWIDTH(100), KWIDTH(220), KWIDTH(220));

    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetRGBFillColor(ctx, 40 / 255.0, 40 / 255.0, 40 / 255.0, 0.5);
    CGContextFillRect(ctx, screenDrawRect);
    
    CGContextClearRect(ctx, clearDrawRect);
    
    CGContextStrokeRect(ctx, clearDrawRect);
    CGContextSetRGBStrokeColor(ctx, 1, 1, 1, 1);
    CGContextSetLineWidth(ctx, 0.8);
    CGContextAddRect(ctx, clearDrawRect);
    CGContextStrokePath(ctx);
    
    
    [self addCornerLineWithContext:ctx rect:clearDrawRect];

}

- (void)addCornerLineWithContext:(CGContextRef)ctx rect:(CGRect)rect{
    
    //画四个边角
    CGContextSetLineWidth(ctx, 2);
    CGContextSetRGBStrokeColor(ctx, 83 /255.0, 239/255.0, 111/255.0, 1);//绿色
    
    //左上角
    CGPoint poinsTopLeftA[] = {
        CGPointMake(rect.origin.x+0.7, rect.origin.y),
        CGPointMake(rect.origin.x+0.7 , rect.origin.y + 15)
    };
    
    CGPoint poinsTopLeftB[] = {CGPointMake(rect.origin.x, rect.origin.y +0.7),CGPointMake(rect.origin.x + 15, rect.origin.y+0.7)};
    [self addLine:poinsTopLeftA pointB:poinsTopLeftB ctx:ctx];
    
    //左下角
    CGPoint poinsBottomLeftA[] = {CGPointMake(rect.origin.x+ 0.7, rect.origin.y + rect.size.height - 15),CGPointMake(rect.origin.x +0.7,rect.origin.y + rect.size.height)};
    CGPoint poinsBottomLeftB[] = {CGPointMake(rect.origin.x , rect.origin.y + rect.size.height - 0.7) ,CGPointMake(rect.origin.x+0.7 +15, rect.origin.y + rect.size.height - 0.7)};
    [self addLine:poinsBottomLeftA pointB:poinsBottomLeftB ctx:ctx];
    
    //右上角
    CGPoint poinsTopRightA[] = {CGPointMake(rect.origin.x+ rect.size.width - 15, rect.origin.y+0.7),CGPointMake(rect.origin.x + rect.size.width,rect.origin.y +0.7 )};
    CGPoint poinsTopRightB[] = {CGPointMake(rect.origin.x+ rect.size.width-0.7, rect.origin.y),CGPointMake(rect.origin.x + rect.size.width-0.7,rect.origin.y + 15 +0.7 )};
    [self addLine:poinsTopRightA pointB:poinsTopRightB ctx:ctx];
    
    CGPoint poinsBottomRightA[] = {CGPointMake(rect.origin.x+ rect.size.width -0.7 , rect.origin.y+rect.size.height+ -15),CGPointMake(rect.origin.x-0.7 + rect.size.width,rect.origin.y +rect.size.height )};
    CGPoint poinsBottomRightB[] = {CGPointMake(rect.origin.x+ rect.size.width - 15 , rect.origin.y + rect.size.height-0.7),CGPointMake(rect.origin.x + rect.size.width,rect.origin.y + rect.size.height - 0.7 )};
    [self addLine:poinsBottomRightA pointB:poinsBottomRightB ctx:ctx];
    CGContextStrokePath(ctx);
}

- (void)addLine:(CGPoint[])pointA pointB:(CGPoint[])pointB ctx:(CGContextRef)ctx {
    CGContextAddLines(ctx, pointA, 2);
    CGContextAddLines(ctx, pointB, 2);
}


- (void)show {
    
    [UIView animateWithDuration:0.02 animations:^{
        
        CGRect rect = _line.frame;
        rect.origin.y = _lineY;
        _line.frame = rect;
        
    } completion:^(BOOL finished) {
        
        CGFloat maxBorder = KWIDTH(100) + KWIDTH(220);
        if (_lineY> maxBorder) {
            
            _lineY = KWIDTH(100);
        }
        _lineY++;
    }];
}

- (void)stopRunning {
    [_timer setFireDate:[NSDate distantFuture]];
    [_timer invalidate];
    
}

@end
