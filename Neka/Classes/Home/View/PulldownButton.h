//
//  PulldownButton.h
//  下拉选择控件
//
//  Created by ma c on 2017/2/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PulldownButton : UIButton
@property (nonatomic, strong)UILabel *label;
@property (nonatomic, strong)UIImageView *image;
@property (nonatomic, strong)NSString *text;
@end
