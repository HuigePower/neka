//
//  HGChartView.m
//  表格
//
//  Created by ma c on 2017/2/14.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGChartView.h"
#define WIDTH_SCALE 0.4
#define CELL_HEIGHT 44.0 //行高

@implementation HGChartView

- (instancetype)initWithFrame:(CGRect)frame andDataSource:(NSDictionary *)warrantyData {
    if (self = [super initWithFrame:frame]) {
        NSString *area_name = warrantyData[@"area_name"];
        NSString *own_volume_number = warrantyData[@"own_volume_number"];
        NSString *order_id = warrantyData[@"order_id"];
        NSString *product_name = warrantyData[@"product_name"];
        NSString *user_name = warrantyData[@"user_name"];
        NSString *car_name = warrantyData[@"car_name"];
        NSString *car_number =[NSString stringWithFormat:@"%@%@", warrantyData[@"car_number_name"], warrantyData[@"car_number"]];
        NSString *keep_years = [NSString stringWithFormat:@"%@年", warrantyData[@"keep_years"]];
        //时间戳转时间
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        
        NSDate *date1 = [NSDate dateWithTimeIntervalSince1970:[warrantyData[@"own_start_time"] floatValue]];
        NSString *own_start_time = [dateFormatter stringFromDate:date1];
        NSDate *date2 = [NSDate dateWithTimeIntervalSince1970:[warrantyData[@"own_end_time"] floatValue]];
        NSString *own_end_time = [dateFormatter stringFromDate:date2];
        
        
        _dataSource = @[@[@"所属城市", area_name],
                        @[@"质保单号", own_volume_number],
                        @[@"订单号码", order_id],
                        @[@"产品名称", product_name],
                        @[@"客户姓名", user_name],
                        @[@"品牌车型", car_name],
                        @[@"车牌号码", car_number],
                        @[@"质保时间", keep_years],
                        @[@"贴膜日期", own_start_time],
                        @[@"到期时间", own_end_time],
                        ];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:layout];
        _leftWidth = frame.size.width * WIDTH_SCALE;
        _rightWidth = frame.size.width - _leftWidth;
        
        //_collectionView.scrollEnabled = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.layer.borderWidth = 1;
        _collectionView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [self addSubview:_collectionView];
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        
    }
    return self;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return _dataSource.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 2;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //cell的宽和高
    CGFloat width = cell.frame.size.width;
    CGFloat height = cell.frame.size.height;
    
    //行之间的色差
    if (indexPath.section % 2 == 0) {
        cell.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    }else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    //内容
    UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, width - 20, height)];
    contentLabel.font = [UIFont systemFontOfSize:14.0];
    contentLabel.text = _dataSource[indexPath.section][indexPath.row];
    [cell addSubview:contentLabel];
    
    return  cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row % 2 == 0) {
        return CGSizeMake(_leftWidth, CELL_HEIGHT);
    }else {
        return CGSizeMake(_rightWidth, CELL_HEIGHT);
    }
}

@end
