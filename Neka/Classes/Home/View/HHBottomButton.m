//
//  HHBottomButton.m
//  Neka1.0
//
//  Created by ma c on 16/10/17.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HHBottomButton.h"
#import "PrefixHeader.pch"
@interface HHBottomButton ()
@property (nonatomic, strong)UIImageView *imgView; //图片
@property (nonatomic, strong)UILabel *majorTitle; //主标题
@property (nonatomic, strong)UILabel *secondaryTitle; //副标题
@property (nonatomic, strong)UIImageView *backView; //背景
@end

@implementation HHBottomButton

- (instancetype)initWithFrame:(CGRect)frame withDistributionMode:(DistributionMode)mode {
    if (self = [super initWithFrame:frame]) {
        
        _backView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:_backView];
        _mode = mode;
        _imgView = [UIImageView new];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        _majorTitle = [UILabel new];
        _secondaryTitle = [UILabel new];
        
        
        if (_mode == DistributionModeTitleImage) {//标题在上/图片在下
            [self addSubview:_imgView];
            [self addSubview:_majorTitle];
            _majorTitle.frame = CGRectMake(0, 5, self.frame.size.width, 20);
            _majorTitle.font = [UIFont systemFontOfSize:14];
            _majorTitle.textAlignment = NSTextAlignmentCenter;
            _majorTitle.textColor = [UIColor blackColor];

            
            _imgView.frame = CGRectMake(10, 25, self.frame.size.width - 20, self.frame.size.height - 30);
            _imgView.layer.masksToBounds = YES;
            _imgView.layer.cornerRadius = 3;
            
            
            
        }else { //纵向
            [self addSubview:_imgView];
            [self addSubview:_majorTitle];
            [self addSubview:_secondaryTitle];
            //副标题
            _secondaryTitle.frame = CGRectMake(0, self.frame.size.height - 15, self.frame.size.width, 10);
            _secondaryTitle.font = [UIFont systemFontOfSize:12];
            _secondaryTitle.textColor = [UIColor lightGrayColor];
            _secondaryTitle.textAlignment = NSTextAlignmentCenter;
            
            //主标题
            _majorTitle.frame = CGRectMake(0, self.frame.size.height - 35, self.frame.size.width, 20);
            _majorTitle.font = [UIFont systemFontOfSize:15];
            _majorTitle.textAlignment = NSTextAlignmentCenter;
//            _majorTitle.textColor = mRGB(0, 104, 219, 1);
            _majorTitle.textColor = [UIColor blackColor];
            
            
            CGFloat width = self.frame.size.height - 35 - 10;
            
            _imgView.frame = CGRectMake(0, 5, width, width);
            _imgView.center = CGPointMake(self.frame.size.width / 2, 5 + width / 2);
            

            
        }
    }
    return self;
}

- (void)setImage:(NSString *)imageName majorTitle:(NSString *)majorTitle secondaryTitle:(NSString *)secondaryTitle {
    
    _imgView.image = [UIImage imageNamed:imageName];
    _majorTitle.text = majorTitle;
    _secondaryTitle.text = secondaryTitle;
    
}







@end
