//
//  HGButton.m
//  Neka1.0
//
//  Created by ma c on 2016/12/8.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HGButton.h"

@implementation HGButton
{
    CGFloat _verticalSpace;
    CGFloat _horizontalSpace;
}
- (instancetype)initWithFrame:(CGRect)frame andHorizontalSpace:(CGFloat)horizontalSpace verticalSpace:(CGFloat)verticalSpace {
    if (self = [super initWithFrame:frame]) {
        _horizontalSpace = horizontalSpace;
        _verticalSpace = verticalSpace;
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat imageWidth = self.frame.size.width - _horizontalSpace * 2;
    CGFloat imageHeight = imageWidth;
    CGFloat labelWidth = self.titleLabel.intrinsicContentSize.width;
    CGFloat labelHeight = self.titleLabel.intrinsicContentSize.height;
    
    CGFloat imageX = (self.frame.size.width - imageWidth) / 2;
    CGFloat imageY = _verticalSpace;
    CGFloat labelX = (self.frame.size.width - labelWidth) / 2;
    CGFloat spaceTmp = (self.frame.size.height - _verticalSpace - imageHeight - labelHeight) / 2;
    CGFloat labelY = imageY + imageHeight + spaceTmp;
    
    self.imageView.frame = CGRectMake(imageX, imageY, imageWidth, imageHeight);
    self.titleLabel.frame = CGRectMake(labelX, labelY, labelWidth, labelHeight);
//    NSLog(@"%f--%f--%f--%f", imageX, imageY, imageWidth, imageHeight);
//    NSLog(@"%f--%f--%f--%f", labelX, labelY, labelWidth, labelHeight);
}




@end
