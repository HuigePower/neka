//
//  RecommendSubViews.m
//  Neka
//
//  Created by yu on 2017/12/6.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "RecommendSubViews.h"
#import "UIView+Border.h"
#import "HGOrderWebViewController.h"
//背景颜色
#define BGColor [UIColor colorWithHexString:@"#F4F3F9"]
//分割线颜色
#define LineColor [UIColor colorWithHexString:@"#D5D5D5"]
@implementation RecommendSubViews


- (instancetype)initWithFrame:(CGRect)frame andNavigationController:(UINavigationController *)nav {
    if (self = [super initWithFrame:frame]) {
        _nav = nav;
    }
    return self;
}

//进入会员店铺
+ (RecommendSubViews *)shopEntranceViewWithFrame:(CGRect)frame Title:(NSString *)title andNav:(UINavigationController *)nav {
    RecommendSubViews *view = [[RecommendSubViews alloc] initWithFrame:frame andNavigationController:nav];
    view.backgroundColor = BGColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, frame.size.width - 100, frame.size.height)];
    label.font = [UIFont systemFontOfSize:12];
    label.textColor = [UIColor colorWithHexString:@"666666"];
    label.text = title;
    [view addSubview:label];
    
    UIButton *enterBtn = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width - 47, 5, 32, 20)];
    [enterBtn setImage:[UIImage imageNamed:@"shopEnter"] forState:UIControlStateNormal];
    [enterBtn addTarget:view action:@selector(enterShop) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:enterBtn];
    
    [view setBorderTopRightLeftBottom:UIEdgeInsetsMake(0, 0, 0.5, 0) color:LineColor];
    return view;
    
}

- (void)enterShop {
    NSLog(@"进入店铺");
}


//推荐预约/专属预约/洗车预约
+ (RecommendSubViews *)recommendTitleViewWithFrame:(CGRect)frame andTitle:(NSString *)title andNav:(UINavigationController *)nav {
    RecommendSubViews *view = [[RecommendSubViews alloc] initWithFrame:frame andNavigationController:nav];
    
    view.backgroundColor = BGColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, frame.size.width - 30, frame.size.height)];
    label.textColor = [UIColor colorWithHexString:@"999999"];
    label.text = title;
    label.font = [UIFont systemFontOfSize:12];
    [view addSubview:label];
    [view setBorderTopRightLeftBottom:UIEdgeInsetsMake(0, 0, 0.5, 0) color:LineColor];
    return view;
}


//推荐预约-主要预约
+ (RecommendSubViews *)recommendMainAppointmentViewWithNav:(UINavigationController *)nav {
    RecommendSubViews *view = [[RecommendSubViews alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(125)) andNavigationController:nav];
    
    view.backgroundColor = BGColor;
    
    CGFloat width = SCREEN_SIZE.width / 3;
    
    NSArray *arr = @[@{@"bigTitle": @"改色贴膜",
                       @"smallTitle": @"爱车炫起来",
                       @"img": @"appoint_gs",
                       @"color": @"#1CF0E3",
                       @"price": @"NEKA全车 ¥2999"
                       },
                     @{@"bigTitle": @"隐形车衣",
                       @"smallTitle": @"漆面防护盾",
                       @"img": @"appoint_cy",
                       @"color": @"#F66702",
                       @"price": @"NEKA全车 ¥9900"
                       },
                     @{@"bigTitle": @"玻璃贴膜",
                       @"smallTitle": @"隔热与防爆",
                       @"img": @"appoint_bl",
                       @"color": @"#5493FF",
                       @"price": @"NEKA全车 ¥1999"
                       }];
    
    for (int i = 0; i < 3; i ++) {
        NSDictionary *data = arr[i];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(width * i, 0, width, KWIDTH_6S(125))];
        btn.userInteractionEnabled = YES;
        btn.tag = 10 + i;
        UILabel *bigLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH_6S(10), width, KWIDTH_6S(20))];
        bigLabel.text = data[@"bigTitle"];
        bigLabel.font = [UIFont systemFontOfSize:18];
        bigLabel.textColor = [UIColor blackColor];
        bigLabel.textAlignment = NSTextAlignmentCenter;
        [btn addSubview:bigLabel];
        
        UILabel *smallLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH_6S(35), width, KWIDTH_6S(15))];
        smallLabel.text = data[@"smallTitle"];
        smallLabel.font = [UIFont systemFontOfSize:12];
        smallLabel.textColor = [UIColor colorWithHexString:data[@"color"]];
        smallLabel.textAlignment = NSTextAlignmentCenter;
        [btn addSubview:smallLabel];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH_6S(65), KWIDTH_6S(65))];
        imageView.center = CGPointMake(width / 2, KWIDTH_6S(80));
        imageView.image = [UIImage imageNamed:arr[i][@"img"]];
        [btn addSubview:imageView];
        
        [btn addTarget:view action:@selector(selectAppointment:) forControlEvents:UIControlEventTouchUpInside];
        
        
        UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH_6S(110), width, KWIDTH_6S(10))];
        priceLabel.text = data[@"price"];
        priceLabel.textAlignment = NSTextAlignmentCenter;
        priceLabel.textColor = [UIColor colorWithHexString:@"FF2829"];
        priceLabel.attributedText = [RecommendSubViews makeAttributeStringWith:priceLabel.text];
        priceLabel.font = [UIFont systemFontOfSize:9];
        
        [btn addSubview:priceLabel];
        
        
        [view addSubview:btn];
        
        if (i == 1) {
            [btn setBorderTopRightLeftBottom:UIEdgeInsetsMake(0, 0.5, 0, 0.5) color:LineColor];
        }
    }
    
    [view setBorderTopRightLeftBottom:UIEdgeInsetsMake(0, 0, 0.5, 0) color:LineColor];
    
    return view;
}




//推荐预约-次要预约
+ (RecommendSubViews *)recommendSecondaryAppointmentViewWithNav:(UINavigationController *)nav {
    RecommendSubViews *view = [[RecommendSubViews alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(135)) andNavigationController:nav];
    view.backgroundColor = BGColor;
    NSArray *arr = @[@{@"title": @"洗车", @"img": @"xc"},
                     @{@"title": @"打蜡", @"img": @"dl"},
                     @{@"title": @"镀膜", @"img": @"dm"},
                     @{@"title": @"镀晶", @"img": @"dj"},
                     @{@"title": @"内饰清洗", @"img": @"nsqx"},
                     @{@"title": @"轮毂清洗", @"img": @"lgqx"},
                     @{@"title": @"空调清洗", @"img": @"ktqx"},
                     @{@"title": @"全部预约", @"img": @"qbyy"}
                     ];
    CGFloat width = SCREEN_SIZE.width / 4;
    CGFloat height = KWIDTH_6S(50);
    for (int i = 0; i < 8; i++) {
        int column = i % 4; //第几列
        int line = i / 4;   //第几行
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(width * column, KWIDTH_6S(10) * (line + 1) + height * line, width, height)];
        btn.userInteractionEnabled = YES;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH_6S(30), KWIDTH_6S(30))];
        
        imageView.center = CGPointMake(width / 2, KWIDTH(15));
        imageView.image = [UIImage imageNamed:arr[i][@"img"]];
        [btn addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH_6S(35), width, KWIDTH_6S(15))];
        label.text = arr[i][@"title"];
        label.font = [UIFont systemFontOfSize:12];
        label.textColor = [UIColor colorWithHexString:@"666666"];
        label.textAlignment = NSTextAlignmentCenter;
        [btn addSubview:label];
        
        [view addSubview:btn];
        [btn addTarget:view action:@selector(selectAppointment:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 13 + i;
    }
    return view;
}

//专属预约
+ (RecommendSubViews *)exclusiveAppointmentViewWithNav:(UINavigationController *)nav {
    NSArray *list = @[@{@"title": @"宝马专属", @"tag": @"低至一元", @"img": @"baomashengji", @"color": @"#007EDE"},
                      @{@"title": @"奔驰服务", @"tag": @"一站齐全", @"img": @"benchishengji", @"color": @"#FF6B04"},
                      @{@"title": @"重塑奥迪", @"tag": @"原厂升级", @"img": @"aodishengji", @"color": @"#EB3330"},
                      @{@"title": @"保时捷专区", @"tag": @"保真更优惠", @"img": @"baoshijieshengji", @"color": @"#FF9847"}
                      ];
    RecommendSubViews *view = [[RecommendSubViews alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(110)) andNavigationController:nav];
    view.backgroundColor = BGColor;
    CGFloat width = SCREEN_SIZE.width / 4;
    CGFloat height = KWIDTH_6S(110);
    for (int i = 0; i < 4; i++) {
        NSDictionary *data = list[i];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(width * i, 0, width, height)];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH_6S(40), KWIDTH_6S(40))];
        imageView.image = [UIImage imageNamed:data[@"img"]];
        imageView.center = CGPointMake(width / 2, KWIDTH_6S(30));
        [btn addSubview:imageView];
        
        UILabel *mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH_6S(50), width, KWIDTH_6S(30))];
        mainLabel.text = data[@"title"];
        mainLabel.textColor = [UIColor blackColor];
        mainLabel.font = [UIFont systemFontOfSize:15];
        mainLabel.textAlignment = NSTextAlignmentCenter;
        [btn addSubview:mainLabel];
        
        UILabel *secondaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, KWIDTH_6S(80), btn.frame.size.width, KWIDTH_6S(15))];
        secondaryLabel.text = data[@"tag"];
        secondaryLabel.textColor = [UIColor colorWithHexString:data[@"color"]];
        secondaryLabel.font = [UIFont systemFontOfSize:12];
        secondaryLabel.textAlignment = NSTextAlignmentCenter;
        [btn addSubview:secondaryLabel];
        [btn addTarget:view action:@selector(selectAppointment:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 21 + i;
        [view addSubview:btn];
        if (i != 0) {
            [btn setBorderTopRightLeftBottom:UIEdgeInsetsMake(0, 0.5, 0, 0) color:LineColor];
        }
    }
    return view;
}



- (void)selectAppointment:(UIButton *)btn {
    NSLog(@"%ld", btn.tag);
    
    //改色贴膜、隐形车衣、玻璃贴膜（0~2）
    //洗车、打蜡、镀膜、镀晶、内饰清洗、轮毂清洗、空调清洗、全部预约(3~10)
    //宝马、奔驰、奥迪、保时捷(11~14)
    NSArray *urls =
        @[@"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=category&cat_id=8",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=toStore&cat_id=91",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=glassToStore&cat_id=98",
          
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=two_category&cat_id=89",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=two_category&cat_id=104",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=two_category&cat_id=105",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=two_category&cat_id=106",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=two_category&cat_id=107",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=two_category&cat_id=108",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=two_category&cat_id=109",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=two_category&cat_id=110",
          
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=category&cat_id=1",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=category&cat_id=3",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=category&cat_id=4",
          @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=category&cat_id=5"];
    
    NSInteger index = btn.tag - 10;
    NSString *url = urls[index];
    if (url.length > 0) {
        HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
        web.urlString = url;
        [self.nav pushViewController:web animated:YES];
    }else {
        [MBProgressHUD showError:@"功能待完善..." toView:[UIApplication sharedApplication].keyWindow];
    }
    
}

#pragma mark -- 制作富文本
+ (NSMutableAttributedString *)makeAttributeStringWith:(NSString *)string {
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] initWithData:nil ofType:nil];
    textAttachment.image = [UIImage imageNamed:@"qi"];
    textAttachment.bounds = CGRectMake(KWIDTH_6S(2), KWIDTH_6S(0), KWIDTH_6S(12), KWIDTH_6S(12));
    
    NSAttributedString *str = [NSAttributedString attributedStringWithAttachment:textAttachment];
    [attributedString appendAttributedString:str];
    return attributedString;
}




@end
