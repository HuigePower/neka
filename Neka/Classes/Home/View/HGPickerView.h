//
//  HGPickerView.h
//  选择器
//
//  Created by ma c on 2017/2/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PulldownButton.h"
#import "WarrantyAddViewController.h"

typedef NS_ENUM(NSUInteger, PickType) {
    PickTypeProvince,       //省
    PickTypeCity,           //市
    PickTypeOrderID,        //订单ID
    PickTypeCarName,        //车品牌
    PickTypeCarNameSeries,  //车系
    PickTypeCarNumberName,  //车牌号码前缀
    PickTypeWarrantyID      //质保号
};

typedef void(^SelectResult) (id data, PickType type);

@interface HGPickerView : UIView<UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong)UIPickerView *pickerView;
@property (nonatomic, strong)NSMutableArray *dataSource;
@property (nonatomic, copy)SelectResult result;


- (void)showInSuperViewControl:(WarrantyAddViewController *)viewControl withDataSource:(NSArray *)dataSource andButton:(PulldownButton *)button pickerType:(PickType)pickerType;


- (void)showInSuperViewControl:(WarrantyAddViewController *)viewControl withDataSource:(NSArray *)dataSource andButton:(PulldownButton *)button;

- (void)showInSuperViewControl:(WarrantyAddViewController *)viewControl withDataSource:(NSArray *)dataSource;

- (void)showInSuperViewControl:(WarrantyAddViewController *)viewControl;

- (void)hide;


@end
