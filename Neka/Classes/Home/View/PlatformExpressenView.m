//
//  PlatformExpressenView.m
//  平台快报
//
//  Created by ma c on 16/8/27.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "PlatformExpressenView.h"
#import "HGNewsModel.h"
#import "ExpressenViewController.h"
@interface PlatformExpressenView ()<UIScrollViewDelegate>
@property (nonatomic, strong)UIImageView *iconView;
@property (nonatomic, strong)UIScrollView *contentView;

@property (nonatomic, strong)NSTimer *timer;
@property (nonatomic, strong)NSMutableArray *viewArr;
@end
@implementation PlatformExpressenView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self addSubview:self.iconView];
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(95, 10, 0.5, self.frame.size.height - 20)];
        line.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
        [self addSubview:line];
        self.backgroundColor = mRGB(240, 240, 248, 1);
    }
    return self;
}

- (void)setNewsArray:(NSArray *)newsArray {
    if (newsArray == nil) return;
    _newsArray = newsArray;
    [self addSubview:self.contentView];
}


- (UIImageView *)iconView {
    if (_iconView == nil) {
        _iconView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2, 80, 40)];
        _iconView.contentMode = UIViewContentModeScaleAspectFit;
        _iconView.image = [UIImage imageNamed:@"platformExpressen"];
    }
    return _iconView;
}



- (UIView *)contentView {
    if (_contentView == nil) {
        _contentView = [[UIScrollView alloc] initWithFrame:CGRectMake(95, 10, self.frame.size.width - 110, self.frame.size.height - 20)];
        _contentView.pagingEnabled = YES;
        _contentView.userInteractionEnabled = NO;
        _contentView.delegate = self;
        NSInteger page = self.newsArray.count;        
        if (page > 1) {
            [self timer];
        }
        
        _contentView.contentSize = CGSizeMake(_contentView.frame.size.width, _contentView.frame.size.height * (page + 2));
        float height = _contentView.frame.size.height / 2;
        _viewArr = [[NSMutableArray alloc] init];
        for (int i = 0; i < page; i++) {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 5, _contentView.frame.size.width - 20, _contentView.frame.size.width - 20)];
            
            for (int j = 0; j < 1; j++) {
                int index = i * 1 + j;
                NSLog(@"%d", j);
                if (index < self.newsArray.count) {
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5 + height * j, _contentView.frame.size.width - 20, height)];
                    HGNewsModel *model = self.newsArray[index];
                    label.text = [NSString stringWithFormat:@"[%@]%@", model.cat_name, model.title];
                    label.font = [UIFont systemFontOfSize:14];
                    label.textColor = [UIColor blackColor];
                    [view addSubview:label];
                }
            }
            [_viewArr addObject:view];
        }
        NSData *firstViewData = [NSKeyedArchiver archivedDataWithRootObject:[_viewArr firstObject]];
        UIView *firstView = [NSKeyedUnarchiver unarchiveObjectWithData:firstViewData];
    
        [_viewArr addObject:firstView];
        NSLog(@"%@", _viewArr);
        for (int i = 0; i < _viewArr.count; i++) {
            NSLog(@"--%d", i);
            UIView *view = (UIView *)_viewArr[i];
            view.frame = CGRectMake(0, height * 2 * i, _contentView.frame.size.width - 20, _contentView.frame.size.width - 10);
            [_contentView addSubview:view];
        }

    }
    return _contentView;
}

- (NSTimer *)timer {
    if (_timer == nil) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(timeGO) userInfo:nil repeats:YES];
        
    }
    return _timer;
}

- (void)timeGO {
    float height = _contentView.frame.size.height;
    int page = _contentView.contentOffset.y / height;
    
    if (++page < _viewArr.count) {
        [UIView animateWithDuration:0.5 animations:^{
            [_contentView setContentOffset:CGPointMake(0, height * page)];
        } completion:^(BOOL finished) {
           int page = _contentView.contentOffset.y / height;
            if (page == _viewArr.count - 1) {
                [_contentView setContentOffset:CGPointMake(0, 0)];
            }

        }];
    }
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSLog(@"%---d", (int)(_contentView.contentOffset.y / self.contentView.frame.size.height));

}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    NSLog(@"%s", __func__);
}



- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
     NSLog(@"%---d", (int)(scrollView.contentOffset.y / scrollView.frame.size.height));
}




- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    ExpressenViewController *expressen = [[ExpressenViewController alloc] init];
    [_nav pushViewController:expressen animated:YES];
}



@end
