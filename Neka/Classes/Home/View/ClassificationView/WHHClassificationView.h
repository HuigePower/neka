//
//  WHHClassificationView.h
//  轮播图
//
//  Created by ma c on 16/8/27.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^SelectBlock)(NSString *title);
@interface WHHClassificationView : UIView
@property (nonatomic, strong)NSArray *titles;
@property (nonatomic, strong)NSArray *iconNames;
@property (nonatomic, copy)SelectBlock result;

- (instancetype)initWithFrame:(CGRect)frame andTitles:(NSArray *)titles iconNames:(NSArray *)iconNames;
@end
