//
//  ClassificationSelection.h
//  Classification
//
//  Created by ma c on 16/10/12.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, ThirdViewShowMode) {
    ThirdViewShowModeLeft = 0,   //左侧弹出
    ThirdViewShowModeCenter      //从当前cell位置上下撑开
};

@protocol ClassificationResultDelegate <NSObject>

//一级分类点击事件
- (void)firstClassTouchEvent;
//选择结果
- (void)resultOfSelet:(NSString *)classStr;

@end


@interface ClassificationSelection : UIView<UITableViewDelegate, UITableViewDataSource>

//代理
@property (nonatomic, retain)id<ClassificationResultDelegate> delegate;


//分类
@property (nonatomic, strong)NSArray *list;
- (void)setList:(NSArray *)list;

//只显示顶部分类是的rect
@property (nonatomic, assign)CGRect smallRect;
//全部展示的rect
@property (nonatomic, assign)CGRect bigRect;
//是否允许改变一级分类按钮的title, 默认为不允许
@property (nonatomic, assign)BOOL isAllowChangeTitle;

/* 一级分类 */
//一级分类视图
@property (nonatomic, strong)UIView *topView;
//视图高度
@property (nonatomic, assign)CGFloat topHeaderHeight;
//Normal状态颜色
@property (nonatomic, strong)UIColor *normalColor;
//Selected状态颜色
@property (nonatomic, strong)UIColor *selectedColor;
//按钮数组
@property (nonatomic, strong)NSMutableArray *btnArr;

//二级视图
@property (nonatomic, strong)UITableView *secondView;

//三级视图
@property (nonatomic, strong)UITableView *thirdView;
@property (nonatomic, strong)NSArray *thirdList;

//分类Index
@property (nonatomic, assign)NSInteger index;

//弹出三级分类视图的方式
@property (nonatomic, assign)ThirdViewShowMode showMode;




- (instancetype)initWithFrame:(CGRect)frame withShowMode:(ThirdViewShowMode)showMode;
@end








