//
//  ClassificationSelection.m
//  Classification
//
//  Created by ma c on 16/10/12.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "ClassificationSelection.h"
#import "OptionButton.h"

#define K_WIDTH self.frame.size.width
@implementation ClassificationSelection
{
    CGRect _tmpRect;
    NSString *_tmpSelection;
    NSInteger  *_selectedIndex;
    
    UILabel *_headerLabel;
    UIImageView *_imageView;
}

- (instancetype)initWithFrame:(CGRect)frame withShowMode:(ThirdViewShowMode)showMode {
    if (self = [super initWithFrame:frame]) {
        _showMode = showMode;
        _bigRect = frame;
        _isAllowChangeTitle = YES;
        //初始化一级分类视图属性
        [self firstClassificationNature];
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 30, 15, 20, 10)];
        
    }
    return self;
}

- (void)setList:(NSArray *)list {
    _list = list;
    
    
    [self addSubview:self.topView];
//    OptionButton *btn = (OptionButton *)[self viewWithTag:10];
//    [self btnClick:btn];
    
    [self addSubview:self.secondView];
    [self hideView];
}


//一级分类视图
- (UIView *)topView {
    if (_topView == nil) {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.topHeaderHeight)];
        _smallRect = CGRectMake(self.frame.origin.x, self.frame.origin.y, _topView.frame.size.width, _topView.frame.size.height);
        _btnArr = [[NSMutableArray alloc] init];
        NSInteger btnCount = self.list.count;
        for (int i = 0; i < btnCount; i++) {
            OptionButton *btn = [[OptionButton alloc] initWithFrame:CGRectMake(K_WIDTH / btnCount * i, 0, K_WIDTH / btnCount, self.topHeaderHeight)];
            NSString *title = self.list[i][0][@"first"];
            [btn setTitle:title fontSize:self.topHeaderHeight / 3 normalColor:self.normalColor andSelectedColor:self.selectedColor];
            btn.tag = 10 + i;
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"%@",self.list[i][0][@"first"]);
            [_btnArr addObject:btn];
            [_topView addSubview:btn];
        }
        
        for (int i = 1; i < btnCount; i++) {
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(K_WIDTH / btnCount * i, 5, 0.5, self.topHeaderHeight - 10)];
            line.backgroundColor = [UIColor lightGrayColor];
            [_topView addSubview:line];
        }
        
        for (int i = 0; i < 2; i++) {
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, (self.topHeaderHeight - 0.5) * i, K_WIDTH, 0.5)];
            line.backgroundColor = [UIColor lightGrayColor];
            [_topView addSubview:line];
        }
    }
    return _topView;
}


- (void)btnClick:(OptionButton *)btn {
    if ([self.delegate respondsToSelector:@selector(firstClassTouchEvent)]) {
        [self.delegate firstClassTouchEvent];
    }
    for (OptionButton *itemBtn in _btnArr) {
        if (itemBtn == btn) {
            BOOL show = !btn.selected;
            [itemBtn setSelected:show];
            if (show) {
                self.frame = _bigRect;
                [self addSubview:self.secondView];
            }else {
                [self hideView];
            }
        }else {
            [itemBtn setSelected:NO];
        }
        _index = btn.tag - 10;
        [self.secondView reloadData];
        [self showThirdView:NO];
    }
    
}

- (void)hideView {

    self.frame = _smallRect;
    [self.secondView removeFromSuperview];
    [self.thirdView removeFromSuperview];
    for (OptionButton *itemBtn in _btnArr) {
        [itemBtn setSelected:NO];
    }
}

//二级视图
- (UITableView *)secondView {
    if (_secondView == nil) {
        _secondView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.topHeaderHeight, K_WIDTH, self.frame.size.height - self.topHeaderHeight)];
        _secondView.delegate = self;
        _secondView.dataSource = self;
        _secondView.bounces = NO;
        _secondView.tableFooterView = [UIView new];
        [_secondView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"myCell"];
    }
    return _secondView;
}


//三级视图
- (UITableView *)thirdView {
    if (_thirdView == nil) {
        _thirdView = [UITableView new];
        _thirdView.delegate = self;
        _thirdView.dataSource = self;
        _secondView.bounces = NO;
        _thirdView.tableFooterView = [UIView new];
        [_thirdView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"myCell"];
    }
    return _thirdView;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.secondView) {
        return 1;
    }else {
        return 1;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _secondView) {
        return [_list[_index] count];
    }else {
        return _thirdList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.textLabel.font = [UIFont systemFontOfSize:10];
    if (tableView == self.secondView) {
        NSDictionary *dic = _list[_index][indexPath.row];
        cell.textLabel.text = dic[@"second"];
        
        NSArray *arr = dic[@"third"];
        if (arr.count == 0) {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }else {
            cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.backgroundColor = [UIColor whiteColor];
    }else {
        cell.textLabel.text = _thirdList[indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryNone;
        if (_showMode == ThirdViewShowModeLeft) {
            cell.backgroundColor = [UIColor lightGrayColor];
        }else {
            cell.backgroundColor = [UIColor whiteColor];
        }
    }
    
    cell.textLabel.font = [UIFont systemFontOfSize:self.topHeaderHeight * 0.4];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == self.thirdView) {
        if (_showMode == ThirdViewShowModeCenter) {
            return 40;
        }
        return 0;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
    btn.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
    [btn addTarget:self action:@selector(hideThirdView) forControlEvents:UIControlEventTouchUpInside];
    
    _headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, _tmpRect.size.width - 20, 20)];
    _headerLabel.backgroundColor = [UIColor clearColor];
    _headerLabel.font = [UIFont systemFontOfSize:10];
    _headerLabel.textColor = [UIColor blackColor];
    _headerLabel.text = _tmpSelection;
    [btn addSubview:_headerLabel];
    
    
    
    [btn addSubview:_imageView];
    
    return btn;
}


- (void)hideThirdView {
    [self showThirdView:NO];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.secondView) {
        NSDictionary *dic = _list[_index][indexPath.row];
        _thirdList = dic[@"third"];
        if (_thirdList.count == 0) {
            [self showThirdView:NO];
            
            NSLog(@"%@", dic[@"second"]);
            //改变按钮title
            if (_isAllowChangeTitle) {
                OptionButton *btn = (OptionButton *)[self viewWithTag:10 + _index];
                [btn setTitle:dic[@"second"] selected:NO];
            }
            [self hideView];
            if ([self.delegate respondsToSelector:@selector(resultOfSelet:)]) {
                [self.delegate resultOfSelet:dic[@"second"]];
            }
        }else {
            CGRect rect = [tableView rectForRowAtIndexPath:indexPath];
            NSLog(@"%f-%f-%f-%f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
            _tmpRect = [tableView convertRect:rect toView:self];
            NSLog(@"%f-%f-%f-%f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
            [self showThirdView:YES];
        }
        _tmpSelection = dic[@"second"];
    }else {
        
        NSLog(@"%@", _thirdList[indexPath.row]);
        
        if (_isAllowChangeTitle) {
            OptionButton *btn = (OptionButton *)[self viewWithTag:10 + _index];
            [btn setTitle:_thirdList[indexPath.row] selected:NO];
        }
        [self hideView];
        if ([self.delegate respondsToSelector:@selector(resultOfSelet:)]) {
            [self.delegate resultOfSelet:_thirdList[indexPath.row]];
        }
    }
    
}

- (void)showThirdView:(BOOL)isShow {
    
    if (_showMode == ThirdViewShowModeLeft) {
        if (isShow) {
            self.thirdView.frame = CGRectMake(K_WIDTH / 2, self.topHeaderHeight, K_WIDTH / 2, self.frame.size.height - self.topHeaderHeight);
            [self addSubview:self.thirdView];
            [self.thirdView reloadData];
            self.secondView.frame = CGRectMake(0, self.topHeaderHeight, K_WIDTH / 2, self.frame.size.height - self.topHeaderHeight);
        }else {
            [self.thirdView removeFromSuperview];
            self.secondView.frame = CGRectMake(0, self.topHeaderHeight, K_WIDTH, self.frame.size.height - self.topHeaderHeight);
        }
    }else if (_showMode == ThirdViewShowModeCenter) {
        if (isShow) {
            _imageView.image = [UIImage imageNamed:@"unfold"];
            self.thirdView.frame = _tmpRect;
            [self addSubview:self.thirdView];
            
            [self.thirdView reloadData];
            [UIView animateWithDuration:0.5 animations:^{
                self.thirdView.frame = self.secondView.frame;
            }];
            
        }else {
            _imageView.image = [UIImage imageNamed:@"fold"];
            [UIView animateWithDuration:0.5 animations:^{
                self.thirdView.frame = _tmpRect;
            } completion:^(BOOL finished) {
                if (finished) {
                    [self.thirdView removeFromSuperview];
                }
            }];
        }
    }
    
}






#pragma mark -- 一级分类视图相关
//初始化一级分类视图属性
- (void)firstClassificationNature {
    _topHeaderHeight = KWIDTH(30);
    _normalColor = [UIColor grayColor];
    _selectedColor = [UIColor blueColor];
    
}


- (void)setNormalColor:(UIColor *)normalColor {
    _normalColor = normalColor;
}

- (void)setSelectedColor:(UIColor *)selectedColor {
    _selectedColor = selectedColor;
}


#pragma mark -- cell的分割线宽度等于屏幕宽度
-(void)layoutSubviews {
    
    NSArray *arr = @[self.secondView, self.thirdView];
    for (UITableView *tableView in arr) {
        if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [tableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([tableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [tableView setLayoutMargins:UIEdgeInsetsZero];
        }
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat {
    NSLog(@"222");
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}



@end







