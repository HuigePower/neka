//
//  OptionsButton.m
//  Neka1.0
//
//  Created by ma c on 16/8/3.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "OptionsButton.h"
@interface OptionsButton ()
@property (nonatomic, strong)UIImageView *myImageView;
@property (nonatomic, strong)UILabel *myLabel;
@property (nonatomic, copy)NSString *title;
@property (nonatomic, copy)NSString *imageName;
@property (nonatomic, copy)NSString *selectedImageName;
@end
@implementation OptionsButton

- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title image:(NSString *)imageName selectedImage:(NSString *)selectedImageName {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        _title = title;
        _imageName = imageName;
        _selectedImageName = selectedImageName;
        self.userInteractionEnabled = YES;
        [self addSubview:self.myLabel];
        [self addSubview:self.myImageView];
    }
    return self;
}

- (UILabel *)myLabel {
    if (_myLabel == nil) {
        _myLabel = [[UILabel alloc] init];
        _myLabel.text = self.title;
        _myLabel.textColor = [UIColor blackColor];
        _myLabel.font = [UIFont systemFontOfSize:12];
        [_myLabel sizeToFit];
        _myLabel.center = CGPointMake(self.frame.size.width / 2 - 10, self.frame.size.height / 2);
    }
    return _myLabel;
}

- (UIImageView *)myImageView {
    if (_myImageView == nil) {
        float x = self.myLabel.frame.origin.x + self.myLabel.frame.size.width;
        float y = (self.frame.size.height - 15) / 2;
        _myImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x + 5, y, 15, 15)];
        _myImageView.image = [UIImage imageNamed:self.imageName];
    }
    return _myImageView;
}


- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if (selected) {
        self.myLabel.textColor = [UIColor blueColor];
        self.myImageView.image = [UIImage imageNamed:_selectedImageName];
    }else {
        self.myLabel.textColor = [UIColor blackColor];
        self.myImageView.image = [UIImage imageNamed:_imageName];
    }
}

- (void)setMyLabelWithStr:(NSString *)str {
    _myLabel.text = str;
    [_myLabel sizeToFit];
    _myLabel.center = CGPointMake(self.frame.size.width / 2 - 10, self.frame.size.height / 2);
    float x = self.myLabel.frame.origin.x + self.myLabel.frame.size.width;
    float y = (self.frame.size.height - 15) / 2;
    _myImageView.frame = CGRectMake(x + 5, y, 15, 15);
}

@end
