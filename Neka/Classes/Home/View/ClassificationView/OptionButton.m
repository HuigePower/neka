//
//  OptionButton.m
//  Classification
//
//  Created by ma c on 16/10/13.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "OptionButton.h"

@implementation OptionButton




- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _tLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _tLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_tLabel];
    }
    return self;
}

- (void)setTitle:(NSString *)title fontSize:(CGFloat)fontSize normalColor:(UIColor *)normalColor andSelectedColor:(UIColor *)seletedColor {
    
    self.normalColor = normalColor;
    self.selectedColor = seletedColor;
    self.fontSize = fontSize;
    [self setTitle:title selected:NO];
}

- (void)setTitle:(NSString *)title selected:(BOOL)isSelected{
    self.title = title;
    [self setSelected:isSelected];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    [self makeTitleLabelWithSelected:self.selected];
}

- (void)makeTitleLabelWithSelected:(BOOL)selected {
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    
    NSMutableAttributedString *strAtt = [[NSMutableAttributedString alloc] initWithString:self.title];
    
    if (selected) {
        [strAtt addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:self.fontSize], NSForegroundColorAttributeName:self.selectedColor} range:NSMakeRange(0, [self.title length])];
        
        attachment.image = [UIImage imageNamed:@"triangle_up"];
    }else {
        [strAtt addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:self.fontSize], NSForegroundColorAttributeName:self.normalColor} range:NSMakeRange(0, [self.title length])];
        attachment.image = [UIImage imageNamed:@"triangle_down"];
    }
    
    attachment.bounds = CGRectMake(0, -2, 15, 15);
    NSMutableAttributedString *imageAtt = (NSMutableAttributedString *)[NSMutableAttributedString attributedStringWithAttachment:attachment];
    [strAtt appendAttributedString:imageAtt];
    
    _tLabel.attributedText = strAtt;
    
}


@end
