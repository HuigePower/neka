//
//  OptionsButton.h
//  Neka1.0
//
//  Created by ma c on 16/8/3.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionsButton : UIButton

- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title image:(NSString *)imageName selectedImage:(NSString *)selectedImageName;
- (void)setMyLabelWithStr:(NSString *)str;
@end
