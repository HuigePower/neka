//
//  OptionButton.h
//  Classification
//
//  Created by ma c on 16/10/13.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionButton : UIButton

//标题
@property (nonatomic, strong)NSString *title;
//标题Label
@property (nonatomic, strong)UILabel *tLabel;
//标题颜色
@property (nonatomic, strong)UIColor *normalColor;
@property (nonatomic, strong)UIColor *selectedColor;
@property (nonatomic, assign)CGFloat fontSize;


- (void)setTitle:(NSString *)title fontSize:(CGFloat)fontSize normalColor:(UIColor *)normalColor andSelectedColor:(UIColor *)seletedColor;
- (void)setTitle:(NSString *)title selected:(BOOL)isSelected;
@end
