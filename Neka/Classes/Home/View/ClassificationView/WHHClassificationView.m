//
//  WHHClassificationView.m
//  分类选择视图
//
//  Created by ma c on 16/8/27.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "WHHClassificationView.h"
#define SCREEN_SIZE [[UIScreen mainScreen] bounds].size
@interface WHHClassificationView ()<UIScrollViewDelegate>
@property (nonatomic, strong)UIScrollView *scrollView;
@property (nonatomic, strong)UIPageControl *pageControl;

@end

@implementation WHHClassificationView

- (instancetype)initWithFrame:(CGRect)frame andTitles:(NSArray *)titles iconNames:(NSArray *)iconNames {
    if (self = [super initWithFrame:frame]) {
        self.titles = titles;
        self.iconNames = iconNames;
        float height = SCREEN_SIZE.width / 4;
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.scrollView.frame.size.width, height);
        [self addSubview:self.scrollView];
        [self addSubview:self.pageControl];
        
//        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 0.5)];
//        line.backgroundColor = [UIColor lightGrayColor];
//        [self addSubview:line];
    }
    return self;
}


- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        NSInteger line = self.titles.count / 4;
        if (self.titles.count % 4 != 0) {
            line++;
        }
        if (line > 2) {
            line = 2;
        }

        float width = SCREEN_SIZE.width / 4;
        float height = width;
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, height)];
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        
        NSInteger page =  self.titles.count / 8;
        if (self.titles.count % 8 > 0) {
            page++;
        }
        
        if (page == 1) {
            self.pageControl.hidden = YES;
        }
        
        
        _scrollView.contentSize = CGSizeMake(SCREEN_SIZE.width * page, height);

        for (int i = 0; i < page; i++) {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width * i, 0, SCREEN_SIZE.width, height)];
            for (int j = i * 8; j < 8 * (i + 1); j++) {
                if (j <  self.titles.count) {
                    NSString *title =  self.titles[j];
                    NSString *iconName = self.iconNames[j];
                    int s = j;
                    if (s >= 8) {
                        s = s - 8;
                    }
                    NSLog(@"%@",  self.titles[j]);
                    
                    UIButton *subview = [self createCellWithFrame:CGRectMake(width * (s % 4), height * (s / 4), width, height) andTitle:title image:iconName];
                    [view addSubview:subview];
                    
                    //分割线
//                    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(width, 0, 0.5, height)];
//                    line.backgroundColor = [UIColor lightGrayColor];
//                    [subview addSubview:line];
                    
                }
            }
            [_scrollView addSubview:view];
        }

    }
    return _scrollView;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int page = self.scrollView.contentOffset.x / SCREEN_SIZE.width;
    self.pageControl.currentPage = page;
}


- (UIButton *)createCellWithFrame:(CGRect)frame andTitle:(NSString *)title image:(NSString *)imageName {
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    float width = frame.size.width;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(width * 0.25, width * 0.1, width * 0.5, width * 0.5)];
    imageView.image = [UIImage imageNamed:imageName];
    [btn addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, width * 0.6, width, width * 0.2)];
    label.font = [UIFont systemFontOfSize:KWIDTH(10)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = title;
    [btn addSubview:label];
    [btn addTarget:self action:@selector(selectClassWithBtn:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}


- (void)selectClassWithBtn:(UIButton *)btn {
    if (_result) {
        self.result(btn.titleLabel.text);
    }
}



- (UIPageControl *)pageControl {
    if (_pageControl == nil) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 15, SCREEN_SIZE.width, 10)];
        NSInteger pages = self.titles.count / 12;
        if (self.titles.count % 12 != 0) {
            pages ++;
        }
        _pageControl.numberOfPages = pages;
        _pageControl.currentPage = 0;
        _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        _pageControl.currentPageIndicatorTintColor = [UIColor blueColor];
        [_pageControl addTarget:self action:@selector(pageChange:) forControlEvents:UIControlEventValueChanged];
    }
    return _pageControl;
}


- (void)pageChange:(UIPageControl *)pageControl {
    _scrollView.contentOffset = CGPointMake(SCREEN_SIZE.width * pageControl.currentPage, 0);
}

@end
