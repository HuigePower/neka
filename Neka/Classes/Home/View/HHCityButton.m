//
//  HHCityButton.m
//  Neka1.0
//
//  Created by ma c on 16/10/17.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "HHCityButton.h"
#define MAX_WIDTH self.frame.size.width - 15  //文字部分的最大宽度
#define FONT_SIZE 16.0
@implementation HHCityButton{
    UILabel *_label;
    UIImageView *_imageView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, MAX_WIDTH, 20)];
        [self addSubview:_label];
        _label.font = [UIFont systemFontOfSize:FONT_SIZE];
        _label.lineBreakMode = NSLineBreakByTruncatingTail;
        _label.textColor = [UIColor whiteColor];
        _label.textAlignment = NSTextAlignmentCenter;
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(MAX_WIDTH + 2, 11.5, 12, 7)];
        _imageView.image = [UIImage imageNamed:@"city_down_white"];
        [self addSubview:_imageView];
        
    }
    return self;
}


- (void)setTitle:(NSString *)title {
    CGRect rect = [title boundingRectWithSize:CGSizeMake(MAX_WIDTH, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:FONT_SIZE]} context:nil];
    NSLog(@"W:%f,H:%f", rect.size.width, rect.size.height);
    if (rect.size.width > MAX_WIDTH) {
        _label.frame = CGRectMake(0, 5, MAX_WIDTH, 20);
        _imageView.frame = CGRectMake(MAX_WIDTH + 2, 11.5, 12, 7);
    }else {
        _label.frame = CGRectMake((MAX_WIDTH - rect.size.width) / 2, 5, rect.size.width, 20);
        _imageView.frame = CGRectMake(_label.frame.origin.x + _label.frame.size.width + 2, 11.5, 12, 7);
    }
    _label.text = title;
    
    //文字
//    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10]}];
//    //图片
//    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
//    attach.image = [UIImage imageNamed:@"city_down"];
//    attach.bounds = CGRectMake(2, 0, 10, 6);
//    NSMutableAttributedString *attImage = (NSMutableAttributedString *)[NSAttributedString attributedStringWithAttachment:attach];
//    
//    [attStr appendAttributedString:attImage];
//    _label.text = nil;
//    _label.attributedText = attStr;
    
}




@end
