//
//  HGCouponCell.m
//  Neka
//
//  Created by ma c on 2017/7/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGCouponCell.h"

@implementation HGCouponCell
{
    UILabel *_conditionLabel;
    UILabel *_moneyLabel;
    UILabel *_expiryDateLabel;
    UIImageView *_imageView;
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 30, 20, 20)];
        _imageView.image = [UIImage imageNamed:@"tickGray"];
        [self addSubview:_imageView];
        
        _conditionLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 10, SCREEN_SIZE.width - 50, 20)];
        _conditionLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_conditionLabel];
        
        _moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 30, SCREEN_SIZE.width - 50, 20)];
        _moneyLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_moneyLabel];
        
        _expiryDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 50, SCREEN_SIZE.width - 50, 20)];
        _expiryDateLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_expiryDateLabel];
        
        
    }
    return self;
}


- (void)setCouponInfo:(HGCouponModel *)model withCouponID:(NSString *)couponID {
    if (model == nil) {
        [_conditionLabel setHidden:YES];
        [_expiryDateLabel setHidden:YES];
        _moneyLabel.text = @"不使用优惠券";
        
    }else {
        [_conditionLabel setHidden:NO];
        [_expiryDateLabel setHidden:NO];
        _conditionLabel.text = [NSString stringWithFormat:@"条件：满%@元可用", model.order_money];
        _moneyLabel.text = [NSString stringWithFormat:@"金额：¥%@", model.discount];
        _expiryDateLabel.text = [NSString stringWithFormat:@"%@", model.time];
        
        if ([couponID isEqualToString:model.had_id]) {
            NSLog(@"选中");
            _imageView.image = [UIImage imageNamed:@"tickGreen"];
        }else {
            NSLog(@"未选中");
            _imageView.image = [UIImage imageNamed:@"tickGray"];
        }
    }
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
