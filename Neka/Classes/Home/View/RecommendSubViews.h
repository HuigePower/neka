//
//  RecommendSubViews.h
//  Neka
//
//  Created by yu on 2017/12/6.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecommendSubViews : UIView
@property (nonatomic, strong)UINavigationController *nav;

//进入会员店铺
+ (RecommendSubViews *)shopEntranceViewWithFrame:(CGRect)frame Title:(NSString *)title andNav:(UINavigationController *)nav;
//推荐预约/专属预约/洗车预约
+ (RecommendSubViews *)recommendTitleViewWithFrame:(CGRect)frame andTitle:(NSString *)title andNav:(UINavigationController *)nav;
//推荐预约-主要预约
+ (RecommendSubViews *)recommendMainAppointmentViewWithNav:(UINavigationController *)nav;
//推荐预约-次要预约
+ (RecommendSubViews *)recommendSecondaryAppointmentViewWithNav:(UINavigationController *)nav;
//专属预约
+ (RecommendSubViews *)exclusiveAppointmentViewWithNav:(UINavigationController *)nav;
@end
