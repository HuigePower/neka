//
//  HGCouponCell.h
//  Neka
//
//  Created by ma c on 2017/7/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGCouponModel.h"

@interface HGCouponCell : UITableViewCell
- (void)setCouponInfo:(HGCouponModel *)model withCouponID:(NSString *)couponID;

@end
