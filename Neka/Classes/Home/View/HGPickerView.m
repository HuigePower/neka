//
//  HGPickerView.m
//  选择器
//
//  Created by ma c on 2017/2/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGPickerView.h"

@implementation HGPickerView
{
    PulldownButton *_button;
    PickType _pickerType;
    NSInteger _currentIndex;
    WarrantyAddViewController *_viewController;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _currentIndex = 0;
        
        
        UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 44)];
        topView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
        [self addSubview:topView];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(topView.frame.size.width - 60, 0, 50, 44)];
        [btn setTitle:@"确定" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [btn setTitleColor:mRGB(14, 138, 240, 1) forState:UIControlStateNormal];
        [btn setTitleColor:mRGB(3, 63, 108, 1) forState:UIControlStateHighlighted];
        [btn addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
        [topView addSubview:btn];
        
        
        self.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:self.pickerView];
        
        
        _dataSource = [[NSMutableArray array] init];
    }
    return self;
}

- (void)makeDataSource:(NSArray *)dataSource {
    [_dataSource removeAllObjects];
    [_dataSource addObjectsFromArray:dataSource];
}

- (void)showInSuperViewControl:(WarrantyAddViewController *)viewControl withDataSource:(NSArray *)dataSource andButton:(PulldownButton *)button pickerType:(PickType)pickerType {
    NSLog(@"%@", dataSource);
    _pickerType = pickerType;
    [self showInSuperViewControl:viewControl withDataSource:dataSource andButton:button];
}

- (void)showInSuperViewControl:(WarrantyAddViewController *)viewControl withDataSource:(NSArray *)dataSource andButton:(PulldownButton *)button {
    _button = button;
    [self showInSuperViewControl:viewControl withDataSource:dataSource];
    
}

- (void)showInSuperViewControl:(WarrantyAddViewController *)viewControl withDataSource:(NSArray *)dataSource {
    [self makeDataSource:dataSource];
    [self showInSuperViewControl:viewControl];
}

- (void)showInSuperViewControl:(WarrantyAddViewController *)viewControl {
    [_pickerView reloadComponent:0];
    
    if (_pickerType == PickTypeProvince) {//省份
        int index = 0;
        for (int i = 0; i < _dataSource.count; i++) {
            NSDictionary *dic = _dataSource[i];
            if ([_button.text isEqualToString:dic[@"area_name"]]) {
                index = i;
                break;
            }
        }
        _currentIndex = index;
        [_pickerView selectRow:index inComponent:0 animated:YES];
        _viewController = viewControl;
        [_viewController.view addSubview:self];
        [UIView animateWithDuration:0.5 animations:^{
            [viewControl whenShowPickerView];
            self.frame = CGRectMake(0, SCREEN_SIZE.height - 244, SCREEN_SIZE.width, 244);
        }];
        
    }else if (_pickerType == PickTypeCity) {//城市
        int index = 0;
        for (int i = 0; i < _dataSource.count; i++) {
            NSDictionary *dic = _dataSource[i];
            if ([_button.text isEqualToString:dic[@"area_name"]]) {
                index = i;
                break;
            }
        }
        _currentIndex = index;
        [_pickerView selectRow:index inComponent:0 animated:YES];
        _viewController = viewControl;
        [_viewController.view addSubview:self];
        [UIView animateWithDuration:0.5 animations:^{
            [viewControl whenShowPickerView];
            self.frame = CGRectMake(0, SCREEN_SIZE.height - 244, SCREEN_SIZE.width, 244);
        }];
        
    }else if (_pickerType == PickTypeWarrantyID) {//质保单号
        int index = 0;
        for (int i = 0; i < _dataSource.count; i++) {
            NSDictionary *dic = _dataSource[i];
            if ([_button.text isEqualToString:dic[@"volume_number"]]) {
                index = i;
                break;
            }
        }
        _currentIndex = index;
        [_pickerView selectRow:index inComponent:0 animated:YES];
        _viewController = viewControl;
        [_viewController.view addSubview:self];
        [UIView animateWithDuration:0.5 animations:^{
            [viewControl whenShowPickerView];
            self.frame = CGRectMake(0, SCREEN_SIZE.height - 244, SCREEN_SIZE.width, 244);
        }];
        
    }else if (_pickerType == PickTypeOrderID) {//订单号
        int index = 0;
        for (int i = 0; i < _dataSource.count; i++) {
            NSString *orderId = _dataSource[i];
            if ([_button.text isEqualToString:orderId]) {
                index = i;
                break;
            }
        }
        _currentIndex = index;
        [_pickerView selectRow:index inComponent:0 animated:YES];
        _viewController = viewControl;
        [_viewController.view addSubview:self];
        [UIView animateWithDuration:0.5 animations:^{
            [viewControl whenShowPickerView];
            self.frame = CGRectMake(0, SCREEN_SIZE.height - 244, SCREEN_SIZE.width, 244);
        }];
        
    }else if (_pickerType == PickTypeCarName) {//汽车品牌
        int index = 0;
        for (int i = 0; i < _dataSource.count; i++) {
            NSDictionary *dic = _dataSource[i];
            if ([_button.text isEqualToString:dic[@"car_name"]]) {
                index = i;
                break;
            }
        }
        _currentIndex = index;
        [_pickerView selectRow:index inComponent:0 animated:YES];
        _viewController = viewControl;
        [_viewController.view addSubview:self];
        [UIView animateWithDuration:0.5 animations:^{
            [viewControl whenShowPickerView];
            self.frame = CGRectMake(0, SCREEN_SIZE.height - 244, SCREEN_SIZE.width, 244);
        }];
        
    }else if (_pickerType == PickTypeCarNameSeries) {//车系
        int index = 0;
        for (int i = 0; i < _dataSource.count; i++) {
            NSDictionary *dic = _dataSource[i];
            if ([_button.text isEqualToString:dic[@"car_name"]]) {
                index = i;
                break;
            }
        }
        _currentIndex = index;
        [_pickerView selectRow:index inComponent:0 animated:YES];
        _viewController = viewControl;
        [_viewController.view addSubview:self];
        [UIView animateWithDuration:0.5 animations:^{
            [viewControl whenShowPickerView];
            self.frame = CGRectMake(0, SCREEN_SIZE.height - 244, SCREEN_SIZE.width, 244);
        }];
        
    }else if (_pickerType == PickTypeCarNumberName) {//车牌号前缀
        int index = 0;
        for (int i = 0; i < _dataSource.count; i++) {
            NSDictionary *dic = _dataSource[i];
            if ([_button.text isEqualToString:dic[@"cat_num_name"]]) {
                index = i;
                break;
            }
        }
        _currentIndex = index;
        [_pickerView selectRow:index inComponent:0 animated:YES];
        _viewController = viewControl;
        [_viewController.view addSubview:self];
        [UIView animateWithDuration:0.5 animations:^{
            [viewControl whenShowPickerView];
            self.frame = CGRectMake(0, SCREEN_SIZE.height - 244, SCREEN_SIZE.width, 244);
        }];
        
    }else {
        NSInteger index = [_dataSource indexOfObject:_button.text];
        _currentIndex = index;
        [_pickerView selectRow:index inComponent:0 animated:YES];
        _viewController = viewControl;
        [_viewController.view addSubview:self];
        
        [UIView animateWithDuration:0.5 animations:^{
            [viewControl whenShowPickerView];
            self.frame = CGRectMake(0, SCREEN_SIZE.height - 244, SCREEN_SIZE.width, 244);
        }];
    }
    
    
}



- (void)hide {
    if (_pickerType == PickTypeProvince) {//省份
        _button.text = _dataSource[_currentIndex][@"area_name"];
        if (_result) {
            _result(_dataSource[_currentIndex], _pickerType);
        }
    }else if (_pickerType == PickTypeCity) {//城市
        _button.text = _dataSource[_currentIndex][@"area_name"];
        if (_result) {
            _result(_dataSource[_currentIndex], _pickerType);
        }
    }else if (_pickerType == PickTypeWarrantyID) {//城市
        _button.text = _dataSource[_currentIndex][@"volume_number"];
        if (_result) {
            _result(_dataSource[_currentIndex], _pickerType);
        }
    }else if (_pickerType == PickTypeOrderID) {//订单信息
        _button.text = _dataSource[_currentIndex];
        if (_result) {
            _result(_dataSource[_currentIndex], _pickerType);
        }
    }else if (_pickerType == PickTypeCarName) {//汽车品牌
        _button.text = _dataSource[_currentIndex][@"car_name"];
        if (_result) {
            _result(_dataSource[_currentIndex], _pickerType);
        }
    }else if (_pickerType == PickTypeCarNameSeries) {//车系
        _button.text = _dataSource[_currentIndex][@"car_name"];
        if (_result) {
            _result(_dataSource[_currentIndex], _pickerType);
        }
    }else if (_pickerType == PickTypeCarNumberName) {//车牌号前缀
        _button.text = _dataSource[_currentIndex][@"cat_num_name"];
        if (_result) {
            _result(_dataSource[_currentIndex][@"cat_num_name"], _pickerType);
        }
    }
    
    
    [UIView animateWithDuration:0.5 animations:^{
        [_viewController whenHidePickerView];
        self.frame = CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, 244);
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        _currentIndex = 0;
    }];
    
}


#pragma mark -- PickerView


- (UIPickerView *)pickerView {
    if (_pickerView == nil) {
        _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, self.frame.size.width, self.frame.size.height - 44)];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
    }
    return _pickerView;
}

#pragma mark -- delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _dataSource.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (_pickerType == PickTypeProvince) {
        return _dataSource[row][@"area_name"];
    }else if (_pickerType == PickTypeCity) {
        return _dataSource[row][@"area_name"];
    }else if (_pickerType == PickTypeWarrantyID) {
        return _dataSource[row][@"volume_number"];
    }else if (_pickerType == PickTypeWarrantyID) {
        return _dataSource[row];
    }else if (_pickerType == PickTypeCarName) {//汽车品牌
        return _dataSource[row][@"car_name"];
    }else if (_pickerType == PickTypeCarNameSeries) {//车系
        return _dataSource[row][@"car_name"];
    }else if (_pickerType == PickTypeCarNumberName) {//车牌号前缀
        return _dataSource[row][@"cat_num_name"];
    }else {
        return _dataSource[row];
    }
    
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSLog(@"row=%ld, component=%ld", row, component);
    _currentIndex = row;
}




@end
