//
//  UIButton+Fit.h
//  Neka1.0
//
//  Created by ma c on 16/8/31.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Fit)
- (void)sizeFit;
@end
