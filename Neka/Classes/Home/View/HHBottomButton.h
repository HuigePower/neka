//
//  HHBottomButton.h
//  Neka1.0
//
//  Created by ma c on 16/10/17.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, DistributionMode) {
    DistributionModeTitleImage = 0, //标题在上/图片在下
    DistributionModeImageTitleTitle  //图片在上/主标题在中间/副标题在下
};

@interface HHBottomButton : UIButton

@property (nonatomic, assign)DistributionMode mode;

- (instancetype)initWithFrame:(CGRect)frame withDistributionMode:(DistributionMode)mode;
- (void)setImage:(NSString *)imageName majorTitle:(NSString *)majorTitle secondaryTitle:(NSString *)secondaryTitle;

@end
