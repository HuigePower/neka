//
//  WHHTabbarView.h
//  轮播图
//
//  Created by ma c on 16/8/27.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ResultBlock)(NSString *catID);
@interface WHHTabbarView : UIView

@property (nonatomic, copy)ResultBlock result;

@property (nonatomic, strong)NSArray *catList;

- (instancetype)initWithFrame:(CGRect)frame;

- (void)setSelectedIndex:(NSInteger)index;
@end
