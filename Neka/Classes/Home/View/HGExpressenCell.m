//
//  HGExpressenCell.m
//  Neka
//
//  Created by ma c on 2017/8/9.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGExpressenCell.h"

@implementation HGExpressenCell
{
    UILabel *_titleTextView;
    UIImageView *_singleImageView;
    UIView *_multipleImageView;
    UILabel *_visitNumberLabel;
    UILabel *_dateLabel;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //标题
        
        _titleTextView = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(110), KWIDTH(40))];
        _titleTextView.font = [UIFont systemFontOfSize:14];
        _titleTextView.contentMode = UIViewContentModeTopLeft;
        [self addSubview:_titleTextView];
        
        //单张图片
        _singleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(90), KWIDTH(5), KWIDTH(80), KWIDTH(60))];
        [self addSubview:_singleImageView];
        //多张图片
        _multipleImageView = [[UIView alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(30), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(60))];
        [self addSubview:_multipleImageView];
        //浏览量
        _visitNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(180), KWIDTH(45), KWIDTH(80), KWIDTH(20))];
        _visitNumberLabel.font = [UIFont systemFontOfSize:12];
        _visitNumberLabel.textAlignment = NSTextAlignmentRight;
        _visitNumberLabel.textColor = [UIColor grayColor];
        [self addSubview:_visitNumberLabel];
        //时间
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(45), KWIDTH(120), KWIDTH(20))];
        _dateLabel.font = [UIFont systemFontOfSize:12];
        _dateLabel.textColor = [UIColor grayColor];
        [self addSubview:_dateLabel];
    }
    return self;
}

- (void)makeSubviewsWithModel:(HGExpressenModel *)model {
    _titleTextView.text = model.title;
    if (model.content_img.count == 0) {
        
        _titleTextView.frame = CGRectMake(KWIDTH(10), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(40));
        [_titleTextView topAlignment];
        
        [_singleImageView setHidden:YES];
        [_multipleImageView setHidden:YES];
        
        _dateLabel.frame = CGRectMake(KWIDTH(10), KWIDTH(40), KWIDTH(120), KWIDTH(20));
        [self setDate:model.add_time];
        _visitNumberLabel.frame = CGRectMake(SCREEN_SIZE.width - KWIDTH(90), KWIDTH(40), KWIDTH(80), KWIDTH(20));
        
    }else if (model.content_img.count == 1) {
        
        _titleTextView.frame = CGRectMake(KWIDTH(10), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(105), KWIDTH(40));
        [_titleTextView topAlignment];
        //隐藏多图片
        [_multipleImageView setHidden:YES];
        [self supviewRemoveSubviews:_multipleImageView];
        //显示单图片
        [_singleImageView setHidden:NO];
        NSString *imgUrl = [NSString stringWithFormat:@"https://www.nekahome.com%@", model.content_img[0]];
        [_singleImageView setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:nil];
        [self addSubview:_singleImageView];
        
        _dateLabel.frame = CGRectMake(KWIDTH(10), KWIDTH(45), KWIDTH(120), KWIDTH(20));
        [self setDate:model.add_time];
        _visitNumberLabel.frame = CGRectMake(SCREEN_SIZE.width - KWIDTH(180), KWIDTH(45), KWIDTH(80), KWIDTH(20));
        [self setvisitNumber:model.custom_visits];
        
    }else {
        
        _titleTextView.frame = CGRectMake(KWIDTH(10), KWIDTH(5), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(20));
        [_titleTextView topAlignment];
        
        //隐藏单图片
        [_singleImageView setHidden:YES];
        //显示多图片
        [_multipleImageView setHidden:NO];
        [self supviewRemoveSubviews:_multipleImageView];
        CGFloat aWidth = _multipleImageView.frame.size.width;
        CGFloat bWidth = (aWidth - KWIDTH(20)) / 3;
        NSInteger m = 0;
        if (model.content_img.count > 3) {
            m = 3;
        }else {
            m = model.content_img.count;
        }
        for (int i = 0; i < m; i++) {
            NSString *imgUrl = [NSString stringWithFormat:@"https://www.nekahome.com%@", model.content_img[i]];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((bWidth + KWIDTH(10)) * i, 0, bWidth, KWIDTH(60))];
            [imageView setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:nil];
            [_multipleImageView addSubview:imageView];
        }
        
        _dateLabel.frame = CGRectMake(KWIDTH(10), KWIDTH(90), KWIDTH(120), KWIDTH(20));
        [self setDate:model.add_time];
        _visitNumberLabel.frame = CGRectMake(SCREEN_SIZE.width - KWIDTH(90), KWIDTH(90), KWIDTH(80), KWIDTH(20));
        [self setvisitNumber:model.custom_visits];
        
    }
}

- (void)setvisitNumber:(NSString *)number {
    
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:@"eye"];
    attachment.bounds = CGRectMake(KWIDTH(0), KWIDTH(0), KWIDTH(11.5), KWIDTH(7.5));
    NSAttributedString *attTemp = [NSAttributedString attributedStringWithAttachment:attachment];
    
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", number]];
    [att insertAttributedString:attTemp atIndex:0];
    _visitNumberLabel.attributedText = att;
    
}

- (void)setDate:(NSString *)timestamp {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy年MM月dd日";
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timestamp floatValue]];
    _dateLabel.text = [formatter stringFromDate:date];;
}



- (void)supviewRemoveSubviews:(UIView *)supview {
    NSArray *subviews = supview.subviews;
    for (UIView *subview in subviews) {
        [subview removeFromSuperview];
    }
}








@end
