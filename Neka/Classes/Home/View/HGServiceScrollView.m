//
//  HGServiceScrollView.m
//  Neka
//
//  Created by ma c on 2017/8/8.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGServiceScrollView.h"
#import "HGOrderWebViewController.h"
#import "WZCheckViewController.h"
@implementation HGServiceScrollView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
//        NSArray *arr = @[@"签约洗车", @"合作4S", @"服务招标", @"车辆保险", @"车主金融", @"原厂升级", @"玻璃贴膜", @"隐形车衣", @"改色贴膜", @"常用配件", @"违章查询"];
//        NSArray *arr = @[@"签约洗车", @"原厂升级", @"玻璃贴膜", @"隐形车衣", @"合作4S", @"车辆保险", @"改色贴膜", @"常用配件", @"违章查询"];
//        NSArray *imageNameArr = @[@"qyxc", @"hz4s", @"fwzb", @"clbx", @"czjr", @"ycsj", @"bltm", @"yxcy", @"gstm", @"cypj", @"wzcx"];
//        NSArray *imageNameArr = @[@"qyxc", @"ycsj", @"bltm", @"yxcy", @"hz4s", @"clbx", @"gstm", @"cypj", @"wzcx"];
        
        NSArray *arr = @[@"签约洗车", @"隐形车衣", @"改色贴膜", @"玻璃贴膜", @"原厂升级"];
        NSArray *imageNameArr = @[@"qyxc", @"yxcy", @"gstm", @"bltm", @"ycsj"];
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        NSInteger page = ceilf(arr.count * 0.2);
        NSLog(@"页数%ld", page);
        _scrollView.contentSize = CGSizeMake(SCREEN_SIZE.width * page, KWIDTH(75));
        _scrollView.userInteractionEnabled = YES;
        
        
        for (int i = 0; i < arr.count; i++) {
            UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 5 * i, 0, SCREEN_SIZE.width / 5, KWIDTH_6S(80))];
            
            CGFloat imgW = KWIDTH_6S(42.5);
            CGFloat imgH = KWIDTH_6S(42.5);
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imgW, imgH)];
            imageView.image = [UIImage imageNamed:imageNameArr[i]];
            [btn addSubview:imageView];
            imageView.center = CGPointMake(btn.frame.size.width / 2, KWIDTH_6S(31.25));
            
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, btn.bounds.size.height - KWIDTH_6S(25), btn.bounds.size.width, KWIDTH_6S(20))];
            label.text = arr[i];
            label.textAlignment = NSTextAlignmentCenter;
            label.font = [UIFont systemFontOfSize:12];
            label.textColor = mRGB(102, 102, 102, 1);
            [btn addSubview:label];
            [_scrollView addSubview:btn];
            
            btn.tag = 10 + i;
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
        [self addSubview:_scrollView];
        
//        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, KWIDTH(65), SCREEN_SIZE.width, KWIDTH(5))];
//        _pageControl.numberOfPages = page;
//        _pageControl.currentPage = 0;
//        _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
//        _pageControl.currentPageIndicatorTintColor = [UIColor blueColor];
//        _pageControl.enabled = NO;
//        [self addSubview:_pageControl];
    }
    return self;
}

- (void)btnClick:(UIButton *)btn {
    NSInteger index = btn.tag - 10;
    //签约洗车、隐形车衣、改色贴膜、玻璃贴膜、原厂升级
    NSArray *urls = @[
                      @"https://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=two_category&cat_id=89",
                      @"http://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=toStore&cat_id=91&appoint_type=0",
                      @"http://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=category&cat_id=8",
                      @"http://www.nekahome.com/wap.php?g=Wap&c=Appoint&a=category&cat_id=6",
                      @"http://www.nekahome.com/wap.php?g=Wap&c=User_bindcat&a=index&small_category_id=1"];
    if (index == 8) {
        WZCheckViewController *vc = [[WZCheckViewController alloc] init];
        [self.nav pushViewController:vc animated:YES];
        
    }else {
        NSString *url = urls[index];
        if (url.length > 0) {
            HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
            web.urlString = url;
            [self.nav pushViewController:web animated:YES];
        }else {
            [MBProgressHUD showError:@"功能待完善..." toView:[UIApplication sharedApplication].keyWindow];
        }
    }
    
    
}


//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    NSInteger page = scrollView.contentOffset.x / SCREEN_SIZE.width;
//    _pageControl.currentPage = page;
//}






@end
