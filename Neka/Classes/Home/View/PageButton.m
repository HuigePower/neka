//
//  PageButton.m
//  Neka1.0
//
//  Created by ma c on 2017/2/15.
//  Copyright © 2017年 ma c. All rights reserved.
//  翻页按钮

#import "PageButton.h"

@implementation PageButton

- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title {
    if (self == [super initWithFrame:frame]) {
        [self setTitle:title forState:UIControlStateNormal];
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 5;
        self.layer.borderWidth = 0.5;
        [self setAvailabel:YES];
    }
    return self;
}

- (void)setAvailabel:(BOOL)availabel {
    if (availabel) {
        [self stateOfAvailabel];
    }else {
        [self stateOfUnavailable];
    }
}



- (void)stateOfAvailabel {
    [self setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    self.layer.borderColor = [UIColor blackColor].CGColor;
}


- (void)stateOfUnavailable {
    
    [self setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
}







@end
