
//
//  CustomTableViewCell.m
//  Neka1.0
//
//  Created by ma c on 16/9/3.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell
{
    UILabel *_line;
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)addCustomSubview:(UIView *)view {
    if (view == nil) {
        NSLog(@"error:%s, 子视图nil", __func__);
        return;
    }
    [self addSubview:view];
    if (self.subviewArray == nil) {
        self.subviewArray = [[NSMutableArray alloc] init];
    }
    
    [self.subviewArray addObject:view];
}



- (void)deleteSubviews {
    for (UIView *view in self.subviewArray) {
        [view removeFromSuperview];
    }
    [_line removeFromSuperview];
    [self.subviewArray removeAllObjects];
}

- (void)addCustomSeparatorWithHeight:(float)height {
    [self addCustomSeparatorWithHeight:height leftSpace:0 rightSpace:0];
}

- (void)addCustomSeparatorWithHeight:(float)height leftSpace:(CGFloat)leftSapce rightSpace:(CGFloat)rightSpace {
    _line = [[UILabel alloc] initWithFrame:CGRectMake(leftSapce, height - 0.5, SCREEN_SIZE.width - leftSapce - rightSpace, 0.5)];
    _line.backgroundColor = [UIColor lightGrayColor];
    [self addCustomSubview:_line];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
