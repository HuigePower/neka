//
//  MapViewController.m
//  Neka1.0
//
//  Created by ma c on 16/7/28.
//  Copyright © 2016年 ma c. All rights reserved.
//  附近店铺

#import "MapViewController.h"
#import "CustomTableViewCell.h"
#import "HGOrderWebViewController.h"
extern CLLocationCoordinate2D coordinate;
@interface MapViewController ()<BMKMapViewDelegate, BMKLocationServiceDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong)BMKMapView *mapView;
@property (nonatomic, strong)NSMutableArray *annoArray;
@property (nonatomic, strong)NSArray *shopList;

@property (nonatomic, strong)UIView *backgroundView;
@property (nonatomic, strong)UITableView *tableView;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"附近店铺";
    
    _mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 0 , SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
    _mapView.delegate = self;
    _mapView.zoomLevel = 18;
     _mapView.centerCoordinate = coordinate;
    [_mapView setMapType:BMKMapTypeStandard];
    [self.view addSubview:_mapView];
    
    
    
    
    UIButton *listBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(40), SCREEN_SIZE.height - KWIDTH(50), KWIDTH(30), KWIDTH(30))];
    [listBtn setImage:[UIImage imageNamed:@"listBtn"] forState:UIControlStateNormal];
    [listBtn addTarget:self action:@selector(showShopList:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:listBtn];
    
    _annoArray = [NSMutableArray new];
    [self initDataSource];

    
}
#pragma mark -- BMKMapViewDelegate

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}



- (void)mapViewDidFinishLoading:(BMKMapView *)mapView {
    NSLog(@"地图加载完成");
    NSLog(@"%f--%f", _mapView.centerCoordinate.latitude, _mapView.centerCoordinate.longitude);


}

- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    NSLog(@"%f--%f", _mapView.centerCoordinate.latitude, _mapView.centerCoordinate.longitude);
    [_mapView removeAnnotations:_mapView.annotations];
    
    [self initDataSource];
    
}

- (void)mapView:(BMKMapView *)mapView annotationViewForBubble:(BMKAnnotationView *)view {
    NSLog(@"%@", view.annotation.subtitle);
    NSMutableString *subtitle = [[NSMutableString alloc] initWithString:view.annotation.subtitle];
    NSString *phoneNum = [subtitle stringByReplacingOccurrencesOfString:@"联系电话:" withString:@"tel:"];
    [self callTelWithPhoneNum:phoneNum];
}


#pragma mark -- 请求数据
- (void)initDataSource {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"lng": [NSString stringWithFormat:@"%f", _mapView.centerCoordinate.longitude], @"lat": [NSString stringWithFormat:@"%f", _mapView.centerCoordinate.latitude]};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Merchant&a=around", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            _shopList = responseObject[@"result"];
            if (_shopList.count > 0) {
                [self addAnnotationViewsWithArray:_shopList];
            }
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
    
}


- (void)addAnnotationViewsWithArray:(NSArray *)array {
    for (NSDictionary *dic in array) {
        CGFloat lat = [dic[@"lat"] floatValue];
        CGFloat lng = [dic[@"long"] floatValue];
        
        BMKPointAnnotation *pointAn = [[BMKPointAnnotation alloc] init];
        pointAn.coordinate = CLLocationCoordinate2DMake(lat, lng);
        pointAn.title = [NSString stringWithFormat:@"店铺:%@", dic[@"sname"]];
        pointAn.subtitle =[NSString stringWithFormat:@"联系电话:%@", dic[@"sphone"]];
        [_mapView addAnnotation:pointAn];
        [_annoArray addObject:pointAn];
        
    }
}


- (void)callTelWithPhoneNum:(NSString *)phoneNum {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNum]];

}

- (UIView *)backgroundView {
    if (_backgroundView == nil) {
        _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
        _backgroundView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.5];
        [_backgroundView addSubview:self.tableView];
    }
    
    return _backgroundView;
}


- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(30, 0, SCREEN_SIZE.width - 60, 0) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[CustomTableViewCell class] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _shopList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell) {
        [cell deleteSubviews];
    }
    NSDictionary *dic = _shopList[indexPath.row];
    
    UILabel *shopNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width - 20, 20)];
    shopNameLabel.font = [UIFont systemFontOfSize:12];
    shopNameLabel.text = dic[@"name"];
    [cell addCustomSubview:shopNameLabel];
    
    UILabel *phoneNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, tableView.frame.size.width - 20, 20)];
    phoneNumberLabel.font = [UIFont systemFontOfSize:12];
    phoneNumberLabel.text = [NSString stringWithFormat:@"电话：%@", dic[@"phone"]];
    [cell addCustomSubview:phoneNumberLabel];
    
    UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, tableView.frame.size.width - 20, 20)];
    addressLabel.font = [UIFont systemFontOfSize:12];
    addressLabel.text = [NSString stringWithFormat:@"地址：%@", dic[@"adress"]];
    [cell addCustomSubview:addressLabel];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic = _shopList[indexPath.row];
    HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
    web.urlString = dic[@"url"];
    [self.navigationController pushViewController:web animated:YES];
}


- (void)showShopList:(UIButton *)btn {
    if (_shopList.count == 0) {
        [MBProgressHUD showError:@"屏幕地图中没有店铺"];
        return;
    }
        
    NSInteger rowNumber = _shopList.count > 6 ? 6 : _shopList.count;
    NSLog(@"%ld", (long)rowNumber),
    self.tableView.frame = CGRectMake(30, (SCREEN_SIZE.height - 64 - 60 * rowNumber) / 2, SCREEN_SIZE.width - 60, 60 * rowNumber);
    [self.view addSubview:self.backgroundView];
    [_tableView reloadData];
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.backgroundView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
