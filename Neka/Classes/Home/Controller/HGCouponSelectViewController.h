//
//  HGCouponSelectViewController.h
//  Neka
//
//  Created by ma c on 2017/7/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGCouponCell.h"
#import "HGCouponModel.h"

typedef void(^CouponSelectResultBlock)(HGCouponModel *couponModel);
@interface HGCouponSelectViewController : UIViewController
@property (nonatomic, copy)NSString *order_id;
@property (nonatomic, copy)NSString *order_type;
@property (nonatomic, copy)NSString *coupon_type;
@property (nonatomic, copy)NSString *coupon_id;

@property (nonatomic, copy)CouponSelectResultBlock couponBlock;

@end
