//  302219151272
//  MainViewController.m
//  Neka1.0
//
//  Created by ma c on 16/7/26.
//  Copyright © 2016年 ma c. All rights reserved.
//  首页

#import "HomeViewController.h"
#import "MapViewController.h"
#import "ScanViewController.h"
#import "CityPickerViewController.h"
#import "WHHClassificationView.h"
#import "PlatformExpressenView.h"
#import "ExpressenViewController.h"
#import "WHHTopView.h"
#import "HHBottomButton.h"
#import "HGCarouselView.h"
#import "WarrantyViewController.h"
#import "AppointWashTableViewCell.h"
#import "LoginViewController.h"
#import "HGAdverModel.h"
#import "HGNewsModel.h"
#import "HGWebViewRootController.h"
#import "HGOrderWebViewController.h"
#import "HGConfirmAppointOrderViewController.h"
#import "MJRefreshBackStateFooter.h"
#import "HGServiceScrollView.h"
#import <JPUSHService.h>
#import "RecommendSubViews.h"
@interface HomeViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)WHHTopView *topView;

//轮播图
@property (nonatomic, strong)HGCarouselView *carouselView1;
@property (nonatomic, strong)NSMutableArray *images1;
@property (nonatomic, strong)NSMutableArray *adverModelArray1;
//轮播图
@property (nonatomic, strong)HGCarouselView *carouselView2;
@property (nonatomic, strong)NSMutableArray *images2;
@property (nonatomic, strong)NSMutableArray *adverModelArray2;
//分类选择视图
//@property (nonatomic, strong)UIView *classificationView;
//平台快报
@property (nonatomic, strong)PlatformExpressenView *expressenView;
@property (nonatomic, strong)NSMutableArray *newsList;

@property (nonatomic, strong)UIPageControl *pageCtrl;

@property (nonatomic, strong)UIButton *selectCity;

//分类服务
@property (nonatomic, strong)HGServiceScrollView *serviceView;
//分类发布/优惠券/礼品
@property (nonatomic, strong)UIView *threeBtnView;
//附近洗车
@property (nonatomic, strong)NSMutableArray *washCarData;
//下拉刷新
@property (nonatomic, strong)MJRefreshNormalHeader *header;
//上拉加载
@property (nonatomic, strong)MJRefreshBackNormalFooter *footer;

//分页-总页数
@property (nonatomic, assign)NSInteger totalPages;
//分页-当前页数
@property (nonatomic, assign)NSInteger currentPage;

//刷新
@property (nonatomic, assign)BOOL isRefreshing;
//加载更多
@property (nonatomic, assign)BOOL isLoadingMore;

@property (nonatomic, strong)UIView *statusBarBgView;



@property (nonatomic, strong)NSMutableArray *secondSectionArr;
@property (nonatomic, strong)NSMutableArray *thirdSecotionArr;
@property (nonatomic, strong)NSMutableArray *fourthSecotionArr;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [JPUSHService getAllTags:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
        NSLog(@"标签：%@", iTags);
    } seq:0];
    
    self.title = @"首页";
    
    _secondSectionArr = [NSMutableArray new];
    
    //耐卡自营
    RecommendSubViews *sectionHeaderView2 = [RecommendSubViews recommendTitleViewWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(30)) andTitle:@"耐卡自营" andNav:nil];
    [_secondSectionArr addObject:sectionHeaderView2];
    RecommendSubViews *mainRecommendView = [RecommendSubViews recommendMainAppointmentViewWithNav:self.navigationController];
    [_secondSectionArr addObject:mainRecommendView];
    
    //推荐预约
    _thirdSecotionArr = [NSMutableArray new];
    RecommendSubViews *sectionHeaderView3 = [RecommendSubViews recommendTitleViewWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(30)) andTitle:@"推荐预约" andNav:nil];
    [_thirdSecotionArr addObject:sectionHeaderView3];
    RecommendSubViews *secondaryRecommendView = [RecommendSubViews recommendSecondaryAppointmentViewWithNav:self.navigationController];
    [_thirdSecotionArr addObject:secondaryRecommendView];
    
    
    _fourthSecotionArr = [NSMutableArray new];
    //专属预约
    RecommendSubViews *sectionHeaderView4 = [RecommendSubViews recommendTitleViewWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(30)) andTitle:@"专属预约" andNav:nil];
    [_fourthSecotionArr addObject:sectionHeaderView4];
    RecommendSubViews *exclusiveAppointmentView = [RecommendSubViews exclusiveAppointmentViewWithNav:self.navigationController];
    [_fourthSecotionArr addObject:exclusiveAppointmentView];
    
    
    [self isOpenGPRS];
    [self initDataSource];
    
    self.view.backgroundColor = [UIColor whiteColor];

    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, -20, SCREEN_SIZE.width, SCREEN_SIZE.height - 49 + 20) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableView registerClass:[AppointWashTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_tableView];

    
    _header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refresh:)];
    _tableView.mj_header = _header;
    
    _footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerAction:)];
    _tableView.mj_footer = _footer;
    
    _statusBarBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 20)];
    _statusBarBgView.alpha = 0;
    _statusBarBgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_statusBarBgView];
    
    [JPUSHService getAllTags:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
        NSLog(@"%@", iTags);
    } seq:0];
}

#pragma mark -- tableview代理
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    AppointWashTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell != nil) {
        NSArray *arr = cell.subviews;
        for (UIView *subView in arr) {
            [subView removeFromSuperview];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.userInteractionEnabled = YES;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [cell addSubview:self.carouselView1];
            [cell addSubview:self.topView];
        }else if (indexPath.row == 1) {
            [cell addSubview:self.serviceView];
            
        }else if (indexPath.row == 2) {
            [cell addSubview:self.expressenView];
        }
    }else if (indexPath.section == 1) {
        UIView *view = _secondSectionArr[indexPath.row];
        [cell addSubview:view];
    }else if (indexPath.section == 2) {
        UIView *view = _thirdSecotionArr[indexPath.row];
        [cell addSubview:view];
    }else  if (indexPath.section == 3) {
        UIView *view = _fourthSecotionArr[indexPath.row];
        [cell addSubview:view];
    }else  if (indexPath.section == 4) {
        [cell addSubview:self.carouselView2];
    }else {
        NSDictionary *dic = _washCarData[indexPath.row];
        [cell setData:dic];
    }
    
    return cell;
}

- (void)refresh:(MJRefreshNormalHeader *)header {
    _currentPage = 1;
    [_footer endRefreshing];
    _isRefreshing = YES;
    [_washCarData removeAllObjects];
    [self requestCarData];
}

- (void)footerAction:(MJRefreshBackNormalFooter *)footer {
    
    if (_currentPage < _totalPages) {
        [_footer setState:MJRefreshStateIdle];
        _isLoadingMore = YES;
        _currentPage ++;
        [self requestCarData];
    }else {
        [_footer setState:MJRefreshStateNoMoreData];
        
    }

}

- (void)endMj {
    if (_isRefreshing){
        [_header endRefreshing];
    }else if (_isLoadingMore) {
        [_footer endRefreshing];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tabBarController.tabBar setHidden:NO];
    [self.navigationController.navigationBar setHidden:YES];
    [self.carouselView1 restart];
    

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
    [self.carouselView1 suspend];
    
}



#pragma mark -- 数据请求
- (void)initDataSource {
    _currentPage = 1;
    _washCarData = [NSMutableArray new];
    _isRefreshing = NO;
    _isLoadingMore = NO;
    [self requestDefaultData];
    [self requestCarData];
}

- (void)requestDefaultData {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSLog(@"%@--%@--%@", DEVICE_ID, LATITUDE, LATITUDE);
    NSDictionary *body = @{@"Device-Id": DEVICE_ID,  @"lat": LATITUDE, @"long": LONGITUDE, @"longlat": @"baidu"};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Home&a=index", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            
            //轮播图
            NSArray *adverArray = responseObject[@"result"][@"head_adver"];
            _images1 = [NSMutableArray new];
            _adverModelArray1 = [NSMutableArray new];
            for (NSDictionary *item in adverArray) {
                [_images1 addObject:item[@"pic"]];
                HGAdverModel *model = [[HGAdverModel alloc] initWithDict:item];
                [_adverModelArray1 addObject:model];
                
            }
            [self.carouselView1 setImagesWithNames:@[@"ad_001", @"ad_003", @"ad_002"] showBottom:YES];
            //快报
            NSArray *newsList = responseObject[@"result"][@"news_list"];
            _newsList = [NSMutableArray new];
            
            for (NSDictionary *dic in newsList) {
                HGNewsModel *model = [[HGNewsModel alloc] initWithDict:dic];
                [_newsList addObject:model];
            }
            [self.expressenView setNewsArray:_newsList];
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
}



//获取洗车列表
- (void)requestCarData {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID,  @"lat": LATITUDE, @"long": LONGITUDE, @"longlat": @"baidu", @"page": [NSString stringWithFormat:@"%ld", _currentPage]};
    
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Home&a=new_appoint_car_wash", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            //附近洗车
            _totalPages = [responseObject[@"result"][@"totalPage"] integerValue];
            NSArray *arr = responseObject[@"result"][@"group_list"];
            [_washCarData addObjectsFromArray:arr];
            [_tableView reloadData];
            
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
        
        [self endMj];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoading];
        [MBProgressHUD showError:error.description];
        [self endMj];
    }];
    
}


#pragma mark -- 定位/搜索栏
- (UIView *)topView {
    if (_topView == nil) {
        _topView = [[WHHTopView alloc] initWithFrame:CGRectMake(0, 10, SCREEN_SIZE.width, 50) withViewController:self];
        [_topView showLocationBtn];
    }
    return _topView;
}

#pragma mark -- 轮播图
- (HGCarouselView *)carouselView1 {
    if (_carouselView1 == nil) {
        _carouselView1 = [[HGCarouselView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(120) + 60) andPlaceholderImage:nil];
        
        __block HomeViewController *weakSelf = self;
        [_carouselView1 setResult:^(NSInteger index) {
            NSLog(@"图%ld", (long)index);
            [weakSelf intoAdverDetailWithSection:0 index:index];
//            if (index == 1) {
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"TABBAR_CHANGE" object:nil userInfo:@{@"index": @"2"}];
//            }else {
//                [weakSelf intoAdverDetailWithSection:0 index:index];
//            }
            
        }];
    }
    return _carouselView1;
}


- (void)intoAdverDetailWithSection:(NSInteger)section index:(NSInteger)index {
    if (section == 0) {
        HGAdverModel *model = _adverModelArray1[index];
        NSLog(@"%@", model.description);
        HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
        web.urlString = model.url;
        [self.navigationController pushViewController:web animated:YES];
    }else {
        
    }
}

#pragma mark -- 品牌汽车入口


#pragma mark -- 平台快报
- (PlatformExpressenView *)expressenView {
    if (_expressenView == nil) {
        _expressenView = [[PlatformExpressenView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, KWIDTH_6S(40))];
        _expressenView.nav = self.navigationController;
        [self.view addSubview:_expressenView];
    }
    return _expressenView;
}

#pragma mark -- 项目预约专区
- (UIView *)serviceView {
    if (_serviceView == nil) {
        _serviceView = [[HGServiceScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(80))];
        _serviceView.nav = self.navigationController;
    }
    return _serviceView;
}




#pragma mark -- 轮播图
- (HGCarouselView *)carouselView2 {
    if (_carouselView2 == nil) {
        _carouselView2 = [[HGCarouselView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH(80)) andPlaceholderImage:nil];
        [_carouselView2 setImagesWithURLs:@[@"http://www.nekahome.com/tpl/Wap/pure/static/images/banner4.jpg", @"http://www.nekahome.com/tpl/Wap/pure/static/images/banner5.jpg", @"http://www.nekahome.com/tpl/Wap/pure/static/images/banner3.jpg"] showBottom:NO];
    }
    return _carouselView2;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    }else if (section == 1) {
        return _secondSectionArr.count;
    }else if (section == 2) {
        return _thirdSecotionArr.count;
    }else if (section == 3) {
        return _fourthSecotionArr.count;
    }else if (section == 4) {
        return 1;
    }else {
        return _washCarData.count;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return self.carouselView1.frame.size.height;
        }else if (indexPath.row == 1) {
            return self.serviceView.frame.size.height;
        }else {
            return self.expressenView.frame.size.height;
        }
    }else if (indexPath.section == 1) {
        UIView *view = _secondSectionArr[indexPath.row];
        return view.frame.size.height;
    }else if (indexPath.section == 2) {
        UIView *view = _thirdSecotionArr[indexPath.row];
        return view.frame.size.height;
    }else if (indexPath.section == 3) {
        UIView *view = _fourthSecotionArr[indexPath.row];
        return view.frame.size.height;
    }else if (indexPath.section == 4) {
        return self.carouselView2.frame.size.height;
    }else {
        return KWIDTH(95);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [UIView new];
    if (section == 5) {
        view = [RecommendSubViews recommendTitleViewWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(30)) andTitle:@"洗车预约" andNav:nil];
    }
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 5) {
        return KWIDTH_6S(30);
    }else {
        return 0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 4) {
        
        HGOrderWebViewController *webView = [[HGOrderWebViewController alloc] init];
        webView.urlString = _washCarData[indexPath.row][@"url"];
        
//        GoodDetailViewController *goodDetail = [[GoodDetailViewController alloc] init];
        [self.navigationController pushViewController:webView animated:YES];
    }
    
    
}




- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int currentPage = scrollView.contentOffset.x / SCREEN_SIZE.width;
    _pageCtrl.currentPage = currentPage;
}


#pragma mark -- 城市选择

- (void)citySelect:(UIButton *)btn {
    CityPickerViewController *picker = [[CityPickerViewController alloc] init];
    picker.currentCity = _selectCity.titleLabel.text;
    picker.result = ^(NSDictionary *cityData) {
        NSLog(@"%@", cityData);
        [_selectCity setTitle:cityData[@"area_name"] forState:UIControlStateNormal];
    };
    [self.navigationController pushViewController:picker animated:YES];
}


#pragma mark -- cell的分割线宽度等于屏幕宽度
-(void)viewDidLayoutSubviews {
    
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)])  {
        [_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY < 160) {
        _statusBarBgView.alpha = 0.00625 * offsetY;
    }else if (offsetY > 160) {
        _statusBarBgView.alpha = 1;
    }
    
}

- (BOOL)isOpenGPRS {
    CLAuthorizationStatus type = [CLLocationManager authorizationStatus];
    if (![CLLocationManager locationServicesEnabled] || type == kCLAuthorizationStatusDenied) {
        NSLog(@"定位功能未开启或用户未授权");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"定位功能未授权" message:@"是否去授权" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
        UIAlertAction *comfirmAction = [UIAlertAction actionWithTitle:@"去授权" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            UIApplication *application = [UIApplication sharedApplication];
            NSURL *url = [NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"];
            if ([application canOpenURL:url]) {
                [application openURL:url];
            }
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:comfirmAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }

    return YES;
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
















