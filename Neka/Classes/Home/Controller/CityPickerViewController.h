//
//  CityPickerViewController.h
//  Neka1.0
//
//  Created by ma c on 2017/2/9.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^SelectResult)(NSDictionary * cityData);

@interface CityPickerViewController : UIViewController
@property (nonatomic, copy)SelectResult result;
//当前选择城市
@property (nonatomic, strong)NSString *currentCity;
@end
