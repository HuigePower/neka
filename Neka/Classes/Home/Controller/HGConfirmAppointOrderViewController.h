//
//  HGConfirmAppointOrderViewController.h
//  Neka
//
//  Created by ma c on 2017/7/6.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGConfirmAppointOrderViewController : UIViewController
@property (nonatomic, copy)NSString *order_id;
@property (nonatomic, copy)NSString *type;
@end
