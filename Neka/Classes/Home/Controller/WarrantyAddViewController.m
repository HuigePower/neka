//
//  WarrantyAddViewController.m
//  Neka1.0
//
//  Created by ma c on 2017/2/12.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "WarrantyAddViewController.h"
#import "PulldownButton.h"
#import "HGPickerView.h"
#import "CustomTableViewCell.h"
@interface WarrantyAddViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)UIImageView *topView;
@property (nonatomic, strong)UILabel *topLabel;
@property (nonatomic, strong)UIButton *commit;

@property (nonatomic, strong)PulldownButton *provinceBtn; //省
@property (nonatomic, strong)PulldownButton *cityBtn;     //市
@property (nonatomic, strong)PulldownButton *orderNumberBtn; //订单号
@property (nonatomic, strong)PulldownButton *carBrandBtn;   //汽车品牌
@property (nonatomic, strong)PulldownButton *carLineBtn;    //车系
@property (nonatomic, strong)PulldownButton *carCityBtn;   //车牌号的省市直辖市简称
@property (nonatomic, strong)UITextField *carNumberTf; //车牌号码

@property (nonatomic, strong)PulldownButton *warrantyNumberBtn;  //质保号

@property (nonatomic, strong)UITextField *nameTf; //客户姓名
@property (nonatomic, strong)UILabel *productLabel; //车牌名称
@property (nonatomic, strong)UILabel *startLabel;  //装贴时间(质保开始时间)
@property (nonatomic, strong)UILabel *periodLabel;  //保质时间

//选择器
@property (nonatomic, strong)HGPickerView *pickerView;


@property (nonatomic, strong)NSArray *inputViews;

@property (nonatomic, strong)NSArray *provinceList;
@property (nonatomic, strong)NSDictionary *provinceData;

@property (nonatomic, strong)NSArray *cityList;
@property (nonatomic, strong)NSDictionary *cityData;

@property (nonatomic, strong)NSArray *orderList;
@property (nonatomic, strong)NSString *orderID;
@property (nonatomic, strong)NSDictionary *orderData;


@property (nonatomic, strong)NSArray *carNameList;
@property (nonatomic, strong)NSDictionary *carNameData;

@property (nonatomic, strong)NSArray *carNameSeriesList;
@property (nonatomic, strong)NSDictionary *carNameSeriesData;

@property (nonatomic, strong)NSArray *carNumNameList;
@property (nonatomic, strong)NSString *carNumName;
//@property (nonatomic, strong)NSString *carNum;

@property (nonatomic, strong)NSArray *warrantyNumList;
@property (nonatomic, strong)NSDictionary *warrantyNumData;
@property (nonatomic, strong)NSString *keepYears;

@property (nonatomic, strong)NSDictionary *data;

@end

@implementation WarrantyAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的质保卡";
    [self.tabBarController.tabBar setHidden:YES];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self makeInputViews];
    [self initDataSource];
    _topView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(320), KWIDTH(77))];
    _topView.image = [UIImage imageNamed:@"banner"];
    
    _topLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,KWIDTH(77), KWIDTH(320), 40)];
    _topLabel.text = @"完善质保信息  获得耐卡官方质保";
    _topLabel.textColor = mRGB(38, 115, 216, 1);
    _topLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    _topLabel.textAlignment = NSTextAlignmentCenter;
    _topLabel.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    
    [self.view addSubview:_tableView];
    
    [_tableView registerClass:[CustomTableViewCell class] forCellReuseIdentifier:@"cell"];
    
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 9;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell) {
        [cell deleteSubviews];
    }
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *arr = @[@"选择城市:", @"选择订单号:", @"客户姓名:", @"产品名称:", @"装贴时间:", @"选择车型:", @"车牌号码:", @"选择质保号:", @"保质时间:"];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 5, KWIDTH(80), 30)];
    label.text = arr[indexPath.row];
    label.font = [UIFont systemFontOfSize:14];
    [cell addCustomSubview:label];
    
    if (indexPath.row == 0) {
        [cell addCustomSubview:_provinceBtn];
        [cell addCustomSubview:_cityBtn];
    }else if (indexPath.row == 1) {
        [cell addCustomSubview:_orderNumberBtn];
    }else if (indexPath.row == 2) {
        [cell addCustomSubview:_nameTf];
    }else if (indexPath.row == 3) {
        [cell addCustomSubview:_productLabel];
    }else if (indexPath.row == 4) {
        [cell addCustomSubview:_startLabel];
    }else if (indexPath.row == 5) {
        [cell addCustomSubview:_carBrandBtn];
        [cell addCustomSubview:_carLineBtn];
    }else if (indexPath.row == 6) {
        [cell addCustomSubview:_carCityBtn];
        [cell addCustomSubview:_carNumberTf];
    }else if (indexPath.row == 7) {
        [cell addCustomSubview:_warrantyNumberBtn];
    }else if (indexPath.row == 8) {
        [cell addCustomSubview:_periodLabel];
    }
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(10), 40 - 0.5, SCREEN_SIZE.width - KWIDTH(20), 0.5)];
    line.backgroundColor = [UIColor lightGrayColor];
    [cell addCustomSubview:line];

    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [UIView new];
    
    [view addSubview:_topView];
    [view addSubview:_topLabel];
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [UIView new];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(10), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(20), KWIDTH(40))];
    [btn setTitle:@"提交" forState:UIControlStateNormal];
    btn.backgroundColor = mRGB(38, 115, 216, 1);
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = 5;
    
    [btn addTarget:self action:@selector(commitClick) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    
    return view;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(77) + 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return KWIDTH(60);
}




- (void)makeInputViews {
    //省份或直辖市
    _provinceBtn = [[PulldownButton alloc] initWithFrame:CGRectMake(KWIDTH(100), 10, KWIDTH(60), 20)];
    
    //市或直辖市
    _cityBtn = [[PulldownButton alloc] initWithFrame:CGRectMake(KWIDTH(170), 10, KWIDTH(60), 20)];
    //选择订单号
    _orderNumberBtn = [[PulldownButton alloc] initWithFrame:CGRectMake(KWIDTH(100), 10, KWIDTH(80), 20)];
    //客户姓名
    _nameTf = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(100), 5, KWIDTH(150), 30)];
    _nameTf.font = [UIFont systemFontOfSize:14];
    _nameTf.placeholder = @"请填写您的姓名";
    //产品名称
    _productLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(100), 10, KWIDTH(200), 20)];
    _productLabel.font = [UIFont systemFontOfSize:14];
    //装贴时间
    _startLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(100), 10, KWIDTH(150), 20)];
    //选择车型
    _carBrandBtn = [[PulldownButton alloc] initWithFrame:CGRectMake(KWIDTH(100), 10, KWIDTH(60), 20)];
    //车系
    _carLineBtn = [[PulldownButton alloc] initWithFrame:CGRectMake(KWIDTH(170), 10, KWIDTH(80), 20)];
    
    
    //车牌号码
    _carCityBtn = [[PulldownButton alloc] initWithFrame:CGRectMake(KWIDTH(100), 10, KWIDTH(80), 20)];
    _carCityBtn.label.text = @"请选择";
    _carNumberTf = [[UITextField alloc] initWithFrame:CGRectMake(KWIDTH(180), 5, KWIDTH(150), 30)];
    _carNumberTf.placeholder = @"请填写车牌号码";
    _carNumberTf.font = [UIFont systemFontOfSize:14];
    
    _warrantyNumberBtn = [[PulldownButton alloc] initWithFrame:CGRectMake(KWIDTH(100), 10, KWIDTH(80), 20)];
    
    _periodLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(100), 10, KWIDTH(100), 20)];
    _periodLabel.font = [UIFont systemFontOfSize:14];
    
    
    //选择器
    __weak WarrantyAddViewController *weakSelf = self;
    _pickerView = [[HGPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, 244)];
    _pickerView.result = ^(id data, PickType type) {
        if (type == PickTypeProvince) {
            _provinceData = data;
            NSLog(@"省：%@", data);
            [weakSelf requestCityList];
        }else if (type == PickTypeCity){
            _cityData = data;
            NSLog(@"市：%@", data);
            [weakSelf requestWarrantyList];
        }else if (type == PickTypeWarrantyID){
            _warrantyNumData = data;
            NSLog(@"质保信息：%@", data);
            [weakSelf showWarrantyInfo];
        }else if (type == PickTypeOrderID){
            _orderID = data;
            NSLog(@"订单号：%@", data);
            [weakSelf requestOrderInfo];
        }else if (type == PickTypeCarName) {
            _carNameData = data;
            NSLog(@"订单号：%@", data);
            [weakSelf requestCarNameSeriesList];
        }else if (type == PickTypeCarNameSeries) {
            _carNameSeriesData = data;
            NSLog(@"订单号：%@", data);
        }else if (type == PickTypeCarNumberName) {
            _carNumName = data;
            NSLog(@"订单号：%@", data);
        }
    };
    
    NSArray *btnArr = @[_provinceBtn, _cityBtn, _orderNumberBtn, _carBrandBtn, _carLineBtn, _carCityBtn, _warrantyNumberBtn];
    
    NSArray *selArr = @[@"selectProvince:", @"selectCity:", @"selectOrderNumber:", @"selectCarBrand:", @"selectCarLine:", @"selectCarCity:", @"selectWarrantyNumber:"];
    for (int i = 0; i < btnArr.count; i++) {
        PulldownButton *btn = btnArr[i];
        
        [btn addTarget:self action:NSSelectorFromString(selArr[i]) forControlEvents:UIControlEventTouchUpInside];
    }
  
}




- (void)showWarrantyInfo {
    _periodLabel.text = [NSString stringWithFormat:@"%@年", _warrantyNumData[@"keep_years"]];
    
}

- (void)showOrderInfo {
    
}

- (void)selectProvince:(PulldownButton *)btn {
    if (_provinceList.count == 0) {
        [MBProgressHUD showError:@"没有可选省份"];
    }else {
        [_pickerView showInSuperViewControl:self withDataSource:_provinceList andButton:btn pickerType:PickTypeProvince];
    }
    
}

- (void)selectCity:(PulldownButton *)btn {
    if (_cityList.count == 0) {
        [MBProgressHUD showError:@"没有可选城市"];
    }else {
        [_pickerView showInSuperViewControl:self withDataSource:_cityList andButton:btn pickerType:PickTypeCity]; 
    }
    
}

- (void)selectOrderNumber:(PulldownButton *)btn {
    if (_orderList.count == 0) {
        [MBProgressHUD showError:@"没有可选订单"];
    }else {
        [_pickerView showInSuperViewControl:self withDataSource:_orderList andButton:btn pickerType:PickTypeOrderID];
    }
    
}

- (void)selectCarBrand:(PulldownButton *)btn {
    if (_carNameList.count == 0) {
        [MBProgressHUD showError:@"没有可选汽车品牌"];
    }else {
        [_pickerView showInSuperViewControl:self withDataSource:_carNameList    andButton:btn pickerType:PickTypeCarName];
    }
    
}

- (void)selectCarLine:(PulldownButton *)btn {
    if (_carNameSeriesList.count == 0) {
        [MBProgressHUD showError:@"没有可选车系"];
    }else {
        [_pickerView showInSuperViewControl:self withDataSource:_carNameSeriesList andButton:btn pickerType:PickTypeCarNameSeries];
    }
    
}

- (void)selectCarCity:(PulldownButton *)btn {
    [_pickerView showInSuperViewControl:self withDataSource:self.carNumNameList andButton:btn pickerType:PickTypeCarNumberName];
}

- (void)selectWarrantyNumber:(PulldownButton *)btn {
    if (_warrantyNumList.count == 0) {
        [MBProgressHUD showError:@"该城市没有质保号"];
    } else {
        [_pickerView showInSuperViewControl:self withDataSource:_warrantyNumList andButton:btn pickerType:PickTypeWarrantyID];
    }
    
}

- (void)whenHidePickerView {
    _tableView.frame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64);
}

- (void)whenShowPickerView {
    _tableView.frame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - 244);
}



#pragma mark -- 数据相关
//初始数据--省份列表、车品牌列表
- (void)initDataSource {
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=User_warranty&a=user_warranty_add", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            _data = responseObject[@"result"];
            _provinceList = _data[@"f_area_list"];
            _orderList = _data[@"now_user_orders"];
            _carNameList = _data[@"f_car_list"];
        }else {
            UIAlertController *alter = [UIAlertController alertControllerWithTitle:@"提示" message:responseObject[@"errorMsg"] preferredStyle:UIAlertControllerStyleAlert];
            alter.message = responseObject[@"errorMsg"];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            [alter addAction:action];
            [self presentViewController:alter animated:YES completion:nil];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
    
}



//城市列表
- (void)requestCityList {

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"area_pid": _provinceData[@"area_id"]};

    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=User_warranty&a=ajax_area_get_category", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSString *errorCode = [NSString stringWithFormat:@"%@", data[@"error"]];
        if ([errorCode isEqualToString:@"0"]) {
            _cityList = data[@"area_list"];
        }else {
            [MBProgressHUD showError:data[@"msg"]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
}

//质保单列表
- (void)requestWarrantyList {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [self showLoading];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"area_id": _cityData[@"area_id"]};
    [manager POST:[NSString stringWithFormat:@"%@c=User_warranty&a=ajax_own_product_volume_number", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSString *errorCode = [NSString stringWithFormat:@"%@", data[@"error"]];
        if ([errorCode isEqualToString:@"0"]) {
            _warrantyNumList = data[@"own_product_list"];
        }else {
            [MBProgressHUD showError:data[@"msg"]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
}

//获取订单信息
- (void)requestOrderInfo {

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"order_id": _orderID};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=User_warranty&a=ajax_own_order_info", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideLoading];
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSString *errorCode = [NSString stringWithFormat:@"%@", data[@"error"]];
        if ([errorCode isEqualToString:@"0"]) {
            _orderData = data[@"order_list"];
            _productLabel.text = _orderData[@"order_name"];
            _startLabel.text = _orderData[@"pay_end_time"];
        }else {
            [MBProgressHUD showError:data[@"msg"]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
}

//获取车系信息
- (void)requestCarNameSeriesList {

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket], @"car_fid": _carNameData[@"car_id"]};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=User_warranty&a=ajax_car_get_category", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSString *errorCode = [NSString stringWithFormat:@"%@", data[@"error"]];
        if ([errorCode isEqualToString:@"0"]) {
            _carNameSeriesList = data[@"car_list"];
        }else {
            [MBProgressHUD showError:data[@"msg"]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
}


- (void)commitClick {
    NSLog(@"提交");
    
    if (_provinceData == nil) {
        [MBProgressHUD showError:@"请选择省份"];
        return;
    }
    if (_cityData == nil) {
        [MBProgressHUD showError:@"请选择城市"];
        return;
    }
    if (_orderID.length == 0) {
        [MBProgressHUD showError:@"请选择订单"];
        return;
    }
    if (_nameTf.text.length == 0) {
        [MBProgressHUD showError:@"请填写姓名"];
        return;
    }
    
    if (_carNameData == nil) {
        [MBProgressHUD showError:@"请选择汽车品牌"];
        return;
    }
    
    if (_carNameSeriesData == nil) {
        [MBProgressHUD showError:@"请选择车系"];
        return;
    }
    
    if (_carNumName.length == 0 || _carNumberTf.text.length < 6) {
        [MBProgressHUD showError:@"请填写车牌号"];
        return;
    }
    if (_warrantyNumData == nil) {
        [MBProgressHUD showError:@"请选择质保号"];
        return;
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID,
                           @"ticket": [UserDefaultManager ticket],
                           @"area_pid": _provinceData[@"area_id"],
                           @"area_id": _cityData[@"area_id"],
                           @"order_id": _orderID,
                           @"user_name": _nameTf.text,
                           @"product_name": _productLabel.text,
                           @"two_own_start_time": _orderData[@"user_pay_time"],
                           @"car_fid": _carNameData[@"car_id"],
                           @"car_id": _carNameSeriesData[@"car_id"],
                           @"car_number_name": _carNumName,
                           @"car_number": _carNumberTf.text,
                           @"own_id": _warrantyNumData[@"own_id"],
                           @"keep_years": _warrantyNumData[@"keep_years"]};
    
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=User_warranty&a=user_warranty_do_add", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSString *errorCode = [NSString stringWithFormat:@"%@", data[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            [MBProgressHUD showSuccess:@"添加成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [MBProgressHUD showError:data[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
    }];
    
}



- (NSArray *)carNumNameList {
    _carNumNameList = @[
                        @{
                            @"cat_city_id": @"1",
                            @"cat_num_name": @"京"
                            },
                        @{
                            @"cat_city_id": @"2",
                            @"cat_num_name": @"津"
                            },
                        @{
                            @"cat_city_id": @"3",
                            @"cat_num_name": @"冀"
                            },
                        @{
                            @"cat_city_id": @"4",
                            @"cat_num_name": @"沪"
                            },
                        @{
                            @"cat_city_id": @"5",
                            @"cat_num_name": @"渝"
                            },
                        @{
                            @"cat_city_id": @"6",
                            @"cat_num_name": @"豫"
                            },
                        @{
                            @"cat_city_id": @"7",
                            @"cat_num_name": @"云"
                            },
                        @{
                            @"cat_city_id": @"8",
                            @"cat_num_name": @"辽"
                            },
                        @{
                            @"cat_city_id": @"9",
                            @"cat_num_name": @"黑"
                            },
                        @{
                            @"cat_city_id": @"10",
                            @"cat_num_name": @"湘"
                            },
                        @{
                            @"cat_city_id": @"11",
                            @"cat_num_name": @"皖"
                            },
                        @{
                            @"cat_city_id": @"12",
                            @"cat_num_name": @"鲁"
                            },
                        @{
                            @"cat_city_id": @"13",
                            @"cat_num_name": @"新"
                            },
                        @{
                            @"cat_city_id": @"14",
                            @"cat_num_name": @"苏"
                            },
                        @{
                            @"cat_city_id": @"15",
                            @"cat_num_name": @"浙"
                            },
                        @{
                            @"cat_city_id": @"16",
                            @"cat_num_name": @"赣"
                            },
                        @{
                            @"cat_city_id": @"17",
                            @"cat_num_name": @"鄂"
                            },
                        @{
                            @"cat_city_id": @"18",
                            @"cat_num_name": @"桂"
                            },
                        @{
                            @"cat_city_id": @"19",
                            @"cat_num_name": @"甘"
                            },
                        @{
                            @"cat_city_id": @"20",
                            @"cat_num_name": @"晋"
                            },
                        @{
                            @"cat_city_id": @"21",
                            @"cat_num_name": @"蒙"
                            },
                        @{
                            @"cat_city_id": @"22",
                            @"cat_num_name": @"陕"
                            },
                        @{
                            @"cat_city_id": @"23",
                            @"cat_num_name": @"吉"
                            },
                        @{
                            @"cat_city_id": @"24",
                            @"cat_num_name": @"闽"
                            },
                        @{
                            @"cat_city_id": @"25",
                            @"cat_num_name": @"贵"
                            },
                        @{
                            @"cat_city_id": @"26",
                            @"cat_num_name": @"粤"
                            },
                        @{
                            @"cat_city_id": @"27",
                            @"cat_num_name": @"青"
                            },
                        @{
                            @"cat_city_id": @"28",
                            @"cat_num_name": @"藏"
                            },
                        @{
                            @"cat_city_id": @"29",
                            @"cat_num_name": @"川"
                            },
                        @{
                            @"cat_city_id": @"30",
                            @"cat_num_name": @"宁"
                            },
                        @{
                            @"cat_city_id": @"31",
                            @"cat_num_name": @"琼"
                            }
                        ];
    return _carNumNameList;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
