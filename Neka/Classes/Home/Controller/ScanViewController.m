//
//  ScanViewController.m
//  Neka1.0
//
//  Created by ma c on 16/7/30.
//  Copyright © 2016年 ma c. All rights reserved.
//  二维码扫描

#import "ScanViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "ScanView.h"

@interface ScanViewController ()<AVCaptureMetadataOutputObjectsDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,
    UIAlertViewDelegate>
@property (nonatomic, strong)AVCaptureDevice *device;
@property (nonatomic, strong)AVCaptureDeviceInput *input;
@property (nonatomic, strong)AVCaptureMetadataOutput *outPut;
@property (nonatomic, strong)AVCaptureSession *session;
@property (nonatomic, strong)AVCaptureVideoPreviewLayer *previewLayer;

@property (nonatomic, strong)ScanView *scanView;

@end

@implementation ScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"二维码";
    [self.tabBarController.tabBar setHidden:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIButton *photoAlbum = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [photoAlbum setTitle:@"相册" forState:UIControlStateNormal];
    [photoAlbum setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoAlbum addTarget:self action:@selector(openPhotoAlbum:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:photoAlbum];
    
    [self openQR];
    
    
    
}

- (void)openPhotoAlbum:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    }
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
    
    
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    NSLog(@"%@", info);
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
    if (image) {
        CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{CIDetectorAccuracy : CIDetectorAccuracyHigh}];
        NSArray *features = [detector featuresInImage:[CIImage imageWithCGImage:image.CGImage]];
        if (features.count == 0) {
            [picker dismissViewControllerAnimated:YES completion:nil];
            return;
        }
        CIQRCodeFeature *feature = [features objectAtIndex:0];
        if (feature) {
            NSString *scannedResult = feature.messageString;
            NSLog(@"%@", scannedResult);
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"扫描结果" message:scannedResult delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"扫描结果" message:@"未发现二维码" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    }else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"扫描结果" message:@"" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//开启二维码扫描
- (void)openQR {
    if ([self validateCamera] && [self canUseCamera]) {
        [self startRunning];
        _scanView = [[ScanView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
        _scanView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_scanView];
    }else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"没有摄像或摄像头不可用" delegate:self cancelButtonTitle:@"确定"otherButtonTitles:nil, nil];
        [alertView show];
    }
}





//获取相机权限
- (BOOL)canUseCamera {
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    
    if (authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied) {
        
        NSLog(@"相机权限受限");
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请在设备的设置-隐私-相机中允许访问相机" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        return NO;
    }
    
    return YES;
    
}

//验证相机
- (BOOL)validateCamera {
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}



#pragma mark -- ConfigQR
- (void)startRunning {
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    //input
    NSError *error;
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    if (error) {
        NSLog(@"error:%@", error.localizedDescription);
    }
    
    //output
    _outPut = [[AVCaptureMetadataOutput alloc] init];
    [_outPut setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    _outPut.rectOfInterest = CGRectMake(KWIDTH(100) / SCREEN_SIZE.height, KWIDTH(50) / SCREEN_SIZE.width, KWIDTH(220) / SCREEN_SIZE.height, KWIDTH(220) / SCREEN_SIZE.width);
    
    //session
    _session = [[AVCaptureSession alloc] init];
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
    if ([_session canAddInput:self.input]) {
        [_session addInput:self.input];
    }
    
    if ([_session canAddOutput:self.outPut]) {
        [_session addOutput:self.outPut];
    }
    
    AVCaptureConnection *outputConnection = [_outPut connectionWithMediaType:AVMediaTypeVideo];
    outputConnection.videoOrientation = AVCaptureVideoOrientationPortrait;
    
    //条码类型
    _outPut.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];
    
    //preview
    
    _previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    _previewLayer.videoGravity = AVLayerVideoGravityResize;
    _previewLayer.frame = CGRectMake(0, 64, SCREEN_SIZE.width, SCREEN_SIZE.height - 64);
    [self.view.layer insertSublayer:_previewLayer atIndex:0];
    _previewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
    
    [_session startRunning];
    
    
}


#pragma mark AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    NSString *stringValue = @"";
    if ([metadataObjects count] >0)
    {
        //停止扫描
        [_session stopRunning];
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex:0];
        stringValue = metadataObject.stringValue;
    }

    NSLog(@"%@", stringValue);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"扫描结果" message:stringValue delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
    [_scanView stopRunning];

}








- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
