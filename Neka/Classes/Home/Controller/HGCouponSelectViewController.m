//
//  HGCouponSelectViewController.m
//  Neka
//
//  Created by ma c on 2017/7/13.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGCouponSelectViewController.h"

@interface HGCouponSelectViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSMutableArray *dataSource;
@property (nonatomic, strong)UIView *noCouponView;
@end

@implementation HGCouponSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.title = @"选择优惠券";
    [self initDataSource];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStylePlain];
    _tableView.tableFooterView = [UIView new];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    [_tableView registerClass:[HGCouponCell class] forCellReuseIdentifier:@"cell"];
}

- (UIView *)noCouponView {
    if (_noCouponView == nil) {
        _noCouponView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
        _noCouponView.center = self.view.center;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(50, 50, 100, 100)];
        imageView.image = [UIImage imageNamed:@"pinglun"];
        [_noCouponView addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 170, 200, 20)];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = [UIColor grayColor];
        if ([_coupon_type isEqualToString:@"mer"]) {
            label.text = @"暂无商家优惠券";
        }else {
            label.text = @"暂无平台优惠券";
        }
        [_noCouponView addSubview:label];
        
    }
    return _noCouponView;
}


- (void)initDataSource {
    _dataSource = [[NSMutableArray alloc] init];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID,
                           @"ticket": [UserDefaultManager ticket],
                           @"order_id": _order_id,
                           @"type": _order_type,
                           @"coupon_type": _coupon_type};
    
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@g=Appapi&c=My&a=new_select_coupon", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            NSArray *arr = responseObject[@"result"];
            for (NSDictionary *dic in arr) {
                HGCouponModel *model = [[HGCouponModel alloc] initWithDict:dic];
                [_dataSource addObject:model];
                [_tableView reloadData];
            }
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_dataSource.count == 0) {
        [self.view addSubview:self.noCouponView];
        return 0;
    }else {
        [self.noCouponView removeFromSuperview];
        return _dataSource.count + 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    HGCouponCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (indexPath.row == 0) {
        [cell setCouponInfo:nil withCouponID:@""];
    }else {
        [cell setCouponInfo:_dataSource[indexPath.row - 1] withCouponID:_coupon_id];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_couponBlock) {
        HGCouponModel *model = _dataSource[indexPath.row - 1];
        _couponBlock(model);
        [self.navigationController popViewControllerAnimated:YES];
        
    }else {
        [MBProgressHUD showError:@"选择失败"];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
