//
//  CityPickerViewController.m
//  Neka1.0
//
//  Created by ma c on 2017/2/9.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "CityPickerViewController.h"

//#import "AreaList.h"
//#import "CityModel.h"

#define SCREEN_SIZE [[UIScreen mainScreen] bounds].size

@interface CityPickerViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong)UITableView *tableView;

@property (nonatomic, strong)UIView *topView;


@property (nonatomic, strong)NSDictionary *nowCity;
@property (nonatomic, strong)NSArray *allProvince;
@property (nonatomic, strong)NSArray *hotCity;
@property (nonatomic, strong)NSArray *aCity;
@property (nonatomic, strong)NSDictionary *allCity;
@property (nonatomic, strong)NSArray *cityHistoryList;

@property (nonatomic, strong)UILabel *currentCityLabel;
@end

@implementation CityPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"选择城市";
    [self.tabBarController.tabBar setHidden:YES];
    
    _nowCity = [NSDictionary new];
    _allProvince = [NSArray new];
    _hotCity = [NSArray new];
    _aCity = [NSArray new];
    _allCity = [NSDictionary new];
    
    
    _currentCityLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 36)];
    _currentCityLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nav_bg"]];
    _currentCityLabel.textAlignment = NSTextAlignmentCenter;
    _currentCityLabel.textColor = [UIColor whiteColor];
    _currentCityLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:_currentCityLabel];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 36, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - 36) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];
    
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"myCell"];
    [self initDataSource];
}

- (void)initDataSource {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=Changecity&a=index", BASE_URL] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            _nowCity = [[NSUserDefaults standardUserDefaults] objectForKey:@"nowCity"];
            _allProvince = responseObject[@"result"][@"all_province"];
            _hotCity = responseObject[@"result"][@"hot_city"];
            _aCity = responseObject[@"result"][@"aCity"];
            _allCity = responseObject[@"result"][@"all_city"];
            _currentCityLabel.text = [NSString stringWithFormat:@"当前城市-%@", _nowCity[@"area_name"]];
            
            [_tableView reloadData];
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _aCity.count + 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    }else {
        NSString *key = _aCity[section - 1];
        return [_allCity[key] count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section != 0) {
        NSString *key = _aCity[indexPath.section - 1];
        NSString *cityName = _allCity[key][indexPath.row][@"area_name"];
        cell.textLabel.text = cityName;
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return self.topView;
    }else {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 30)];
        view.backgroundColor = [UIColor colorWithRed:0.9569 green:0.9569 blue:0.9451 alpha:1.0];
        
        UILabel *label = [[UILabel alloc ]initWithFrame:CGRectMake(10, 5, SCREEN_SIZE.width - 20, 20)];
        label.text = [NSString stringWithFormat:@"  %@",_aCity[section - 1]];
        [view addSubview:label];
        
        return view;
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return self.topView.frame.size.height;
    }else {
        return 30;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}


- (UIView *)topView {
    
    _topView = [[UIView alloc] init];
    _topView.backgroundColor = [UIColor colorWithRed:0.8941 green:0.8941 blue:0.902 alpha:1.0];
    CGFloat height = 0;
    CGFloat y = 0;
    //已定位城市
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, SCREEN_SIZE.width - 20, 30)];
    label1.text = @"已定位城市";
    label1.font = [UIFont systemFontOfSize:12];
    label1.textColor = [UIColor grayColor];
    [_topView addSubview:label1];
    
    if (_nowCity.count > 0) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(10, 30, (SCREEN_SIZE.width - 40) / 3, 30)];
        btn.backgroundColor = [UIColor whiteColor];
        [btn setTitle:_nowCity[@"area_name"] forState:UIControlStateNormal];
        btn.titleLabel.font =  [UIFont systemFontOfSize:12];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = 10;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_topView addSubview:btn];
    }
    height = height + 60;
    y = y + 60;
    //最近访问城市
    if (self.cityHistoryList.count > 0) {
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, y, SCREEN_SIZE.width - 20, 30)];
        label2.text = @"最近访问城市";
        label2.font = [UIFont systemFontOfSize:12];
        label2.textColor = [UIColor grayColor];
        [_topView addSubview:label2];
        height = height + 30;
        y = y + 30;
        for (int i = 0; i < self.cityHistoryList.count; i++) {
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(10 * (i % 3 + 1) + (SCREEN_SIZE.width - 40) / 3 * (i % 3), y + 40 * (i / 3), (SCREEN_SIZE.width - 40) / 3, 30)];
            btn.backgroundColor = [UIColor whiteColor];
            [btn setTitle:self.cityHistoryList[i][@"area_name"] forState:UIControlStateNormal];
            btn.titleLabel.font =  [UIFont systemFontOfSize:12];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            btn.tag = 1000 + i;
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [_topView addSubview:btn];
        }
        if (self.cityHistoryList.count > 3) {
            height = height + 70;
            y = y + 70;
        }else {
            height = height + 30;
            y = y + 30;
        }
    }
    
    //热门城市
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, y, SCREEN_SIZE.width - 20, 30)];
    label3.text = @"热门城市";
    label3.font = [UIFont systemFontOfSize:12];
    label3.textColor = [UIColor grayColor];
    [_topView addSubview:label3];
    height = height + 30;
    y = y + 30;
    if (_hotCity.count > 0) {
        for (int i = 0; i < _hotCity.count; i++) {
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(10 * (i + 1) + (SCREEN_SIZE.width - 40) / 3 * i, y, (SCREEN_SIZE.width - 40) / 3, 30)];
            btn.backgroundColor = [UIColor whiteColor];
            [btn setTitle:_hotCity[i][@"area_name"] forState:UIControlStateNormal];
            btn.titleLabel.font =  [UIFont systemFontOfSize:12];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            btn.tag = 100 + i;
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [_topView addSubview:btn];
        }
    }
    height = height + 30;
    
    _topView.frame = CGRectMake(0, 0, SCREEN_SIZE.width, height + 10);
    return _topView;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section != 0) {
        NSString *key = _aCity[indexPath.section - 1];
        NSDictionary *city = _allCity[key][indexPath.row];
        if (_result) {
            [[NSUserDefaults standardUserDefaults] setObject:city forKey:@"nowCity"];
            [self saveCityHistory:city];
            self.result(city);
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}


- (void)saveCityHistory:(NSDictionary *)cityData {
    NSMutableArray *tempArr = [NSMutableArray arrayWithArray:self.cityHistoryList];
    NSLog(@"%@", tempArr);
    NSLog(@"%@", cityData);
    for (int i = 0; i < tempArr.count; i++) {
        NSDictionary *dic = tempArr[i];
        if ([dic[@"area_name"] isEqualToString:cityData[@"area_name"]]) {
            [tempArr removeObjectAtIndex:i];
        }
    }
    
    [tempArr insertObject:cityData atIndex:0];
    if (tempArr.count > 6) {
        [tempArr removeObjectAtIndex:6];
    }
    [[NSUserDefaults standardUserDefaults] setObject:tempArr forKey:@"cityHistory"];
    [self changeCookie];
}

- (NSArray *)cityHistoryList {
    NSArray *cityList = [[NSUserDefaults standardUserDefaults] objectForKey:@"cityHistory"];
    return cityList;
    
}

- (void)btnClick:(UIButton *)btn {
    NSDictionary *city = [NSDictionary new];
    if (btn.tag == 10) {
        city = _nowCity;
    }else if (btn.tag >= 100 && btn.tag < 1000) {
        city = _hotCity[btn.tag - 100];
    }else if (btn.tag >= 1000) {
        city = self.cityHistoryList[btn.tag - 1000];
    }
    
    if (_result) {
        [[NSUserDefaults standardUserDefaults] setObject:city forKey:@"nowCity"];
        [self saveCityHistory:city];
        self.result(city);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

//设置cookie中的定位
- (void)changeCookie {
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    NSLog(@"cookie:%@", cookies);
    
    for (id c in cookies) {
        if ([c isKindOfClass:[NSHTTPCookie class]]) {
            NSHTTPCookie * cookie = (NSHTTPCookie *)c;
            if ([cookie.name isEqualToString:@"now_city"]) {
                
                NSLog(@"%@", cookie.properties);
                NSString *value = cookie.value;
                NSLog(@"value:%@", value);
                NSMutableDictionary *cookieProperties = [[NSMutableDictionary alloc] initWithDictionary:cookie.properties];
                NSDictionary *city = [[NSUserDefaults standardUserDefaults] objectForKey:@"nowCity"];
                NSLog(@"cookie-city:%@", city),
                [cookieProperties setValue:city[@"area_url"] forKey:@"Value"];
                NSHTTPCookie *newCookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
                
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:newCookie];
            }
        }
    }
}


#pragma mark -- cell的分割线宽度等于屏幕宽度
-(void)viewDidLayoutSubviews {
    
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)])  {
        [_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

/*
 1.首次登陆,默认位置"北京",允许定位之后根据定位更换城市
 
 
 */


