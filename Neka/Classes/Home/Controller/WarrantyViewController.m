//
//  WarrantyViewController.m
//  Neka1.0
//
//  Created by ma c on 2017/2/12.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "WarrantyViewController.h"
#import "WarrantyAddViewController.h"
#import "HGChartView.h"
#import "CustomTableViewCell.h"
#import "PageButton.h"
@interface WarrantyViewController ()<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)UIImageView *topView;
@property (nonatomic, strong)UILabel *topLabel;
@property (nonatomic, strong)UIButton *addWarrantyBtn;

@property (nonatomic, strong)UIScrollView *scrollView;
@property (nonatomic, strong)PageButton *backBtn;
@property (nonatomic, strong)PageButton *nextBtn;
@property (nonatomic, strong)UILabel *pageLabel;

@property (nonatomic, strong)NSArray *dataSource;

@end

@implementation WarrantyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的质保卡";
    [self.tabBarController.tabBar setHidden:YES];
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    [_tableView registerClass:[CustomTableViewCell class] forCellReuseIdentifier:@"cell"];
 
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self makeSubviews];
    [self initDataSource];
    
}

- (void)initDataSource {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"ticket": [UserDefaultManager ticket]};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@c=User_warranty&a=index", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            _dataSource = responseObject[@"result"];
            _pageLabel.text = [NSString stringWithFormat:@"1 / %ld", _dataSource.count];
            _topLabel.text = @"感谢您对NEKAPPF的信任，质保信息如下";
            [_tableView reloadData];
        }else {
            _dataSource = responseObject[@"result"];
            _topLabel.text = @"亲，您还没有添加质保卡，赶紧添加一个吧！";
            [_tableView reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];
}


- (void)makeSubviews {
    _topView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH(320), KWIDTH(77))];
    _topView.image = [UIImage imageNamed:@"banner"];
    
    
    _topLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,KWIDTH(77), KWIDTH(320), 40)];
    _topLabel.textColor = NK_Custom_Blue;
    _topLabel.font = [UIFont systemFontOfSize:KWIDTH(14)];
    _topLabel.textAlignment = NSTextAlignmentCenter;
    _topLabel.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    
    //上一页
    _backBtn = [[PageButton alloc] initWithFrame:CGRectMake(KWIDTH(20), 460, KWIDTH(60), 30) andTitle:@"上一页"];
    _backBtn.availabel = NO;
    [_backBtn addTarget:self action:@selector(backPage) forControlEvents:UIControlEventTouchUpInside];

    
    
    //下一页
    _nextBtn = [[PageButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width -  KWIDTH(80), 460, KWIDTH(60), 30) andTitle:@"下一页"];
    _nextBtn.availabel = YES;
    [_nextBtn addTarget:self action:@selector(nextPage) forControlEvents:UIControlEventTouchUpInside];
    
    _pageLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH(90), 460, SCREEN_SIZE.width -  KWIDTH(160), 30)];
    _pageLabel.textAlignment = NSTextAlignmentCenter;
    
    

}



- (UIButton *)addWarrantyBtn {
    if (_addWarrantyBtn == nil) {
        _addWarrantyBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(20), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(40), KWIDTH(30))];
        _addWarrantyBtn.center = CGPointMake(SCREEN_SIZE.width / 2, SCREEN_SIZE.height / 2 - 64);
        [_addWarrantyBtn setTitle:@"添加质保卡" forState:UIControlStateNormal];
        _addWarrantyBtn.layer.masksToBounds = YES;
        _addWarrantyBtn.layer.cornerRadius = 5;
        _addWarrantyBtn.backgroundColor = NK_Custom_Blue;
        [_addWarrantyBtn addTarget:self action:@selector(addBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_tableView addSubview:_addWarrantyBtn];
    }
    return _addWarrantyBtn;
}

- (void)addBtnClick:(UIButton *)btn {
    WarrantyAddViewController *add = [[WarrantyAddViewController alloc] init];
    [self.navigationController pushViewController:add animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_dataSource == 0) {
        self.addWarrantyBtn.hidden = NO;
        return 0;
    }else {
        self.addWarrantyBtn.hidden = YES;
    }
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell) {
        [cell deleteSubviews];
    }
    if (indexPath.row == 0) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 44 * 10 + 20)];
        _scrollView.contentSize = CGSizeMake(SCREEN_SIZE.width * _dataSource.count, _scrollView.frame.size.height);
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        for (int i = 0; i < _dataSource.count; i++) {
            
            HGChartView *chartView = [[HGChartView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width * i + KWIDTH(10), 10, KWIDTH(300), 44 * 10) andDataSource:_dataSource[i]];
            [_scrollView addSubview:chartView];
        }
        
        [cell addCustomSubview:_scrollView];
        [cell addCustomSubview:_backBtn];
        [cell addCustomSubview:_nextBtn];
        [cell addCustomSubview:_pageLabel];
        

    }
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [UIView new];
    
    [view addSubview:_topView];
    [view addSubview:_topLabel];
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [UIView new];
    if (_dataSource.count != 0) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH(20), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(40), KWIDTH(30))];
        [btn setTitle:@"继续添加" forState:UIControlStateNormal];
        btn.backgroundColor = NK_Custom_Blue;
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 5;
        
        [btn addTarget:self action:@selector(addBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
    }
    
    
    
    
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KWIDTH(77) + 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return KWIDTH(80);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44 * 10 + 20 + 40;
}



#pragma mark -- UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if (scrollView != _scrollView) {
        return;
    }
    NSLog(@"1");
    NSInteger page = scrollView.contentOffset.x / SCREEN_SIZE.width + 1;
    _pageLabel.text = [NSString stringWithFormat:@"%ld / %ld", (long)page, _dataSource.count];
    if (_dataSource.count < 2) {
        _backBtn.availabel = NO;
        _nextBtn.availabel = NO;
    }else if (_dataSource.count == 2) {
        if (page == 1) {
            _backBtn.availabel = NO;
            _nextBtn.availabel = YES;
        }else if (page == 2) {
            _nextBtn.availabel = NO;
            _backBtn.availabel = YES;
        }
    }else {
        if (page == 1) {
            _backBtn.availabel = NO;
        }else if (page == _dataSource.count ) {
            _nextBtn.availabel = NO;
        }else {
            _backBtn.availabel = YES;
            _nextBtn.availabel = YES;
        }
    }
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    if (scrollView != _scrollView) {
        return;
    }
    NSLog(@"2");
    NSInteger page = scrollView.contentOffset.x / SCREEN_SIZE.width + 1;
    _pageLabel.text = [NSString stringWithFormat:@"%ld / %ld", (long)page, _dataSource.count];
    if (_dataSource.count < 2) {
        _backBtn.availabel = NO;
        _nextBtn.availabel = NO;
    }else if (_dataSource.count == 2) {
        if (page == 1) {
            _backBtn.availabel = NO;
            _nextBtn.availabel = YES;
        }else if (page == 2) {
            _nextBtn.availabel = NO;
            _backBtn.availabel = YES;
        }
    }else {
        if (page == 1) {
            _backBtn.availabel = NO;
        }else if (page == _dataSource.count ) {
            _nextBtn.availabel = NO;
        }else {
            _backBtn.availabel = YES;
            _nextBtn.availabel = YES;
        }
    }

}

- (void)nextPage {
    NSInteger page = _scrollView.contentOffset.x / SCREEN_SIZE.width;
    if (page == _dataSource.count - 1) {
        return;
    }
    NSLog(@"11");
    [_scrollView setContentOffset:CGPointMake(SCREEN_SIZE.width * (page + 1), 0) animated:YES];
}

- (void)backPage {
    NSInteger page = _scrollView.contentOffset.x / SCREEN_SIZE.width;
    if (page == 0) {
        return;
    }
    NSLog(@"22");
    [_scrollView setContentOffset:CGPointMake(SCREEN_SIZE.width * (page - 1), 0) animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
