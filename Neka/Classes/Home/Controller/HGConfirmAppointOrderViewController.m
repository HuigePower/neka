//
//  HGConfirmAppointOrderViewController.m
//  Neka
//
//  Created by ma c on 2017/7/6.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGConfirmAppointOrderViewController.h"
#import "CustomTableViewCell.h"
#import <GDataXMLNode.h>
#import "HGAppointOrderModel.h"
#import "HGNoncestrTool.h"
#import "HGOrderWebViewController.h"
#import <WXApi.h>
#import "HGMD5Tool.h"
#import "HGNoncestrTool.h"
#import "HGIPAddressTool.h"
#import "HGASCIISortTool.h"
#import "HGCouponSelectViewController.h"
#import "AlipaySDK.h"
#import "Order.h"
#import "DataSigner.h"
@interface HGConfirmAppointOrderViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong)UITableView *tableView;

@property (nonatomic, strong)UIImageView *productImageView;     //产品图片
@property (nonatomic, strong)UITextView *productNameTextView;   //产品名称
@property (nonatomic, strong)UILabel *totalPriceLabel;          //总价
@property (nonatomic, strong)UILabel *balanceLabel;             //账户余额
@property (nonatomic, strong)UILabel *memberBalanceLabel;       //会员余额
@property (nonatomic, strong)UILabel *realityPayLabel;          //实际支付
@property (nonatomic, strong)UILabel *needPayLabel;             //还需支付
@property (nonatomic, strong)UILabel *merchantCouponLabel;      //商家优惠券
@property (nonatomic, strong)UILabel *platformCouponLabel;      //平台优惠券
@property (nonatomic, strong)UILabel *scoreLabel;             //积分信息

@property (nonatomic, strong)UIImageView *scorePayPayBtn;    //积分支付按钮
@property (nonatomic, strong)UIImageView *memberPayBtn;     //会员余额支付
@property (nonatomic, strong)UIImageView *balancePayBtn;    //余额支付
@property (nonatomic, strong)UIImageView *weixinPayBtn;     //微信支付
@property (nonatomic, strong)UIImageView *aliPayBtn;        //支付宝支付

@property (nonatomic, assign)BOOL useMerchantCoupon;//是否使用商家优惠券
@property (nonatomic, assign)BOOL useSystemCoupon;  //是否使用平台优惠券
@property (nonatomic, assign)BOOL useScorePay;    //是否使用积分支付
@property (nonatomic, assign)BOOL useMerchantPay;     //是否使用商家会员卡余额
@property (nonatomic, assign)BOOL useBalancePay;    //是否使用余额支付
@property (nonatomic, assign)BOOL useWeixinPay;     //是否使用微信支付
@property (nonatomic, assign)BOOL useAliPay;        //是否使用支付宝支付

@property (nonatomic, strong)UIButton *goPayBtn;    //确认支付
@property (nonatomic, strong)HGAppointOrderModel *orderModel;

@property (nonatomic, strong)NSDictionary *systemCouponData;
@property (nonatomic, strong)NSDictionary *merchantCouponData;
//@property (nonatomic, assign)CGFloat totalMoney;     //订单总价
//@property (nonatomic, assign)CGFloat scoreMoney;     //积分可抵扣金额
//@property (nonatomic, assign)CGFloat merchantMoney;  //商家会员余额
//@property (nonatomic, assign)CGFloat balanceMoney;   //可用余额
@property (nonatomic, assign)CGFloat needPayMoney;   //还需支付的金额
@property (nonatomic, copy)NSString *longOrderID;    //第三方支付长订单号

@end

@implementation HGConfirmAppointOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"确认订单";
    [self.tabBarController.tabBar setHidden:YES];
    [self registerNotifications];
    [self initData];
    [self createViews];
    [self getOrderInfo];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 * 2) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    [_tableView registerClass:[CustomTableViewCell class] forCellReuseIdentifier:@"cellID"];
    
    
}

- (void)initData {
    
    _useMerchantCoupon = YES;
    _useSystemCoupon = YES;
    _useScorePay = YES;
    _useMerchantPay = YES;
    _useBalancePay = YES;
    _useWeixinPay = YES;
    _useAliPay = NO;
    
}


- (void)createViews {
    
    //产品图片
    _productImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(80), KWIDTH(80))];
    _productImageView.image = [UIImage imageNamed:@"loading"];
    //产品名称
    _productNameTextView = [[UITextView alloc] initWithFrame:CGRectMake(15 + KWIDTH(85), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(85) - 30, KWIDTH(80))];
    _productNameTextView.editable = NO;
    _productNameTextView.font = [UIFont systemFontOfSize:16];
    
    
    //订单总价
    _totalPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - 15 - KWIDTH(100), KWIDTH(10), KWIDTH(100), KWIDTH(20))];
    _totalPriceLabel.font = [UIFont systemFontOfSize:14];
    _totalPriceLabel.text = @"¥ 2000.00";
    _totalPriceLabel.textColor = [UIColor redColor];
    _totalPriceLabel.textAlignment = NSTextAlignmentRight;
    

    
    _scorePayPayBtn = [UIImageView new];
    _memberPayBtn = [UIImageView new];
    _balancePayBtn = [UIImageView new];
    _weixinPayBtn = [UIImageView new];
    _aliPayBtn = [UIImageView new];
    
    NSArray *arr = @[_scorePayPayBtn,
                     _memberPayBtn,
                     _balancePayBtn,
                     _weixinPayBtn,
                     _aliPayBtn];
    
    for (int i = 0; i < arr.count; i++) {
        UIImageView * imageView = arr[i];
        imageView.frame = CGRectMake(SCREEN_SIZE.width - KWIDTH(20) - 15, KWIDTH(10), KWIDTH(20), KWIDTH(20));
        imageView.image = [UIImage imageNamed:@"tickGray"];
        imageView.tag = 10 + i;
    }
    
    //商家优惠券
    _merchantCouponLabel = [[UILabel alloc] initWithFrame:CGRectMake(15 + KWIDTH(110), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(130) - 30, KWIDTH(20))];
    _merchantCouponLabel.textColor = [UIColor lightGrayColor];
    _merchantCouponLabel.font = [UIFont systemFontOfSize:14];
    _merchantCouponLabel.textAlignment = NSTextAlignmentRight;
    //平台优惠券
    _platformCouponLabel = [[UILabel alloc] initWithFrame:CGRectMake(15 + KWIDTH(110), KWIDTH(10), SCREEN_SIZE.width - KWIDTH(130) - 30, KWIDTH(20))];
    _platformCouponLabel.textColor = [UIColor lightGrayColor];
    _platformCouponLabel.font = [UIFont systemFontOfSize:14];
    _platformCouponLabel.textAlignment = NSTextAlignmentRight;
    //可使用积分
    _scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(250), KWIDTH(20))];
    _scoreLabel.text = [NSString stringWithFormat:@"可使用积分%@分，抵扣金额¥%@", @"1000", @"10.00"];
    _scoreLabel.font = [UIFont systemFontOfSize:14];
    //商家会员卡
    _memberBalanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, KWIDTH(25), KWIDTH(100), KWIDTH(15))];
    _memberBalanceLabel.font = [UIFont systemFontOfSize:12];
    _memberBalanceLabel.textColor = [UIColor redColor];
    //账户余额
    _balanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, KWIDTH(25), KWIDTH(100), KWIDTH(15))];
    _balanceLabel.font = [UIFont systemFontOfSize:12];
    _balanceLabel.textColor = [UIColor redColor];
    //实际支付
    _realityPayLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH(80) - 15, KWIDTH(10), KWIDTH(80), KWIDTH(20))];
    _realityPayLabel.font = [UIFont systemFontOfSize:14];
    _realityPayLabel.textColor = [UIColor redColor];
    _realityPayLabel.textAlignment = NSTextAlignmentRight;
    //还需支付
    _needPayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height - 64 * 2, SCREEN_SIZE.width / 2, 64)];
    _needPayLabel.textAlignment = NSTextAlignmentCenter;
    _needPayLabel.font = [UIFont systemFontOfSize:14];
    _needPayLabel.text = @"还需支付：¥2000.00";
    [self.view addSubview:_needPayLabel];
    //确认支付
    _goPayBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width / 2, SCREEN_SIZE.height - 64 * 2, SCREEN_SIZE.width / 2, 64)];
    _goPayBtn.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nav_bg"]];
    [_goPayBtn setTitle:@"确认支付" forState:UIControlStateNormal];
    [_goPayBtn addTarget:self action:@selector(goPay) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_goPayBtn];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return 2;
    }else if (section == 1) {
        return 6;
    }else {
        return 2;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_needPayMoney > 0) {
        [self.view addSubview:_needPayLabel];
        return 3;
    }
    [_needPayLabel removeFromSuperview];
    return 2;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell) {
        [cell deleteSubviews];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            
            [_productImageView setImageWithURL:[NSURL URLWithString:_orderModel.order_info[@"img"]] placeholderImage:[UIImage imageNamed:@"loading"]];
            _productNameTextView.text = _orderModel.order_info[@"order_name"];
            
            [cell addCustomSubview:_productImageView];
            [cell addCustomSubview:_productNameTextView];
            
        }else {
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(100), KWIDTH(20))];
            label.text = @"订单总价";
            label.font = [UIFont systemFontOfSize:14];
            [cell addCustomSubview:label];
            
            _totalPriceLabel.text = [NSString stringWithFormat:@"¥%@", _orderModel.order_info[@"order_total_money"]];
            [cell addCustomSubview:_totalPriceLabel];
            
            
            
        }
    }else if (indexPath.section == 1) {
        
        if (indexPath.row == 0) {//商家优惠券
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(100), KWIDTH(20))];
            name.text = @"商家优惠券";
            name.font = [UIFont systemFontOfSize:14];
            [cell addCustomSubview:name];
            
            
            if ([_merchantCouponData[@"had_id"] length] > 0) {
                _merchantCouponLabel.text = [NSString stringWithFormat:@"满%@减%@", _orderModel.mer_coupon[@"order_money"], _orderModel.mer_coupon[@"discount"]];
                _merchantCouponLabel.textColor = [UIColor redColor];
            }else {
                _merchantCouponLabel.text = @"不使用优惠券";
                _merchantCouponLabel.textColor = [UIColor lightGrayColor];
            }
            _merchantCouponLabel.text = @"不使用优惠券";
            [cell addCustomSubview:_merchantCouponLabel];
            
        }else if (indexPath.row == 1) {//平台优惠券
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(100), KWIDTH(20))];
            name.text = @"平台优惠券";
            name.font = [UIFont systemFontOfSize:14];
            [cell addCustomSubview:name];
            
            if ([_systemCouponData[@"had_id"] length] > 0) {
                _platformCouponLabel.text = [NSString stringWithFormat:@"满%@减%@", _orderModel.system_coupon[@"order_money"], _orderModel.system_coupon[@"discount"]];
                _platformCouponLabel.textColor = [UIColor redColor];
            }else {
                _platformCouponLabel.text = @"不使用优惠券";
                _platformCouponLabel.textColor = [UIColor lightGrayColor];
            }
            
            [cell addCustomSubview:_platformCouponLabel];
            
            
        }else if (indexPath.row == 2) {//使用积分
            
            _scoreLabel.text = [NSString stringWithFormat:@"可使用积分%@分，抵扣金额¥%@", _orderModel.pay_config[@"score_count"], _orderModel.pay_config[@"score_money"]];
            [cell addCustomSubview:_scoreLabel];
            [cell addCustomSubview:_scorePayPayBtn];
            if (_useScorePay) {
                _scorePayPayBtn.image = [UIImage imageNamed:@"tickGreen"];
            }else {
                _scorePayPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
            
        }else if (indexPath.row == 3) {//商家会员余额支付
            UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15, KWIDTH(5), KWIDTH(250), KWIDTH(20))];
            name.text = @"商家会员卡余额支付";
            name.font = [UIFont systemFontOfSize:14];
            [cell addCustomSubview:name];
            
            _memberBalanceLabel.text = [NSString stringWithFormat:@"¥%@", _orderModel.pay_config[@"merchant_money"]];
            [cell addCustomSubview:_memberBalanceLabel];
            [cell addCustomSubview:_memberPayBtn];
            if (_useMerchantPay) {
                _memberPayBtn.image = [UIImage imageNamed:@"tickGreen"];
            }else {
                _memberPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
            
        }else if (indexPath.row == 4) {//账户余额支付
            UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15, KWIDTH(5), KWIDTH(100), KWIDTH(20))];
            name.text = @"余额支付";
            name.font = [UIFont systemFontOfSize:14];
            [cell addCustomSubview:name];
            
            
            _balanceLabel.text = [NSString stringWithFormat:@"¥%@", _orderModel.pay_config[@"balance_money"]];
            [cell addCustomSubview:_balanceLabel];
            [cell addCustomSubview:_balancePayBtn];
            if (_useBalancePay) {
                _balancePayBtn.image = [UIImage imageNamed:@"tickGreen"];
            }else {
                _balancePayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
            
        }else if (indexPath.row == 5) {//实际支付
            UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(100), KWIDTH(20))];
            name.text = @"实际支付：";
            name.font = [UIFont systemFontOfSize:14];
            [cell addCustomSubview:name];
            
            
            _realityPayLabel.text = [NSString stringWithFormat:@"¥%@", _orderModel.pay_config[@"pay_infact"]];
            [cell addCustomSubview:_realityPayLabel];
            
        }
        
        
    }else {
        
        if (indexPath.row == 0) {
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(20), KWIDTH(20))];
            imageView.image = [UIImage imageNamed:@"wxpay"];
            [cell addCustomSubview:imageView];
            
            UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15 + KWIDTH(25), KWIDTH(10), KWIDTH(100), KWIDTH(20))];
            name.text = @"微信支付";
            name.font = [UIFont systemFontOfSize:14];
            [cell addCustomSubview:name];
            [cell addCustomSubview:_weixinPayBtn];
            if (_useWeixinPay) {
                //选择微信
                _weixinPayBtn.image = [UIImage imageNamed:@"tickGreen"];
            }else {
                _weixinPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
            
        }else {
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, KWIDTH(10), KWIDTH(20), KWIDTH(20))];
            imageView.image = [UIImage imageNamed:@"alipay"];
            [cell addCustomSubview:imageView];
            
            UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15 + KWIDTH(25), KWIDTH(10), KWIDTH(100), KWIDTH(20))];
            name.text = @"支付宝支付";
            name.font = [UIFont systemFontOfSize:14];
            [cell addCustomSubview:name];
            [cell addCustomSubview:_aliPayBtn];
            if (_useAliPay) {
                //选择支付宝
                _aliPayBtn.image = [UIImage imageNamed:@"tickGreen"];
            }else {
                _aliPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return KWIDTH(100);
        }else {
            return KWIDTH(40);
        }
    }else if (indexPath.section == 1) {
        return KWIDTH(40);
    }else {
        return KWIDTH(40);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            HGCouponSelectViewController *couponSelect = [[HGCouponSelectViewController alloc] init];
            couponSelect.order_id = _order_id;
            couponSelect.order_type = _type;
            couponSelect.coupon_type = @"mer";
            couponSelect.coupon_id = _merchantCouponData[@"had_id"];
            couponSelect.couponBlock = ^(HGCouponModel *couponModel) {
                _merchantCouponData = @{@"discount": couponModel.discount,
                                      @"had_id": couponModel.had_id,
                                      @"order_money": couponModel.order_money};
                [self getOrderInfo];
            };
            [self.navigationController pushViewController:couponSelect animated:YES];
        }else if (indexPath.row == 1) {
            HGCouponSelectViewController *couponSelect = [[HGCouponSelectViewController alloc] init];
            couponSelect.order_id = _order_id;
            couponSelect.order_type = _type;
            couponSelect.coupon_type = @"system";
            couponSelect.coupon_id = _systemCouponData[@"had_id"];
            couponSelect.couponBlock = ^(HGCouponModel *couponModel) {
                _systemCouponData = @{@"discount": couponModel.discount,
                                      @"had_id": couponModel.had_id,
                                      @"order_money": couponModel.order_money};
                [self getOrderInfo];
            };
            [self.navigationController pushViewController:couponSelect animated:YES];
        }else if (indexPath.row == 2) {
            _useScorePay = !_useScorePay;
            if (_useScorePay) {
                _scorePayPayBtn.image = [UIImage imageNamed:@"tickGreen"];
            }else {
                _scorePayPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
            [self getOrderInfo];
        }else if (indexPath.row == 3) {
            _useMerchantPay = !_useMerchantPay;
            if (_useMerchantPay) {
                _memberPayBtn.image = [UIImage imageNamed:@"tickGreen"];
            }else {
                _memberPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
            [self getOrderInfo];
        }else if (indexPath.row == 4) {
            _useBalancePay = !_useBalancePay;
            if (_useBalancePay) {
                _balancePayBtn.image = [UIImage imageNamed:@"tickGreen"];
            }else {
                _balancePayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
            [self getOrderInfo];
        }
    }else if(indexPath.section == 2) {
        if (indexPath.row == 0) {
            
            _useWeixinPay = YES;
            if (_useWeixinPay) {
                //选择微信
                _weixinPayBtn.image = [UIImage imageNamed:@"tickGreen"];
                _useAliPay = NO;
                //取消支付宝
                _aliPayBtn.image = [UIImage imageNamed:@"tickGray"];
                
            }else {
                _weixinPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
            
            
        }else {
            
            _useAliPay = YES;
            if (_useAliPay) {
                //选择支付宝
                _aliPayBtn.image = [UIImage imageNamed:@"tickGreen"];
                //取消微信支付
                _useWeixinPay = NO;
                _weixinPayBtn.image = [UIImage imageNamed:@"tickGray"];
                
            }else {
                _aliPayBtn.image = [UIImage imageNamed:@"tickGray"];
            }
            
            
        }
    }
}




#pragma mark -- 获取订单信息
- (void)getOrderInfo {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    NSMutableDictionary *body = [[NSMutableDictionary alloc] initWithDictionary:@{@"Device-Id": DEVICE_ID,
                             @"ticket": [UserDefaultManager ticket],
                             @"order_id": _order_id,
                             @"type": _type,
                             @"app_version": @"80",
                             @"use_sys_coupon": [NSString stringWithFormat:@"%d", _useSystemCoupon],
                             @"use_mer_coupon": [NSString stringWithFormat:@"%d", _useMerchantCoupon],
                             @"use_score": [NSString stringWithFormat:@"%d", _useScorePay],
                             @"use_merchant_money": [NSString stringWithFormat:@"%d", _useMerchantPay],
                             @"use_balance_money": [NSString stringWithFormat:@"%d", _useBalancePay]}];
    if ([_merchantCouponData[@"had_id"] length] > 0) {
        [body setObject:_merchantCouponData[@"had_id"] forKey:@"merchant_coupon_id"];
    }
    if ([_systemCouponData[@"had_id"] length] > 0) {
        [body setObject:_systemCouponData[@"had_id"] forKey:@"system_coupon_id"];
    }
    
    NSLog(@"%@", body);
    [manager POST:[NSString stringWithFormat:@"%@g=Appapi&c=Pay&a=check", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@", responseObject);
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            NSDictionary *result = responseObject[@"result"];
            
            _orderModel = [[HGAppointOrderModel alloc] initWithDic:result];
            _systemCouponData = _orderModel.system_coupon;
            _merchantCouponData = _orderModel.mer_coupon;
            
            [_tableView reloadData];
            NSLog(@"%@", [_orderModel description]);

            _needPayMoney = [_orderModel.pay_config[@"online_pay"] floatValue];
            NSLog(@"needPayMoney:%f", _needPayMoney);
            [_tableView reloadData];
            _needPayLabel.text = [NSString stringWithFormat:@"还需支付：%.2f", _needPayMoney];
            
        }else {
            
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
            
        }
        [hud hideAnimated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [hud hideAnimated:YES];
        [MBProgressHUD showError:error.description];
    }];
    
}

#pragma mark -- 确定订单信息是否正确，并获取长订单号
- (void)goPay {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    NSMutableDictionary *body = [[NSMutableDictionary alloc] initWithDictionary:@{@"Device-Id": DEVICE_ID,
                             @"ticket": [UserDefaultManager ticket],
                             @"order_id": _order_id,
                             @"order_type": _type,
                             @"app_version": @"80",
                             @"use_sys_coupon": [NSString stringWithFormat:@"%d", _useSystemCoupon],
                             @"use_mer_coupon": [NSString stringWithFormat:@"%d", _useMerchantCoupon],
                             @"use_score": [NSString stringWithFormat:@"%d", _useScorePay],
                             @"use_merchant_money": [NSString  stringWithFormat:@"%d", _useMerchantPay],
                             @"use_balance_money": [NSString stringWithFormat:@"%d", _useBalancePay]}];
    if ([_merchantCouponData[@"had_id"] length] > 0) {
        [body setObject:_merchantCouponData[@"had_id"] forKey:@"card_id"];
    }
    if ([_systemCouponData[@"had_id"] length] > 0) {
        [body setObject:_systemCouponData[@"had_id"] forKey:@"coupon_id"];
    }
    if (_needPayMoney > 0) {
        if (_useWeixinPay) {
            [body setObject:@"weixin" forKey:@"pay_type"];
        }else {
            [body setObject:@"alipay" forKey:@"pay_type"];
        }
    }
    NSLog(@"body:%@", body);
    
    
    [manager POST:[NSString stringWithFormat:@"%@g=Appapi&c=Pay&a=go_pay", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@", responseObject);
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            NSDictionary *result = responseObject[@"result"];
            NSLog(@"result:%@", result);
            if (_needPayMoney > 0) {
                if (_useWeixinPay) {
                    _longOrderID = responseObject[@"result"];
                    [self weixinGoPay];
                }else {
                    _longOrderID = responseObject[@"result"];
                    [self alipayGoPay];
                }
            }else {
                
            }
        }else if ([errorCode isEqualToString:@"1"]) {
            [MBProgressHUD showSuccess:responseObject[@"errorMsg"]];
            NSString *url = [NSString stringWithFormat:@"http://www.nekahome.com/wap.php?g=Wap&c=My&a=appoint_order&order_id=%@", _order_id];
            HGOrderWebViewController *webView = [[HGOrderWebViewController alloc] init];
            webView.urlString = url;
            [self.navigationController pushViewController:webView animated:YES];
        }else {
            
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
            
        }
        [hud hideAnimated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [hud hideAnimated:YES];
        [MBProgressHUD showError:error.description];
    }];
    
}


#pragma mark -- 微信支付--统一下单

- (void)weixinGoPay {
    NSLog(@"微信支付");
    NSString *url = @"https://api.mch.weixin.qq.com/pay/unifiedorder";
    NSDictionary *tempBody =
    @{@"appid": @"wx27b5587b7b499c79",                     //应用ID
      @"mch_id": @"1448060602",                            //商户号
      @"nonce_str": [HGNoncestrTool noncestrWithLength:32],//随机字符串
      @"body": [NSString stringWithFormat:@"耐卡汽车平台-%@", _orderModel.order_info[@"order_name"]],                       //商品描述
      @"out_trade_no": [NSString stringWithFormat:@"%@_%@", _orderModel.order_info[@"order_type"], _longOrderID],                              //商户订单号
      @"total_fee": [NSString stringWithFormat:@"%.f", _needPayMoney * 100],                                 //总金额
      @"spbill_create_ip": [HGIPAddressTool getIPAddress:YES],               //终端IP
      @"notify_url": @"https://www.nekahome.com/source/appapi_weixin_notice.php", //通知地址
      @"trade_type": @"APP"                                     //交易类型
      };
    
    NSArray *sortedArr = [HGASCIISortTool sortedWithDictionary:tempBody];
    
    NSMutableString *tempStr = [NSMutableString new];
    for (int i = 0; i < sortedArr.count; i++) {
        if (i == 0) {
            [tempStr appendFormat:@"%@", [NSString stringWithFormat:@"%@=%@", sortedArr[i],  tempBody[sortedArr[i]]]];
        }else {
            [tempStr appendFormat:@"&%@", [NSString stringWithFormat:@"%@=%@", sortedArr[i],  tempBody[sortedArr[i]]]];
        }
    }
    [tempStr appendFormat:@"&key=0c55e7ad0af830214d1361813102e2e6"];
    NSLog(@"temp:%@", tempStr);
    NSString *signValue = [HGMD5Tool MD5ForUpper32Bate:tempStr];
    NSLog(@"sign:%@", signValue);
    
    NSMutableDictionary *body = [NSMutableDictionary dictionaryWithDictionary:tempBody];
    [body setObject:signValue forKey:@"sign"];
    NSLog(@"===%@", body);
    GDataXMLElement *xmlEle = [GDataXMLElement elementWithName:@"xml"];
    for (int i = 0; i < sortedArr.count; i++) {
        GDataXMLElement *childXml = [GDataXMLElement elementWithName:sortedArr[i] stringValue:tempBody[sortedArr[i]]];
        [xmlEle addChild:childXml];
    }
    GDataXMLElement *childXml = [GDataXMLElement elementWithName:@"sign" stringValue:signValue];
    [xmlEle addChild:childXml];
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithRootElement:xmlEle];
    NSLog(@"doc:%@", doc);
    NSData *data = [doc XMLData];
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"str:%@", str);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:10];
    request.HTTPMethod = @"POST";
    request.HTTPBody = data;
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:data encoding:NSUTF8StringEncoding error:nil];
        NSData *data1 = [doc XMLData];
        NSString *str = [[NSString alloc] initWithData:data1 encoding:NSUTF8StringEncoding];
        NSLog(@"xml:%@", str);
        
        [self praserXMLWithXML:doc];
    }];
    
    [task resume];
    
}

#pragma mark -- 微信--调用支付接口

- (void)praserXMLWithXML:(GDataXMLDocument *)xmlDoc {
    GDataXMLElement *rootEle = [xmlDoc rootElement];
    NSArray *array = [rootEle children];
    NSLog(@"%@", array);
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    for (int i = 0; i < array.count; i++) {
        GDataXMLElement *ele = [array objectAtIndex:i];
        NSLog(@"%@--%@", [ele name], [ele stringValue]);
        [dic setObject:[ele stringValue] forKey:[ele name]];
    }
    NSLog(@"%@", dic);
    
    if ([dic[@"return_code"] isEqualToString:@"SUCCESS"] && [dic[@"result_code"] isEqualToString:@"SUCCESS"]) {
        
        PayReq *request = [[PayReq alloc] init];
        request.partnerId = dic[@"mch_id"];
        request.prepayId = dic[@"prepay_id"];
        request.package = @"Sign=WXPay";
        request.nonceStr = dic[@"nonce_str"];
        NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
        request.timeStamp = (UInt32)interval;
        
        
        NSDictionary *tempDic = @{@"appid": WX_APPID, @"partnerid": request.partnerId, @"prepayid": request.prepayId, @"package": request.package, @"noncestr": request.nonceStr, @"timestamp": [NSString stringWithFormat:@"%d", request.timeStamp]};
        NSArray *sortedArr = [HGASCIISortTool sortedWithDictionary:tempDic];
        NSMutableString *tempStr = [NSMutableString new];
        for (int i = 0; i < sortedArr.count; i++) {
            if (i == 0) {
                [tempStr appendFormat:@"%@", [NSString stringWithFormat:@"%@=%@", sortedArr[i],  tempDic[sortedArr[i]]]];
            }else {
                [tempStr appendFormat:@"&%@", [NSString stringWithFormat:@"%@=%@", sortedArr[i],  tempDic[sortedArr[i]]]];
            }
        }
        [tempStr appendFormat:@"&key=0c55e7ad0af830214d1361813102e2e6"];
        NSLog(@"temp:%@", tempStr);
        NSString *signValue = [HGMD5Tool MD5ForUpper32Bate:tempStr];
        NSLog(@"sign:%@", signValue);
        
        
        
        request.sign = signValue;
        
        [WXApi sendReq:request];
    }else {
        if (dic[@"err_code_des"]) {
            NSLog(@"错误原因:%@", dic[@"err_code_des"]);
        }else {
            NSLog(@"%@", dic[@"return_msg"]);
        }
        
    }
    
    
}


#pragma mark -- 注册通知
- (void)registerNotifications {
    //支付成功
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySuccessAction:) name:@"PaySuccess" object:nil];
    //支付出错
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payErrorAction:) name:@"PayError" object:nil];
    //支付取消
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payCancelAction:) name:@"PayCancel" object:nil];
    
}

#pragma mark -- 支付成功事件
- (void)paySuccessAction:(NSNotification *)noti {
    
    NSString *payType = noti.userInfo[@"pay_type"];
    if ([payType isEqualToString:@"weixin"]) {//微信支付成功
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSDictionary *body = @{@"Device-Id": DEVICE_ID,
                               @"ticket": [UserDefaultManager ticket],
                               @"order_id": _order_id,
                               @"order_type": _type,
                               @"pay_status": @"0"};
        [self showLoading];
        [manager POST:[NSString stringWithFormat:@"%@c=Pay&a=app_weixin_back", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            [self hideLoading];
            NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
            if ([errorCode isEqualToString:@"0"]) {
                NSLog(@"%@", responseObject);
                NSString *url = responseObject[@"result"];
                HGOrderWebViewController *webView = [[HGOrderWebViewController alloc] init];
                webView.urlString = url;
                [self.navigationController pushViewController:webView animated:YES];
            }else {
                [MBProgressHUD showError:responseObject[@"errorMsg"]];
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self hideLoading];
            [MBProgressHUD showError:error.description];
        }];
        
        
    }else {//支付宝支付成功
        NSString *alipayReturn = noti.userInfo[@"alipay_return"];
        NSLog(@"alipayReturn:%@", alipayReturn);
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSDictionary *body = @{@"Device-Id": DEVICE_ID,
                               @"ticket": [UserDefaultManager ticket],
                               @"order_id": _order_id,
                               @"order_type": _orderModel.order_info[@"order_type"],
                               @"pay_type": @"alipay",
                               @"alipay_return": alipayReturn};
        
        [self showLoading];
        [manager POST:[NSString stringWithFormat:@"%@c=Pay&a=app_alipay_back", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"%@", responseObject);
            NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
            if ([errorCode isEqualToString:@"0"]) {
                NSString *url = responseObject[@"result"][@"url"];
                HGOrderWebViewController *webView = [[HGOrderWebViewController alloc] init];
                webView.urlString = url;
                [self.navigationController pushViewController:webView animated:YES];
            }else {
                [MBProgressHUD showError:responseObject[@"errorMsg"]];
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self hideLoading];
            [MBProgressHUD showError:error.description];
        }];
        
    }
    
}

#pragma mark -- 支付错误事件
- (void)payErrorAction:(NSNotification *)noti {
    NSLog(@"支付错误");
    
}

#pragma mark -- 取消支付事件
- (void)payCancelAction:(NSNotification *)noti {
    NSLog(@"取消支付");
}

- (void)alipayGoPay {
    //公共参数
    NSString *partnerID = @"2088421249688882";
    NSString *sellerID = @"453813097@qq.com";
    NSString *notify_url = @"https://www.nekahome.com/source/appapi_alipay_notice.php";
    Order *order = [[Order alloc] init];
    order.partner = partnerID;
    order.sellerID = sellerID;
    order.outTradeNO = [NSString stringWithFormat:@"%@_%@", _orderModel.order_info[@"order_type"], _longOrderID];
    order.subject = _orderModel.order_info[@"order_name"];
    order.body = @"无";
    order.totalFee = [NSString stringWithFormat:@"%.2f", _needPayMoney];
    order.notifyURL = notify_url;
    order.service = @"mobile.securitypay.pay";
    order.paymentType = @"1";
    order.inputCharset = @"utf-8";
    order.itBPay = @"30m";
    order.showURL = @"m.alipay.com";
    
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [order description];
    NSLog(@"orderSpec = %@",orderSpec);
    //签名
    id<DataSigner> signer = CreateRSADataSigner(AlipayPrivateKey);
    NSString *signedString = [signer signString:orderSpec];
    NSLog(@"signedString = %@",signedString);
    NSString *appScheme = @"nekaAlipay";
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            NSLog(@"reslut = %@",resultDic);
        }];
    }
    
}



- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"PaySuccess" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"PayError" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"PayError" object:nil];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
