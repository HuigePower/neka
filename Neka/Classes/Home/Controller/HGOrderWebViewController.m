//
//  HGAppointWebViewController.m
//  Neka
//
//  Created by ma c on 2017/7/6.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGOrderWebViewController.h"
#import "HGConfirmAppointOrderViewController.h"
#import "HGConfirmGroupOrderViewController.h"
#import "HGConfirmRechargeOrderViewController.h"
#import "LoginViewController.h"
#import <JSHAREService.h>
@interface HGOrderWebViewController ()

@end

@implementation HGOrderWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self recreateWebView];
}



- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    self.lastURL = request.URL;
    NSLog(@"-->%s", __func__);
    NSURL *url = request.URL;
    NSLog(@"url:%@", url);
    
//    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
//    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    NSString *scheme = url.scheme;
//    pigcmso2o://gopay/appoint/528
    
    if ([scheme isEqualToString:@"pigcmso2o"]) {
        self.currentURL = url.absoluteString;
        NSString *host = url.host;
        NSLog(@"%@", url.host);
        if ([url.host isEqualToString:@"login"]) {
            NSLog(@"goLogin");
            self.webView = nil;
            LoginViewController *login = [[LoginViewController alloc] init];
            [self.navigationController pushViewController:login animated:YES];
            
        }else if ([url.host isEqualToString:@"getLocation"]) {
            NSLog(@"getLocation");
//            NSDictionary *d = bm
            NSString *jsStr = [NSString stringWithFormat:@"callbackLocation('%@,%@')", LONGITUDE, LATITUDE];
            [webView stringByEvaluatingJavaScriptFromString:jsStr];
            
        }else {
            NSArray *pathItems = [url.path componentsSeparatedByString:@"/"];
            NSLog(@"paths:%@", pathItems);
            NSString *item1 = pathItems[1];
            NSString *item2 = pathItems[2];
            
            if ([host isEqualToString:@"gopay"]) {
                [self gopayWithType:item1 andOrderId:item2];
            }else {
                NSLog(@"---------------\n\n%@\n\n---------------------", url.host);
            }
        }
    }

    return YES;
}



- (void)gopayWithType:(NSString *)type andOrderId:(NSString *)orderId {
    NSLog(@"支付用途:%@, 订单id:%@", type, orderId);
    if ([type isEqualToString:@"appoint"] || [type isEqualToString:@"balance-appoint"]) {
        HGConfirmAppointOrderViewController *confirmOrder = [[HGConfirmAppointOrderViewController alloc] init];
        confirmOrder.type = type;
        confirmOrder.order_id = orderId;
        [self.navigationController pushViewController:confirmOrder animated:YES];
    }else if ([type isEqualToString:@"group"]) {
        HGConfirmGroupOrderViewController *confirmOrder = [[HGConfirmGroupOrderViewController alloc] init];
        confirmOrder.type = type;
        confirmOrder.order_id = orderId;
        [self.navigationController pushViewController:confirmOrder animated:YES];
    }else if ([type isEqualToString:@"recharge"]) {
        HGConfirmRechargeOrderViewController *confirmOrder = [[HGConfirmRechargeOrderViewController alloc] init];
        confirmOrder.type = type;
        confirmOrder.order_id = orderId;
        [self.navigationController pushViewController:confirmOrder animated:YES];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
