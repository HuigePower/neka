//
//  ExpressenViewController.m
//  Neka1.0
//
//  Created by ma c on 16/8/27.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "ExpressenViewController.h"
#import "WHHTabbarView.h"
#import "HGOrderWebViewController.h"
#import "HGExpressenModel.h"
#import "HGExpressenCell.h"
@interface ExpressenViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>
@property (nonatomic, strong)NSArray *titleArr;
@property (nonatomic, strong)WHHTabbarView *tabbarView;
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)UIScrollView *scrollView;
@property (nonatomic, strong)NSArray *catList;  //分类列表
@property (nonatomic, strong)NSMutableArray *newsList; //子分类列表
@property (nonatomic, copy)NSString *catID;
@property (nonatomic, assign)NSInteger totalPage;
@property (nonatomic, assign)NSInteger currentPage;

@property (nonatomic, strong)MJRefreshNormalHeader *header;
@property (nonatomic, strong)MJRefreshBackNormalFooter *footer;
@property (nonatomic, assign)BOOL isShowHud;
@property (nonatomic, assign)BOOL isLoading;
@property (nonatomic, assign)BOOL isRefresh;
@end

@implementation ExpressenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"平台快报";
    [self.tabBarController.tabBar setHidden:YES];
    
    _catID = @"1";
    _totalPage = 1;
    _currentPage = 1;
    _isShowHud = YES;
    _isLoading = NO;
    _newsList = [NSMutableArray new];
    [self requestCatList];
    [self requestNewsList];
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.view addSubview:self.tabbarView];
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, SCREEN_SIZE.width, SCREEN_SIZE.height - 64 - 40)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    
    _header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refresh)];
    _footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadingMore)];
    _tableView.mj_header = _header;
    _tableView.mj_footer = _footer;

    
    
    [self.view addSubview:_tableView];
    [_tableView registerClass:[HGExpressenCell class] forCellReuseIdentifier:@"myCell"];
    

    
}


- (void)refresh {
    _totalPage = 1;
    _currentPage = 1;
    _isShowHud = NO;
    _isRefresh = YES;
    [self requestNewsList];
}

- (void)loadingMore {
    if (_currentPage < _totalPage) {
        _currentPage ++;
        _isShowHud = NO;
        [self requestNewsList];
    }else {
        [_footer setState:MJRefreshStateNoMoreData];
    }
}


- (void)requestCatList {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@g=Appapi&c=Appnews&a=index", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            _catList = [[NSArray alloc] init];
            _catList = responseObject[@"result"][@"category"];
            [self.tabbarView setCatList:_catList];
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [MBProgressHUD showError:error.description];
    }];

}


- (void)requestNewsList {
    if (_isLoading) {
        return;
    }
    _isLoading = YES;
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *body = @{@"Device-Id": DEVICE_ID, @"cat_id": _catID, @"page": [NSString stringWithFormat:@"%ld", _currentPage]};
    [self showLoading];
    [manager POST:[NSString stringWithFormat:@"%@g=Appapi&c=Appnews&a=news", BASE_URL] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoading];
        NSString *errorCode = [NSString stringWithFormat:@"%@", responseObject[@"errorCode"]];
        if ([errorCode isEqualToString:@"0"]) {
            NSArray *array = responseObject[@"result"][@"news_list"];
            NSMutableArray *tmp = [NSMutableArray new];
            for (NSDictionary *dic in array) {
                HGExpressenModel *model = [[HGExpressenModel alloc] initWithDict:dic];
                [tmp addObject:model];
            }
            if (_isRefresh) {
                [_newsList removeAllObjects];
                _isRefresh = NO;
            }
            [_newsList addObjectsFromArray:tmp];
            _totalPage = [responseObject[@"result"][@"totalPage"] integerValue];
            [_tableView reloadData];
        }else {
            [MBProgressHUD showError:responseObject[@"errorMsg"]];
        }
        [self hideLoading];
        [_header endRefreshing];
        [_footer endRefreshing];
        _isLoading = NO;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideLoading];
        [_header endRefreshing];
        [_footer endRefreshing];
        
        [MBProgressHUD showError:error.description];
        _isLoading = NO;
        
    }];
}

- (WHHTabbarView *)tabbarView {
    if (_tabbarView == nil) {
        _tabbarView = [[WHHTabbarView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 40)];
        __weak ExpressenViewController *weafSelf = self;
        __weak NSMutableArray *weakNewsList = _newsList;
        _tabbarView.result = ^(NSString *catID) {
            _isShowHud = YES;
            [weakNewsList removeAllObjects];
            NSLog(@"catID:%@", catID);
            _catID = catID;
            _currentPage = 1;
            [weafSelf requestNewsList];
        };
    }
    return _tabbarView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _newsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"myCell";
    self.automaticallyAdjustsScrollViewInsets = NO;
    HGExpressenCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    HGExpressenModel *model = _newsList[indexPath.row];
    [cell makeSubviewsWithModel:model];

    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    HGExpressenModel *model = _newsList[indexPath.row];
    if (model.content_img.count == 0) {
        return KWIDTH(65);
    }else if (model.content_img.count == 1) {
        return KWIDTH(70);
    }else {
        return KWIDTH(110);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HGExpressenModel *model = _newsList[indexPath.row];
    HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
    web.urlString = model.url;
    [self.navigationController pushViewController:web animated:YES];
}

#pragma mark -- cell的分割线宽度等于屏幕宽度
-(void)viewDidLayoutSubviews {
    
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)])  {
        [_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
