//
//  HGNoncestrTool.h
//  MD5
//
//  Created by ma c on 2017/6/24.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGNoncestrTool : NSObject

//随机生成length位字符串
+ (NSString *)noncestrWithLength:(NSInteger)length;

@end
