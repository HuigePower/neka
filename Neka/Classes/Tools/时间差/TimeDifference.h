//
//  TimeDifference.h
//  SuperDancer
//
//  Created by ma c on 2017/10/17.
//  Copyright © 2017年 yu. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <UIKit/UIKit.h>
@interface TimeDifference : NSObject
+ (NSString *)timeDifferenceWithTimeStr:(NSString *)timeStr;

@end
