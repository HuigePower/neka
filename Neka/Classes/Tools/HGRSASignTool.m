//
//  HGRSASignTool.m
//  AlipayDemo3
//
//  Created by ma c on 2017/8/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGRSASignTool.h"
#import "openssl_wrapper.h"
#import "NSDataEx.h"
#import <AFNetworking.h>

@implementation HGRSASignTool


+ (NSString *)signString:(NSString *)string withPath:(NSString *)path {
    
    
    path = [[NSBundle mainBundle] pathForResource:@"app_private_key" ofType:@"pem"];
    
    NSString *signedString = nil;
    const char *message = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int messageLength = (int)strlen(message);
    unsigned char *sig = (unsigned char *)malloc(256);
    unsigned int sig_len;
    int ret = rsa_sign_with_private_key_pem((char *)message, messageLength, sig, &sig_len, (char *)[path UTF8String], NO);
    //签名成功,需要给签名字符串base64编码和UrlEncode,该两个方法也可以根据情况替换为自己函数
    if (ret == 1) {
        NSString * base64String = base64StringFromData([NSData dataWithBytes:sig length:sig_len]);
        //NSData * UTF8Data = [base64String dataUsingEncoding:NSUTF8StringEncoding];
        signedString = [self urlEncodedString:base64String];
        
    }
    
    free(sig);
    return signedString;
    
    
    

}








+ (NSString*)urlEncodedString:(NSString *)string
{
    NSString * encodedString = (__bridge_transfer  NSString*) CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)string, NULL, (__bridge CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 );
    
    return encodedString;
}

@end
