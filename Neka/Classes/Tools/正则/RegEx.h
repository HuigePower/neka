//
//  RegEx.h
//  NEKA
//
//  Created by ma c on 16/4/26.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegEx : NSObject
/**
 *  验证手机号格式
 *
 *  @param mobileNum 手机号
 *
 *  @return YES/NO
 */
+ (BOOL)isMobileNumber:(NSString *)mobileNum;
/**
 *  验证密码格式
 *
 *  @param mobileNum 密码
 *
 *  @return YES/NO
 */
+ (BOOL)isCorrectPassword:(NSString *)password;
/**
 *  验证验证码格式
 *
 *  @param mobileNum 验证码
 *
 *  @return YES/NO
 */
+ (BOOL)isCorrectVerificationCode:(NSString *)VerificationCode;

/**
 *  验证身份证号格式
 *
 *  @param identityCardNumber 身份证号
 *
 *  @return YES/NO
 */
+ (BOOL)isCorrectIdentityCardNumber:(NSString *)identityCardNumber;

/**
 *  验证真实姓名
 *
 *  @param realName 真实姓名
 *
 *  @return YES/NO
 */
+ (BOOL)isCorrectRealName:(NSString *)realName;


/**
 *  验证车牌号
 *
 *  @param carNumber 车牌号
 *
 *  @return YES/NO
 */
+ (BOOL)isCorrectCarNumber:(NSString *)carNumber;

/**
 *  验证昵称
 *
 *  @param nickname 昵称
 *
 *  @return YES/NO
 */
+ (BOOL)isCorrectNickname:(NSString *)nickname;


/**
 *  判断单个字符串是否为字母
 *
 *  @param nickname 字符串
 *
 *  @return YES/NO
 */
+ (BOOL)isCharacter:(NSString *)str;





@end

