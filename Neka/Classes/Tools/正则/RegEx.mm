//
//  RegEx.m
//  NEKA
//
//  Created by ma c on 16/4/26.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "RegEx.h"

@implementation RegEx

+ (BOOL)isMobileNumber:(NSString *)mobileNum
{
    if (mobileNum.length != 11)
    {
        return NO;
    }
    /**
     * 手机号码:
     * 13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 17[6, 7, 8], 18[0-9], 170[0-9]
     * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
     * 联通号段: 130,131,132,155,156,185,186,145,176,1709
     * 电信号段: 133,153,180,181,189,177,1700
     */
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|70)\\d{8}$";
    
    /**
     * 中国移动：China Mobile
     * 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
     */
    NSString *CM = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\\d{8}$)|(^1705\\d{7}$)";
    /**
     * 中国联通：China Unicom
     * 130,131,132,155,156,185,186,145,176,1709
     */
    NSString *CU = @"(^1(3[0-2]|4[5]|5[56]|7[6]|8[56])\\d{8}$)|(^1709\\d{7}$)";
    /**
     * 中国电信：China Telecom
     * 133,153,180,181,189,177,1700
     */
    NSString *CT = @"(^1(33|53|77|8[019])\\d{8}$)|(^1700\\d{7}$)";
    
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+ (BOOL)isCorrectPassword:(NSString *)password {
    //可输入数字/字母/下划线
    NSString *PW = @"^([0-9]|[a-zA-Z]|[_]){6,18}$";
    NSPredicate *regextestPassword = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PW];
    
    if ([regextestPassword evaluateWithObject:password]) {
        return YES;
    }else {
        return NO;
    }
    
    
}


+ (BOOL)isCorrectVerificationCode:(NSString *)VerificationCode {
    NSString *vfCode = @"^[0-9]{4}$";
    NSPredicate *regextestVfCode = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", vfCode];
    
    if ([regextestVfCode evaluateWithObject:VerificationCode]) {
        return YES;
    }else {
        return NO;
    }
}


+ (BOOL)isCorrectIdentityCardNumber:(NSString *)identityCardNumber {
    NSString *cardNO = @"^([0-9]{17})([0-9]|x|X)$";
    NSPredicate *regextestCardNO = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cardNO];
    
    if ([regextestCardNO evaluateWithObject:identityCardNumber]) {
        return YES;
    }else {
        return NO;
    }
}


+ (BOOL)isCorrectRealName:(NSString *)realName {
    NSString *name = @"^[\u4e00-\u9fa5]{3,5}$";
    NSPredicate *regexTestRealName = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", name];
    if ([regexTestRealName evaluateWithObject:realName]) {
        return YES;
    }else {
        return NO;
    }
}



+ (BOOL)isCorrectCarNumber:(NSString *)carNumber {
    NSString *format = @"^[\u4E00-\u9FA5][A-Z][A-Z_0-9]{5}$";
    NSPredicate *regexTestCarNO = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", format];
    if ([regexTestCarNO evaluateWithObject:carNumber]) {
        return YES;
    }else {
        return NO;
    }

}



+ (BOOL)isCorrectNickname:(NSString *)nickname {
    NSString *format = @"^[\u4E00-\u9FA5a-zA-Z](.){1,15}$";
    NSPredicate *regexTestNickname = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", format];
    if ([regexTestNickname evaluateWithObject:nickname]) {
        return YES;
    }else {
        return NO;
    }
}


+ (BOOL)isCharacter:(NSString *)str {
    NSString *regex = @"[a-zA-Z]*";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:str];
}








@end
