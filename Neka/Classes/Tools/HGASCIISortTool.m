//
//  HGASCIISortTool.m
//  MD5
//
//  Created by ma c on 2017/6/24.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGASCIISortTool.h"

@implementation HGASCIISortTool

//ASCII从小到大排序
+ (NSArray *)sortedWithArray:(NSArray *)array {
    
    NSStringCompareOptions comp = NSCaseInsensitiveSearch | NSNumericSearch | NSWidthInsensitiveSearch | NSForcedOrderingSearch;
    NSComparator sort = ^(NSString *obj1, NSString *obj2) {
        NSRange range = NSMakeRange(0, obj1.length);
        return [obj1 compare:obj2 options:comp range:range];
    };
    
    NSArray *resultArray = [array sortedArrayUsingComparator:sort];
    return resultArray;
}


+ (NSArray *)sortedWithDictionary:(NSDictionary *)dic {
    
    NSArray *arr = [dic allKeys];
    NSStringCompareOptions comp = NSCaseInsensitiveSearch | NSNumericSearch | NSWidthInsensitiveSearch | NSForcedOrderingSearch;
    NSComparator sort = ^(NSString *obj1, NSString *obj2) {
        NSRange range = NSMakeRange(0, obj1.length);
        return [obj1 compare:obj2 options:comp range:range];
    };
    
    NSArray *resultArray = [arr sortedArrayUsingComparator:sort];
    return resultArray;
}


@end
