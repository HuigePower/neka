//
//  HGNoncestrTool.m
//  MD5
//
//  Created by ma c on 2017/6/24.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGNoncestrTool.h"

@implementation HGNoncestrTool

//随机生成length位字符串
+ (NSString *)noncestrWithLength:(NSInteger)length {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [[NSMutableString alloc] init];
    for (NSInteger i = 0; i < 32; i++) {
        [randomString appendFormat:@"%C", [letters characterAtIndex: arc4random() % letters.length]];
    }
    return randomString;
    
}
@end
