//
//  HGRSASignTool.h
//  AlipayDemo3
//
//  Created by ma c on 2017/8/3.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGRSASignTool : NSObject

+ (NSString *)signString:(NSString *)string withPath:(NSString *)path;

@end
