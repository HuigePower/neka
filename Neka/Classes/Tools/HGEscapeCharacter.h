//
//  HGEscapeCharacter.h
//  Neka
//
//  Created by ma c on 2017/8/17.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGEscapeCharacter : NSObject
+ (NSString *)HTML:(NSString *)html;
@end
