//
//  HGIPAddressTool.h
//  XMLDemo
//
//  Created by ma c on 2017/6/30.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGIPAddressTool : NSObject
+ (NSString *)getIPAddress:(BOOL)preferIPv4;
@end
