//
//  RecordVoice.m
//  NEKA
//
//  Created by ma c on 16/5/20.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import "RecordVoice.h"

@implementation RecordVoice
{
    int _duration;
}
- (instancetype)initWithPath:(NSString *)strUrl displayName:(NSString *)displayName {
    if (self = [super init]) {
        _urlStr = strUrl;
        NSLog(@"%@", strUrl);
        _displayName = displayName;
        [self setAudioSession];
    }
    return self;
}

//设置音频会话
- (void)setAudioSession {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    if ([audioSession respondsToSelector:@selector(requestRecordPermission:)]) {
        [audioSession requestRecordPermission:^(BOOL available) {
            
            if (available) {
                NSLog(@"有权限");
            }else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[UIAlertView alloc] initWithTitle:@"无法录音" message:@"请在“设置-隐私-麦克风”选项中允许xx访问你的麦克风" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil] show];
                    return;
                });
            }
        }];
    }
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [audioSession setActive:YES error:nil];
    [self record];
}

//获得录音对象
- (AVAudioRecorder *)audioRecorder {
    if (!_audioRecorder) {
        //创建录音文件保存路径
        NSURL *url = [NSURL fileURLWithPath:_urlStr];
        //创建录音格式设置
        NSDictionary *setting = [self getAudioSetting];
        //创建录音机
        NSError *error = nil;
        _audioRecorder = [[AVAudioRecorder alloc] initWithURL:url settings:setting error:&error];
        _audioRecorder.delegate = self;
        _audioRecorder.meteringEnabled = YES;//如果要监控声波必须设置为YES
        if (error) {
            NSLog(@"创建录音对象是发生错误--%@", error.description);
            return nil;
        }
    }
    return _audioRecorder;
}


//取得录音文件设置
- (NSDictionary *)getAudioSetting {
    NSMutableDictionary *dicM = [NSMutableDictionary dictionary];
    //设置录音格式
    [dicM setObject:@(kAudioFormatLinearPCM) forKey:AVFormatIDKey];
    //设置录音采样率,8000是电话采样率,对于一般录音已经够了
    [dicM setObject:@(8000) forKey:AVSampleRateKey];
    //设置通道.这里采用单声道
    [dicM setObject:@(1) forKey:AVNumberOfChannelsKey];
    //每个采样点位数,分为8,16,24,32
    [dicM setObject:@(8) forKey:AVLinearPCMBitDepthKey];
    //是否使用浮点数采样
    [dicM setObject:@(YES) forKey:AVLinearPCMIsFloatKey];
    return dicM;
}

- (NSTimer *)timer {
    if (!_timer) {
        NSLog(@"开始计时");
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(recordTime) userInfo:nil repeats:YES];
        _duration = 0;
    }
    return _timer;
    
}

- (void)recordTime {
    NSLog(@"+1");
    _duration = _duration + 1;
    NSLog(@"%d", _duration);
}

//录音
- (void)record {
    NSLog(@"开始");
    if (![self.audioRecorder isRecording]) {
        [self.audioRecorder record];
        [self.timer setFireDate:[NSDate distantPast]];
    }
}

//停止
- (void)stop {
    NSLog(@"停止");
    [self.audioRecorder stop];
    [self.timer setFireDate:[NSDate distantFuture]];

}
//录音完成,
- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
    
    NSLog(@"录音完成!");

    if (_recoredFinished) {
        self.recoredFinished(_urlStr, _displayName,_duration);
    }
    _duration = 1;
}

- (void)dealloc {
    [_timer invalidate];
    _timer = nil;
}

@end
