//
//  RecordVoice.h
//  NEKA
//
//  Created by ma c on 16/5/20.
//  Copyright © 2016年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
typedef void(^RecordFinished)(NSString *urlStr, NSString *displayName, int durantion);
@interface RecordVoice : NSObject<AVAudioRecorderDelegate>
@property (nonatomic, strong)AVAudioRecorder *audioRecorder;
@property (nonatomic, strong)AVAudioPlayer *audioPlayer;
@property (nonatomic, strong)NSString *urlStr;
@property (nonatomic, strong)NSString *displayName;
@property (nonatomic, strong)NSTimer *timer;
@property (nonatomic, copy)RecordFinished recoredFinished;



- (instancetype)initWithPath:(NSString *)strUrl displayName:(NSString *)displayName;
//录音
- (void)record;
//停止
- (void)stop;


@end
