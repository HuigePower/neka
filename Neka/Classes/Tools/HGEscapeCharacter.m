//
//  HGEscapeCharacter.m
//  Neka
//
//  Created by ma c on 2017/8/17.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "HGEscapeCharacter.h"

@implementation HGEscapeCharacter


+ (NSString *)HTML:(NSString *)html {
    NSScanner *theScaner = [NSScanner scannerWithString:html];
    NSDictionary *dict = @{@"&amp;": @"&",
                           @"&lt;": @"<",
                           @"&gt": @">",
                           @"&nbsp;": @"",
                           @"&quot;": @"\"",
                           @"width": @"wid"
                           };
    NSArray *array = @[@"&amp;", @"&lt;", @"&gt", @"&nbsp;", @"&quot;", @"&quot;"];
    while ([theScaner isAtEnd] == NO) {
        for (int i = 0; i < array.count; i++) {
            [theScaner scanUpToString:array[i] intoString:NULL];
            html = [html stringByReplacingOccurrencesOfString:dict[array[i]] withString:array[i]];
            
        }
    }
    return html;
}


@end
