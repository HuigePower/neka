//
//  HGASCIISortTool.h
//  MD5
//
//  Created by ma c on 2017/6/24.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGASCIISortTool : NSObject

//ASCII从小到大排序
+ (NSArray *)sortedWithArray:(NSArray *)array;
+ (NSArray *)sortedWithDictionary:(NSDictionary *)dic;
@end
