//
//  KPViewController.m
//  Neka
//
//  Created by ma c on 2017/8/14.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "KPViewController.h"
#import "HGCarouselView.h"
#import "CustomTableViewCell.h"
#import "HGOrderWebViewController.h"
@interface KPViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)HGCarouselView *carouselView;
@end

@implementation KPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.title = @"服务快派首页";
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 70) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    [_tableView registerClass:[CustomTableViewCell class] forCellReuseIdentifier:@"cell"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    }else {
        return 4;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell) {
        [cell deleteSubviews];
    }
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [cell addCustomSubview:self.carouselView];
        }else if (indexPath.row == 1) {
            UIImageView *leftLine = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH_6S(25), KWIDTH_6S(20), KWIDTH_6S(80), KWIDTH_6S(20))];
            leftLine.image = [UIImage imageNamed:@"blue_line"];
            leftLine.contentMode = UIViewContentModeScaleAspectFit;
            [cell addCustomSubview:leftLine];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(130), KWIDTH_6S(20), KWIDTH_6S(115), KWIDTH_6S(20))];
            label.text = @"发布需求流程示意图";
            label.textAlignment = NSTextAlignmentCenter;
            label.font = [UIFont systemFontOfSize:12];
            label.textColor = [UIColor colorWithHexString:@"#007ede"];
            [cell addCustomSubview:label];
            
            UIImageView *rightLine = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH_6S(105), KWIDTH_6S(20), KWIDTH_6S(80), KWIDTH_6S(20))];
            rightLine.image = [UIImage imageNamed:@"blue_line"];
            rightLine.contentMode = UIViewContentModeScaleAspectFit;
            [cell addCustomSubview:rightLine];
            
            UIImageView *flowChartView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH_6S(25), KWIDTH_6S(60), SCREEN_SIZE.width - KWIDTH_6S(50), KWIDTH_6S(40))];
            flowChartView.image = [UIImage imageNamed:@"flowchart"];
            flowChartView.contentMode = UIViewContentModeScaleAspectFit;
            [cell addCustomSubview:flowChartView];
            
        }else if (indexPath.row == 2) {
            
            UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(120))];
            bg.backgroundColor = [UIColor colorWithHexString:@"#f4f3f9"];
            [cell addCustomSubview:bg];
            
            CGFloat width = (SCREEN_SIZE.width - KWIDTH_6S(60)) * 0.5;
            NSArray *titles = @[@"发布需求", @"需求订单"];
            NSArray *imgs = @[@"fbxq", @"xqdd"];
            NSArray *colors = @[[UIColor colorWithHexString:@"#007ede"], [UIColor colorWithHexString:@"#33a5fe"]];
            for (int i = 0; i < 2; i++) {
                UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH_6S(20) + (width + KWIDTH_6S(20)) * i, KWIDTH_6S(20), width, KWIDTH_6S(45))];
                btn.layer.masksToBounds = YES;
                btn.layer.cornerRadius = 5;
                btn.backgroundColor = colors[i];
                [cell addCustomSubview:btn];
                
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH_6S(10), KWIDTH_6S(7.5), KWIDTH_6S(45), KWIDTH_6S(30))];
                imageView.image = [UIImage imageNamed:imgs[i]];
                imageView.contentMode = UIViewContentModeScaleAspectFit;
                [btn addSubview:imageView];
                
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(55), 0, KWIDTH_6S(100), KWIDTH_6S(45))];
                label.text = titles[i];
                label.font = [UIFont systemFontOfSize:18];
                label.textColor = [UIColor whiteColor];
                [btn addSubview:label];
                [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
                btn.tag = 10 + i;
                
            }
            
            
            UIImageView *leftLine = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH_6S(20), KWIDTH_6S(85), KWIDTH_6S(80), KWIDTH_6S(20))];
            leftLine.image = [UIImage imageNamed:@"xuxian"];
            leftLine.contentMode = UIViewContentModeScaleAspectFit;
            [cell addCustomSubview:leftLine];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(100), KWIDTH_6S(85), KWIDTH_6S(175), KWIDTH_6S(20))];
            label.text = @"选择以下需求分类快捷发布";
            label.textAlignment = NSTextAlignmentCenter;
            label.font = [UIFont systemFontOfSize:12];
            label.textColor = [UIColor colorWithHexString:@"#666666"];
            [cell addCustomSubview:label];
            
            UIImageView *rightLine = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_SIZE.width - KWIDTH_6S(100), KWIDTH_6S(85), KWIDTH_6S(80), KWIDTH_6S(20))];
            rightLine.image = [UIImage imageNamed:@"xuxian"];
            rightLine.contentMode = UIViewContentModeScaleAspectFit;
            [cell addCustomSubview:rightLine];
            
        }
    }else {
        
        UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, KWIDTH_6S(5), SCREEN_SIZE.width, KWIDTH_6S(90))];
        bg.userInteractionEnabled = YES;
        bg.backgroundColor = [UIColor colorWithWhite:0.98 alpha:1];
        [cell addCustomSubview:bg];
        NSArray *titles = @[@"我要买车", @"我要用车", @"车辆年检", @"车辆保险"];
        NSArray *imgs = @[@"nianjian", @"nianjian", @"yongche", @"chexian"];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH_6S(20), KWIDTH_6S(5), KWIDTH_6S(60), KWIDTH_6S(60))];
        imageView.image = [UIImage imageNamed:imgs[indexPath.row]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [bg addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH_6S(20), KWIDTH_6S(60), KWIDTH_6S(60), KWIDTH_6S(20))];
        label.text = titles[indexPath.row];
        label.font = [UIFont systemFontOfSize:12];
        label.textAlignment = NSTextAlignmentCenter;
        [bg addSubview:label];
        
        if (indexPath.row == 0) {
            NSArray *titles = @[@">买宝马", @">买奔驰", @">买奥迪", @">买保时捷"];
            for (int i = 0; i < titles.count; i++) {
                UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH_6S(100) + KWIDTH_6S(100) * (i % 2), KWIDTH_6S(10) + (i / 2) * KWIDTH_6S(30), KWIDTH_6S(80), KWIDTH_6S(30))];
                [btn setTitle:titles[i] forState:UIControlStateNormal];
                btn.titleLabel.font = [UIFont systemFontOfSize:12];
                [btn setTitleColor:[UIColor colorWithHexString:@"#007ede"] forState:UIControlStateNormal];
                [btn sizeToFit];
                [btn addTarget:self action:@selector(serviceSelectAction:) forControlEvents:UIControlEventTouchUpInside];
                [bg addSubview:btn];
            }
            
            
        }else if (indexPath.row == 1) {
            NSArray *titles = @[@">用豪车", @">用车队"];
            for (int i = 0; i < titles.count; i++) {
                UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH_6S(100) + KWIDTH_6S(100) * (i % 2), KWIDTH_6S(10) + (i / 2) * KWIDTH_6S(30), KWIDTH_6S(80), KWIDTH_6S(30))];
                [btn setTitle:titles[i] forState:UIControlStateNormal];
                btn.titleLabel.font = [UIFont systemFontOfSize:12];
                [btn setTitleColor:[UIColor colorWithHexString:@"#007ede"] forState:UIControlStateNormal];
                [btn sizeToFit];
                [btn addTarget:self action:@selector(serviceSelectAction:) forControlEvents:UIControlEventTouchUpInside];
                [bg addSubview:btn];
            }
        }else if (indexPath.row == 2) {
            NSArray *titles = @[@">全程陪同", @">全程代办"];
            for (int i = 0; i < titles.count; i++) {
                UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH_6S(100) + KWIDTH_6S(100) * (i % 2), KWIDTH_6S(10) + (i / 2) * KWIDTH_6S(30), KWIDTH_6S(80), KWIDTH_6S(30))];
                [btn setTitle:titles[i] forState:UIControlStateNormal];
                btn.titleLabel.font = [UIFont systemFontOfSize:12];
                [btn setTitleColor:[UIColor colorWithHexString:@"#007ede"] forState:UIControlStateNormal];
                [btn sizeToFit];
                [btn addTarget:self action:@selector(serviceSelectAction:) forControlEvents:UIControlEventTouchUpInside];
                [bg addSubview:btn];
            }
        }else if (indexPath.row == 3) {
            NSArray *titles = @[@">新车保险", @">老车续保"];
            for (int i = 0; i < titles.count; i++) {
                UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH_6S(100) + KWIDTH_6S(100) * (i % 2), KWIDTH_6S(10) + (i / 2) * KWIDTH_6S(30), KWIDTH_6S(80), KWIDTH_6S(30))];
                [btn setTitle:titles[i] forState:UIControlStateNormal];
                btn.titleLabel.font = [UIFont systemFontOfSize:12];
                [btn setTitleColor:[UIColor colorWithHexString:@"#007ede"] forState:UIControlStateNormal];
                [btn sizeToFit];
                [btn addTarget:self action:@selector(serviceSelectAction:) forControlEvents:UIControlEventTouchUpInside];
                [bg addSubview:btn];
            }
        }
    }
    
   
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return KWIDTH_6S(120);
    }else {
        return KWIDTH_6S(100);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}




#pragma mark -- 轮播图
- (HGCarouselView *)carouselView {
    if (_carouselView == nil) {
        _carouselView = [[HGCarouselView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, KWIDTH_6S(120)) andPlaceholderImage:[UIImage imageNamed:@"kp_banner"]];
        [_carouselView setImagesWithNames:@[@"kp_banner"] showBottom:NO];
        __weak KPViewController *weakSelf = self;
        _carouselView.result = ^(NSInteger index) {
//            NSLog(@"index=%ld", index);
//            HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
//            web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=Service&a=index";
//            [weakSelf.navigationController pushViewController:web animated:YES];
        };
    }
    return _carouselView;
}


#pragma mark -- 点击事件
- (void)btnClick:(UIButton *)btn {
    if (btn.tag == 10) {
        HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
        web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=Service&a=cat_list";
        [self.navigationController pushViewController:web animated:YES];
    }else if (btn.tag == 11) {
        HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
        web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=Service&a=need_list";
        [self.navigationController pushViewController:web animated:YES];
    }
    NSLog(@"btn.tag=%ld", btn.tag);
}

- (void)serviceSelectAction:(UIButton *)btn {
    NSLog(@"%@", btn.titleLabel.text);
    if ([btn.titleLabel.text isEqualToString:@">买宝马"]) {
        HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
        web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=Service&a=cat_list&cid=44";
        [self.navigationController pushViewController:web animated:YES];
    }else if ([btn.titleLabel.text isEqualToString:@">买奔驰"]) {
        HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
        web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=Service&a=cat_list&cid=43";
        [self.navigationController pushViewController:web animated:YES];
    }else if ([btn.titleLabel.text isEqualToString:@">买奥迪"]) {
        HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
        web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=Service&a=cat_list&cid=45";
        [self.navigationController pushViewController:web animated:YES];
    }else if ([btn.titleLabel.text isEqualToString:@">买保时捷"]) {
        HGOrderWebViewController *web = [[HGOrderWebViewController alloc] init];
        web.urlString = @"http://www.nekahome.com/wap.php?g=Wap&c=Service&a=cat_list&cid=46";
        [self.navigationController pushViewController:web animated:YES];
    }else {
        [MBProgressHUD showError:@"功能正在完善中…"];
    }
//    }else if ([btn.titleLabel.text isEqualToString:@">用豪车"]) {
//        
//    }else if ([btn.titleLabel.text isEqualToString:@">用车队"]) {
//        
//    }else if ([btn.titleLabel.text isEqualToString:@">全程陪同"]) {
//        
//    }else if ([btn.titleLabel.text isEqualToString:@">全程代办"]) {
//        
//    }else if ([btn.titleLabel.text isEqualToString:@">新车保险"]) {
//        
//    }else if ([btn.titleLabel.text isEqualToString:@">老年续保"]) {
//        
//    }
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
