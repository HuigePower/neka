//
//  UIView+Border.h
//  Neka
//
//  Created by yu on 2017/12/6.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Border)

//使用UIEdgeInsets的(top, left, bottom, right)分别代表view上、左、下、右
- (void)setBorderTopRightLeftBottom:(UIEdgeInsets)borderWidths color:(UIColor *)color;
@end
