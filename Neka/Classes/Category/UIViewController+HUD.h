//
//  UIViewController+HUD.h
//  Neka
//
//  Created by ma c on 2017/11/5.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (HUD)
- (void)showLoading;

- (void)hideLoading;
@end
