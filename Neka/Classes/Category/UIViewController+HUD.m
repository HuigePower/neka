//
//  UIViewController+HUD.m
//  Neka
//
//  Created by ma c on 2017/11/5.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "UIViewController+HUD.h"

@implementation UIViewController (HUD)
- (void)showLoading {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)hideLoading {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
@end
