//
//  UIView+Border.m
//  Neka
//
//  Created by yu on 2017/12/6.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import "UIView+Border.h"

@implementation UIView (Border)


//使用UIEdgeInsets的(top, left, bottom, right)分别代表view上、左、下、右
- (void)setBorderTopRightLeftBottom:(UIEdgeInsets)borderWidths color:(UIColor *)color {
    
    CGFloat top = borderWidths.top;
    CGFloat right = borderWidths.right;
    CGFloat bottom = borderWidths.bottom;
    CGFloat left = borderWidths.left;
    
    //顶部边框
    if (top > 0) {
        CALayer *topLineLayer = [CALayer layer];
        topLineLayer.backgroundColor = color.CGColor;
        topLineLayer.frame = CGRectMake(0, 0, self.frame.size.width, top);
        [self.layer addSublayer:topLineLayer];
    }
    
    //左边框
    if (left > 0) {
        CALayer *leftLineLayer = [CALayer layer];
        leftLineLayer.backgroundColor = color.CGColor;
        leftLineLayer.frame = CGRectMake(0, 0, left, self.frame.size.height);
        [self.layer addSublayer:leftLineLayer];
    }
    
    //下边框
    if (bottom > 0) {
        CALayer *bottomLineLayer = [CALayer layer];
        bottomLineLayer.backgroundColor = color.CGColor;
        bottomLineLayer.frame = CGRectMake(0, self.frame.size.height - bottom, self.frame.size.width, bottom);
        [self.layer addSublayer:bottomLineLayer];
    }
    
    //右边框
    if (right > 0) {
        CALayer *rightLineLayer = [CALayer layer];
        rightLineLayer.backgroundColor = color.CGColor;
        rightLineLayer.frame = CGRectMake(self.frame.size.width - right, 0, right, self.frame.size.height);
        [self.layer addSublayer:rightLineLayer];
    }
    
}


@end

