//
//  UIColor+ColorChange.h
//  Neka
//
//  Created by ma c on 2017/8/15.
//  Copyright © 2017年 ma c. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorChange)
+ (UIColor *)colorWithHexString:(NSString *)color;
@end
